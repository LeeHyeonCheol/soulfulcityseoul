﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class FoodGame : MiniGame
{
    float currPosX;

    Tweener foodTween1, foodTween2;

    int orderIndex;

    [Header("UI")]
    [SerializeField] Slider slider;
    [SerializeField] Text timer;
    [SerializeField] GameObject failEff;

    bool startGame;
    bool endGame;
    bool gameOver;

    [SerializeField]
    GameObject[] answerObjs;
    int assignNum;
    string[,] answers = 
    {
        {"Food011", "Food03", "Food011", "Food05", "Food03", ""},
        {"Food011", "Food03", "Food011", "Food07", "Food03", ""},
        {"Food08", "Food011", "Food010", "Food03", "Food05", ""},
        {"Food011", "Food04", "Food05", "Food03", "Food05", "Food011"},
        {"Food08", "Food09", "Food08", "Food09", "Food011", "Food01"},
        {"Food08", "Food07", "Food03", "Food05", "Food04", "Food09"}
    };
    int[] answerCnt = {5, 5, 5, 6, 6, 6};
    [SerializeField]
    GameObject successEff;

    List<string> materialNames;

    void Start()
    {
        materialNames = new List<string>();
        currPosX = -576f; orderIndex = 50;
        gameOver = false; 
        Init(); 

        assignNum = UnityEngine.Random.Range(0, 6);
        answerObjs[assignNum].SetActive(true);

        startGame = true;
        endGame   = false;
    }

    void LateUpdate()
    {        
        if(endGame) return;
        if(isPause) return;

        if(startGame)
        {
            #region Check Time
            currentTime -= Time.deltaTime;
            if (timerFlag == false) { currentTime = limitTime; }
            UpdateTimeUI(slider, timer,limitTime, currentTime);

            if(currentTime <= 0)
            {
                startGame = false;
                gameOver = true;
            }
            else
            {
                if(materialNames.Count == answerCnt[assignNum])
                {
                    bool isClear = true;
                    for(int i = 0; i < answerCnt[assignNum]; i++)
                    {
                        if(answers[assignNum, i] != materialNames[i])
                        {
                            isClear = false;
                            break;
                        }
                    }
                    if(isClear)
                    {
                        GameClear();
                    }
                    else
                    {
                        startGame = false;
                        gameOver = true;
                    }
                }
            }
            #endregion
        }
        else if(gameOver)
        {
            GameOver();
        }
    }

    public void OnClickFood(GameObject obj)
    {
        if(isPause) return;
        
        float nextPosX = currPosX + obj.GetComponent<RectTransform>().sizeDelta.y / 7.5f;
        currPosX = nextPosX;

        obj.GetComponent<Image>().raycastTarget = false;
        obj.transform.SetSiblingIndex(orderIndex);
        orderIndex++;
        
        foodTween1 = obj.transform.DOLocalMove(new Vector3(0, currPosX, 0), 0.2f);
        foodTween1.SetEase(Ease.Flash);
        foodTween2 = obj.transform.DOLocalRotate(new Vector3(0, 0, -90), 0.2f);
        foodTween2.SetEase(Ease.InSine);

        currPosX += obj.GetComponent<RectTransform>().sizeDelta.y / 6;

        materialNames.Add(obj.name);
    }

    void GameClear()
    {
        startGame = false;
        successEff.SetActive(true);
        StartCoroutine(RewardResult()); endGame = true;
    }

    void GameOver()
    {
        GameObject obj = Instantiate(failEff);
        obj.transform.SetParent(this.transform);
        obj.transform.localScale    = Vector3.one;
        obj.transform.localPosition = Vector3.zero;

        StartCoroutine(RewardResult()); endGame = true;
    }

    IEnumerator RewardResult()
    {
        yield return new WaitForSeconds(3f);

        OnClose();
    }

    int GetScoreLevel()
    {
        if(slider.value >= 0.6f) return 3;
        else if(slider.value >= 0.4f) return 2;
        else if(slider.value >= 0.1f) return 1;

        return 0;
    }

    void OnClose()
    {
        int scoreLevel = -1;
        // 리워드 결과 셋팅
        if(gameOver == false) scoreLevel = GetScoreLevel();
        GetComponent<DestroyEvent>().SetRewardLevel(scoreLevel);
        
        Destroy(this.gameObject);
    }

    [Header("Play Time")]
    [SerializeField] float limitTime;
    [SerializeField] float currentTime;
    [SerializeField] bool timerFlag;
    DG.Tweening.Core.TweenerCore<float, float, DG.Tweening.Plugins.Options.FloatOptions> sliderTween;
    DG.Tweening.Core.TweenerCore<string, string, DG.Tweening.Plugins.Options.StringOptions> timeTween;
    
    void Init()
    {
        currentTime = limitTime;
        slider.value = 0.0f;
        if(sliderTween != null)
        {
            sliderTween.Kill(false);
            sliderTween = null;
        }
        if(timeTween != null)
        {
            timeTween.Kill(false);
            timeTween = null;
        }

        sliderTween = null;
        timeTween = null;
        timerFlag = false;
    }

    #region UI reference
    void UpdateTimeUI(Slider slider,Text timer,float max, float current)
    {
        if(sliderTween == null)
        {
            float value = current / max;
            float difference = Mathf.Abs(slider.value - value) * 2;
            sliderTween = slider.DOValue(value, difference);
            sliderTween.SetEase(Ease.Linear);
            sliderTween.OnComplete(
            ()=>
            {
                sliderTween = null;
                timerFlag = true;
            });
        }
        if(timeTween == null && timerFlag)
        {
            timeTween = timer.DOText(((int)current).ToString(), 0.05f).
            OnComplete(() =>
            {
                timeTween = null;
            });
        }
        else if(timerFlag == false)
        {
            timer.text = "Ready!";
        }
    }
    #endregion

    public override void Pause()
    {
        base.Pause();

    }

    public override void Continue()
    {
        base.Continue();
        
    }
}
