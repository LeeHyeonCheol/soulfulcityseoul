﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{

    // {"Leaderboard":
    // [{"DisplayName":null,
    // "PlayFabId":"9D7B2B00123209EC",
    // "Position":0,
    
    // "Profile":{"AdCampaignAttributions":null,"AvatarUrl":null,"BannedUntil":null,
    // "ContactEmailAddresses":null,"Created":null,"DisplayName":null,
    // "ExperimentVariants":null,"LastLogin":null,"LinkedAccounts":null,
    // "Locations":null,"Memberships":null,"Origination":null,
    // "PlayerId":"9D7B2B00123209EC","PublisherId":"FCEE844BDD9474B4",
    // "PushNotificationRegistrations":null,"Statistics":null,"Tags":null,
    // "TitleId":"1DF60","TotalValueToDateInUSD":null,"ValuesToDate":null},
    // "StatValue":1300}, 

    // 내 주변 랭킹
    public void GetLeaderBoard(Action<List<PlayerLeaderboardEntry>> callback, int max = 50) 
    {   
        PlayFabClientAPI.GetLeaderboard(new GetLeaderboardRequest 
        {
            StatisticName   = Common.LeaderBoardName,
            MaxResultsCount = max
        }, 

        // PlayFabClientAPI.GetLeaderboardAroundPlayer(new GetLeaderboardAroundPlayerRequest 
        // {
        //     StatisticName   = Common.LeaderBoardName,
        //     MaxResultsCount = 50
        // }, 
        result => 
        {
            callback?.Invoke(result.Leaderboard);
        }, DisplayPlayFabError);
    }

    public void GetLeaderboardAroundRandomPlayer(Action<PlayerLeaderboardEntry> callback) 
    {   
        PlayFabClientAPI.GetLeaderboardAroundPlayer(new GetLeaderboardAroundPlayerRequest 
        {
            StatisticName   = Common.LeaderBoardName,
            MaxResultsCount = 50
        }, 
        result => 
        {
            PlayerLeaderboardEntry myEntry = result.Leaderboard.Find(item => item.PlayFabId == myPlayFabID);
            if(myEntry != null) result.Leaderboard.Remove(myEntry);

            int randNum = UnityEngine.Random.Range(0, result.Leaderboard.Count);
            PlayerLeaderboardEntry playerEntry = result.Leaderboard[randNum];

            callback?.Invoke(playerEntry);
        }, DisplayPlayFabError);
    }

    public void UpdateStatistic(int value, Action successCallback = null)
    {
        PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest 
        {
           Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = Common.LeaderBoardName,
                    Value = value
                }
            }
        }, 
        result => 
        {
            successCallback?.Invoke();
            //print(result.ToJson());
        }, DisplayPlayFabError); 
    }
}
