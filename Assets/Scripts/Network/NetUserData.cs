﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{
    Action rouletteCallback;
    public void UpdateRouletteData(string gold, int spinCount, string resultParam, int magnification, Action callBack)
    {
        rouletteCallback = callBack;

        Dictionary<string, string> dicData = new Dictionary<string, string>();

        ulong nGold = Convert.ToUInt64(gold);// * (ulong)magnification;

        dicData.Add("gold", nGold.ToString()); 
        dicData.Add("spincount",     spinCount.ToString());
        dicData.Add("magnification", magnification.ToString());
        // if(Common.RULItem.ID.Shield.ToString().CompareTo(resultParam) == 0)
        // {
        //     if(UserManager.instance.ShieldCount < 5)
        //     {
        //         UserManager.instance.ShieldCount++;
        //         dicData.Add("shieldcount", UserManager.instance.ShieldCount.ToString());
        //     }
        // }

        UpdateUserDataRequest request = new UpdateUserDataRequest() 
        {
            Data = dicData
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result) 
        { 
            SuccessEndConnect(result.ToJson());
            if(Common.RULItem.ID.Attack.ToString().CompareTo(resultParam) == 0)
            {
                GetLeaderboardAroundRandomPlayer(SuccessGetRandomPlayer);
            }
            // else if(Common.RULItem.ID.Steal.ToString().CompareTo(resultParam) == 0)
            // {
                
            // }
            else
                rouletteCallback?.Invoke();
        }

        void OnError(PlayFabError error) 
        {
            serverErrorCall?.Invoke(error.GenerateErrorReport());
            //failCallBack?.Invoke(error.GenerateErrorReport());
        } 
    }

    string anotherPlayFabId;
    void SuccessGetRandomPlayer(PlayerLeaderboardEntry anotherEntry)
    {
        anotherPlayFabId = anotherEntry.PlayFabId;

        if (TutorialManager.isTutorial) anotherPlayFabId = "CAF64FC7DF6EF151";

        GetUserBuildingData(anotherPlayFabId, SuccessGetReadyAttack);
    }

    GetUserDataResult anotherUserData;
    void SuccessGetReadyAttack(GetUserDataResult res)
    {
        anotherUserData = res;
        rouletteCallback?.Invoke();
    }

    public void StartAttack()
    {
        // 건물 안지은 유저 당첨되면 디폴트 값으로
        BuildingManager.instance.SetFriendData(NetworkManager.instance.CheckArrangeBuildingId(anotherUserData),
            NetworkManager.instance.CheckArrangeBuildingLevel(anotherUserData), anotherPlayFabId, NetworkManager.instance.CheckArrangeShieldCount(anotherUserData), anotherPlayFabId);
    }
}
