﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{
    //void DisplayFriends(List<FriendInfo> friendsCache) { friendsCache.ForEach(f => Debug.Log(f.FriendPlayFabId)); }
    void DisplayPlayFabError(PlayFabError error) { Debug.Log(error.GenerateErrorReport()); }
    //void DisplayError(string error) { Debug.LogError(error); }

    List<FriendInfo> _friends = null;
    List<PlayerLeaderboardEntry> leaderboardEntries = null;

    public void GetFriends(Action<List<PlayerLeaderboardEntry>> friendsCallback)
    {
        PlayFabClientAPI.GetFriendLeaderboard(new GetFriendLeaderboardRequest 
        {
            StatisticName          = Common.LeaderBoardName,
            IncludeSteamFriends    = false,
            IncludeFacebookFriends = true,
            MaxResultsCount        = 20
        }, 
        result => 
        {
            leaderboardEntries = result.Leaderboard;
            friendsCallback?.Invoke(leaderboardEntries);
        }, DisplayPlayFabError);
    }

    public void GetRecommendFriend(Action<List<PlayerLeaderboardEntry>> callback) 
    {   
        PlayFabClientAPI.GetLeaderboardAroundPlayer(new GetLeaderboardAroundPlayerRequest 
        {
            StatisticName   = Common.LeaderBoardName,
            MaxResultsCount = 20
        }, 
        result => 
        {
            callback?.Invoke(result.Leaderboard);
        }, DisplayPlayFabError);
    }

    public enum FriendIdType { PlayFabId, Username, Email, DisplayName };

    public void AddFriend(/*FriendIdType idType,*/ string friendId, Action callback) 
    {
        var request = new AddFriendRequest();
        // switch (idType) 
        // {
        //     case FriendIdType.PlayFabId:
        //         request.FriendPlayFabId = friendId;
        //         break;
        //     case FriendIdType.Username:
        //         request.FriendUsername = friendId;
        //         break;
        //     case FriendIdType.Email:
        //         request.FriendEmail = friendId;
        //         break;
        //     case FriendIdType.DisplayName:
        //         request.FriendTitleDisplayName = friendId;
        //         break;
        // }
        request.FriendPlayFabId = friendId;
        
        PlayFabClientAPI.AddFriend(request, result => 
        {
            callback?.Invoke();
        }, DisplayPlayFabError);
    }

    public void RemoveFriend(FriendInfo friendInfo) 
    {
        PlayFabClientAPI.RemoveFriend(new RemoveFriendRequest 
        {
            FriendPlayFabId = friendInfo.FriendPlayFabId
        }, 
        result => 
        {
            _friends.Remove(friendInfo);
        }, DisplayPlayFabError);
    }
}
