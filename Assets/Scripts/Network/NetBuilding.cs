﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{
    public void UpdateBuildingData(string id, string level, ulong gold, string time, Action callBack)
    {
        UpdateUserDataRequest request = new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>()
            {
                {"building_id",    id },
                {"building_level", level},
                {"gold",           gold.ToString()}
            },
            
            Permission = UserDataPermission.Public
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result)
        {
            SuccessEndConnect(result.ToJson());
            callBack?.Invoke();
        }

        void OnError(PlayFabError error)
        {
            Debug.Log("UpdateUserData : Fail ...");
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
            //failCallBack?.Invoke(error.GenerateErrorReport());
        }
    }

    public void GetUserBuildingData(string playFabID, Action<GetUserDataResult> callback)
	{
		PlayFabClientAPI.GetUserData(new GetUserDataRequest() 
        {
            PlayFabId = playFabID,
            Keys = new List<string>
            {
                {"building_id"}, 
                {"building_level"}
                //=={"building_bombing"}
                //=={"shieldcount"}
            }
        }, result => 
        {
            SuccessEndConnect(result.ToJson());
            callback?.Invoke(result);
        }, (error) => 
        {
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        });
	}

    [HideInInspector]
    public Dictionary<string, string> setWorkerIds;
    [HideInInspector]
    public Dictionary<string, string> setWorkerTimes;

    public void UpdateSetWorker(int id, int index, int workmanID, Action<long> callBack)
    {
        int i;
        string szWorkKey    = "worker_" + id;
        string[] currWorker = setWorkerIds[szWorkKey].Split('|');
        currWorker[index]   = workmanID.ToString(); 
        string resWorker = null;
        for(i = 0; i < currWorker.Length; i++)
        {
            if(i == currWorker.Length - 1)
                resWorker += currWorker[i];
            else
                resWorker += currWorker[i] + '|';
        }
        setWorkerIds[szWorkKey] = resWorker;

        string szTimeKey  = "setWTime_" + id;
        string[] currTime = setWorkerTimes[szTimeKey].Split('|');

        long nowT  = HoneyPlug.Utils.Time.GetMillisecondsByDateTime(DateTime.UtcNow);
        int addMin = 0;
        if(index == 0)
            addMin = Common.WorkLevel1_MinTime;
        else if(index == 1)
            addMin = Common.WorkLevel2_MinTime;
        else
            addMin = Common.WorkLevel4_MinTime;

        long endT = HoneyPlug.Utils.Time.GetUniversalTimeByLongType_AddMin(nowT, addMin);
        currTime[index] = endT.ToString();
        string resTime  = null;
        for(i = 0; i < currTime.Length; i++)
        {
            if(i == currTime.Length - 1)
                resTime += currTime[i];
            else
                resTime += currTime[i] + '|';
        }
        setWorkerTimes[szTimeKey] = resTime;

        UpdateUserDataRequest request = new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>()
            {
                {szWorkKey, resWorker},
                {szTimeKey, resTime}
            }
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result)
        {
            SuccessEndConnect(result.ToJson());
            callBack?.Invoke(endT);
        }

        void OnError(PlayFabError error)
        {
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        }
    }
    // 아르바이트생 급여 주기
    Action<string> sendStrCallback;
    Action<string, string> sendStr2Callback;
    public void RewardToWorker(int id, long[] endTimes, Action<string, string> callBack, bool isADReward = false)
    {
        int i;

        sendStr2Callback = callBack;
        this.isADReward  = isADReward;

        string szWorkKey    = Common.WorkerBasicKey + id;
        string[] currWorker = setWorkerIds[szWorkKey].Split('|');
        string szTimeKey    = Common.WorkEndBasicKey + id;
        string[] currTime   = setWorkerTimes[szTimeKey].Split('|');

        long nowT = HoneyPlug.Utils.Time.GetMillisecondsByDateTime(DateTime.UtcNow);

        string[] endWork = new string[endTimes.Length];
        for(i = 0; i < endTimes.Length; i++)
        {
            if(endTimes[i] > 0 && endTimes[i] <= nowT)
            {
                currWorker[i] = "0";
                currTime[i]   = "0";
                endWork[i]    = "Y";
            }
            else
            {
                endWork[i]    = "N";
            }
        }
        string resWorker = null;
        for(i = 0; i < currWorker.Length; i++)
        {
            if(i == currWorker.Length - 1)
                resWorker += currWorker[i];
            else
                resWorker += currWorker[i] + '|';
        }
        setWorkerIds[szWorkKey] = resWorker;

        string resTime  = null;
        for(i = 0; i < currTime.Length; i++)
        {
            if(i == currTime.Length - 1)
                resTime += currTime[i];
            else
                resTime += currTime[i] + '|';
        }
        setWorkerTimes[szTimeKey] = resTime;

        UpdateUserDataRequest request = new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>()
            {
                {szWorkKey, resWorker},
                {szTimeKey, resTime}
            }
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result)
        {
            SuccessEndConnect(result.ToJson());
            UpdateRewardToWorker(id, endWork);
        }

        void OnError(PlayFabError error)
        {
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        }
    }
    // 내 자산 + 아르바이트생 급여
    void UpdateRewardToWorker(int id, string[] endWork)
    {
        // print("id " + id);
        // print("endWork[0] " + endWork[0]);
        // print("endWork[1] " + endWork[1]);
        // print("endWork[2] " + endWork[2]);
        // print("CheckAD_Reward() " + CheckAD_Reward());

        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "ProcRewardToWorker", GeneratePlayStreamEvent = true,
            FunctionParameter = new { id = id, endWork1 = endWork[0], endWork2 = endWork[1], endWork3 = endWork[2], isADReward = CheckAD_Reward()}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessGetRewardToWorker, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    void SuccessGetRewardToWorker(ExecuteCloudScriptResult result)
    {
        netState = State.End;
        var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        //Debug.Log("SuccessGetRewardAtk :::::: " + serializer.SerializeObject(result.FunctionResult));
        GetRewardGoldPlus rewardData = JsonUtility.FromJson<GetRewardGoldPlus>(serializer.SerializeObject(result.FunctionResult));

        sendStr2Callback?.Invoke(rewardData.gold, rewardData.getGold);
    }

    public void GetWorkerDataFromUserData(Action<GetUserDataResult> callback)
	{
        setWorkerIds   = new Dictionary<string, string>();
        setWorkerTimes = new Dictionary<string, string>();
        
        int i;
        List<string> workerDatas = new List<string>();
        for(i = 1; i <= Common.BUDMaxCount; i++) 
        {
            workerDatas.Add("worker_" + i);
        }
        for(i = 1; i <= Common.BUDMaxCount; i++) 
        {
            workerDatas.Add("setWTime_" + i);
        }

		PlayFabClientAPI.GetUserData(new GetUserDataRequest() 
        {
            PlayFabId = myPlayFabID,
            Keys = workerDatas
        }, result => 
        {
            SuccessEndConnect(result.ToJson());
            callback?.Invoke(result);
        }, (error) => 
        {
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        });
	}

    public void CleaningBuilding(string datas, Action callback)
	{
        UpdateUserDataRequest request = new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>()
            {
                {"building_bombing", datas}
            }
            //Permission = UserDataPermission.Public
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result)
        {
            SuccessEndConnect(result.ToJson());
            callback?.Invoke();
        }

        void OnError(PlayFabError error)
        {
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        }
	}

    // [Serializable]
    // class ConstructId
    // {
    //     public string resData;
    // }

    // public void StartConstruction(int id, Action<string> callback)
    // {
    //     netState = State.Connect;
    //     sendStrCallback = callback;
    //     var request = new ExecuteCloudScriptRequest() 
    //     { 
    //         FunctionName = "StartConstruction", GeneratePlayStreamEvent = true,
    //         FunctionParameter = new { buildingId = id}
    //     };

    //     PlayFabClientAPI.ExecuteCloudScript(request, SuccessConstruction, 
    //     (error) => Debug.LogError(error.GenerateErrorReport()));
    // }

    // void SuccessConstruction(ExecuteCloudScriptResult result)
    // {
    //     netState = State.End;
    //     var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
    //     ConstructId res = JsonUtility.FromJson<ConstructId>(serializer.SerializeObject(result.FunctionResult));

    //     print("resData : " + res.resData);

    //     sendStrCallback?.Invoke(res.resData);
    // }

    // ADD 2020.11.19
    // 내 건물의 폭격 상태와 쉴드 카운트 정보 
    // public void MyBuilding_ShieldState(Action<string, int> callback)
    // {
    //     PlayFabClientAPI.GetUserData(new GetUserDataRequest() 
    //     {
    //         PlayFabId = myPlayFabID,
    //         Keys = new List<string>
    //         {
    //             {"building_bombing"},
    //             {"shieldcount"}
    //         }
    //     }, result => 
    //     {
    //         SuccessEndConnect(result.ToJson());

    //         string bombing = Common.InitSetBuildingBombing;
    //         if(result.Data.ContainsKey("building_bombing"))
    //             bombing = result.Data["building_bombing"].Value;
            
    //         int count = 0;
    //         if(result.Data.ContainsKey("shieldcount"))
    //             count = Convert.ToInt32(result.Data["shieldcount"].Value);

    //         callback?.Invoke(bombing, count);
    //     }, (error) => 
    //     {
    //         Debug.Log(error.GenerateErrorReport());
    //         serverErrorCall?.Invoke(error.GenerateErrorReport());
    //     });
    // }
}
