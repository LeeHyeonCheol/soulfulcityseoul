﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PlayFab.ClientModels;
using PlayFab.Internal;

public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
{
    PlayFabController playFabController;
    void Start()
    {
        playFabController = GetComponent<PlayFabController>();
    }

    Action serverDataCallback;
    public void ConnectUser(Action serverDataCallback)
    {
        this.serverDataCallback = serverDataCallback;

#region Register Server Error Msg
        playFabController.SetServerErrorCall(OnPlayfabError);
#endregion
        playFabController.LoginWithAndroidDeviceID(GetUserData);
    }

    public void GetWorkerDataFromUserData(Action serverDataCallback)
    {
        this.serverDataCallback = serverDataCallback;

        playFabController.GetWorkerDataFromUserData(SuccessWorkerData);
    }

    void OnPlayfabError(string log)
    {
        PopupManager.instance.Open(
            Common.PopupCode.PlayfabError,
            log,
            PopupManager.ButtonStyle.Ok,
            PopupManager.MoveType.CenterPumping,
            null,
            null, true);
    }

    string gold;
    int spinCount;
    int magnification;
    int shieldCount;
    string buildingsId;
    string buildingsLevel;
    string buildingsBombState;

    string sculpture_Inven;
    string sculpture_Arrange;

    string bookDatas;

    List<string> friendAttackedId;

    public string rank1PlayerInfo;

    bool isTutorial;

    void GetUserData(GetTitleDataResult initRes, GetUserDataResult result)
    {
        if(result.Data.ContainsKey("gold")) 
            gold = result.Data["gold"].Value;
        else 
            gold = initRes.Data["InitGold"];

        if(result.Data.ContainsKey("spincount")) 
            spinCount = Convert.ToInt32(result.Data["spincount"].Value);
        else 
            spinCount = Convert.ToInt32(initRes.Data["InitSpinCount"]);

        if(result.Data.ContainsKey("magnification")) 
            magnification = Convert.ToInt32(result.Data["magnification"].Value);
        else 
            magnification = 1;

        if(result.Data.ContainsKey("tuto")) isTutorial = false;
        else isTutorial = true; 
        
        UpdateMyBuildingData(result);
        serverDataCallback?.Invoke();
    }

    Action UpdateCheckCallback;
    public void UpdateCheckDatas(Action UpdateCheckCallback)
    {
        this.UpdateCheckCallback = UpdateCheckCallback;

        if(isTutorial)
        {
            spinCount = 50; gold = "0";
            Dictionary<string, string> datas = new Dictionary<string, string>();
            datas.Add("spincount", spinCount.ToString());
            datas.Add("gold",      gold);
            datas.Add("building_level", Common.InitSetBuildingLevel);
            playFabController.SetUserDatas(datas, SuccessCheckDatas);

            buildingsLevel = Common.InitSetBuildingLevel;
            return;
        }

        if(spinCount < Common.RULUseCountMax)
        {
            //print("DateTime.UtcNow : " +  DateTime.UtcNow);
            //print("playFabController.lastLoginTime : " +  playFabController.lastLoginTime);
            TimeSpan tSpan  = DateTime.UtcNow - playFabController.lastLoginTime;
            double gapValue = tSpan.TotalSeconds / Common.RULRecoveryDelay;
            //print("tSpan.TotalSeconds : " +  tSpan.TotalSeconds);
            //print("gapValue : " +  gapValue);
            spinCount      += (int)gapValue;
            if(spinCount > Common.RULUseCountMax) spinCount = Common.RULUseCountMax;

            playFabController.SetUserData("spincount", spinCount.ToString(), SuccessCheckDatas);
        }
        else UpdateCheckCallback.Invoke();
    }

    void SuccessCheckDatas()
    {
        UpdateCheckCallback.Invoke();
    }

    public void UpdateMyBuildingData(GetUserDataResult result)
    {
        // if(result.Data.ContainsKey("shieldcount")) 
        //     shieldCount = Convert.ToInt32(result.Data["shieldcount"].Value);
        // else 
        //     shieldCount = 0;
            
        if(result.Data.ContainsKey("building_id")) 
            buildingsId = result.Data["building_id"].Value;
        else 
            buildingsId = Common.InitSetBuildingId;

        if(result.Data.ContainsKey("building_level")) 
            buildingsLevel = result.Data["building_level"].Value;
        else 
            buildingsLevel = Common.InitSetBuildingLevel;

        // if(result.Data.ContainsKey("building_bombing"))
        //     buildingsBombState = result.Data["building_bombing"].Value;
        // else 
        //     buildingsBombState = Common.InitSetBuildingBombing;

        if(result.Data.ContainsKey("sculpture_inven"))
            sculpture_Inven = result.Data["sculpture_inven"].Value;
        else
            sculpture_Inven = "";

        if(result.Data.ContainsKey("sculpture_arrange"))
            sculpture_Arrange = result.Data["sculpture_arrange"].Value;
        else
            sculpture_Arrange = "";

        if(result.Data.ContainsKey("BookDatas"))
            bookDatas = result.Data["BookDatas"].Value;
        else
            bookDatas = "";

        if(result.Data.ContainsKey("friendAtkLst"))
            SetFriendAttackedList(result.Data["friendAtkLst"].Value);
        else
            SetFriendAttackedList("");
    }

    void SuccessWorkerData(GetUserDataResult result)
    {
        int i;
        string idKey   = "worker_";
        string timeKey = "setWTime_";

        string szKeyName = null;
        for(i = 1; i <= Common.BUDMaxCount; i++)
        {
            szKeyName = idKey + i;
            if(result.Data.ContainsKey(szKeyName))
                playFabController.setWorkerIds.Add(szKeyName, result.Data[szKeyName].Value);
            else
                playFabController.setWorkerIds.Add(szKeyName, "0|0|0");
        }
        for(i = 1; i <= Common.BUDMaxCount; i++)
        {
            szKeyName = timeKey + i;
            if(result.Data.ContainsKey(szKeyName))
                playFabController.setWorkerTimes.Add(szKeyName, result.Data[szKeyName].Value);
            else
                playFabController.setWorkerTimes.Add(szKeyName, "0|0|0");
        }

        serverDataCallback?.Invoke();
    }

    // 공격한 친구 리스트
    void SetFriendAttackedList(string value)
    {
        friendAttackedId = new List<string>();
        if(value == null) return;

        if(value.Length > 0)
        {
            string[] szIds = value.Split(Common.splitWord);
            
            for(int i = 0; i < szIds.Length; i++)
                friendAttackedId.Add(szIds[i]);
        }
    }

    public string GetFriendAttackedList(string newId)
    {
        friendAttackedId.Add(newId);

        string szResult = null;
        for(int i = 0; i < friendAttackedId.Count; i++)
        {
            if(i == friendAttackedId.Count - 1)
                szResult += friendAttackedId[i];
            else
                szResult += friendAttackedId[i] + Common.splitWord;
        }

        return szResult;
    }

    public bool CheckFriendAttacked(string id)
    {
        if(friendAttackedId.Count == 0) return true;

        return friendAttackedId.Find(item => item == id) == null;
    }

    public string GetGoldFromServer()
    {
        return gold;
    }

    public int GetSpinCount()
    {
        return spinCount;
    }

    public int GetMagnification()
    {
        return magnification;
    }

    public int GetShieldCount()
    {
        return shieldCount;
    }

    public string GetBuildingsId()
    {
        return buildingsId;
    }

    public string GetBuildingsLevel()
    {
        return buildingsLevel;
    }

    public string GetBuildingsBombState()
    {
        return buildingsBombState;
    }

    public string GetSculptureInven()
    {
        return sculpture_Inven;
    }

    public string GetSculptureArrange()
    {
        if(sculpture_Arrange == null) sculpture_Arrange = "";
        return sculpture_Arrange;
    }

    public string BookDatas
    {
        get { return  bookDatas; }
        set { bookDatas = value; }
    }

    public bool EqualMyId(string playFabID)
    {
        return playFabController.EqualMyId(playFabID);
    }

    public void UpdateRouletteData(ulong gold, int spinCount, string resultParam, int magnification, Action callBack)
    {
        playFabController.UpdateRouletteData(gold.ToString(), spinCount, resultParam, magnification, callBack);
    }
    public void UpdateBuildingData(string id,string level, ulong gold, string time,Action callBack)
    {
        playFabController.UpdateBuildingData(id, level, gold, time,callBack);
    }

    public void GetMyFriendList(Action<List<PlayerLeaderboardEntry>> callback)
    {
        playFabController.GetFriends(callback);
    }

    public void GetRecommendFriend(Action<List<PlayerLeaderboardEntry>> callback)
    {
        playFabController.GetRecommendFriend(callback);
    }

    public void AddFriend(string friendId, Action callback) 
    {
        playFabController.AddFriend(friendId, callback);
    }

    public void GetLeaderBoard(Action<List<PlayerLeaderboardEntry>> callback, int max = 50)
    {
        playFabController.GetLeaderBoard(callback.Invoke, max);
    }

    public void GetUserBuildingData(string playFabID, Action<GetUserDataResult> callback)
    {
        playFabController.GetUserBuildingData(playFabID, callback);
    }

    public string CheckArrangeBuildingId(GetUserDataResult res)
    {
        if(res.Data.ContainsKey("building_id"))
            return res.Data["building_id"].Value;
        else
            return Common.InitSetBuildingId;
    }

    public string CheckArrangeBuildingLevel(GetUserDataResult res)
    {
        if(res.Data.ContainsKey("building_level"))
            return res.Data["building_level"].Value;
        else
            return Common.InitSetBuildingLevel;
    }

    public string CheckArrangeBuildingBombing(GetUserDataResult res)
    {
        // if(res.Data.ContainsKey("building_bombing"))
        //     return res.Data["building_bombing"].Value;
        // else
            return Common.InitSetBuildingBombing;
    }

    public int CheckArrangeShieldCount(GetUserDataResult res)
    {
        // if(res.Data.ContainsKey("shieldcount"))
        //     return Convert.ToInt32(res.Data["shieldcount"].Value);
        // else
            return 0;
    }

    public bool IsEnableAttackBuilding(GetUserDataResult res)
    {
        if(res.Data.ContainsKey("building_level"))
        {
            if(res.Data["building_level"].Value.Length == 0) return false;
            else return true;
        }
        
        return false;
    }

    // 다른 플레이어 공격후 보상 받기
    public void GetRewardAtkOtherPlayer(Action<string, string> rewardCallback, string otherPlayFabId, int id, int level, bool isShield, bool isADReward = false)
    {
        //playFabController.GetRewardAtkOtherPlayer(rewardCallback, otherPlayFabId, id, level, isShield, isADReward);
    }

    // 건물 청소하기 보상
    public void GetRewardCleaning(Action<string, string> rewardPlusCallback, int level, bool isADReward = false)
    {
        playFabController.GetRewardCleaning(rewardPlusCallback, level, isADReward);
    }
    // 친구 건물 방문
    public void RewardBonus_VisitFriend(Action<string, string> rewardPlusCallback, int level, int touchCnt, bool isADReward = false)
    {
        playFabController.RewardBonus_VisitFriend(rewardPlusCallback, level, touchCnt, isADReward);
    }

    public void RewardBonus_Quiz(Action<string, string> rewardPlusCallback, string answer, bool isADReward = false)
    {
        playFabController.RewardBonus_Quiz(rewardPlusCallback, answer, isADReward);
    }

    public void RewardSpinCount(Action callback, bool isADReward = false)
    {
        playFabController.RewardSpinCount(callback, isADReward);
    }

    public void StartAttack()
    {
        playFabController.StartAttack();
    }

    public void UpdateFriendAttackedList(string friendId, Action callback)
    {
        playFabController.UpdateFriendAttackedList(friendId, callback);
    }

    public void UpdateSetWorker(int id, int index, int workmanID, Action<long> callBack)
    {
        playFabController.UpdateSetWorker(id, index, workmanID, callBack);
    }

    public void RewardToWorker(int id, long[] endTimes, Action<string, string> callBack, bool isADReward = false)
    {
        playFabController.RewardToWorker(id, endTimes, callBack, isADReward);
    }

    public string[] GetValuesSetWorkerIds()
    {
        int i = 0;
        string[] ids = new string[playFabController.setWorkerIds.Count];
        foreach(string value in playFabController.setWorkerIds.Values) 
        {
            ids[i] = value; 

            //print(ids[i]);

            i++;
        } 

        return ids;
    }

    public string[] GetValuesSetWorkerTimes()
    {
        int i = 0;
        string[] times = new string[playFabController.setWorkerTimes.Count];
        foreach(string value in playFabController.setWorkerTimes.Values) 
        {
            times[i] = value; 
            i++;
        }

        return times;
    }

    public long[] GetWorkerEndLongTimeByBUID(int id)
    {
        string[] szT  = playFabController.setWorkerTimes["setWTime_" + id].Split(Common.splitWord);
        long[] lDataT = new long[szT.Length];
        for(int i = 0; i < lDataT.Length; i++)
            lDataT[i] = Convert.ToInt64(szT[i]);

        return lDataT;
    }

    public string[] GetWorkerEndStrTimesByBUID(int id)
    {
        string[] szT  = playFabController.setWorkerTimes["setWTime_" + id].Split(Common.splitWord);
        return szT;
    }

    public string GetWorkerFromBuildingId(int id)
    {
        string keyName = Common.WorkerBasicKey + id;
        return playFabController.setWorkerIds[keyName];
    }
    // 건설 시작
    // public void StartConstruction(int id, Action<string> callback)
    // {
    //     playFabController.StartConstruction(id, callback);
    // }

    public void UpdateSculptureInven(ulong price, int id, Action callback)
    {
        playFabController.UpdateSculptureInven(price, id, callback);
    }

    public void UpdateSculptureFromInstall(string[] datas, Action<Vector3> callback,Vector3 position)
    {
        playFabController.UpdateSculptureFromInstall(datas, callback, position);
    }
    public void UpdateSculptureFromWithDraw(string[] datas, Action callback)
    {
        playFabController.UpdateSculptureFromWithDraw(datas, callback);
    }

    // 미니게임 클리어 보상받기
    public void GetRewardMiniGames(Action<string> rewardCallback, int totalLv, int clearLv, bool isADReward = false)
    {
        playFabController.GetRewardMiniGames(rewardCallback, totalLv, clearLv, isADReward);
    }

    public void ExeGacha(Action<string, bool> callBack)
    {
        //playFabController.ExeGacha(callBack);
    }

    public void UpdateLeaderBoard(int value, Action successCallback = null)
    {
        playFabController.UpdateStatistic(value * 1000, successCallback);
    }
    // 다른 유저로 부터 골드 훔치기
    public void StealGoldFromOtherPlayer(Action<string> callback) 
    {
        playFabController.StealGoldFromOtherPlayer(callback);
    }
    // 골드 훔치기 보상액
    public void RewardStealGold(string rankGold, Action<ulong> callback, bool isADReward = false)
    {
        playFabController.RewardStealGold(rankGold, callback, isADReward);
    }
    // 건물 청소
    public void CleaningBuilding(string datas, Action callback)
    {
        playFabController.CleaningBuilding(datas, callback);
    }

    public void GetCatalogItems(string catalogName, Action<List<CatalogItem>> successCallBack = null, Action<string> failCallBack = null)
    {
        playFabController.GetCatalogItems(catalogName, successCallBack);
    }

    public void ProcPurchaseItems(string productId, Action<ulong, int> endPurchaseCallback)
    {
        playFabController.ProcPurchaseItems(productId, endPurchaseCallback);
    }

    public void GetRewardFromADs(Action rewardADsCallback, int type)
    {
        playFabController.GetRewardFromADs(rewardADsCallback, type);
    }

    public bool IsTutorial()
    {
        return isTutorial;
    }

    public void EndTutorial(Action callback)
    {
        playFabController.SetUserData("tuto", "1", callback);
    }
}
