﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{
    void OnPlayfabError(string log)
    {
        PopupManager.instance.Open(
            Common.PopupCode.PlayfabError,
            log,
            PopupManager.ButtonStyle.Ok,
            PopupManager.MoveType.CenterPumping,
            null,
            null, true);
    }
}
