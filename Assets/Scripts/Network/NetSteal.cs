﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{
    public void StealGoldFromOtherPlayer(Action<string> callback) 
    {   
        netState = State.Connect;
        sendStrCallback = callback;

        PlayFabClientAPI.GetLeaderboardAroundPlayer(new GetLeaderboardAroundPlayerRequest 
        {
            StatisticName   = Common.LeaderBoardName,
            MaxResultsCount = 30
        }, 
        result => 
        {
            ProcStealGold(result.Leaderboard);
        }, (error) => 
        {
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        });
    }

    string[] otherPlayers;
    void ProcStealGold(List<PlayerLeaderboardEntry> entrys)
    {
        PlayerLeaderboardEntry myEntry = null;
        myEntry = entrys.Find(item => item.PlayFabId == myPlayFabID);

        if(myEntry != null)
            entrys.Remove(myEntry); // 랭킹에서 자신 제외

        otherPlayers = new string[3];

        int randPlayerNum = UnityEngine.Random.Range(0, entrys.Count);
        otherPlayers[0]   = entrys[randPlayerNum].PlayFabId; entrys.RemoveAt(randPlayerNum);

        randPlayerNum   = UnityEngine.Random.Range(0, entrys.Count);
        otherPlayers[1] = entrys[randPlayerNum].PlayFabId; entrys.RemoveAt(randPlayerNum);

        randPlayerNum   = UnityEngine.Random.Range(0, entrys.Count);
        otherPlayers[2] = entrys[randPlayerNum].PlayFabId; entrys.RemoveAt(randPlayerNum);

        // print("user1Id " + otherPlayers[0]);
        // print("user2Id " + otherPlayers[1]);
        // print("user3Id " + otherPlayers[2]);

        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "ProcStealGold", GeneratePlayStreamEvent = true,
            FunctionParameter = new { user1 = otherPlayers[0], user2 = otherPlayers[1], user3 = otherPlayers[2]}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessStealGold, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    [Serializable]
    class StealUserInfo
    {
        public string resUser1;
        public string resUser2;
        public string resUser3;
    }
    void SuccessStealGold(ExecuteCloudScriptResult result)
    {
        netState = State.End;
        var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        StealUserInfo res = JsonUtility.FromJson<StealUserInfo>(serializer.SerializeObject(result.FunctionResult));

        // print("res.resUser1 " + res.resUser1);
        // print("res.resUser2 " + res.resUser2);
        // print("res.resUser3 " + res.resUser3);

        string resData = otherPlayers[0] + "," + res.resUser1 + "|" + otherPlayers[1] + "," + res.resUser2 + "|" +
            otherPlayers[2] + "," + res.resUser3; 

        //print(resData);
        
        sendStrCallback?.Invoke(resData);
    }

    Action<ulong> uGoldCallback;
    public void RewardStealGold(string rankGold, Action<ulong> callback, bool isADReward = false)
    {
        netState = State.Connect;
        uGoldCallback   = callback;
        this.isADReward = isADReward;
        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "RewardStealGold", GeneratePlayStreamEvent = true,
            FunctionParameter = new { stealGold = rankGold, isADReward = CheckAD_Reward()}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessGetSteal, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    void SuccessGetSteal(ExecuteCloudScriptResult result)
    {
        netState = State.End;
        var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        GetRewardGold res = JsonUtility.FromJson<GetRewardGold>(serializer.SerializeObject(result.FunctionResult));

        //print("res.gold " + res.gold);
    
        uGoldCallback?.Invoke(Convert.ToUInt64(res.gold));
    }
}
