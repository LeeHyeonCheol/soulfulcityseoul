﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{
    public void UpdateSculptureInven(ulong price, int id, Action callback)
    {
        string szInven = SculptureManger.Instance.MakeServerPacket_Inventory(id);
        UserManager.instance.GoldOnEvent -= price;

        Dictionary<string, string> dicData = new Dictionary<string, string>();
        dicData.Add("gold",            UserManager.instance.GoldOnEvent.ToString()); 
        dicData.Add("sculpture_inven", szInven);

        UpdateUserDataRequest request = new UpdateUserDataRequest() 
        {
            Data = dicData
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result) 
        { 
            SuccessEndConnect(result.ToJson());
            callback?.Invoke();
        }

        void OnError(PlayFabError error) 
        { 
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        }
    }

    public void UpdateSculptureFromInstall(string[] datas, Action<Vector3> callback,Vector3 position)
    {
        UpdateUserDataRequest request = new UpdateUserDataRequest() 
        {
            Data = new Dictionary<string, string>()
            {
                {"sculpture_inven",    datas[1]},
                {"sculpture_arrange",  datas[0]}
            }
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result) 
        { 
            SuccessEndConnect(result.ToJson());
            callback?.Invoke(position);
        }

        void OnError(PlayFabError error) 
        { 
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        }
    }

    public void UpdateSculptureFromWithDraw(string []datas,Action callback)
    {
        UpdateUserDataRequest request = new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>()
            {
                {"sculpture_inven",    datas[1]},
                {"sculpture_arrange",  datas[0]}
            }
        };

        netState = State.Connect;

        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result)
        {
            SuccessEndConnect(result.ToJson());
            callback?.Invoke();
        }

        void OnError(PlayFabError error)
        {
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        }

    }
}
