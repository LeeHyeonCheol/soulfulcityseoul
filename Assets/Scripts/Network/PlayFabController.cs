﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{
    public DateTime lastLoginTime;
    public enum State
    {
        Connect,
        End,
        DisConnect
    }

    public State netState;

    Action connectionErrorCall;
    Action<string> serverErrorCall;

    [SerializeField]
    string testAccountId;

    void Start()
    {
        
    }

    public void SetConnectionErrorCall(Action callBack)
    {
        connectionErrorCall = callBack;
    }

    public void SetServerErrorCall(Action<string> callBack)
    {
        serverErrorCall = callBack;
    }

#if UNITY_ANDROID
    // public void LoginWithGoogleAccount(Action<GetUserDataResult> callback)
    // {
    //     userDataResult = callback;

    //     PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
    //     .AddOauthScope("profile")
    //     .RequestServerAuthCode(false)
    //     .Build();
    //     PlayGamesPlatform.InitializeInstance(config);

    //     // recommended for debugging:
    //     PlayGamesPlatform.DebugLogEnabled = true;

    //     // Activate the Google Play Games platform
    //     PlayGamesPlatform.Activate();

    //     OnSigned();
    // }
#endif

//     private void OnSigned()
//     {
//         Social.localUser.Authenticate((bool success) => {

//             if (success)
//             {
// #if UNITY_ANDROID
//                 var serverAuthCode = PlayGamesPlatform.Instance.GetServerAuthCode();
//                 Debug.Log("Server Auth Code: " + serverAuthCode);
//                 PlayFabClientAPI.LoginWithGoogleAccount(new LoginWithGoogleAccountRequest()
//                 {
//                     TitleId = PlayFabSettings.TitleId,
//                     ServerAuthCode = serverAuthCode,
//                     CreateAccount = true
//                 }, (result) =>
//                 {
//                     OnRegistration(result);
//                 },(error) => 
//                 {
//                     Debug.Log("Got error retrieving user data:");
//                     Debug.Log(error.GenerateErrorReport());
//                     serverErrorCall?.Invoke(error.GenerateErrorReport());
//                 });
// #elif UNITY_IOS
//                 PlayFabClientAPI.LoginWithGameCenter(new LoginWithGameCenterRequest()
//                 {
//                     TitleId = PlayFabSettings.TitleId,
//                     EncryptedRequest = null,
//                     PlayerId = Social.localUser.id,
//                     CreateAccount = true
//                 }, (result) =>
//                 {
//                     OnRegistration(result);
//                 },(error) => 
//                 {
//                     Debug.Log("Got error retrieving user data:");
//                     Debug.Log(error.GenerateErrorReport());
//                     serverErrorCall?.Invoke(error.GenerateErrorReport());
//                 });
// #endif
//             }
//             else
//             {
                
//             }

//         });
//     }

#if UNITY_IOS
    // public void LoginWithIOSAccount(Action<GetUserDataResult> callback)
    // {
    //     userDataResult = callback;

    //     GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
    //     OnSigned();
    // }
#endif

#region Account Login
    public void LoginWithAndroidDeviceID(Action<GetTitleDataResult, GetUserDataResult> callback)
    {
        initDataResult = callback;
#if UNITY_ANDROID
		LoginWithAndroidDeviceIDRequest request = new LoginWithAndroidDeviceIDRequest();
		request.TitleId = PlayFabSettings.TitleId;
        if(Application.platform == RuntimePlatform.Android)
		    request.AndroidDeviceId = SystemInfo.deviceUniqueIdentifier;
        else
            //request.AndroidDeviceId = "27fc2fc86fa4bd05f67658a28df9122c";
		    request.AndroidDeviceId = testAccountId; // honeyplug_test013
        request.AndroidDevice = SystemInfo.deviceModel;
#elif UNITY_IOS
        LoginWithIOSDeviceIDRequest request = new LoginWithIOSDeviceIDRequest();
        request.TitleId = PlayFabSettings.TitleId;
        if(Application.platform == RuntimePlatform.IPhonePlayer)
		    request.DeviceId = SystemInfo.deviceUniqueIdentifier;
        else
            //request.AndroidDeviceId = "27fc2fc86fa4bd05f67658a28df9122c";
		    request.DeviceId = testAccountId; // honeyplug_test002
        request.DeviceModel = SystemInfo.deviceModel; 
#endif

		request.OS = SystemInfo.operatingSystem;
		request.CreateAccount = true;
#if UNITY_ANDROID
		PlayFabClientAPI.LoginWithAndroidDeviceID (request, OnRegistration, null);
#elif UNITY_IOS
        PlayFabClientAPI.LoginWithIOSDeviceID(request, OnRegistration, null);
#endif
    }

    public string nickName;
    public string myPlayFabID;
    void OnRegistration (LoginResult result)
	{
        SuccessEndConnect(result.ToJson());
        if(result.LastLoginTime != null)
        {
            lastLoginTime = (DateTime)result.LastLoginTime;
            lastLoginTime = lastLoginTime.ToUniversalTime();
        }
        else lastLoginTime = new DateTime();

        myPlayFabID = result.PlayFabId; nickName = myPlayFabID;

        // 계정정보 받아옴
        var request = new GetAccountInfoRequest { Username = null};
        PlayFabClientAPI.GetAccountInfo(request, GetAccountSuccess, null);

        //DateTime ddd = DateTime.UtcNow.AddHours(-2);
        //Debug.Log(HoneyPlug.Utils.Time.GetMillisecondsByDateTime(ddd));
	}

    private void GetAccountSuccess(GetAccountInfoResult result)
    {    
        string nickname = result.AccountInfo.TitleInfo.DisplayName;
        // if(nickname == null)
        //     PlayerDataManager.instance.nickName = playFabID;
        // else
        //     PlayerDataManager.instance.nickName = nickname;

        //GetUserData("9D7B2B00123209EC");

        ClientGameSetData();
    }

    public bool EqualMyId(string playFabID)
    {
        return myPlayFabID == playFabID;
    }

#endregion

#region 초기 게임 설정 데이터 셋팅
    Action<GetTitleDataResult, GetUserDataResult> initDataResult;
    GetTitleDataResult titleDataRes;
    void ClientGameSetData() 
    {
        PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(),
        result => 
        {
            //GameSet.instance.MaxBallCombo = int.Parse(result.Data["MaxBallCombo"]);
            
            titleDataRes = result;
            GetUserData(myPlayFabID);
        },
        error => 
        {
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        }
        );
    }
#endregion

	void GetUserData(string playFabID)
	{
		PlayFabClientAPI.GetUserData(new GetUserDataRequest() 
        {
            // PlayFabId = playFabID,
            // Keys = new List<string>
            // {
            //     {"gold"}, 
            //     {"spincount"},
            //     {"shieldcount"},
            //     {"building_id"},
            //     {"building_level"},
            //     {"building_bombing"}
            // }
        }, result => {
            SuccessEndConnect(result.ToJson());
            initDataResult?.Invoke(titleDataRes, result);

            //GetLeaderBoard();

        }, (error) => {
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        });
	}

    public void SetUserData(string keyName, string setValue, Action callBack)
    {
        UpdateUserDataRequest request = new UpdateUserDataRequest() 
        {
            Data = new Dictionary<string, string> 
            {
                {keyName, setValue}
            } 
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result) 
        { 
            SuccessEndConnect(result.ToJson());
            callBack?.Invoke();
        }

        void OnError(PlayFabError error) 
        {
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        } 
    }

    public void SetUserDatas(Dictionary<string, string> datas, Action callBack)
    {
        UpdateUserDataRequest request = new UpdateUserDataRequest() 
        {
            Data = datas,
            Permission = UserDataPermission.Public
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result) 
        { 
            SuccessEndConnect(result.ToJson());
            callBack?.Invoke();
        }

        void OnError(PlayFabError error) 
        {
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        } 
    }

    public void GetUserData(string keyName, Action<string> callBack)
    {
        netState = State.Connect;
        PlayFabClientAPI.GetUserData(new GetUserDataRequest() 
        {
            PlayFabId = myPlayFabID,
            Keys = new List<string>
            {
                {keyName}
            }
        }, result => 
        {
            SuccessEndConnect(result.ToJson());
            if(result.Data.ContainsKey(keyName))
                callBack?.Invoke(result.Data[keyName].Value);
            else
                callBack?.Invoke(null);
        }, (error) => 
        {
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        });
    }

    public void GetUserVirtualCurrency(Action<Dictionary<string, int>> invenCallback)
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest()
        {

        }, result => {
            invenCallback?.Invoke(result.VirtualCurrency);
        }, (error) => {
            Debug.Log("Got error retrieving user data:");
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        });
    }

    public void AddUserVirtualCurrency(int amount, string vc, Action<int> Callback)
    {
        netState = State.Connect;
        PlayFabClientAPI.AddUserVirtualCurrency(new AddUserVirtualCurrencyRequest()
        {
            Amount = amount,
            VirtualCurrency = vc
        }, result => {
            Callback.Invoke(result.Balance);
        }, (error) => {
            Debug.Log("Got error retrieving user data:");
            Debug.Log(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        });
    }

    public void SubtractUserVirtualCurrency(int amount, string vc, Action<int> Callback)
    {
        netState = State.Connect;
        PlayFabClientAPI.SubtractUserVirtualCurrency(new SubtractUserVirtualCurrencyRequest()
        {
            Amount = amount,
            VirtualCurrency = vc
        }, result => {
            SuccessEndConnect(result.ToJson());
            Callback.Invoke(result.Balance);
        }, (error) => {
            Debug.Log("Got error retrieving user data:");
            Debug.Log(error.GenerateErrorReport()); netState = State.End;
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        });
    }
    
    //Action<DateTime> timeCallBack;
    // public void GetUTCTimeFromCloud(Action<DateTime> timeCallBack = null)
    // {
    //     this.timeCallBack = timeCallBack; netState = State.Connect;
    //     PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
    //     {
    //         FunctionName = "GetTimeNow",
    //         FunctionParameter = new { },
    //         GeneratePlayStreamEvent = false,
    //     }, OnGetUTCNow, OnErrorShared);
    // }

    // private void OnGetUTCNow(ExecuteCloudScriptResult result) 
    // {
    //     //Debug.Log(JsonWrapper.SerializeObject(result.FunctionResult));
    //     // JsonObject jsonResult = (JsonObject)result.FunctionResult;
    //     // object messageValue;
    //     // jsonResult.TryGetValue("messageValue", out messageValue); // note how "messageValue" directly corresponds to the JSON values set in CloudScript
    //     // Debug.Log((string)messageValue);

    //     var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
    //     Debug.Log(serializer.SerializeObject(result.FunctionResult));
    //     DateTime date = HoneyPlug.Utils.Time.GetUTCNow(serializer.SerializeObject(result.FunctionResult));
    //     Debug.Log(date);
    //     timeCallBack?.Invoke(date); netState = State.End;
    // }

    private void OnErrorShared(PlayFabError error)
    {
        Debug.Log(error.GenerateErrorReport());
    }

    Action<List<CatalogItem>> getCatalogCallback;
    public void GetCatalogItems(string catalogName, Action<List<CatalogItem>> successCallBack = null, Action<string> failCallBack = null)
    {
        getCatalogCallback = successCallBack;
        GetCatalogItemsRequest request = new GetCatalogItemsRequest() 
        {
            CatalogVersion = catalogName
        };

        netState = State.Connect;
        PlayFabClientAPI.GetCatalogItems(request, OnSuccess, OnError);

        void OnSuccess(GetCatalogItemsResult result) 
        { 
            SuccessEndConnect(result.ToJson());
            successCallBack?.Invoke(result.Catalog);
        }

        void OnError(PlayFabError error) 
        { 
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        } 
    }
    
    //Action<GetStoreItemsResult> getStoreCallback;
    public void GetStoreItems(string catalogName, string storeId, Action<GetStoreItemsResult> successCallBack = null, Action<string> failCallBack = null)
    {
        GetStoreItemsRequest request = new GetStoreItemsRequest() 
        {
            CatalogVersion = catalogName,
            StoreId = storeId
        };

        netState = State.Connect;
        PlayFabClientAPI.GetStoreItems(request, OnSuccess, OnError);

        void OnSuccess(GetStoreItemsResult result) 
        { 
            SuccessEndConnect(result.ToJson());
            successCallBack?.Invoke(result);
        }

        void OnError(PlayFabError error) 
        { 
            Debug.Log("UpdateUserData : Fail ..."); 
            Debug.Log(error.GenerateErrorReport()); 
            //failCallBack?.Invoke(error.GenerateErrorReport());
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        } 
    }

    private void LateUpdate() 
    {
        ;
    }

    // void DefinePurchase()
    // {
    //     var primaryCatalogName = "New";
    //     var storeId = "Shop_1001";
    //     var request = new StartPurchaseRequest
    //     {
    //         CatalogVersion = primaryCatalogName,
    //         StoreId = storeId,
    //         Items = new List<ItemPurchaseRequest> 
    //         {
    //             // The presence of these lines are based on the results from GetStoreItems, and user selection - Yours will be more generic
    //             new ItemPurchaseRequest { ItemId = "Item_001", Quantity = 1,}
    //         }
    //     };

    //     Debug.Log("Start Purchase");

    //     PlayFabClientAPI.StartPurchase(request, result => { 
    //         Debug.Log("Purchase started: " + result.OrderId);
    //         Id = result.OrderId; 
    //         ConfirmPurchase();
    //         }, PurchaseFailure);

        
    // }

    List<ItemInstance> items;
    Action<List<ItemInstance>> purchaseItemCallback;
    public void PurchaseStoreItem(string primaryCatalogName, string storeId, string itemId, int price, string currency, Action<List<ItemInstance>> purchaseItemCallback)
    {
        this.purchaseItemCallback = purchaseItemCallback;
        var request = new PurchaseItemRequest
        {
            CatalogVersion = primaryCatalogName,
            CharacterId = null,
            ItemId = itemId,
            Price = price,
            StoreId = storeId,
            VirtualCurrency = currency
        };

        Debug.Log("Start Purchase");
        netState = State.Connect;
        PlayFabClientAPI.PurchaseItem(request, result => 
        { 
            //Debug.Log("PurchaseItem: " + result.ToJson());
            if(storeId.Contains("Gold") || storeId.Contains("Gem"))
            {
                SuccessEndConnect(result.ToJson());
                purchaseItemCallback?.Invoke(result.Items);
            }
            else
            {
                ItemInstance origin = result.Items.Find(d => d.ItemId == itemId);
                result.Items.Remove(origin);
                this.items = result.Items;

                purchaseItemCallback?.Invoke(result.Items);
                //ConsumeStoreItem(1, this.items[0].ItemInstanceId);
            }
            }, PurchaseFailure);
    }

    // void ConsumeStoreItem(int cnt, string instId)
    // {
    //     var request = new ConsumeItemRequest
    //     {
    //         CharacterId = null,
    //         ConsumeCount = cnt,
    //         ItemInstanceId = instId
    //     };
        
    //     netState = State.Connect;
    //     PlayFabClientAPI.ConsumeItem(request, result => { 
    //         Debug.Log("ConsumeItem: " + result.ToJson());
    //         netState = State.End;
    //         purchaseItemCallback?.Invoke(items);
    //         }, PurchaseFailure);
    // }

    string Id;

    private void PurchaseFailure(PlayFabError error)
    {
        Debug.Log(error.GenerateErrorReport());
    }

    // void ConfirmPurchase()
    // {
    //     var request = new ConfirmPurchaseRequest
    //     {
    //         OrderId = Id
    //     };

    //     Debug.Log("ConfirmPurchase");

    //     PlayFabClientAPI.ConfirmPurchase(request, result => { Debug.Log("Purchase started: " + result.ToJson()); }, PurchaseFailure);
    // }

    void SuccessEndConnect(string szResult)
    {
        netState = State.End;
        // if(Application.platform == RuntimePlatform.OSXEditor)
        //     print(szResult);
    }
}
