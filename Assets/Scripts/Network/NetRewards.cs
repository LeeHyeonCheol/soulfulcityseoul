﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{
    Action<string> rewardCallback;
    Action<string, string> rewardPlusCallback;
    string boolIsWin;
    string otherPlayFabId;
    string atkBuildingId;

    // public void GetRewardAtkOtherPlayer(Action<string, string> rewardPlusCallback, string otherPlayFabId, int id, int level, bool isShield, bool isADReward = false)
    // {
    //     this.otherPlayFabId = otherPlayFabId;
    //     atkBuildingId       = id.ToString();
    //     boolIsWin           = "N";
    //     this.isADReward     = isADReward;

    //     this.rewardPlusCallback = rewardPlusCallback;
    //     netState = State.Connect;

    //     if(isShield == false) boolIsWin = "Y";
    //     var request = new ExecuteCloudScriptRequest() 
    //     { 
    //         FunctionName = "ProcAttackOtherPlayer", GeneratePlayStreamEvent = true, // true이면 이벤트가 생성되어 PlayStream 콘솔에서 확인할 수 있다 };  
    //         FunctionParameter = new { id = id, level = level, isWin = boolIsWin, isADReward = CheckAD_Reward()}
    //     };

    //     PlayFabClientAPI.ExecuteCloudScript(request, SuccessGetRewardAtk, 
    //     (error) => Debug.LogError(error.GenerateErrorReport()));
    // }

    [Serializable]
    class GetRewardGold
    {
        public string gold;
    }

    [Serializable]
    class GetRewardGoldPlus
    {
        public string gold;
        public string getGold;
    }

    // void SuccessGetRewardAtk(ExecuteCloudScriptResult result)
    // {
    //     netState = State.End;
    //     var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
    //     //Debug.Log("SuccessGetRewardAtk :::::: " + serializer.SerializeObject(result.FunctionResult));
    //     GetRewardGoldPlus rewardData = JsonUtility.FromJson<GetRewardGoldPlus>(serializer.SerializeObject(result.FunctionResult));

    //     //print("SuccessGetRewardAtk : " + rewardData.gold);
    //     ProcBuildingAttacked(rewardData.gold, rewardData.getGold);
    // }
    
    // ADD : 2020.11.19
    // 공격 받은 상대방 건물 
    // Test Script
    // {revengePlayerId: "9D7B2B00123209EC",
    // atkBuildingId: "8",
    // playerId: "3CF79E3DF9210F3E",
    // isWin: "Y"
    // }
    string atkRewardGold;
    string atkRewardPlus;
    // void ProcBuildingAttacked(string rewardGold, string rewardPlus)
    // {
    //     atkRewardGold = rewardGold;
    //     atkRewardPlus = rewardPlus;
    //     netState = State.Connect;

    //     // ADD : 2020.11.20
    //     // 공격한 상대 ID 추가, 복수 기능
    //     var request = new ExecuteCloudScriptRequest() 
    //     { 
    //         FunctionName = "ProcBuildingAttacked", GeneratePlayStreamEvent = true, 
    //         FunctionParameter = new { revengePlayerId = myPlayFabID, atkBuildingId = atkBuildingId, playerId = otherPlayFabId, isWin = boolIsWin}
    //     };

    //     PlayFabClientAPI.ExecuteCloudScript(request, ResBuildingAttacked, 
    //     (error) => Debug.LogError(error.GenerateErrorReport()));
    // }

    // void ResBuildingAttacked(ExecuteCloudScriptResult result)
    // {
    //     netState = State.End; rewardPlusCallback?.Invoke(atkRewardGold, atkRewardPlus);
    // }

    // 친구 공격했는지 여부 업데이트
    public void UpdateFriendAttackedList(string friendId, Action callback)
    {
        UpdateUserDataRequest request = new UpdateUserDataRequest() 
        {
            Data = new Dictionary<string, string>()
            {
                {"friendAtkLst", NetworkManager.instance.GetFriendAttackedList(friendId)}
            }
        };

        netState = State.Connect;
        PlayFabClientAPI.UpdateUserData(request, OnSuccess, OnError);
        void OnSuccess(UpdateUserDataResult result) 
        { 
            SuccessEndConnect(result.ToJson());
            callback?.Invoke();
        }

        void OnError(PlayFabError error) 
        { 
            serverErrorCall?.Invoke(error.GenerateErrorReport());
        }
    }

    public void GetRewardMiniGames(Action<string> rewardCallback, int totalLv, int clearLv, bool isADReward = false)
    {
        this.rewardCallback = rewardCallback;
        netState = State.Connect;

        this.isADReward = isADReward;
        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "RewardMiniGames", GeneratePlayStreamEvent = true, 
            FunctionParameter = new { totalLv = totalLv, clearLv = clearLv, isADReward = CheckAD_Reward()}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessRewardMiniGames, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    void SuccessRewardMiniGames(ExecuteCloudScriptResult result)
    {
        netState = State.End;
        var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        GetRewardGold rewardData = JsonUtility.FromJson<GetRewardGold>(serializer.SerializeObject(result.FunctionResult));

        rewardCallback?.Invoke(rewardData.gold);
    }

    bool isADReward;
    string CheckAD_Reward()
    {
        string adView = "N";
        if(isADReward) adView = "Y";

        return adView;
    }

    Action rewardADsCallback;
    public void GetRewardFromADs(Action rewardADsCallback, int type)
    {
        this.rewardADsCallback = rewardADsCallback;
        netState = State.Connect;

        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "RewardFromADs", GeneratePlayStreamEvent = true, 
            FunctionParameter = new { type = type}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessRewardFromADs, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    void SuccessRewardFromADs(ExecuteCloudScriptResult result)
    {
        //print("SuccessRewardFromADs");
        netState = State.End;
        //var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        //GetRewardGold rewardData = JsonUtility.FromJson<GetRewardGold>(serializer.SerializeObject(result.FunctionResult));

        rewardADsCallback?.Invoke();
    }

#region ################### 건물 청소 추가 ###################
    public void GetRewardCleaning(Action<string, string> rewardPlusCallback, int level, bool isADReward = false)
    {
        this.isADReward         = isADReward;
        this.rewardPlusCallback = rewardPlusCallback;

        netState = State.Connect;
        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "ProcCleaning", GeneratePlayStreamEvent = true, // true이면 이벤트가 생성되어 PlayStream 콘솔에서 확인할 수 있다 };  
            FunctionParameter = new { level = level, isADReward = CheckAD_Reward()}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessGetRewardClean, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    void SuccessGetRewardClean(ExecuteCloudScriptResult result)
    {
        netState = State.End;
        var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        GetRewardGoldPlus rewardData = JsonUtility.FromJson<GetRewardGoldPlus>(serializer.SerializeObject(result.FunctionResult));

        rewardPlusCallback?.Invoke(rewardData.gold, rewardData.getGold);
    }
#endregion

    // ADD : 친구 방문 보너스 21.02.19
    public void RewardBonus_VisitFriend(Action<string, string> rewardPlusCallback, int level, int touchCnt, bool isADReward = false)
    {
        this.isADReward         = isADReward;
        this.rewardPlusCallback = rewardPlusCallback;

        netState = State.Connect;
        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "ProcVisitFriendBonus", GeneratePlayStreamEvent = true,  
            FunctionParameter = new { level = level, touchCnt = touchCnt, isADReward = CheckAD_Reward()}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessBonus_VisitFriend, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    void SuccessBonus_VisitFriend(ExecuteCloudScriptResult result)
    {
        netState = State.End;
        var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        GetRewardGoldPlus rewardData = JsonUtility.FromJson<GetRewardGoldPlus>(serializer.SerializeObject(result.FunctionResult));

        rewardPlusCallback?.Invoke(rewardData.gold, rewardData.getGold);
    }

    public void RewardSpinCount(Action callback, bool isADReward = false)
    {
        this.isADReward  = isADReward;
        rouletteCallback = callback;

        netState = State.Connect;
        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "RewardSpinCount", GeneratePlayStreamEvent = true,  
            FunctionParameter = new { isADReward = CheckAD_Reward()}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessGetSpinCount, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    [Serializable]
    class ResSpinCount
    {
        public int spincount;
    }

    void SuccessGetSpinCount(ExecuteCloudScriptResult result)
    {
        netState = State.End;
        var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        ResSpinCount rewardData = JsonUtility.FromJson<ResSpinCount>(serializer.SerializeObject(result.FunctionResult));

        GameDataManager.instance.Roulette.SetUseCount(rewardData.spincount);
        rouletteCallback?.Invoke();
    }

    public void RewardBonus_Quiz(Action<string, string> rewardPlusCallback, string answer, bool isADReward = false)
    {
        this.isADReward         = isADReward;
        this.rewardPlusCallback = rewardPlusCallback;

        netState = State.Connect;
        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "RewardQuiz", GeneratePlayStreamEvent = true,  
            FunctionParameter = new { answer = answer, isADReward = CheckAD_Reward()}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessBonus_Quiz, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    void SuccessBonus_Quiz(ExecuteCloudScriptResult result)
    {
        netState = State.End;
        var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        GetRewardGoldPlus rewardData = JsonUtility.FromJson<GetRewardGoldPlus>(serializer.SerializeObject(result.FunctionResult));

        rewardPlusCallback?.Invoke(rewardData.gold, rewardData.getGold);
    }
}
