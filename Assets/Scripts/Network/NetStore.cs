﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using PlayFab.Internal;
//using PlayFab.PfEditor.Json;
// #if UNITY_ANDROID
// using GooglePlayGames;
// using GooglePlayGames.BasicApi;
// #elif UNITY_IOS
// using UnityEngine.SocialPlatforms.GameCenter;
// #endif
using HoneyPlug.Utils;

public partial class PlayFabController : SingletonMonoBehaviour<PlayFabController>
{
    Action<ulong, int> endPurchaseCallback;
    public void ProcPurchaseItems(string productId, Action<ulong, int> endPurchaseCallback)
    {
        netState = State.Connect;
        this.endPurchaseCallback = endPurchaseCallback;
        var request = new ExecuteCloudScriptRequest() 
        { 
            FunctionName = "PocPurchaseItem", GeneratePlayStreamEvent = true, 
            FunctionParameter = new { productId = productId}
        };

        PlayFabClientAPI.ExecuteCloudScript(request, SuccessEndPurchase, 
        (error) => Debug.LogError(error.GenerateErrorReport()));
    }

    [Serializable]
    class ResEndPurchase
    {
        public string gold;
        public string spincount;
    }

    void SuccessEndPurchase(ExecuteCloudScriptResult result)
    {
        netState = State.End;
        var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
        ResEndPurchase resData = JsonUtility.FromJson<ResEndPurchase>(serializer.SerializeObject(result.FunctionResult));

        endPurchaseCallback?.Invoke(Convert.ToUInt64(resData.gold), 
            Convert.ToInt32(resData.spincount));
    }
}
