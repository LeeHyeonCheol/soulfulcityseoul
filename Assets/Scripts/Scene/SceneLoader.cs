﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    public static SceneLoader instance = null;

    [SerializeField] Image fadeInOut;
    [SerializeField] Image fadeInOutBack;
    [SerializeField, ReadOnly] string currentScene;

    [SerializeField] List<GameObject> sampleScene;
    [SerializeField] List<GameObject> buildingScene;

    #region Drage reference
    [SerializeField] Canvas canvas;
    [SerializeField, ReadOnly] Vector2 beginDrag;
    [SerializeField, ReadOnly] Vector2 endDrag;
    #endregion

    Coroutine loadRoutine;
    Dictionary<string, Action> actions;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
#if UNITY_EDITOR || USE_DEBUG
            Debug.LogWarning("There is more than one scene loader.");
#endif
            Destroy(this.gameObject);
        }

        actions = new Dictionary<string, Action>();
        actions.Add(Common.RULSceneName, SampleScene);
        actions.Add(Common.BUDSceneName, BuildingTest);

        currentScene = Common.RULSceneName;
    }
    public void LoadScene(string name,Action callback = null)
    {
        currentScene = name;

        if(loadRoutine == null)
        {
            loadRoutine = StartCoroutine(Loading(actions[name], callback));
        }
    }

    IEnumerator Loading(Action action,Action callback = null)
    {
        float w = Screen.width + fadeInOut.rectTransform.rect.width + fadeInOutBack.rectTransform.rect.width;

        fadeInOut.transform.localPosition = new Vector2(-w, 0);

        for(float i = 0; i < w; i += (w * .05f))
        {
            fadeInOut.transform.localPosition = new Vector2(-w + i, 0);
            yield return null;
        }


        yield return null;

        // 활성화 관련
        action.Invoke();
        callback?.Invoke();

        for (float i = 0; i < w; i += (w * .05f))
        {
            fadeInOut.transform.localPosition = new Vector2(i, 0);
            yield return null;
        }

        loadRoutine = null;
    }

    void SampleScene()
    {
        for(int i = 0; i < buildingScene.Count; i++)
        {
            buildingScene[i].gameObject.SetActive(false);
        }
        for(int i = 0; i < sampleScene.Count; i++)
        {
            sampleScene[i].gameObject.SetActive(true);
        }
    }
    void BuildingTest()
    {
        for (int i = 0; i < buildingScene.Count; i++)
        {
            buildingScene[i].gameObject.SetActive(true);
        }
        for (int i = 0; i < sampleScene.Count; i++)
        {
            sampleScene[i].gameObject.SetActive(false);
        }
    }

    public string GetCurrentScene()
    {
        return currentScene;
    }
    public bool EndLoad()
    {
        return (loadRoutine == null);
    }

    #region Drag Move
    public void OnBegineDrag()
    {
        beginDrag = Input.mousePosition;
    }
    public void OnEndDrag()
    {
        endDrag = Input.mousePosition;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            canvas.transform as RectTransform,
            endDrag, canvas.worldCamera,
            out endDrag);

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
           canvas.transform as RectTransform,
           beginDrag, canvas.worldCamera,
           out beginDrag);

        Vector2 direction = endDrag - beginDrag;
        bool isLeftDrag = (direction.normalized.x < 0);
        float distance = Mathf.Abs(endDrag.x - beginDrag.x);

        if(isLeftDrag && (Screen.width * .15f < distance) &&
            GetCurrentScene() == Common.RULSceneName &&
            (GameDataManager.instance.Roulette.ID != Common.RULItem.ID.Attack && GameDataManager.instance.Roulette.ID != Common.RULItem.ID.Steal) &&
            GameDataManager.instance.Roulette.AnimationFlag == false)
        {
            BuildingManager.instance.NextScene();
        }
        else
        {
            //= 2020 01 18
            //= 회의 결과 : 씬 이동 연출 추가 없음
        }
    }
    #endregion

}
