﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TutorialManager : MonoBehaviour
{
    [Header("Step reference")]
    [SerializeField] List<Image> hands;
    [SerializeField] List<GameObject> steps;
    [SerializeField] List<Text> notice;
    [SerializeField] Image otherTouchBlock;

    [Header("Class Reference")]
    [SerializeField] NPCManager npcManager;
    [SerializeField] GoldUI goldUI;

    [Header("Prefeb reference")]
    [SerializeField] Canvas bookCanvas;
    [SerializeField] TutorialBook book;
    [SerializeField, ReadOnly] TutorialBook instanceBook;

    public static bool isTutorial;
    
    private void Start()
    {
        //Init(false);
        //SetStepStart_Zero();
    }

    public void Init(bool isClear)
    {
        if(steps != null)
        {
            for(int i = 0; i < steps.Count; i++)
            {
                steps[i].SetActive(false);
            }
        }

        isTutorial = !isClear;
        if (isClear) 
        {
            otherTouchBlock.gameObject.SetActive(false);
            return;
        }
        else
        {
            SetStepStart_Zero();

            //= 로티 생성 금지
            npcManager.IsDisableUpdateNPC = true;

            //= 골드 초기화
            UserManager.instance.GoldOnEvent = 0;

            //= level 초기화
            BuildingManager.instance.Init("", Common.InitSetBuildingLevel, "", "");
        }
    }

    void SetMoveHand(Image hand)
    {
        Vector3 direction = hand.transform.up + (hand.transform.right * -1 /* left */);

        if (hand.transform.localScale.x == -1) direction += (hand.transform.right);
        else direction += (hand.transform.right * -1 /* left */);
        direction = direction.normalized;

        float power = 20.0f;

        Sequence move = DOTween.Sequence();
        move.Append(hand.transform.DOLocalMove(hand.transform.localPosition + (direction * power), .5f).SetEase(Ease.Linear));
        move.Append(hand.transform.DOLocalMove(hand.transform.localPosition - (direction * power), .5f).SetEase(Ease.Linear));
        move.SetLoops(-1, LoopType.Yoyo);
        move.Play();
    }

    #region Step function

    //= 골드 지급 튜토리얼
    public void SetStepStart_Zero()
    {
        if (steps != null && steps.Count != 0)
        {
            steps[0].gameObject.SetActive(true);
        }

        SetMoveHand(hands[0]);
        TextSetting(notice[0], "Tutuorial_1");
        InColorChangeActive(steps[0].transform,.7f);
        InColorChangeActive(notice[0],.7f);

        if (GameDataManager.instance != null && GameDataManager.instance.Roulette != null)
        {
            //= 제일 비싼 골드
            GameDataManager.instance.Roulette.FastenReward(3);
        }
        else
        {
#if USE_DEBUG
            Debug.LogError("Don't init : game data manager in tutorial");
#endif
        }
    }
    public void SetStepEnd_Zero()
    {
        InColorChangeDisable(steps[0].transform);
        InColorChangeDisable(notice[0]);

        StartCoroutine(StartFunction_OnReady_One());
    }
    IEnumerator StartFunction_OnReady_One()
    {
        otherTouchBlock.gameObject.SetActive(true);
        yield return new WaitUntil(() => UserManager.instance.GoldOffEvent != 0 && goldUI.GoldBase != 0);
        otherTouchBlock.gameObject.SetActive(false);
        SetStepStart_One();
    }

    //= 공격 뽑기 튜토리얼
    public void SetStepStart_One()
    {
        if (steps != null && steps.Count != 0)
        {
            steps[1].gameObject.SetActive(true);
        }


        SetMoveHand(hands[1]);
        TextSetting(notice[1], "Tutuorial_2");
        InColorChangeActive(steps[1].transform,.5f);
        InColorChangeActive(notice[1],.5f);

        if (GameDataManager.instance != null && GameDataManager.instance.Roulette != null)
        {
            GameDataManager.instance.Roulette.FastenReward(5);
            GameDataManager.instance.Roulette.addResultAnimationCallback = SetStepStart_Two;
        }
        else
        {
#if USE_DEBUG
            Debug.LogError("Don't init : game data manager in tutorial");
#endif
        }

    }
    public void SetStepEnd_One()
    {
        InColorChangeDisable(steps[1].transform);
        InColorChangeDisable(notice[1]);
    }

    //= 공격 튜토리얼
    public void SetStepStart_Two()
    {
        if (steps != null && steps.Count != 0)
        {
            steps[2].gameObject.SetActive(true);
        }
        SetMoveHand(hands[2]);
        InColorChangeActive(steps[2].transform);
        InColorChangeActive(notice[2]);
        TextSetting(notice[2], "Tutuorial_3");

        //= Building Manager
        BuildingManager.instance.attackTutorial_start = SetStepEnd_Two;
        BuildingManager.instance.attackTutorial_finish = SetStepStart_Three;
    }
    public void SetStepEnd_Two()
    {
        InColorChangeDisable(steps[2].transform);
        InColorChangeDisable(notice[2]);
    }

    //= 빌딩씬으로 이동하기 튜토리얼
    public void SetStepStart_Three()
    {
        if (steps != null && steps.Count != 0)
        {
            steps[3].gameObject.SetActive(true);
        }

        SetMoveHand(hands[3]);
        InColorChangeActive(steps[3].transform);
        InColorChangeActive(notice[3]);
        TextSetting(notice[3], "Tutuorial_4");
    }
    public void SetStepEnd_Three()
    {
        InColorChangeDisable(steps[3].transform);
        InColorChangeDisable(notice[3]);
        SetStepStart_Four();
    }

    //= 업그레이드 창 열기 튜토리얼
    public void SetStepStart_Four()
    {
        if (steps != null && steps.Count != 0)
        {
            steps[4].gameObject.SetActive(true);
        }

        SetMoveHand(hands[4]);
        InColorChangeActive(steps[4].transform);
        InColorChangeActive(notice[4]);
        TextSetting(notice[4], "Tutuorial_5");
    }
    public void SetStepEnd_Four()
    {
        InColorChangeDisable(steps[4].transform);
        InColorChangeDisable(notice[4]);
        SetStepStart_Five();
    }

    //= 업그레이드 하기 튜토리얼
    public void SetStepStart_Five()
    {
        if (steps != null && steps.Count != 0)
        {
            steps[5].gameObject.SetActive(true);
        }

        SetMoveHand(hands[5]);
        InColorChangeActive(steps[5].transform,0.5f);
        InColorChangeActive(notice[5],0.5f);
        TextSetting(notice[5], "Tutuorial_6");
    }
    public void SetStepEnd_Five()
    {
        UIUpgradeBuild upgradeBuild = GameObject.FindObjectOfType<UIUpgradeBuild>();
        upgradeBuild.Construction(0 /* main building*/);

        InColorChangeDisable(steps[5].transform);
        InColorChangeDisable(notice[5]);
        SetStepStart_Six();
    }

    //= 업그레이드 확인
    public void SetStepStart_Six()
    {
        if (steps != null && steps.Count != 0)
        {
            steps[6].gameObject.SetActive(true);
        }

        SetMoveHand(hands[6]);
        TextSetting(notice[6], "Tutuorial_7");

        Button button = FindChildButton(steps[6].transform);
        if(button != null)
        {
            button.onClick.AddListener(PopupManager.instance.NowPopupUI.MainButton.onClick.Invoke);
            button.onClick.AddListener(
            delegate
            {
                SetStepEnd_Six();

            });
        }
    }
    public void SetStepEnd_Six()
    {
        InColorChangeDisable(steps[6].transform);
        InColorChangeDisable(notice[6]);
        StartCoroutine(StartFunction_OnReady_Seven());
    }
    IEnumerator StartFunction_OnReady_Seven()
    {
        yield return new WaitForSeconds(2.0f);
        yield return new WaitUntil(() => BuildingManager.instance.UiSet.buildings[0].Construction == false);
        SetStepStart_Seven();
    }

    //= 룰렛으로 돌아감
    public void SetStepStart_Seven()
    {
        if (steps != null && steps.Count != 0)
        {
            steps[7].gameObject.SetActive(true);
        }
        SetMoveHand(hands[7]);
        TextSetting(notice[7], "Tutuorial_8");
        InColorChangeActive(steps[7].transform, 1.0f);
        InColorChangeActive(notice[7], 1.0f);
    }
    public void SetStepEnd_Seven()
    {
        InColorChangeDisable(steps[7].transform);
        InColorChangeDisable(notice[7]);
        StartCoroutine(StartFunction_OnReady_Eight());
    }
    IEnumerator StartFunction_OnReady_Eight()
    {
        yield return new WaitUntil(() => SceneLoader.instance.GetCurrentScene() == Common.RULSceneName);
        yield return new WaitForSeconds(0.5f);

        SetStepStart_Eight();
    }
    //= 사전 펼침
    public void SetStepStart_Eight()
    {
        if (steps != null && steps.Count != 0)
        {
            steps[8].gameObject.SetActive(true);
        }
        TextSetting(notice[8], "Tutuorial_10");
        InColorChangeActive(steps[8].transform);
        InColorChangeActive(notice[8]);

        StartCoroutine(CreateBook_Delay(.5f));
    }
    IEnumerator CreateBook_Delay(float delay)
    {
        yield return new WaitForSeconds(delay);

        instanceBook = Instantiate(book);
        instanceBook.transform.SetParent(bookCanvas.transform);
        instanceBook.transform.localPosition = Vector3.zero;
        instanceBook.transform.localScale = Vector3.one;
        instanceBook.transform.SetSiblingIndex(2);
    }

    public void SetStepEnd_Eight()
    {
        InColorChangeDisable(steps[8].transform);
        InColorChangeDisable(notice[8]);

        npcManager.IsDisableUpdateNPC = false;

        //= 서버로 튜토리얼 종료됬음을 알리기
        Debug.Log("<color=red> Tutorial 종료</color>");
        NetworkManager.instance.EndTutorial(null);
    }

    #endregion

    void InColorChangeActive(Transform parent,float duration = 2.0f)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Image image = parent.GetChild(i).GetComponent<Image>();

            if (image != null)
            {
                Color existing = image.color;
                image.color = Common.ColorAlpha;
                image.DOColor(existing, duration).SetEase(Ease.InExpo);
            }
        }
    }
    void InColorChangeDisable(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Image image = parent.GetChild(i).GetComponent<Image>();

            if (image != null)
            {
                image.DOColor(Common.ColorAlpha, 1.0f).SetEase(Ease.OutExpo).
                    OnComplete(()=> image.gameObject.SetActive(false));
            }
        }
    }
    Button FindChildButton(Transform parent)
    {
        for(int i = 0; i < parent.childCount; i++)
        {
            Button button = parent.GetChild(i).GetComponent<Button>();
            if (button != null)
            {
                return button;
            }
        }

        return null;
    }
    void TextSetting(Text text,string textCode)
    {
        ArabicText arabicText = text.GetComponent<ArabicText>();
        if (LanguageManager.instance.IsArabic() == false)
        {
            arabicText.enabled = false;
            text.alignment = TextAnchor.UpperLeft;
        }
        else
        {
            arabicText.enabled = true;
            text.alignment = TextAnchor.UpperRight;
        }

        if (LanguageManager.instance.IsArabic() == false)
            text.text = LanguageManager.instance.GetString(textCode);
        else
        {
            arabicText.Text = LanguageManager.instance.GetString(textCode);
        }
    }
    void InColorChangeActive(Text text,float duration = 2.0f)
    {
        Color existing = text.color;
        text.color = Common.ColorAlpha;
        text.DOColor(existing, duration).SetEase(Ease.InExpo);
    }
    void InColorChangeDisable(Text text)
    {
        text.DOColor(Common.ColorAlpha, 1.0f).SetEase(Ease.OutExpo).
                    OnComplete(() => text.gameObject.SetActive(false));
    }
}

