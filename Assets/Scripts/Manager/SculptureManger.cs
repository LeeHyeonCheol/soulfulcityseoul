﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class SculptureManger : MonoBehaviour
{
    static SculptureManger instance = null;
    [System.Serializable]
    enum State
    {
        Normal,
        Arrangement
    }

    [Header("Set UI")]
    [SerializeField] Canvas canvas;
    [SerializeField] RectTransform parent;
    [SerializeField] Sculpture packetPivot;
    [SerializeField] ScrollRect mainScrollview;
    [SerializeField] RectTransform buildingContent;
    [SerializeField] Button offArragementButton;

    [Header("Inventory reference")]
    [SerializeField] Button closeButton;
    [SerializeField] RectTransform invenPivot;
    [SerializeField] RectTransform content;
    [SerializeField, ReadOnly] List<KeyValuePair<Sculpture /* id */, int /* count */>> inventory;

    [Header("Arrangement reference")]
    [SerializeField] GameObject arrangementZone;
    [SerializeField] Image cursor;
    [SerializeField] Image cursorGreen;
    [SerializeField] Image cursorRed;
    [SerializeField] Image chaser;
    [SerializeField] Sculpture selected;

    [Header("Move Reference")]
    [SerializeField] Transform leftZone;
    [SerializeField] Transform rightZone;
    [SerializeField] Transform moveChaser;
        
    [Header("Event button reference")]
    [SerializeField] Button withdraw;
    [SerializeField] Button reArrangement;
    [SerializeField] Button exit;

    [Header("Infomation")]
    [SerializeField, ReadOnly] List<Sculpture> prefabs;
    [SerializeField, ReadOnly] State state = State.Normal;
    [SerializeField, ReadOnly] float screenW;

    Dictionary<int, List<Sculpture>> arrangement;

    #region Property list
    public static SculptureManger Instance { get => instance; }
    #endregion
    private void Start()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

        LoadingPrefabs();
        Init();

        BuildingManager.instance.updateLevelEvent += RefreshLevelUp;
        screenW = canvas.GetComponent<CanvasScaler>().referenceResolution.x;
        if (screenW < Screen.width)
        {
            screenW = Screen.width;
        }
    }

    void LoadingPrefabs()
    {
        prefabs = new List<Sculpture>();

        Sculpture[] sculpture = Resources.LoadAll<Sculpture>(Common.SculpturePrefabPath);
        Debug.Log(sculpture.ToString());

        for (int i = 0; i < sculpture.Length; i++)
        {
            prefabs.Add(sculpture[i]);
        }
    }
    void Init()
    {
        #region Set Cursor
        cursor.color = Common.ColorAlpha;
        state = State.Normal;
        cursorGreen.gameObject.SetActive(false);
        cursorRed.gameObject.SetActive(false);
        #endregion

        mainScrollview.horizontal = true;
        offArragementButton.gameObject.SetActive(false);

        #region Set Inventory
        //==Init("", "1,2|2,1|3,2|4,1|5,1|6,1|7,1|8,1|9,1|10,1|11,1|12,1|13,1");
        SetInventoryContent(content);
        #endregion
    }
    void SetInventoryContent(RectTransform content)
    {
        float offset = 90;
        float spacing = 175;
        float size = offset * 2 + spacing * ((inventory != null) ? (inventory.Count - 1) : 0);

        float maxSize = (size < Screen.width) ? Screen.width : size;
        content.sizeDelta = new Vector2(maxSize, content.sizeDelta.y);

        if(inventory != null)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                inventory[i].Key.transform.SetParent(content);
                inventory[i].Key.transform.localPosition = new Vector3(offset + (spacing * i), invenPivot.localPosition.y, 0);
            }
        }
    }

    #region External function
    public void Init(string arrangementData, string inventoryData)
    {
        if (prefabs == null || prefabs.Count == 0) LoadingPrefabs();

        #region inventory sculpture
        // kill inventory object
        if (inventory != null)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
#if USE_DEBUG
                Debug.Log(inventory[i].Key.gameObject + "Destroy in arrangment");
#endif
                Destroy(inventory[i].Key.gameObject);
            }
            inventory = null;
        }
        if (inventoryData != null && inventoryData != "")
        {
            Debug.Log(inventoryData);

            // inventory data => id,count|id,count|------|id,count
            inventory = new List<KeyValuePair<Sculpture, int>>();
            string[] data = inventoryData.Split(Common.splitWord);

            for (int i = 0; i < data.Length; i++)
            {
                try
                {
                    string id = data[i].Split(',')[0];
                    string count = data[i].Split(',')[1];
                    InsertSculpture(int.Parse(id), int.Parse(count));
                }
                catch (System.Exception e)
                {
#if USE_DEBUG
                    Debug.Log("Data : " + data[i] + " <color=yellow>" + e.StackTrace + "</color>\n" + inventoryData);
                    Debug.Log(inventoryData);
#endif
                }
            }
        }
        else if (inventory == null)
        {
            inventory = new List<KeyValuePair<Sculpture, int>>();
        }
        #endregion
        #region Arrangement Sculpture
        // kill arrangement object
        if (arrangement != null)
        {
            foreach (int key in arrangement.Keys)
            {
                List<Sculpture> sculptures = arrangement[key];
                for (int i = 0; i < sculptures.Count; i++)
                {
#if USE_DEBUG
                    Debug.Log(sculptures[i] + "Destroy in arrangment");
#endif
                    Destroy(sculptures[i].gameObject);
                    sculptures[i] = null;
                }
            }
            arrangement = null;
        }


        if (arrangementData != null && arrangementData != "")
        {
            arrangement = new Dictionary<int, List<Sculpture>>();

            //= Arrangement data : id|x,y|id|x,y| - - - id|x,y
            string[] words = arrangementData.Split(Common.splitWord);
            arrangement = new Dictionary<int, List<Sculpture>>();

            try
            {
                for (int i = 0; i < words.Length; i += 2)
                {
                    int id = int.Parse(words[i]);

                    #region Set Sculpture 
                    string[] vector = words[i + 1].Split(',');
                    if (vector == null || vector.Length != 3 /* x,y.z */)
                    {
#if USE_DEBUG
                        Debug.Log("Server packet error : not find vector position [ Sculpture ID : " + id + "]");
#endif
                        continue;
                    }

                    ArrangementSculpture(id, new Vector3(float.Parse(vector[0]), float.Parse(vector[1]), float.Parse(vector[2])));
                    #endregion
                }
            }
            catch (System.Exception e)
            {
#if USE_DEBUG
                Debug.Log("<color=yellow>" + e.StackTrace + "</color>\n" + "Data : " + arrangementData);
#endif
            }
        }
        else if (arrangement == null)
        {
            arrangement = new Dictionary<int, List<Sculpture>>();
        }
        #endregion
    }
    public bool ArrangementSculpture(int id, Vector3 pos)
    {
        Sculpture sculpture = prefabs.Find(delegate (Sculpture v)
        {
            return v.ID == id;
        });
        if (sculpture == null)
        {
#if USE_DEBUG
            Debug.Log("ID : " + id + " prefabs not find [ Sculpture ]");
#endif
            return false;
        }

        Sculpture obj = Instantiate(sculpture, parent);

        obj.gameObject.name = "Sculptures " + string.Format("{0,1:D3}", id);
        obj.transform.localScale = Vector3.one;
        SetSculptureEvent(obj, ArrangementEvent);
        obj.SetState(Sculpture.State.Arrangment);
        obj.Init(pos);
        if (arrangement.ContainsKey(id) == false)
        {
            arrangement.Add(id, new List<Sculpture>());
        }

        arrangement[id].Add(obj);
        return true;
    }
    public void OnArrangementSimulation(Sculpture sculpture)
    {
        if (sculpture != null && state == State.Normal)
        {
            state = State.Arrangement;
            mainScrollview.horizontal = false;

            selected = sculpture;
            cursor.color = Common.ColorHalfAlpha;
            //= Note : 최하위(마지막 드로우)로 이동시키기 위한 연산
            cursor.transform.SetParent(this.transform);
            cursor.transform.SetParent(parent);

            cursor.sprite = sculpture.Image.sprite;
            cursor.rectTransform.sizeDelta = sculpture.Image.rectTransform.sizeDelta;
            chaser.rectTransform.sizeDelta = sculpture.Image.rectTransform.sizeDelta;
            closeButton.interactable = false;

            //= 2020 01 20 취소 버튼 추가
            offArragementButton.gameObject.SetActive(true);
            offArragementButton.onClick.RemoveAllListeners();
            offArragementButton.onClick.AddListener(
            () =>
            {
                OffArrangementSimulation(selected);
            });

            //= 2020 01 20 활성화시 인벤토리 비 활성화
            UIManager.instance.CloseUI_Inventory();
        }
    }
    public void OffArrangementSimulation(Sculpture sculpture)
    {
        if (sculpture == selected && state == State.Arrangement)
        {
            state = State.Normal;
            mainScrollview.horizontal = true;

            selected = null;
            cursor.color = Common.ColorAlpha;

            closeButton.interactable = true;

            //= 2020 01 20 취소 버튼 추가
            offArragementButton.gameObject.SetActive(false);
        }
    }
    public bool InsertSculpture(int id, int count = 1)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].Key.ID == id)
            {
                inventory[i] = new KeyValuePair<Sculpture, int>(inventory[i].Key, inventory[i].Value + count);
                inventory[i].Key.SetState(Sculpture.State.Inventory, inventory[i].Value);
                return true;
            }
        }

        // Check [ Sculpture id in prefabs ]
        Sculpture prefab = prefabs.Find(delegate (Sculpture s)
        {
            return s.ID == id;
        });
        if (prefab == null)
        {
#if USE_DEBUG
            Debug.Log("<color=yellow> ID : " + id + " not find sculpture </color>");
#endif
            return false;
        }
        Sculpture sculpture = Instantiate(prefab);
        sculpture.transform.SetParent(content);
        sculpture.SetState(Sculpture.State.Inventory, count);
        sculpture.transform.localScale = Vector3.one;
        sculpture.transform.localPosition = Vector3.zero;
        inventory.Add(new KeyValuePair<Sculpture, int>(sculpture, count));
        SetSculptureEvent(sculpture, InventoryEvent);
        SetInventoryContent(content);
        return true;
    }
    public Sculpture GetSculptureInfo(int id)
    {
        Sculpture info = prefabs.Find(delegate (Sculpture sculpture)
        {
            return sculpture.ID == id;
        });

#if USE_DEBUG
        if (info == null)
        {
            Debug.LogError("Not find sculpture [ ID : " + id + "]");
        }
#endif
        return info;
    }
    public void Refresh()
    {
        #region Rearrangement
        string[] data = MakeServerPacket_Current();
        Init(data[0], data[1]);
        #endregion
    }
    public void OnAllArrangement()
    {
        if (arrangement != null)
        {
            foreach (int key in arrangement.Keys)
            {
                List<Sculpture> list = arrangement[key];
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].gameObject.SetActive(true);
                }
            }
        }
    }
    public void OffAllArrangement()
    {
        if (arrangement != null)
        {
            foreach (int key in arrangement.Keys)
            {
                List<Sculpture> list = arrangement[key];
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].gameObject.SetActive(false);
                }
            }
        }
    }

    #endregion
    #region Sculpture event
    void SetSculptureEvent(Sculpture sculpture, Sculpture.OnClickEvent @event)
    {
        sculpture.onClickEvent = null;
        sculpture.onClickEvent += @event;
    }
    void InventoryEvent(Sculpture sculpture)
    {
        if (state == State.Normal)
        {
            OnArrangementSimulation(sculpture);
            sculpture.Image.color = Common.ColorHalfAlpha;
        }
        else
        {
            OffArrangementSimulation(sculpture);
            sculpture.Image.color = Color.white;
        }
        SetEventButtonActive(false);
    }

    /// <summary>
    /// 설치 가능한 위치 페어런트 변경하여 처리하기 ( 시뮬레이터가 우선적으로 보일수 있게 작업 )
    /// </summary>
    /// <param name="sculpture"></param>

    void ArrangementEvent(Sculpture sculpture)
    {
        if (state == State.Arrangement) return;

        SetEventButtonActive(true);
        SetEventButtonParent(sculpture.transform);
        ReSetButtonEvent();
        selected = sculpture;
        Sequence animation = DOTween.Sequence();
        #region Making Animation

        #region Making path
        float length = 110.0f;
        RectTransform rect = BuildingManager.instance.UiSet.ContentTrans;
        Vector3 direction = Vector3.right;

        float limit = rect.sizeDelta.x;
        float pos = sculpture.transform.localPosition.x + (sculpture.transform as RectTransform).sizeDelta.x * .5f + length + (withdraw.transform as RectTransform).sizeDelta.x;

#if USE_DEBUG
        Debug.Log(limit + " " + pos);
#endif

        if (limit <= pos)
        {
            direction = Vector3.left;
        }

        List<Vector3> path = new List<Vector3>();
        path.Add(length * Vector3.up);
        path.Add(length * (Vector3.up * 2.5F + direction));
        Vector3[] withdrawPath = path.ToArray();
        path.Add(length * (Vector3.up * 1.5f + direction));
        Vector3[] reArrangementPath = path.ToArray();
        path.Add(length * (Vector3.up * .5f + direction));
        Vector3[] exitPath = path.ToArray();

        animation.Append(withdraw.transform.DOLocalPath(withdrawPath, .4F));
        animation.Join(reArrangement.transform.DOLocalPath(reArrangementPath, .6F));
        animation.Join(exit.transform.DOLocalPath(exitPath, .8F));
        #endregion

        animation.OnStart(delegate
        {
            withdraw.interactable = false;
            reArrangement.interactable = false;
            exit.interactable = false;
        });
        animation.OnComplete(delegate
        {
            withdraw.interactable = true;
            reArrangement.interactable = true;
            exit.interactable = true;
        });
        #endregion
        animation.Play();
    }

    void SetEventButtonActive(bool active)
    {
        withdraw.gameObject.SetActive(active);
        reArrangement.gameObject.SetActive(active);
        exit.gameObject.SetActive(active);
    }
    void SetEventButtonParent(Transform parent)
    {
        withdraw.transform.SetParent(parent);
        reArrangement.transform.SetParent(parent);
        exit.transform.SetParent(parent);

        withdraw.transform.localPosition = Vector3.zero;
        reArrangement.transform.localPosition = Vector3.zero;
        exit.transform.localPosition = Vector3.zero;

        withdraw.transform.localScale = Vector3.one;
        reArrangement.transform.localScale = Vector3.one;
        exit.transform.localScale = Vector3.one;
    }
    void ReSetButtonEvent()
    {
        withdraw.onClick.RemoveAllListeners();
        reArrangement.onClick.RemoveAllListeners();
        exit.onClick.RemoveAllListeners();

        withdraw.onClick.AddListener(OnWithDraw);
        reArrangement.onClick.AddListener(OnReArrangement);
        exit.onClick.AddListener(OnExit);
    }
    #region Button event
    void OnWithDraw()
    {
        string[] datas = MakeServerPacket_WithDraw(selected);
        NetworkManager.instance.UpdateSculptureFromWithDraw(datas, CallbackWithDraw);
    }
    void CallbackWithDraw()
    {
        if (selected != null)
        {
            if (arrangement.ContainsKey(selected.ID))
            {
                List<Sculpture> sculptures = arrangement[selected.ID];

                // 리스트에서 제거
                sculptures.Remove(selected);

                // 인벤토리 삽입
                if (InsertSculpture(selected.ID))
                {
                    // 버튼 회수
                    SetEventButtonParent(this.transform);
                    SetEventButtonActive(false);
                    // 오브젝트 제거
                    Destroy(selected.gameObject);
                    selected = null;
                    SetInventoryContent(content);
                }
            }
            else
            {
#if USE_DEBUG
                Debug.LogError("Wrong data [ Sculpture object ] ID : " + selected.ID);
#endif
            }
        }
    }
    void OnReArrangement()
    {
        selected.gameObject.SetActive(false);
        if (arrangement.ContainsKey(selected.ID))
        {
            arrangement[selected.ID].Remove(selected);
            InventoryEvent(selected);
            OnExit();
        }
    }
    void OnExit()
    {
        SetEventButtonActive(false);
        SetEventButtonParent(this.transform);
    }
    #endregion

    #endregion

    #region Server Packet
    // 서버 패킷 제작 : 설치(설치물 추가,인벤토리 감소)
    public string[] MakeServerPacket_Arrangement(int id, Vector3 pos)
    {
        //= Arrangement data : id|x,y,z|id|x,y,z| - - - id|x,y,z
        string arrangmentPacket = "";

        //= Arrangement Make
        foreach (var key in arrangement.Keys)
        {
            for (int i = 0; i < arrangement[key].Count; i++)
            {
                arrangmentPacket += (key.ToString() + Common.splitWord);
                arrangmentPacket += arrangement[key][i].Position.x + "," + arrangement[key][i].Position.y + "," + arrangement[key][i].Position.z + Common.splitWord;
            }
        }
        // plus param
        pos = Sculpture.MakeVectorFormSculputre(packetPivot, pos);
        arrangmentPacket += (id.ToString() + Common.splitWord + pos.x + ',' + pos.y + ',' + pos.z);

#if USE_DEBUG
        Debug.Log("<color=cyan> Packet : " + arrangmentPacket + "</color>");
#endif

        //= inventory data : id,count|id,count|------|id,count
        string inventoryPacket = "";

        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].Key.ID == id)
            {
                if (0 < inventory[i].Value - 1) inventoryPacket += inventory[i].Key.ID.ToString() + "," + (inventory[i].Value - 1).ToString();
                else continue;
            }
            else
            {
                inventoryPacket += inventory[i].Key.ID.ToString() + "," + (inventory[i].Value.ToString());
            }

            if (i != inventory.Count - 1)
            {
                inventoryPacket += Common.splitWord;
            }
        }
        string[] revalue = new string[2];
        revalue[0] = arrangmentPacket;
        revalue[1] = inventoryPacket;

        return revalue;
    }

    // 서버 패킷 제작 : 회수(설치물 제거,인벤토리 증가)
    public string[] MakeServerPacket_WithDraw(Sculpture target)
    {
        string arrangmentPacket = "";

        //= Arrangement Make
        foreach (var key in arrangement.Keys)
        {
            for (int i = 0; i < arrangement[key].Count; i++)
            {
                if (arrangement[key][i] == target) continue;
                arrangmentPacket += (key.ToString() + Common.splitWord);
                arrangmentPacket += arrangement[key][i].Position.x + "," + arrangement[key][i].Position.y + "," + arrangement[key][i].Position.z + Common.splitWord;
            }
        }
        // Remove last splitword
        if (0 < arrangmentPacket.Length)
        {
            arrangmentPacket = arrangmentPacket.Remove(arrangmentPacket.Length - 1);
        }

        string inventoryPacket = MakeServerPacket_Inventory(target.ID);

        string[] revalue = new string[2];
        revalue[0] = arrangmentPacket;
        revalue[1] = inventoryPacket;

        return revalue;
    }

    // 서버 패킷 제작 : 구매(추가)
    public string MakeServerPacket_Inventory(int id)
    {
        // inventory data => id,count|id,count|------|id,count
        if (inventory != null)
        {
            string packet = "";
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].Key.ID == id)
                {
                    packet += inventory[i].Key.ID.ToString() + "," + (inventory[i].Value + 1).ToString();
                }
                else
                {
                    packet += inventory[i].Key.ID.ToString() + "," + inventory[i].Value.ToString();
                }
                if (i != inventory.Count - 1)
                {
                    packet += Common.splitWord;
                }
            }

            #region Find Item : inventory not contains [ id item ]
            bool findFlag = false;
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].Key.ID == id)
                {
                    findFlag = true;
                    break;
                }
            }
            if (findFlag == false)
            {
                if (packet == "") packet = id.ToString() + ',' + 1.ToString();
                else packet += Common.splitWord + id.ToString() + ',' + 1.ToString();
            }
            #endregion


            return packet;
        }
        else
        {
            return "";
        }
    }

    // 서버 패킷 제작 : 보유중
    public string[] MakeServerPacket_Current()
    {
        //= Arrangement data : id|x,y,z|id|x,y,z| - - - id|x,y,z
        string arrangmentPacket = "";

        if (arrangement != null && arrangement.Count != 0)
        {
            //= Arrangement Make
            foreach (var key in arrangement.Keys)
            {
                for (int i = 0; i < arrangement[key].Count; i++)
                {
                    arrangmentPacket += (key.ToString() + Common.splitWord);
                    arrangmentPacket += arrangement[key][i].Position.x + "," + arrangement[key][i].Position.y + "," + arrangement[key][i].Position.z + Common.splitWord;
                }
            }
            // Remove last splitword
            if (0 < arrangmentPacket.Length)
            {
                arrangmentPacket = arrangmentPacket.Remove(arrangmentPacket.Length - 1);
            }
        }

        //= inventory data : id,count|id,count|------|id,count
        string inventoryPacket = "";

        if (inventory != null)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                inventoryPacket += inventory[i].Key.ID.ToString() + "," + inventory[i].Value.ToString();

                if (i != inventory.Count - 1)
                {
                    inventoryPacket += Common.splitWord;
                }
            }
        }
        string[] revalue = new string[2];
        revalue[0] = arrangmentPacket;
        revalue[1] = inventoryPacket;

        return revalue;
    }

    #endregion
    bool CheckIfCanArrangement()
    {
        #region Variable declaration
        Vector2 localPos = cursor.transform.localPosition;
        Vector2 chaserPos = chaser.transform.localPosition;
        float w = cursor.rectTransform.rect.width;
        #endregion

        #region Check arrangement zone
        RectTransform arrangmentRectTrans = (arrangementZone.transform as RectTransform);
        Vector2 arrangmentWH = new Vector2(arrangmentRectTrans.rect.width, arrangmentRectTrans.rect.height);
        Vector2 arrangmentPos = new Vector2(arrangementZone.transform.localPosition.x - arrangmentWH.x * .5f, arrangementZone.transform.localPosition.y);
        Rect arrangmentRect = new Rect(arrangmentPos, arrangmentWH);
        if (arrangmentRect.Contains(localPos) == false) return false;
        #endregion
        #region Check Building

        if (BuildingManager.instance == null) return false;
        List<Building> buildings = BuildingManager.instance.UiSet.buildings;
        float chaserL = chaserPos.x - chaser.rectTransform.sizeDelta.x * .5f;
        float chaserR = chaserL + w;

        for (int i = 0; i < buildings.Count - 1; i++)
        {
            float buildingL = buildings[i].transform.localPosition.x - (buildings[i].transform as RectTransform).sizeDelta.x * .5f;
            float buildingR = buildingL + (buildings[i].transform as RectTransform).sizeDelta.x;

            if ((buildingL <= chaserR && chaserL < buildingL) ||    // 좌측 걸침
                (buildingR <= chaserR && chaserL < buildingR) ||    // 우측 걸침
                (buildingL <= chaserL && chaserR < buildingR))      // 내부 걸침
            {
#if USE_DEBUG
                //= True = [Building] ===Cursor===  [Building]
                Debug.Log("ID :" + i + " " + buildingL + " " + chaserR + " " + chaserL + " " + buildingR);
#endif
                return false;
            }
        }
        #endregion

        return true;
    }

    void OnCompleteArrangement(Sculpture selected, Image cursor)
    {
        state = State.Normal;
        mainScrollview.horizontal = true;

        cursor.color = Common.ColorAlpha;

        closeButton.interactable = true;
        selected.Image.color = Color.white;

        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].Key.ID == selected.ID)
            {
                int count = inventory[i].Value - 1;

                if (count <= 0)
                {
                    Destroy(selected.gameObject);
                    inventory.Remove(inventory[i]);
                    SetInventoryContent(content);
                }
                else
                {
                    selected.Image.color = Color.white;
                    selected.SetState(Sculpture.State.Inventory, count);
                    inventory[i] = new KeyValuePair<Sculpture, int>(selected, count);
                }
            }
        }

        offArragementButton.gameObject.SetActive(false);
    }

    void RefreshLevelUp(int openCount /* delegate dummy */)
    {
        Refresh();
    }

    void MovementArrangementMode()
    {
        float absPoisition = Mathf.Abs(buildingContent.transform.localPosition.x);
        float speed = 150.0f;

        if(moveChaser.localPosition.x <= leftZone.localPosition.x)
        {
            buildingContent.transform.Translate(Vector3.right * speed * Time.deltaTime);
            if (0 <= buildingContent.transform.localPosition.x)
            {
                buildingContent.transform.localPosition = new Vector3(0, buildingContent.transform.localPosition.y);
            }
        }
        else if(rightZone.localPosition.x <= moveChaser.localPosition.x)
        {
            buildingContent.transform.Translate(Vector3.left * speed * Time.deltaTime);
            if (buildingContent.rect.width <= Mathf.Abs(buildingContent.transform.localPosition.x) + screenW)
            {
                buildingContent.transform.localPosition = new Vector3(-(buildingContent.rect.width - screenW), buildingContent.transform.localPosition.y);
            }
        }

        //= 해상도 대응 문제때문에 변경
        //if (cursor.transform.localPosition.x <= leftline)
        //{
        //    buildingContent.transform.Translate(Vector3.right * speed * Time.deltaTime);
        //    if (0 <= buildingContent.transform.localPosition.x)
        //    {
        //        buildingContent.transform.localPosition = new Vector3(0, buildingContent.transform.localPosition.y);
        //    }
        //}
        //else if (rightline <= cursor.transform.localPosition.x)
        //{
        //    buildingContent.transform.Translate(Vector3.left * speed * Time.deltaTime);
        //    if (buildingContent.rect.width <= Mathf.Abs(buildingContent.transform.localPosition.x) + screenW)
        //    {
        //        buildingContent.transform.localPosition = new Vector3(-(buildingContent.rect.width - screenW), buildingContent.transform.localPosition.y);
        //    }
        //}

#if USE_DEBUG
        Debug.Log("Local : " + buildingContent.transform.localPosition + " " + buildingContent.rect.width + " " + cursor.transform.localPosition);
        Debug.Log("World : " + buildingContent.transform.position + " " + buildingContent.rect.width + " " + cursor.transform.position);
#endif
    }

    private void Update()
    {
        if (state == State.Arrangement)
        {
            Vector2 movePos;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                canvas.transform as RectTransform,
                Input.mousePosition, canvas.worldCamera,
                out movePos);

            cursor.transform.position = canvas.transform.TransformPoint(movePos);
            chaser.transform.position = canvas.transform.TransformPoint(movePos);
            moveChaser.transform.position = canvas.transform.TransformPoint(movePos);
        }

        if (Input.GetMouseButtonUp(0 /* left click */) && state == State.Arrangement)
        {
            if (CheckIfCanArrangement())
            {
                string[] packet = MakeServerPacket_Arrangement(selected.ID, cursor.transform.position);
#if USE_DEBUG
                Debug.Log(packet[0] + '\n' + packet[1]);
#endif
                NetworkManager.instance.UpdateSculptureFromInstall(packet, SuccessInstall, cursor.transform.position);
            }
        }
        else if (state == State.Arrangement)
        {
            //= 설치 가능 연산
            if (CheckIfCanArrangement())
            {
                cursorGreen.gameObject.SetActive(true);
                cursorRed.gameObject.SetActive(false);
            }
            else
            {
                cursorGreen.gameObject.SetActive(false);
                cursorRed.gameObject.SetActive(true);
            }

            //= 이동 연산
            MovementArrangementMode();
        }
    }

    void SuccessInstall(Vector3 position)
    {
        if (ArrangementSculpture(selected.ID, position))
        {
            OnCompleteArrangement(selected, cursor);
        }
    }
}
