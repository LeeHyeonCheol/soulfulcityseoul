﻿using UnityEngine;
using System.Collections;

using PlayFab.Internal;

namespace HoneyPlug.Audio
{
    [System.Serializable]
    public class SoundClip
    {
        [SerializeField] private AudioClip audioClip = null;
        public AudioClip AudioClip { get { return audioClip; } }
    }

    public class SoundManager : SingletonMonoBehaviour<SoundManager>
    {
        private const string Sound_PPK = "SOUND_KEY";
        private const string Music_PPK = "MUSIC_KEY";

        [Header("Audio Source References")]
        [SerializeField] private AudioSource soundSource = null;
        [SerializeField] private AudioSource musicSource = null;

        [Header("Audio Clips")]
        public SoundClip background_home = null;
        public SoundClip background_town = null;
        public SoundClip button = null;
        public SoundClip rewarded = null;
        public SoundClip buildup = null;
        public SoundClip rouletteSfx = null;
        public SoundClip rewardSfx = null;
        public SoundClip cannonShot = null;
        public SoundClip bulletHit = null;
        public SoundClip minigame = null;
        public SoundClip miniRewardSfx = null;
        public SoundClip touchBalloon = null;
        public SoundClip increaseTime = null;
        public SoundClip trap = null;
        public SoundClip buildStart = null;
        public SoundClip buildEnd = null;


        public float soundVolume;
        public float musicVolume;

        private void Start()
        {
            if (!PlayerPrefs.HasKey(Music_PPK))
                PlayerPrefs.SetInt(Music_PPK, 1);
            if (!PlayerPrefs.HasKey(Sound_PPK))
                PlayerPrefs.SetInt(Sound_PPK, 1);

            musicVolume = PlayerPrefs.GetFloat("MusicVol", 1);
            musicSource.volume = musicVolume;
            
            soundVolume = PlayerPrefs.GetFloat("SndVol", 1);
            soundSource.volume = soundVolume;

            musicSource.mute = IsMusicOff(); soundSource.mute = IsSoundOff();
        }

        public bool IsSoundOff()
        {
            return (PlayerPrefs.GetInt(Sound_PPK, 1) == 0);
        }

        public bool IsMusicOff()
        {
            return (PlayerPrefs.GetInt(Music_PPK, 1) == 0);
        }

        public bool IsMusicPlaying()
        {
            return musicSource.isPlaying;
        }

        public void PlayOneSound(SoundClip clip)
        {
            soundSource.PlayOneShot(clip.AudioClip);
        }

        public void PlayMusic(SoundClip clip, float volumeUpTime)
        {
            if(musicSource == null) return;
            
            if (!IsMusicOff()) //Music is on
            {
                musicSource.clip = clip.AudioClip;
                musicSource.loop = true;
                musicSource.volume = 0;
                musicSource.Play();
                StartCoroutine(CRVolumeUp(musicVolume));
            }
        }

        public void SetMusicVolume(float vol)
        {
            musicSource.volume = vol;
            PlayerPrefs.SetFloat("MusicVol", vol);
        }

        public void SetSFXVolume(float vol)
        {
            soundSource.volume = vol;
            PlayerPrefs.SetFloat("SndVol", vol);
        }

        /// <summary>
        /// Pauses the music.
        /// </summary>
        public void PauseMusic()
        {
            musicSource.Pause();
        }

        /// <summary>
        /// Resumes the music.
        /// </summary>
        public void ResumeMusic()
        {
            musicSource.UnPause();
        }

        /// <summary>
        /// Stop music.
        /// </summary>
        public void StopMusic(float volumeDownTime)
        {
            musicSource.Stop();
            StartCoroutine(CRVolumeDown(volumeDownTime));
        }


        /// <summary>
        /// Toggles the mute status.
        /// </summary>
        public void ToggleSound()
        {
            if (IsSoundOff())
            {
                //Turn the sound on
                PlayerPrefs.SetInt(Sound_PPK, 1);
                soundSource.mute = false;
            }
            else
            {
                //Turn the sound off
                PlayerPrefs.SetInt(Sound_PPK, 0);
                soundSource.mute = true;
            }
        }

        public void OnSound(bool mute)
        {
            if (mute == false)
            {
                if(soundSource.mute == false) return;
                //Turn the sound on
                PlayerPrefs.SetInt(Sound_PPK, 1);
                soundSource.mute = false;
            }
            else
            {
                if(soundSource.mute == true) return;
                //Turn the sound off
                PlayerPrefs.SetInt(Sound_PPK, 0);
                soundSource.mute = true;
            }
        }

        /// <summary>
        /// Toggles the mute status.
        /// </summary>
        public void ToggleMusic()
        {
            if (IsMusicOff())
            {
                //Turn the music on
                PlayerPrefs.SetInt(Music_PPK, 1);
                musicSource.mute = false;
            }
            else
            {
                //Turn the music off
                PlayerPrefs.SetInt(Music_PPK, 0);
                musicSource.mute = true;
            }
        }

        public void OnMusic(bool mute)
        {
            if (mute == false)
            {
                if(musicSource.mute == false) return;
                //Turn the music on
                PlayerPrefs.SetInt(Music_PPK, 1);
                musicSource.mute = false;
            }
            else
            {
                if(musicSource.mute == true) return;
                //Turn the music off
                PlayerPrefs.SetInt(Music_PPK, 0);
                musicSource.mute = true;
            }
        }

        private IEnumerator CRVolumeUp(float time)
        {
            float t = 0;
            while (t < time)
            {
                t += Time.deltaTime;
                float factor = t / time;
                musicSource.volume = Mathf.Lerp(0, 0.3f, factor);
                yield return null;
            }
        }

        private IEnumerator CRVolumeDown(float time)
        {
            float t = 0;
            float currentVolume = musicSource.volume;
            while (t < time)
            {
                t += Time.deltaTime;
                float factor = t / time;
                musicSource.volume = Mathf.Lerp(currentVolume, 0, factor);
                yield return null;
            }
        }
    }
}
