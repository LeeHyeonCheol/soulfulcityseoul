﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class PopupManager : MonoBehaviour
{
    public static PopupManager instance;

    [SerializeField] PopupUI popupPrefabs = null;
    [SerializeField] PopupUI adPopupPrefabs = null;
    [SerializeField, ReadOnly] List<PopupUI> popups = null;
    [SerializeField, ReadOnly] Queue<PopupUI> pool = null;
    [SerializeField] RectTransform parent;
    [SerializeField] float duration = 0.3f;
    [SerializeField, ReadOnly] bool initFlag;

    [SerializeField] PopupUI nowPopupUI;

    [SerializeField, ReadOnly] Vector2 startPos = Vector2.zero;
    [SerializeField, ReadOnly] Vector2 endPos = Vector2.zero;


    public PopupUI NowPopupUI { get => nowPopupUI; }

    Vector2 center;
    Vector2 left;
    Vector2 right;
    Vector2 up;
    Vector2 down;

    public enum ButtonStyle
    {
        Ok,
        Cancel,
        OkCancel,
        YesNo,
        Yes,
        AD
    }

    public enum MoveType
    {
        None,
        DownToCenter,       // 하단에서 중앙으로 종료시 중앙에서 하단으로 [ ↑ center ↓ ]
        UpToCenter,         // 상단에서 중앙으로 종료시 중앙에서 상단으로 [ ↓ center ↑ ]
        HorizontalUp,       // 하단에서 중앙으로 종료시 중앙에서 상단으로 [ ↑ center ↑ ]
        HorizontalDown,     // 상단에서 중앙으로 종료시 중앙에서 하단으로 [ ↓ center ↓ ]
        LeftToCenter,       // 좌측에서 중앙으로 종료시 중앙에서 좌측으로 [ → center ← ]
        RightToCenter,      // 우측에서 중앙으로 종료시 중앙에서 우측으로 [ ← center → ]
        VerticalRight,      // 좌측에서 중앙으로 종료시 중앙에서 우측으로 [ → center → ]
        VerticalLeft,       // 우측에서 중앙으로 종료시 중앙에서 좌측으로 [ ← center ← ]
        CenterPumping       // 가운데에서 스케일 업
    }

    void Start()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
#if UNITY_EDITOR
            Debug.LogWarning("There is more than one data manager.");
#endif
            Destroy(this.gameObject);
        }
        popupPrefabs.Start();
        center = new Vector2(0, popupPrefabs.transform.rect.height * 0.5f);
        left = new Vector2(-Screen.width * .5f - popupPrefabs.transform.rect.width, 0);
        right = new Vector2(Screen.width * .5f + popupPrefabs.transform.rect.width, 0);
        up = new Vector2(0, Screen.height + popupPrefabs.transform.rect.height);
        down = new Vector2(0, -Screen.height - popupPrefabs.transform.rect.height);

        popups = new List<PopupUI>();
        pool = new Queue<PopupUI>();
    }

    public void Open(uint kindcode,string textCode, ButtonStyle style, MoveType moveType, UnityAction mainEvent, UnityAction subEvent = null,bool netualText = false,float yOffset = 0)
    {
        if (popups.Find(v => v.KindCode == kindcode) != null)
        {
#if USE_DEBUG
            Debug.Log("이미 동일한 종류의 팝업창이 출력중입니다.\nCode : " + kindcode);
#endif
            return;
        }
        // Button Event Set
        PopupUI ui = null;
        if (style != ButtonStyle.AD)
        {
            if (pool.Count == 0) ui = Instantiate(popupPrefabs);
            else
            {
                ui = pool.Dequeue();
                ui.gameObject.SetActive(true);
            }
            popups.Add(ui);
            ui.isAD = false;
        }
        else // AD Popup
        {
            ui = Instantiate(adPopupPrefabs);
            ui.isAD = true;
        }

        initFlag = false;
        ui.Start();
        // Data Set
        ui.KindCode = kindcode;
        StartCoroutine(ButtonSet(ui, style, mainEvent, subEvent));
        // Animation Set
        AnimationSetting(moveType,yOffset);
        StartCoroutine(Run(ui,moveType));
        // NOTE : Notice text Setting
        // TODO : JSON Text code사용하여 적용시키
        ArabicText arabicText = ui.Notice.GetComponent<ArabicText>();
        if(LanguageManager.instance.IsArabic() == false)
        {
            arabicText.enabled = false;
            ui.Notice.alignment = TextAnchor.UpperCenter;
        }
        else
        {
            arabicText.enabled = true;
            ui.Notice.alignment = TextAnchor.UpperRight;
        }
        if (netualText)
        {
            if(LanguageManager.instance.IsArabic() == false)
                ui.Notice.text = textCode;
            else
            {
                arabicText.Text = textCode;
            }
        }
        else
        {
            string content = LanguageManager.instance.GetString(textCode);
            if(LanguageManager.instance.IsArabic() == false)
                ui.Notice.text = content;
            else
            {
                arabicText.Text = content;
            }
        }
        nowPopupUI = ui;
    }
    public void Close(PopupUI popup)
    {
        if (pool == null) pool = new Queue<PopupUI>();

        pool.Enqueue(popup);

    }
    #region Button Set
    IEnumerator ButtonSet(PopupUI ui,ButtonStyle style, UnityAction mainEvent, UnityAction subEvent)
    {
        // 활성화 대기
        yield return new WaitUntil(()=> ui.gameObject.activeSelf );
        #region 버튼 기능 및 위치 셋팅
        switch (style)
        {
            case ButtonStyle.Yes:
                OneButtonSet(ui, LanguageManager.instance.GetString("TCode_B1"), mainEvent);
                break;
            case ButtonStyle.Cancel:
                OneButtonSet(ui, LanguageManager.instance.GetString("TCode_B4"), mainEvent);
                break;
            case ButtonStyle.Ok:
                OneButtonSet(ui, LanguageManager.instance.GetString("TCode_B3"), mainEvent);
                break;
            case ButtonStyle.YesNo:
                TwoButtonSet(ui, LanguageManager.instance.GetString("TCode_B1"), LanguageManager.instance.GetString("TCode_B2"), mainEvent, subEvent);
                break;
            case ButtonStyle.OkCancel:
                TwoButtonSet(ui, LanguageManager.instance.GetString("TCode_B3"), LanguageManager.instance.GetString("TCode_B4"), mainEvent, subEvent);
                break;
            case ButtonStyle.AD:
                ADButtonSet(ui, LanguageManager.instance.GetString("TCode_B3"), mainEvent, subEvent);
                break;
            default:
#if USE_DEBUG
                Debug.LogWarning("Wrong type error : button style in popup");
#endif
                break;
        }

        initFlag = true;


        #endregion
    }
    #region 버튼 이벤트 및 이름 셋팅
    void OneButtonSet(PopupUI ui,string main,UnityAction mainEvent)
    {
        ui.transform.SetParent(parent);
        #region 오브젝트 활성화 및 비활성화
        ui.MainButton.gameObject.SetActive(true);
        ui.MainButtonText.gameObject.SetActive(true);
        ui.SubButton.gameObject.SetActive(false);
        ui.SubButtonText.gameObject.SetActive(false);
        #endregion
        #region 버튼 포지션 셋팅
        ui.MainButton.transform.localPosition = ui.CenterPos.localPosition;
        #endregion
        #region 버튼 이벤트 셋팅
        ui.MainButton.onClick.RemoveAllListeners();
        if(mainEvent != null)
        {
            ui.MainButton.onClick.AddListener(mainEvent);
        }
        else
        {
            ui.MainButton.onClick.AddListener(NullEvent);
        }

        ui.MainButton.onClick.AddListener(ui.Close);
        #endregion

        ui.MainButtonText.text = main;
    }
    void TwoButtonSet(PopupUI ui,string main,string sub,UnityAction mainEvent,UnityAction subEvent)
    {
        ui.transform.SetParent(parent);
        #region 오브젝트 활성화 
        ui.MainButton.gameObject.SetActive(true);
        ui.SubButton.gameObject.SetActive(true);
        ui.MainButtonText.gameObject.SetActive(true);
        ui.SubButtonText.gameObject.SetActive(true);
        #endregion
        #region 버튼 포지션 셋팅
        ui.MainButton.transform.localPosition = ui.LeftPos.localPosition;
        ui.SubButton.transform.localPosition = ui.RightPos.localPosition;
        #endregion
        #region 버튼 이벤트 셋팅
        ui.MainButton.onClick.RemoveAllListeners();
        ui.SubButton.onClick.RemoveAllListeners();
        if(mainEvent != null)
        {
            ui.MainButton.onClick.AddListener(mainEvent);
        }
        else
        {
            ui.MainButton.onClick.AddListener(NullEvent);
        }
        if(subEvent != null)
        {
            ui.SubButton.onClick.AddListener(subEvent);
        }
        else
        {
            ui.SubButton.onClick.AddListener(NullEvent);
        }
        ui.MainButton.onClick.AddListener(ui.Close);
        ui.SubButton.onClick.AddListener(ui.Close);
        #endregion

        ui.MainButtonText.text = main;
        ui.SubButtonText.text = sub;
    }

    // 구조가 다릅니다.
    void ADButtonSet(PopupUI ui,string main,UnityAction successEvent,UnityAction FailEvent)
    {
        ui.transform.SetParent(parent);
        #region 오브젝트 활성화 
        ui.MainButton.gameObject.SetActive(true);
        ui.SubButton.gameObject.SetActive(true);
        ui.MainButtonText.gameObject.SetActive(true);

        // 텍스트 비활성화 ( 종료 버튼으로 사용 )
        ui.SubButtonText.gameObject.SetActive(false);
        #endregion
        #region 버튼 포지션 셋팅
        ui.MainButton.transform.localPosition = ui.LeftPos.localPosition;
        ui.SubButton.transform.localPosition = ui.RightPos.localPosition;
        #endregion
        #region 버튼 이벤트 셋팅
        ui.MainButton.onClick.RemoveAllListeners();
        ui.SubButton.onClick.RemoveAllListeners();

        //= close 선처리
        ui.MainButton.onClick.AddListener(ui.Close);
        ui.SubButton.onClick.AddListener(ui.Close);

        //= 광고 호출
        ui.MainButton.onClick.AddListener(delegate
        {
            //= 광고를 모두 시청하고 종료함.
            if (successEvent != null)
            {
                ADMobManager.instance.callbackCloseEarendAD += delegate { successEvent.Invoke(); }; 
            }
#if USE_DEBUG
            else
            {

                Debug.LogError("<color=cyan>AD Error :</color> NullReferenceException : SuccessEvent paramter in ADButton Set");

            }
#endif

            //= 광고를 모두 시청하지 않고 종료함.
            if (FailEvent != null)
            {
                ADMobManager.instance.callbackADClose += delegate { FailEvent.Invoke(); };  
            }
#if USE_DEBUG
            else
            {

                Debug.LogError("<color=cyan>AD Error :</color> NullReferenceException : FailEvent paramter in ADButton Set");

            }
#endif
            ADMobManager.instance.OpenRewardAD();
        });

        ui.SubButton.onClick.AddListener(FailEvent);
        
        #endregion

        ui.MainButtonText.text = main;

    }
    void NullEvent(){}
    #endregion
    #endregion
    #region 팝업창 애니메이션 셋팅
    void AnimationSetting(MoveType style,float yOffset)
    {
        Vector2 down = new Vector2(this.down.x, this.down.y + yOffset);
        Vector2 up = new Vector2(this.up.x, this.up.y + yOffset);
        Vector2 left = new Vector2(this.left.x, this.left.y + yOffset);
        Vector2 right = new Vector2(this.right.x, this.right.y + yOffset);
        Vector2 center = new Vector2(this.center.x, this.center.y + yOffset);

        switch(style)
        {
            case MoveType.DownToCenter:
                startPos = down;
                endPos = down;
                break;
            case MoveType.UpToCenter:
                startPos = up;
                endPos = up;
                break;
            case MoveType.HorizontalDown:
                startPos = up;
                endPos = down;
                break;
            case MoveType.HorizontalUp:
                startPos = down;
                endPos = up;
                break;
            case MoveType.LeftToCenter:
                startPos = left;
                endPos = left;
                break;
            case MoveType.RightToCenter:
                startPos = right;
                endPos = right;
                break;
            case MoveType.VerticalLeft:
                startPos = right;
                endPos = left;
                break;
            case MoveType.VerticalRight:
                startPos = left;
                endPos = right;
                break;
            case MoveType.CenterPumping:
            case MoveType.None:
                startPos = center;
                endPos = center;
                break;
        }
    }

    // NOTE : 애니메이션 추가가능성을 보고, 함수화.
    Sequence Animation(PopupUI ui,MoveType moveType,bool isStart)
    {
        Sequence move = DOTween.Sequence();

        if(isStart)
        {
            switch(moveType)
            {
                case MoveType.DownToCenter:
                case MoveType.HorizontalDown:
                case MoveType.HorizontalUp:
                case MoveType.LeftToCenter:
                case MoveType.RightToCenter:
                case MoveType.UpToCenter:
                case MoveType.VerticalLeft:
                case MoveType.VerticalRight:
                    move = StartBack(ui);
                    break;
                case MoveType.CenterPumping:
                    move = StartPumping(ui);
                    break;
                case MoveType.None:
                    move.OnStart(() => ui.transform.localPosition = startPos);
                    break;
            }
        }
        else
        {
            switch (moveType)
            {
                case MoveType.DownToCenter:
                case MoveType.HorizontalDown:
                case MoveType.HorizontalUp:
                case MoveType.LeftToCenter:
                case MoveType.RightToCenter:
                case MoveType.UpToCenter:
                case MoveType.VerticalLeft:
                case MoveType.VerticalRight:
                    move = EndBack(ui);
                    break;
                case MoveType.CenterPumping:
                    move = EndPumping(ui);
                    break;
                case MoveType.None:
                    move.OnStart(() => ui.transform.localPosition = startPos);
                    break;
            }

            move.OnComplete(() =>
            {
                ui.LockClick.SetActive(false);
                ui.gameObject.SetActive(false);
                if (ui.isAD == false)
                {
                    popups.Remove(ui);
                    Close(ui);
                }
            });
        }
        return move;
    }

    #region Tween Animation List
    Sequence StartBack(PopupUI ui)
    {
        Sequence move = DOTween.Sequence();
        move.OnStart(() =>
        {
            ui.transform.localPosition = startPos;
            ui.transform.localScale = Vector2.one;
            ui.LockClick.SetActive(true);
        });
        move.Append(ui.transform.DOLocalMove(center, duration)
            .SetEase(Ease.OutBack));
        return move;
    }
    Sequence EndBack(PopupUI ui)
    {
        Sequence move = DOTween.Sequence();
        move.Append(ui.transform.DOLocalMove(endPos, duration)
            .SetEase(Ease.InBack));
        return move;
    }
    Sequence StartPumping(PopupUI ui)
    {
        Sequence move = DOTween.Sequence();
        move.OnStart(() =>
        {
            ui.transform.localPosition = startPos;
            ui.transform.localScale = Vector2.zero;
            ui.LockClick.SetActive(true);
        });
        move.Append(ui.transform.DOScale(Vector2.one, duration).
            SetEase(Ease.OutBack));
        return move;
    }
    Sequence EndPumping(PopupUI ui)
    {
        Sequence move = DOTween.Sequence();
        move.Append(ui.transform.DOScale(Vector2.zero, duration)
            .SetEase(Ease.InBack));
        return move;
    }
    #endregion

    IEnumerator Run(PopupUI ui,MoveType moveType)
    {
        yield return new WaitUntil(()=> initFlag && ui.gameObject.activeSelf);

        ui.Run(Animation(ui, moveType, true),Animation(ui, moveType, false));
    }
    #endregion
}
