﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using HoneyPlug.Audio;
using PlayFab.Internal;
using UnityEngine;

using GoogleMobileAds.Api.Mediation.AdColony;
using GoogleMobileAds.Api.Mediation.AppLovin;
using GoogleMobileAds.Api.Mediation.InMobi;
using GoogleMobileAds.Api.Mediation.MoPub;

public class ADMobManager : SingletonMonoBehaviour<ADMobManager>
{
    #region AD parameter
    private BannerView bannerView;
    private RewardedAd rewardAD;
    private InterstitialAd closeInter;
    #endregion

    private bool isOpenStateRewardAD;
    private bool isEndHandler;
    private bool isCloseHandler;

// #if UNITY_ANDROID
//     string appId = "ca-app-pub-1398300790196247~3449431862";
// #elif UNITY_IPHONE
//     string appId = "ca-app-pub-1398300790196247~3741059992";
// #else
//     string appId = "unexpected_platform";
// #endif

    [System.Obsolete]
    private void Start()
    {
        this.Init();
    }

    [System.Obsolete]
    void Init()
    {
        // Initialize the Mobile Ads SDK.
        MobileAds.Initialize((initStatus) =>
        {
            Dictionary<string, AdapterStatus> map = initStatus.getAdapterStatusMap();
            foreach (KeyValuePair<string, AdapterStatus> keyValuePair in map)
            {
                string className = keyValuePair.Key;
                AdapterStatus status = keyValuePair.Value;
                switch (status.InitializationState)
                {
                case AdapterState.NotReady:
                    // The adapter initialization did not complete.
                    print("Adapter: " + className + " not ready.");
                    break;
                case AdapterState.Ready:
                    // The adapter was successfully initialized.
                    print("Adapter: " + className + " is initialized.");
                    break;
                }
            }
        });
        //MobileAds.Initialize(appId);
        //AdColonyAppOptions.SetTestMode(true);


//         AppLovin.Initialize();
// #if UNITY_ANDROID
//         MoPub.Initialize("64bf5c9bfd2940bf9147575d97028cd4");
//         MoPub.Initialize("09fcf7a47a32419d989616461f6de4f6");
// #elif UNITY_IPHONE
//         MoPub.Initialize("74af1ecd4146442081b530f3537588f5");
//         MoPub.Initialize("ee2dad914ea842dead4274d1404ad836");
// #endif
        OpenBanner();
        LoadReward();
        CloseInterstitial();

        isOpenStateRewardAD = false;
    }

    // 배너 광고 코
    void OpenBanner()
    {
        string adUnitld = null;
#region    TEST ============================================
#if UNITY_ANDROID
        adUnitld = "ca-app-pub-3940256099942544/6300978111";
#elif UNITY_IPHONE
        adUnitld = "ca-app-pub-1398300790196247/5217793196";
#else
        adUnitld = "unexpected_platform";
#endif
#endregion TEST ============================================
        
// #if UNITY_ANDROID
//         adUnitld = "ca-app-pub-1398300790196247/8418671608";
// #elif UNITY_IPHONE
//         adUnitld = "ca-app-pub-1398300790196247/5217793196";
// #else
//         adUnitld = "unexpected_platform";
// #endif

        Debug.Log("<color=cyan> Banner Creating </color>");

        bannerView = new BannerView(adUnitld, AdSize.Banner, AdPosition.Bottom);

        // Set ad request parameters
        // AdColonyMediationExtras extras = new AdColonyMediationExtras();
        // extras.SetShowPrePopup(true);
        // extras.SetShowPostPopup(true);

        AdRequest request = new AdRequest.Builder().Build();

        bannerView.OnAdOpening      += delegate { Debug.Log("<color=cyan> Banner Opening </color>");  };
        bannerView.OnAdFailedToLoad += delegate { Debug.Log("<color=cyan> Fail Load </color>");       };
        bannerView.OnAdLoaded       += delegate { Debug.Log("<color=cyan> Load ad </color>");         };
	    

        bannerView.LoadAd(request);
    }

    #region Reward reference
    // 보상형 광고 로드
    void LoadReward()
    {
        string adrUnitld = null;
#region    TEST ============================================
#if UNITY_ANDROID
        adrUnitld = "ca-app-pub-3940256099942544/5224354917";
#elif UNITY_IPHONE
        adrUnitld = "ca-app-pub-1398300790196247/5217793196";
#else
        adrUnitld = "unexpected_platform";
#endif
#endregion TEST ============================================

        
// #if UNITY_ANDROID
//         adrUnitld = "ca-app-pub-1398300790196247/1661691563";
// #elif UNITY_IPHONE
//         adrUnitld = "ca-app-pub-1398300790196247/7652384848";
// #else
//         adrUnitld = "unexpected_platform";
// #endif

        rewardAD = new RewardedAd(adrUnitld);

        rewardAD.OnUserEarnedReward += HandleUserEarendReward;
        rewardAD.OnAdClosed += HandleRewardedADClose;

        // Set ad request parameters
        // AdColonyMediationExtras extras = new AdColonyMediationExtras();
        // extras.SetShowPrePopup(true);
        // extras.SetShowPostPopup(true);

        AdRequest request = new AdRequest.Builder().Build();

        rewardAD.LoadAd(request);
    }

    // 광고 오픈
    public void OpenRewardAD()
    {
        if (rewardAD.IsLoaded() && isOpenStateRewardAD == false)
        {
            rewardAD.Show();
            isOpenStateRewardAD = true;

            //= 사운드 제어 ( 일시정지 ) 
            SoundManager.instance.PauseMusic();
        }
    }

    // 광고창 종료시 ( 강제종료 포함 ) 발생 함수
    public delegate void CallbackADClose();
    public CallbackADClose callbackADClose;
    void HandleRewardedADClose(object sender, EventArgs args)
    {
        LoadReward();
        isOpenStateRewardAD = false;
        isCloseHandler = true;

        //= 사운드 제어 ( 재생 )
        SoundManager.instance.ResumeMusic();

        StartCoroutine(RewardADCheck());
    }

    // 광고 시청 완료후 보상관련 콜백 부분
    public delegate void CallbackEarendRewardAD();
    public CallbackEarendRewardAD callbackCloseEarendAD;
    void HandleUserEarendReward(object sender,Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        isEndHandler = true;
    }

    IEnumerator RewardADCheck()
    {
        yield return new WaitUntil(()=> isEndHandler || isCloseHandler);
        yield return null;
        if(isEndHandler)
        {
            callbackCloseEarendAD?.Invoke();
            callbackCloseEarendAD = null;
        }
        else
        {
            callbackADClose?.Invoke();
            callbackADClose = null;
        }

        isEndHandler = false;
        isCloseHandler = false;
    }

   
    #endregion
    #region Close ad reference
    private void CloseInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/1033173712";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
        string adUnitId = "unexpected_platform";
#endif
        closeInter = new InterstitialAd(adUnitId);

        AdRequest request = new AdRequest.Builder().Build();

        closeInter.LoadAd(request);
    }
    public void OpenCloseInterstitial()
    {
        if(closeInter.IsLoaded())
        {
            closeInter.Show();
        }
    }
    #endregion

    // Test Code
    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {

           
        }
    }
}
