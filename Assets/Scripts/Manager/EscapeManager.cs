﻿using System.Collections;
using System.Collections.Generic;
using PlayFab.Internal;
using UnityEngine;

public class EscapeManager : SingletonMonoBehaviour<EscapeManager>
{
    enum State
    {
        ObjectDestory,  //= UI등 오브젝트 파괴
        Normal,         //= 씬 이동, 게임 종료
        Disable
    }

    [Header("Escape Info")]
    [SerializeField, ReadOnly] List<GameObject> actives;
    [SerializeField, ReadOnly] State state = State.Normal;

    private void Start()
    {
        Init();
    }
    void Init()
    {
        actives = new List<GameObject>();
        actives.Clear();
    }

    #region Exteral function
    public void SetObjectDestoryAuto(GameObject obj)
    {
        actives.Add(obj);
        state = State.ObjectDestory;

        DestroyEvent _event = obj.GetComponent<DestroyEvent>();
        if (_event == null)
        {
            _event = obj.AddComponent<DestroyEvent>();
        }
        _event.AddEvent( delegate { RemoveActives(obj); });

#if USE_DEBUG
        Debug.Log("<color=yellow> Name : " + obj + "</color>");
#endif
    }
    public void UnableObjectDestory()
    {
        actives.Clear();
        state = State.Normal;
    }
    public void SetDisable()
    {
        actives.Clear();
        state = State.Disable;
    }
    public void SetNormal()
    {
        actives.Clear();
        state = State.Normal;
    }
    public void RemoveActives(GameObject obj)
    {
        GameObject result = actives.Find((GameObject _obj) => _obj == obj);

        //= 활성화 중인 오브젝트가 0개 ( 모두 비활성화 ) 되어있을 경우
        if (result != null)
        {
            actives.Remove(result);
            if (actives.Count == 0)
            {
                SetNormal();
            }
        }

#if USE_DEBUG
        else
        {
            Debug.LogError("Error : Not find destroy object");
        }
#endif
    }
    public void DestroyObject()
    {
        if(actives.Count != 0)
        {
            GameObject peek = actives[actives.Count-1];
            Destroy(peek);
        }
        else
        {
            SetNormal();
        }
    }

    #endregion
    void Update()
    {
        //= Escape 처리
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //= 씬 이동, 게임종료 처리
            if(state == State.Normal && SceneLoader.instance != null)
            {
                if(SceneLoader.instance.EndLoad())
                {
                    if(BuildingManager.instance != null &&
                        SceneLoader.instance.GetCurrentScene() == Common.BUDSceneName &&
                        BuildingManager.instance.IsUser &&
                        BuildingManager.instance.UiSet.NpcManager.StartGame.IsPlaying == false)
                    {
                        //= 룰렛씬으로 이동
                        BuildingManager.instance.PrevScene();
                    }
                    else if(SceneLoader.instance.GetCurrentScene() == Common.RULSceneName)
                    {
                        //= 종료 구현
                        PopupManager.instance.Open(Common.PopupCode.ExitPopup,
                            "EXIT_GAME",
                            PopupManager.ButtonStyle.YesNo,
                            PopupManager.MoveType.CenterPumping,
                            delegate
                            {
                                Application.Quit();
                            });
                    }
                }
            }
            else if(state == State.ObjectDestory && actives != null)
            {
                DestroyObject();
            }
        }
    }
}
