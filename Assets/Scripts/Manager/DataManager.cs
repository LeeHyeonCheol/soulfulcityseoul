﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager instance;

    #region Roulette Packing
    [System.Serializable]
    public class RoulettePack
    {
        public int id;
        public string reward;
    }
    #endregion
    #region Quiz table packing
    [System.Serializable]
    public class QuizTable
    {
        public enum Answer { O, X };
        public string textCode;
        public string answer;
    }
    #endregion


    [SerializeField] TextAsset building = null;
    [SerializeField] TextAsset sculpture = null;
    [SerializeField] TextAsset roulette = null;
    [SerializeField] TextAsset quiz = null;
    public BuildingData[] buildingData = null;
    public ShopItem[] sculptureData = null;
    public RoulettePack[] rouletteData = null;
    public QuizTable[] quiztable = null;

    [SerializeField] TextAsset workman = null;
    public Dictionary<int /* id */, List<Workman.Infomation>> workmanData;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
#if UNITY_EDITOR || USE_DEBUG
            Debug.LogWarning("There is more than one data manager.");
#endif
            Destroy(this.gameObject);
        }

        Load();
    }

    void Load()
    {
        #region Building Data
        buildingData = JsonHelper.FromJson<BuildingData>(building.ToString());
        #endregion
        #region Worman Data
        Workman.Infomation[] infomations = JsonHelper.FromJson<Workman.Infomation>(workman.ToString());
        workmanData = new Dictionary<int, List<Workman.Infomation>>();
        /* 분해 작업 실시 */
        for (int i = 0; i < infomations.Length; i++)
        {
            if (workmanData.ContainsKey(infomations[i].ID) == false)
            {
                List<Workman.Infomation> value = new List<Workman.Infomation>();
                workmanData.Add(infomations[i].ID, value);
            }

            workmanData[infomations[i].ID].Add(infomations[i]);
        }
        #endregion
        #region    Sculpture Data
        sculptureData = JsonHelper.FromJson<ShopItem>(sculpture.ToString());
        #endregion Sculpture
        #region Roulette Data
        rouletteData = JsonHelper.FromJson<RoulettePack>(roulette.ToString());
        #endregion
        #region Quiz table data
        quiztable = JsonHelper.FromJson<QuizTable>(quiz.ToString());
        #endregion
    }

    #region Building
    public List<BuildingData> GetBuildingData(Building.Kind kind,int id)
    {
        List<BuildingData> revalue = new List<BuildingData>();

        for (int i = 0; i < buildingData.Length; i++)
        {
            if (buildingData[i].id == id && buildingData[i].kind == kind.ToString())
            {
                revalue.Add(buildingData[i]);
            }
        }
#if USE_DEBUG
        if (revalue.Count == 0)
        {
            Debug.Log("<color=red>" + "Not find building data [ " + id + " ]</color>");
        }
#endif
        return revalue;
    }
    public BuildingData GetBuildingData(int id, int level)
    {
        for (int i = 0; i < buildingData.Length; i++)
        {
            if (buildingData[i].id == id && buildingData[i].level == level)
            {
                return buildingData[i];
            }
        }

#if UNITY_EDITOR
        Debug.LogWarning("Not find building data [ " + id + " : " + level + " ]");
#endif

        return null;
    }
    public List<int> GetAllBuildingID()
    {
        List<int> ids = new List<int>();
        for (int i = 0; i < buildingData.Length; i++)
        {
            int dataId = buildingData[i].id;
            if(ids.FindIndex(item => item == dataId) < 0) 
            {
                ids.Add(buildingData[i].id);
            }
        }

        return ids;
    }
    #endregion
    #region Workman
    public List<Workman.Infomation> GetWorkmanInformation(int id)
    {
        if (workmanData.ContainsKey(id))
        {
            return workmanData[id];
        }
        else
        {
#if USE_DEBUG

            Debug.LogError("Not find workman information list : [ ID : " + id + "]");
#endif
            return null;
        }
    }
    public Workman.Infomation GetWorkmanInformation(int id,Common.RULItem.ID compensationID)
    {
        if (workmanData.ContainsKey(id))
        {
            return workmanData[id].Find(delegate (Workman.Infomation info)
            {
                return info.compensationID == compensationID;
            });
        }
        else
        {
#if USE_DEBUG

            Debug.LogError("Not find workman information : [ ID : " + id + "]");
#endif
            return new Workman.Infomation();
        }
    }
    #endregion
    #region Roulette
    public string GetRouletteRewardInformation(int id /* level */)
    {
        for(int i = 0; i < rouletteData.Length; i++)
        {
            if(rouletteData[i].id == id)
            {
                return rouletteData[i].reward;
            }
        }
        string dummey = rouletteData[rouletteData.Length-1].reward;

        return dummey;
    }
    #endregion
    #region Shop
    public ShopItem GetSculptureData(int id)
    {
        if (sculptureData == null) Load();
        for(int i = 0; i < sculptureData.Length; i++)
        {
            if(int.Parse(sculptureData[i].id) == id)
            {
                return sculptureData[i];
            }
        }

        return null;
    }
    #endregion
    #region Quiz
    public QuizTable GetRandomQuizTable()
    {
        int rand = Random.Range(0, quiztable.Length);

        return quiztable[rand];
    }
    #endregion
}
