﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : MonoBehaviour
{
    public static EffectManager instance;


    [System.Serializable]
    public class Effect
    {
        public string key;
        public GameObject effect;
    }
    [SerializeField] List<Effect> effects;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public Effect GetEffect(string key)
    {
        Effect find = effects.Find( delegate (Effect v)
        {
            return v.key.ToString().CompareTo(key.ToString()) == 0;
        });
        return find;
    }
}
