﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserManager : MonoBehaviour
{
    public static UserManager instance = null;

    #region Delegate 형식 지정
    public delegate void OnChangeString(string str);
    public delegate void OnChangeInteger(int num);
    public delegate void OnChangeULong(ulong num);
    #endregion

    [SerializeField, ReadOnly] ulong gold;
    [SerializeField, ReadOnly] int shieldCount;

    #region Property
    public event OnChangeULong GoldChange;
    public ulong GoldOnEvent
    {
        get => gold;
        set 
        {
            gold = value;
            GoldChange?.Invoke(value);
        }
    }
    public ulong GoldOffEvent
    {
        get => gold;
        set => gold = value;
    }
    public event OnChangeInteger ShieldChange;
    public int ShieldCount
    {
        get => shieldCount;
        set
        {
            ShieldChange?.Invoke(value);
            shieldCount = value;
        }
    }
    #endregion

    // Start is called before the first frame update
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
#if USE_DEBUG
            Debug.LogWarning("There is more than one user manager.");
#endif
            Destroy(this.gameObject);
        }
    }

    #region 
    public void GoldSet(string plusGold,bool @event = true)
    {
        // NOTE : 서버로 부터 받아온 데이터 삽입
        try
        {
            if (@event)
            {
                GoldOnEvent += ulong.Parse(plusGold);
            }
            else
            {
                GoldOffEvent += ulong.Parse(plusGold);
            }
        }
        catch (System.ArgumentNullException) { }
    }

    public void ShieldSet(int shieldCount)
    {
        this.ShieldCount = shieldCount;
    }
    #endregion

    private void Update()
    {
        //Debug.Log("<color=yellow> Gold : " + gold + "</color>");
    }
}