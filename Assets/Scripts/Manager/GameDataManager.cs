﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PlayFab.Internal;
using System;

public class GameDataManager : SingletonMonoBehaviour<GameDataManager>
{
    [SerializeField]
    TutorialManager tutorialMgr;
    [SerializeField]
    Roulette roulette;
    [SerializeField]
    UserManager userManager;
    [SerializeField]
    BuildingManager buildingManager;
    [SerializeField]
    SculptureManger sculptureManger;

    #region Property function
    public Roulette Roulette { get => roulette;  }
    #endregion

    void Start()
    {
        tutorialMgr.Init(!NetworkManager.instance.IsTutorial());

        
        userManager.GoldSet(NetworkManager.instance.GetGoldFromServer());
        userManager.ShieldCount = NetworkManager.instance.GetShieldCount();
        
        print("NetworkManager.instance.GetBuildingsId() " + NetworkManager.instance.GetBuildingsId());
        print("NetworkManager.instance.GetBuildingsLevel() " + NetworkManager.instance.GetBuildingsLevel());

        buildingManager.Init(NetworkManager.instance.GetBuildingsId(),
            NetworkManager.instance.GetBuildingsLevel(),
            NetworkManager.instance.GetBuildingsBombState(),
            /* 삽입 필요 : 건설진행 여부*/"");

        
        roulette.Init();
        roulette.SetUseCount(NetworkManager.instance.GetSpinCount());
        roulette.SetMagnification(NetworkManager.instance.GetMagnification());

        // Test Code
        //buildingManager.Init(NetworkManager.instance.GetBuildingsId(),
        //    "1|0|0|0|0|0|0|0|0|0|0|0|0|0|0",
        //    "1|0|0|0|0|0|0|0|0|0|0|0|0|0|0");

        //print(NetworkManager.instance.GetSculptureInven());

        sculptureManger.Init(NetworkManager.instance.GetSculptureArrange(),
            NetworkManager.instance.GetSculptureInven());
    }
}
