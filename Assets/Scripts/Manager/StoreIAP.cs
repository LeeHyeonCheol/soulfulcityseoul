﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Purchasing;

using PlayFab;
using PlayFab.Internal;
using PlayFab.ClientModels;

public class JsonData 
{
    public string orderId;
    public string packageName;
    public string productId;
    public long purchaseTime;
    public int purchaseState;
    public string purchaseToken;
}

public class PayloadData 
{
    public JsonData JsonData;
    public string signature;
    public string json;

    public static PayloadData FromJson(string json) 
    {
        var payload      = JsonUtility.FromJson<PayloadData>(json);
        payload.JsonData = JsonUtility.FromJson<JsonData>(payload.json);
        return payload;
    }
}

public class GooglePurchase 
{
    public PayloadData PayloadData;
    public string Store;
    public string TransactionID;
    public string Payload;

    public static GooglePurchase FromJson(string json) 
    {
        var purchase         = JsonUtility.FromJson<GooglePurchase>(json);
        purchase.PayloadData = PayloadData.FromJson(purchase.Payload);
        return purchase;
    }
}

public class ApplePurchase 
{
    public string Store;
    public string TransactionID;
    public string Payload;

    public static ApplePurchase FromJson(string json) 
    {
        var purchase = JsonUtility.FromJson<ApplePurchase>(json);
        return purchase;
    }
}

public class StoreIAP : SingletonMonoBehaviour<StoreIAP>
{
    List<ItemInstance> items;
    Action<string> successBuyCallback;

    public void ProcessPurchase(Product e, Action<string> callback)
    {
        successBuyCallback = callback;
		Debug.Log("Purchase OK: " + e.definition.id);
		Debug.Log("Receipt: " + e.receipt);

        productId = e.definition.id;

        if(Application.platform == RuntimePlatform.OSXEditor)
        {
            successBuyCallback?.Invoke(productId);
        }
        else
            ProcessPurchasedProductReceipt(e);
    }

    void ProcessPurchasedProductReceipt(Product e)
    {
#if UNITY_ANDROID
        var googleReceipt = GooglePurchase.FromJson(e.receipt);

        PlayFabClientAPI.ValidateGooglePlayPurchase(new ValidateGooglePlayPurchaseRequest() 
        {
            CurrencyCode = e.metadata.isoCurrencyCode,
            // $0.99 * 100 = 99
            // 상품 가격 정수형
            PurchasePrice = (uint)(e.metadata.localizedPrice * 100),
            ReceiptJson = googleReceipt.PayloadData.json,
            Signature = googleReceipt.PayloadData.signature
        }, 
        result => 
        {
            Debug.Log("Validation successful!" + e.definition.id);
            successBuyCallback?.Invoke(productId);
        },
           error => Debug.Log("Validation failed: " + error.GenerateErrorReport())
        );

#elif UNITY_IOS

        var appleReceipt = ApplePurchase.FromJson(e.receipt);

        PlayFabClientAPI.ValidateIOSReceipt(new ValidateIOSReceiptRequest()
        {
            CurrencyCode = e.metadata.isoCurrencyCode,
            // $0.99 * 100 = 99
            // 상품 가격 정수형
            PurchasePrice = (int)(e.metadata.localizedPrice * 100),
            ReceiptData = appleReceipt.Payload
        }, 
        result => 
        {
            Debug.Log("Validation successful!" + e.definition.id);
            successBuyCallback?.Invoke(productId);
        },
           error => Debug.Log("Validation failed: " + error.GenerateErrorReport())
        );
#endif  
    }

    string productId;
}
