﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PauseManager : MonoBehaviour
{
    public static PauseManager instance = null;
    public List<Animation> liveAnimation;

    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
#if UNITY_EDITOR
            Debug.LogWarning("There is more than one pause manager.");
#endif
            Destroy(this.gameObject);
        }
    }

    void FindAllObject()
    {
        #region Animation
        liveAnimation = new List<Animation>();

        var animation = GameObject.FindObjectsOfType<Animation>();
        liveAnimation.AddRange(animation);       
        #endregion
    }



    void Pause()
    {
        // 애니메이션
        for(int i = 0; i < liveAnimation.Count; i++)
        {
            liveAnimation[i].DOPause();
        }

        // TODO : 일시정지 구현
    }
    void ReStart()
    {
        // 애니메이션
        for (int i = 0; i < liveAnimation.Count; i++)
        {
            liveAnimation[i].DOPlay();
        }

        // TODO : 재시작 구현
    }
}
