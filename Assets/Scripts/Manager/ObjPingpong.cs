﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

public class ObjPingpong : MonoBehaviour
{
    [SerializeField] List<GameObject> list;

    [Header("Animation")]
    [SerializeField] AnimationCurve curve;
    [SerializeField] float duration;
    [SerializeField] float delay;

    void Start()
    {
        Animation();
    }

    void Animation()
    {
        Vector3[] scales = new Vector3[list.Count];

        for(int i = 0; i < list.Count; i++)
        {
            scales[i] = list[i].transform.localScale;
            list[i].transform.localScale = Vector3.zero;
        }

        for(int i = 0; i < list.Count;i++)
        {
            list[i].transform.DOScale(scales[i], duration).
                SetEase(curve)
                .SetDelay(i * delay);
        }
    }
}
