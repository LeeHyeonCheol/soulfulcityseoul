﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using HoneyPlug.Audio;

public class Roulette : MonoBehaviour
{
    // 룰렛 애니메이션 관련
    [Header("Roulette and Button Animation")]
    [SerializeField] Image roulette = null;
    [SerializeField] Image buttonImg = null;
    [SerializeField] int rotationCount = 5;
    [SerializeField] float rotateDuration = 5.0f;
    [SerializeField] float pressDuration = 0.5f;
    [SerializeField] Image levelUpEffect;
    [SerializeField] Image spineEffect;
    [SerializeField] SoundManager soundManager;
    [SerializeField] Button nextButton;
    [SerializeField] Image nextAnimation;


    [Header("External reward reference")]
    [SerializeField, ReadOnly] int exRewardIndex;
    [SerializeField, ReadOnly] bool exRewardFlag;

    [Header("Press button reference")]
    [SerializeField] bool isPress;
    [SerializeField] bool isPressLock;
    [SerializeField, ReadOnly] float pressOnDelay = 1.2f;
    [SerializeField, ReadOnly] float pressOnCurrent;


    [Header("Chasing")]
    [SerializeField, ReadOnly] Vector2 buttonTransform;
    [SerializeField, ReadOnly] Common.RULItem.ID id;
    [SerializeField] Button[] magnificationButton;

    [Header("Position Reference")]
    [SerializeField] Vector2 defaultValue;
    [SerializeField] Vector2 dotValue;
    [SerializeField] Vector2 halfValue;

    [SerializeField] List<Sprite> normalWord;
    [SerializeField] List<Sprite> specialWord;
    [SerializeField] Common.IList[] position;

    // TODO : 카운트 회복 추가
    [Header("Count")]
    [SerializeField] Image fillGauge;
    [SerializeField] Image subFillGauge;
    [SerializeField] Text useCountText;
    [SerializeField] int useCount;
    [SerializeField] DateTime recoveryTime;
    [SerializeField, ReadOnly] long lastTime;
    [SerializeField, ReadOnly] float stackTime;

    [Header("Magnification")]
    [SerializeField] Text magnificationText = null;
    [SerializeField] List<Text> magnificationUI_Text;
    [SerializeField] List<Image> magnificationUI_Image;
    [SerializeField,ReadOnly] int magnification = 1;

    [Header("Character Animation")]
    [SerializeField] Utilities.Animation.SpriteAnimation characterAnim;


    // [Header("Cloud Animation")]
    // [SerializeField] Image cloudeTop = null;
    // [SerializeField] Image cloudeBottom = null;
    // [SerializeField] float cloudeDuration = 15.0f;
    // [SerializeField, ReadOnly] Vector3 cloudeTopBasePos = Vector3.zero;
    // [SerializeField, ReadOnly] Vector3 cloudeBottomBasePos = Vector3.zero;

    [Header("Reward Animation")]
    [SerializeField] List<AnimationEvent> animationEvents;
    [SerializeField] GameObject quizReward;
    [SerializeField] GameObject attackReward;
    [SerializeField] GameObject findPicture; // FIX : 21.02.22
    [SerializeField, ReadOnly] string resultParam = "";
    [SerializeField, ReadOnly] float rotationAngle;
    Action<string> resultCallBack = null;

    [Header("View Only")]
    [SerializeField, ReadOnly] bool animationFlag = true;
    [SerializeField, ReadOnly] bool pressFlag = false;
    [SerializeField, ReadOnly] float yetAngle = 0.0f;
    // 랭킹 1위 정보 표시
    [SerializeField] Text rank1Infos;

    // 코루틴 및 트윈
    Coroutine rotationRoutine = null;
    Coroutine pressRoutine = null;
    Coroutine unPressRoutine = null;

    TweenerCore<Quaternion, Vector3, QuaternionOptions> rotationTween = null;
    TweenerCore<Vector3, Vector3, VectorOptions> pressTween = null;
    TweenerCore<Vector3, Vector3, VectorOptions> unPressTween = null;
    TweenerCore<Vector3, Vector3, VectorOptions> cloudeTopTween = null;
    TweenerCore<Vector3, Vector3, VectorOptions> cloudeBottomTween = null;

    //  슬롯 10개 각 활률 및 이벤
    private List<int> probability = null;
    private List<Action<string>> eventDB = null;
    private List<string> actionParams = null;

    public SoundManager SoundManager { get => soundManager; set => soundManager = value; }

    #region Property list
    public Common.RULItem.ID ID { get => id; }
    public bool AnimationFlag { get => animationFlag; }
    public int Magnification { get => magnification; }

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        //AnimationCloud();
        yetAngle = 0.0f;
        soundManager.PlayMusic(soundManager.background_home, .5f);

        rank1Infos.text = NetworkManager.instance.rank1PlayerInfo;
    }
    private void OnDisable()
    {
        #region Kill Coroutine
        if (rotationRoutine != null) StopCoroutine(rotationRoutine);
        if (pressRoutine != null) StopCoroutine(pressRoutine);
        if (unPressRoutine != null) StopCoroutine(unPressRoutine);

        rotationRoutine = null;
        pressRoutine = null;
        unPressRoutine = null;
        #endregion

        #region Kill Tween
        rotationTween?.Kill();
        pressTween?.Kill();
        unPressTween?.Kill();
        cloudeTopTween?.Kill();
        cloudeBottomTween?.Kill();

        rotationTween = null;
        pressTween = null;
        unPressTween = null;
        cloudeBottomTween = null;
        cloudeTopTween = null;
        #endregion

        #region Resetting Param
        resultCallBack = null;
        resultParam = "";
        #endregion
        #region Resetting Pos
        //cloudeTop.transform.localPosition = cloudeTopBasePos;
        //cloudeBottom.transform.localPosition = cloudeBottomBasePos;
        #endregion
    }
    private void OnApplicationPause(bool pause)
    {
        if (pause == false)
        {
            recoveryTime = new DateTime(long.Parse(PlayerPrefs.GetString(Common.RULRecoveryTimeFormat, "0")));
            TimeSpan span = DateTime.UtcNow - recoveryTime;
            lastTime = span.Days * 24 * 60 * 60 + span.Hours * 60 * 60 + span.Minutes * 60 + span.Seconds;
        }
    }

#if USE_DEBUG
    private void OnValidate()
    {
        //RewardSet(BuildingManager.instance.OpenCount);
    }
#endif

    public void Init()
    {
        #region 확률 지정 10개
        /// ( 05 % ) 01 STEAL
        /// ( 20 % ) 02 15K
        /// ( 07 % ) 03 SPINS
        /// ( 01 % ) 04 100K
        /// ( 20 % ) 05 10K
        /// ( 02 % ) 06 ATTACK
        /// ( 20 % ) 07 5K
        /// ( 05 % ) 08 50K
        /// ( 05 % ) 09 SHIELD
        /// ( 15 % ) 10 25K

        probability = new List<int>();
        probability.Add(50);
        probability.Add(200);
        // 물약확률 : 레벨에 따라 조절.
        if (BuildingManager.instance.OpenCount == 1) { probability.Add(150); }
        else { probability.Add(70); }
        probability.Add(10);
        probability.Add(200);
        probability.Add(20);    // Attack Test
        probability.Add(200);
        probability.Add(50);
        probability.Add(50);
        probability.Add(150);

        #endregion
        #region 각 슬롯 보상 지정
        // 보상 함수
        eventDB = new List<Action<string>>();
        eventDB.Add(CreateItem);
        eventDB.Add(CreateGold);
        eventDB.Add(CreateItem);
        eventDB.Add(CreateGold);
        eventDB.Add(CreateGold);
        eventDB.Add(CreateItem);
        eventDB.Add(CreateGold);
        eventDB.Add(CreateGold);
        eventDB.Add(CreateItem);
        eventDB.Add(CreateGold);

        // 보상 매개변수 기본 설golds
        actionParams = new List<string>();
        actionParams.Add(Common.RULItem.ID.Steal.ToString());
        actionParams.Add("");
        actionParams.Add(Common.RULItem.ID.Spins.ToString());
        actionParams.Add("");
        actionParams.Add("");
        actionParams.Add(Common.RULItem.ID.Attack.ToString());
        actionParams.Add("");
        actionParams.Add("");
        actionParams.Add(Common.RULItem.ID.Shield.ToString());
        actionParams.Add("");

        // 리워드 결과 셋팅 관련 ( Gold 부분 )
        RewardSet(BuildingManager.instance.OpenCount);
        BuildingManager.instance.updateLevelEvent += RewardSet;
        #endregion

        #region Routine 초기화
        rotationRoutine = null;
        pressRoutine = null;
        unPressRoutine = null;
        #endregion
        #region plug 초기화
        animationFlag = true;
        pressFlag = false;
        #endregion
        #region Tween 초기화
        rotationTween = null;
        pressTween = null;
        unPressTween = null;
        //cloudeTopTween = null;
        //cloudeBottomTween = null;
        #endregion
        #region Data 초기화
        resultCallBack = null;
        resultParam = "";
        #endregion
        #region Roulette 초기화
        #region UI초기화
        //SetUseCount(PlayerPrefs.GetInt(Common.RULUseCountFormat, Common.RULUseCountMax));
        subFillGauge.color = Common.ColorAlpha;
        spineEffect.color = Common.ColorAlpha;
        #endregion
        if (useCount != Common.RULUseCountMax)
        {
            recoveryTime = new DateTime(long.Parse(PlayerPrefs.GetString(Common.RULRecoveryTimeFormat, "0")));
            TimeSpan span = DateTime.UtcNow - recoveryTime;
            lastTime = span.Days * 24 * 60 * 60 + span.Hours * 60 * 60 + span.Minutes * 60 + span.Seconds;
            Debug.Log(lastTime);
        }
        magnification = 1;
        magnificationText.text = magnification.ToString();
        buttonTransform = buttonImg.transform.localPosition;
        #endregion
    }

    //= NOTE : 결제시[ Param : useCount + (구매개수) ]
    public void SetUseCount(int count)
    {
        if (useCount < count)
        {
            FlushAnimation(subFillGauge, 0.0f, 0.5f, 0.1f).Play();
        }

        useCount = count;

        if (lastTime == 0)
        {
            recoveryTime = DateTime.UtcNow;
            PlayerPrefs.SetString(Common.RULRecoveryTimeFormat, recoveryTime.Ticks.ToString());
        }

        fillGauge.DOFillAmount((float)useCount / Common.RULUseCountMax, 0.5f);
        subFillGauge.fillAmount = (float)useCount / Common.RULUseCountMax;
        useCountText.text = string.Format("{0,1:D2}", useCount) + " / " + Common.RULUseCountMax;
    }
    //= 외부에서 UseCount 증가시킬때 사용할 것.
    public void AddUseCount(int count)
    {
        FlushAnimation(subFillGauge, 0.0f, 0.5f, 0.1f).Play();
        useCount += count;

        fillGauge.DOFillAmount((float)useCount / Common.RULUseCountMax, 0.5f);
        subFillGauge.fillAmount = (float)useCount / Common.RULUseCountMax;
        useCountText.text = string.Format("{0,1:D2}", useCount) + " / " + Common.RULUseCountMax;
    }
    /// <summary>
    ///= 외부에서 룰렛 함수 결과를 고정할때 사용되는것.
    /// </summary>
    /// <param name="index">
    /// 0 : 스틸
    /// 1 : 골드
    /// 2 : 스핀
    /// 3 : 골드
    /// 4 : 골드
    /// 5 : 어택
    /// 6 : 골드
    /// 7 : 골드
    /// 8 : 실드
    /// 9 : 골드
    /// </param>
    public void FastenReward(int index)
    {
        exRewardFlag = true;
        exRewardIndex = index;
    }


    public void RewardSet(int level)
    {
        // level 이용해서 reward값 찾아오기
        string reward = DataManager.instance.GetRouletteRewardInformation(level);

        RewardSet(reward);
        SetMagnification(magnification);
    }
    public void RewardSet(string goldList)
    {
        //2500|3500|4750|6000|9500|11000

        #region init list
        //actionParams = new List<string>();
        //actionParams.Add(Common.RULItem.ID.Steal.ToString());
        //actionParams.Add("15K"); << 3 // 1
        //actionParams.Add(Common.RULItem.ID.Spins.ToString());
        //actionParams.Add("100K"); << 6 // 3
        //actionParams.Add("10K"); << 2  // 4
        //actionParams.Add(Common.RULItem.ID.Attack.ToString());
        //actionParams.Add("5K"); << 1  // 6
        //actionParams.Add("50K"); << 5 // 7
        //actionParams.Add(Common.RULItem.ID.Shield.ToString());
        //actionParams.Add("25K"); << 4 // 9
        #endregion

        string[] golds = goldList.Split(Common.splitWord);
        int[] index = { 6, 4, 1, 9, 7, 3 };

        List<string> param = new List<string>();
        for (int i = 0; i < golds.Length; i++)
        {
            param.Add(GoldConversion.GoldToString(ulong.Parse(golds[i])));
            actionParams[index[i]] = param[i];
        }
        int[] order = { 1, 3, 4, 6, 7, 9 };

        SetRewardGraphic(actionParams, order, 3 /* special index */);
    }
    public void SetRewardGraphic(List<string> param, int[] order, int specialIndex)
    {
        // 그래픽 셋팅
        for (int i = 0; i < order.Length; i++)
        {
            Sequence sequence = DOTween.Sequence();
            int paramIndexer = order[i];
            int index = (position[i].Count / 2) - (int)(((float)param[paramIndexer].Length / 2) + .5f);

            #region Set Alpha Color
            for (int x = 0; x < position[i].Count; x++)
            {
                position[i][x].color = Common.ColorAlpha;
            }
            #endregion

            #region Set Position
            Vector2 basePos = position[i][0].transform.localPosition /* pivot */;
            Vector3 setPos = basePos;
            if (index != 0)
            {
                // front
                for (int x = 1; x < index; x++)
                {
                    basePos += GetAddPositionRotation("", x);
                    setPos = basePos;
                    setPos.z = 0;
                    position[i][x].transform.localPosition = setPos;
                }

                // back
                for (int x = index; x < index + param[paramIndexer].Length; x++)
                {
                    basePos += GetAddPositionRotation(param[paramIndexer], x);
                    setPos = basePos;
                    setPos.z = 0;
                    position[i][x].transform.localPosition = setPos;
                }
            }
            else
            {
                // all : zero is pivot
                for (int x = 1; x < param[paramIndexer].Length; x++)
                {
                    basePos += GetAddPositionRotation(param[paramIndexer], x);
                    setPos = basePos;
                    setPos.z = 0;
                    position[i][x].transform.localPosition = setPos;
                }
            }
            #endregion

            if (paramIndexer != specialIndex)
            {
                Sprite[] sprite = GetSprite(normalWord, param[paramIndexer]);

                for (int j = 0; j < param[paramIndexer].Length; j++)
                {
                    position[i][j + index].sprite = sprite[j];
                    sequence.Append(position[i][j + index].DOColor(Color.white, 0.05f));
                }
            }
            else
            {
                Sprite[] sprite = GetSprite(specialWord, param[paramIndexer]);
                for (int j = 0; j < param[paramIndexer].Length; j++)
                {
                    position[i][j + index].sprite = sprite[j];
                    sequence.Append(position[i][j + index].DOColor(Color.white, 0.05f));
                }
            }
            sequence.Play();
        }

        Sprite[] GetSprite(List<Sprite> list, string spriteWord)
        {
            //=> list : 0 1 2 3 4 5 6 7 8 9 . , k
            List<Sprite> revalue = new List<Sprite>();
            for (int i = 0; i < spriteWord.Length; i++)
            {
                switch (spriteWord[i])
                {
                    case '.':
                        revalue.Add(list[10 /* . */]);
                        break;
                    case 'K':
                    case 'k':
                        revalue.Add(list[12 /* K */]);
                        break;
                    default:
                        revalue.Add(list[int.Parse(spriteWord[i].ToString())]);
                        break;
                }
            }
            return revalue.ToArray();
        }

        Vector2 GetAddPositionRotation(string positionParam, int index)
        {
            if (positionParam == "") return defaultValue;

            try
            {
                switch (positionParam[index])
                {
                    case '.':
                        return dotValue;
                    case '1':
                        return halfValue;
                    default:
                        return defaultValue;
                }
            }
            catch (SystemException)
            {
                return defaultValue;
            }
        }
    }
    #region 버튼 event
    public void OnPressButton()
    {
        if (useCount - magnification < 0)
        {
            PopupManager.instance.Open(
                Common.PopupCode.LessPortion,
                "Under_Position",
                PopupManager.ButtonStyle.YesNo,
                PopupManager.MoveType.CenterPumping,
                delegate { UIManager.instance.OpenUI_Shop(); },
                delegate { UIManager.instance.OpenADRewardPortion(); },
                false);
            return;
        }
        if (isPressLock) isPressLock = false;
        if (pressFlag == false)
        {
            pressRoutine = StartCoroutine(PressAnimation());
        }
    }
    public void OnUnPressButton(bool unlock = false)
    {
        if (unlock) isPressLock = false;
        unPressRoutine = StartCoroutine(UnPressAnimation());
    }

    #region 배율 관련
    public void SetMagnification(bool isLeft)
    {
        int[] values = Common.RULMGValue;

        //= NOTE : left is decrease /  right is increase
        //= Set value
        for (int i = 0; i < values.Length; i++)
        {
            if (values[i] == magnification)
            {
                if (isLeft)
                {
                    if (i == 0) magnification = values[values.Length - 1];
                    else magnification = values[i - 1];
                    break;
                }
                else
                {
                    magnification = values[(i + 1) % values.Length];
                    break;
                }
            }
        }


        SetMagnificationGraphic();
    }
    public void SetMagnification(int magnification)
    {
        if(this.magnification != magnification)
        {
            this.magnification = magnification;
            SetMagnificationGraphic();
        }
    }
    #endregion

    void SetMagnificationGraphic()
    {
        #region Set Animation and param
        int[] index = { 6, 4, 1, 9, 7, 3 };
        List<string> actionParam = new List<string>();
        for (int i = 0; i < actionParams.Count; i++)
        {
            if (InIndex(index, i))
            {
                ulong gold = GoldConversion.StringToGold(actionParams[i]) * (ulong)magnification;
                actionParam.Add(GoldConversion.GoldToString(gold));
            }
            else
            {
                actionParam.Add(actionParams[i]);
            }

        }
        int[] order = { 1, 3, 4, 6, 7, 9 };
        SetRewardGraphic(actionParam, order, 3 /* special index */);

        bool InIndex(int[] array, int value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (value == array[i]) return true;
            }

            return false;
        }
        #endregion

        //= SetUI
        magnificationText.text = magnification.ToString();

        if (magnification != 1)
        {
            //= magnificationUI_Image
            //= magnificationUI_Text
            for (int i = 0; i < magnificationUI_Image.Count; i++)
            {
                magnificationUI_Image[i].transform.DOScale(1.2f, .5f)
                .OnStart(()=>
                {
                    if(magnificationUI_Image[i].transform.localScale == Vector3.one)
                    {
                        magnificationUI_Image[i].transform.localScale = Vector3.zero;
                    }
                })
                .OnComplete(() =>
                {
                    magnificationUI_Image[i].transform.DOScale(1.0f, .2f);
                });
            }
            for (int i = 0; i < magnificationUI_Text.Count; i++)
            {
                magnificationUI_Text[i].text = "x" + magnification.ToString();
            }
        }
        else
        {
            for (int i = 0; i < magnificationUI_Image.Count; i++)
            {
                magnificationUI_Image[i].transform.DOScale(Vector3.zero, .7f)
                .OnStart(() => magnificationUI_Image[i].transform.localScale = Vector3.one);
            }
        }
    }
    #endregion
    #region 버튼 애니메이션
    IEnumerator PressAnimation()
    {
        if (useCount - magnification < 0)
        {
#if USE_DEBUG
            Debug.Log("룰랫 회수 부족");
#endif
            yield break;
        }

        yield return new WaitUntil(() => pressFlag == false && rotationRoutine == null && unPressTween == null);
        if (pressTween == null)
        {
            isPress = true;
            pressTween = buttonImg.transform.DOLocalMoveY(buttonTransform.y - 50.0f, pressDuration)
                .OnComplete(() =>
                {
                    pressFlag = true;
                    rotationRoutine = StartCoroutine(Rotation());
                    pressTween = null;
                })
                .SetEase(Ease.OutQuad);
        }
    }
    IEnumerator UnPressAnimation()
    {
        yield return new WaitUntil(() => pressFlag == true && pressTween == null && isPressLock == false);
        if (unPressTween == null)
        {
            if (isPress)
            {
                isPress = false;
                pressOnCurrent = 0; //= 시간 초기화
            }

            unPressTween = buttonImg.transform.DOLocalMoveY(buttonTransform.y, pressDuration)
                .OnComplete(() =>
                {
                    pressFlag = false;
                    if (rotationRoutine != null) StopCoroutine(rotationRoutine);
                    rotationRoutine = null;
                    unPressTween = null;
                })
                .SetEase(Ease.InQuad);
        }
    }
    #endregion
    #region 룰렛 애니메이션

    IEnumerator Rotation()
    {
        this.animationFlag = true;

        while (pressFlag)
        {
            // 애니메이션 진행 및 버튼 이벤트 실시간 확인
            yield return new WaitUntil(() => animationFlag == true || pressFlag == false);
            yield return new WaitUntil(() => resultCallBack == null);
            if (pressFlag == false) break;
            if (isPressLock && useCount - magnification < 0)
            {
                OnUnPressButton(true);
                yield break;
            }


            nextAnimation.color = Common.ColorHalfAlpha;
            nextButton.interactable = false;
            for (int i = 0; i < magnificationButton.Length; i++)
            {
                magnificationButton[i].image.color = Common.ColorHalfAlpha;
                magnificationButton[i].interactable = false;
            }

            int index = GetDBIndex();
            float sloatAngle = 36.0f;
            //= 2020 01 21 add tutorial
            if (exRewardFlag)
            {
                index = exRewardIndex;
                exRewardFlag = false;
            }

            #region 정보 셋팅
            resultCallBack = Result(index);
            resultParam = actionParams[index];
            rotationAngle = (-360 + yetAngle) - (index * sloatAngle) - (360.0f * rotationCount);

            #endregion
            #region 결과 지급
            resultCallBack.Invoke(resultParam);
            #endregion

            Debug.Log(string.Format("Index is {0} : resultParam is {1}", index, resultParam));
            if (useCount < 0)
            {
                OnUnPressButton(true);
            }
            else
            {
                NetworkManager.instance.UpdateRouletteData(UserManager.instance.GoldOnEvent, useCount, resultParam, magnification, RunRouletteAnimation /* 리워드 애니메이션 연출*/);
            }
        }

        rotationRoutine = null;

        yield break;
    }

    void RunRouletteAnimation()
    {
        if (useCount < 0)
        {
            OnUnPressButton(true);
            return;
        }

        if (rotationTween != null) rotationTween.Kill(false);
        animationFlag = true;
        Debug.Log("Rotation Start = Ready");
        spineEffect.DOColor(Color.white, rotateDuration * .2f).
        OnComplete(() => spineEffect.DOColor(Common.ColorAlpha, rotateDuration * .8f));

        rotationTween = roulette.transform.DORotate(new Vector3(0, 0, rotationAngle), rotateDuration)
        .OnStart(() =>
        {
            Debug.Log("Rotation Start");
            nextAnimation.color = Common.ColorHalfAlpha;
            nextButton.interactable = false;
            for (int i = 0; i < magnificationButton.Length; i++)
            {
                magnificationButton[i].image.color = Common.ColorHalfAlpha;
                magnificationButton[i].interactable = false;
            }
            soundManager.PlayOneSound(soundManager.rouletteSfx);
        })
        .SetEase(Ease.OutQuad)
        .OnComplete(() =>
        {
            // 이전 각도 저장
            yetAngle = roulette.transform.localRotation.z;
            rotationTween = null;

            Debug.Log(string.Format("resultParam is {0} ", resultParam));
            id = ResultAnimation(resultParam);
            if (id == Common.RULItem.ID.Gold)
            {
                ulong gold = GoldConversion.StringToGold(resultParam);
                UserManager.instance.GoldOffEvent -= gold * (ulong)magnification;
                UserManager.instance.GoldOnEvent += gold * (ulong)magnification;

                if (GetRandResultOpenUI_Gacha())
                {
                    UIManager.instance.OpenUI_Gacha();
                }
            }
            if (id == Common.RULItem.ID.Attack || id == Common.RULItem.ID.Steal ||
                id == Common.RULItem.ID.Shield) // FIX : 21.02.22
            {
                if (pressFlag)
                {
                    if (rotationRoutine != null) StopCoroutine(rotationRoutine);
                    rotationRoutine = null;
                    OnUnPressButton(true);
                }
            }
            soundManager.PlayOneSound(soundManager.rewardSfx);
            resultCallBack = null;

            characterAnim.ChangePlaying("Wow", EndCharacterAnim);
            animationFlag = false;
            nextButton.image.color = new Color(1, 1, 1, 0);
            nextButton.interactable = true;
            for (int i = 0; i < magnificationButton.Length; i++)
            {
                magnificationButton[i].image.color = Color.white;
                magnificationButton[i].interactable = true;
            }
        });


        bool GetRandResultOpenUI_Gacha()
        {
            // 통상 확률 2% : 테스트 확률 100%
            // int winningMaxValue = 2;
            int winningMaxValue = 2;
            int randRangeValue = 100;

            int rand = UnityEngine.Random.Range(0, randRangeValue);

            // 002 % 일땐 rand <= 2
            // 100 % 일땐 rand <= 100
            if (randRangeValue <= winningMaxValue)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    void EndCharacterAnim()
    {
        characterAnim.ChangePlaying("Idle1", null);
    }

    Sequence FlushAnimation(Image image, float min, float max, float duration)
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(image.DOColor(new Color(1, 1, 1, max), duration));
        seq.Append(image.DOColor(new Color(1, 1, 1, min), duration));
        seq.Append(image.DOColor(new Color(1, 1, 1, max), duration));
        seq.Append(image.DOColor(new Color(1, 1, 1, min), duration));
        seq.Append(image.DOColor(new Color(1, 1, 1, max), duration));
        seq.Append(image.DOColor(new Color(1, 1, 1, min), duration));
        return seq;
    }
    #endregion
    #region 룰렛 연산 후처리
    int GetDBIndex()
    {
        //=2020.12.18 확률 계산방식 수정
        int randLimit = 0;
        for (int i = 0; i < probability.Count; i++)
        {
            randLimit += probability[i];
        }

        int prob = UnityEngine.Random.Range(0, randLimit);
        int stackTrace = 0;
        for (int i = 0; i < probability.Count; i++)
        {
            if (stackTrace <= prob && prob < stackTrace + probability[i])
            {
                return i;
            }
            else
            {
                stackTrace += probability[i];
            }
        }

        return 0;
    }
    Action<string> Result(int index)
    {
#if UNITY_EDITOR
        if (index == -1)
        {
            Debug.LogError("ERROR : probability is -1");
        }
#endif

        return eventDB[index];
    }
    void CreateItem(string key)
    {
        const int resultSpine = 5;
#if USE_DEBUG
        Debug.Log("아이템 지급" + key);
#endif
        if (Common.RULItem.ID.Shield.ToString().CompareTo(key) == 0)
        {
            // 실드
            //==UserManager.instance.ShieldCount++;
            //==PlayerPrefs.SetInt(Common.RULItem.ShildFormat, UserManager.instance.ShieldCount);
        }
        if (Common.RULItem.ID.Steal.ToString().CompareTo(key) == 0)
        {
            // 스틸
        }
        if (Common.RULItem.ID.Attack.ToString().CompareTo(key) == 0)
        {
            // 어택
        }
        if (Common.RULItem.ID.Spins.ToString().CompareTo(key) == 0)
        {
            // 스핀
            SetUseCount(useCount - magnification);
            NetworkManager.instance.UpdateRouletteData(UserManager.instance.GoldOffEvent, useCount + resultSpine, resultParam, magnification, ResultSpine /* 룰렛 횟수 추가 */);
        }
        else
        {
            // 횟수 차감
            SetUseCount(useCount - magnification);
        }

        void ResultSpine()
        {
            SetUseCount(useCount + resultSpine * magnification);
        }
    }
    void CreateGold(string param)
    {
        // 골드 변환
        ulong gold = GoldConversion.StringToGold(param) * (ulong)Magnification;
        UserManager.instance.GoldOffEvent += gold;

#if USE_DEBUG
        Debug.Log("Gold : " + gold + " 획득");
#endif

        // 횟수 차감
        SetUseCount(useCount - magnification);
    }
    #endregion
    // #region 구름 애니메이션
    // void AnimationCloud()
    // {
    //     float magnification = Screen.width / 720.0f;

    //     if (cloudeTopTween == null)
    //     {
    //         cloudeTopBasePos = cloudeTop.transform.localPosition;
    //         cloudeTopTween = cloudeTop.transform.DOLocalMoveX(cloudeTop.transform.localPosition.x + Screen.width * 5f, cloudeDuration * magnification * 1.3f)
    //             .SetLoops(-1);
    //     }

    //     if (cloudeBottomTween == null)
    //     {
    //         cloudeBottomBasePos = cloudeBottom.transform.localPosition;
    //         cloudeBottom.transform.DOLocalMoveX(cloudeBottom.transform.localPosition.x + Screen.width * 5f, cloudeDuration * magnification)
    //         .SetLoops(-1);
    //     }
    // }
    // #endregion
    #region 보상 애니메이션
    Common.RULItem.ID ResultAnimation(string key)
    {
        for (int i = 0; i < animationEvents.Count; i++)
        {
            if (animationEvents[i].Key.ToString().CompareTo(key) == 0)
            {
                AnimationEvent animationEvent = Instantiate(animationEvents[i]);
                GameObject effect = animationEvent.gameObject;
                effect.transform.SetParent(this.transform.parent);
                effect.transform.localScale = Vector3.one;
                effect.transform.localPosition = Vector3.zero;
                SetResultAnimationCallback(animationEvents[i].Key, animationEvent);
                return animationEvents[i].Key;
            }
            else if (animationEvents[i].Key.ToString().CompareTo(Common.RULItem.ID.Gold.ToString()) == 0 && GoldConversion.IsGold(key))
            {
                GameObject effect = Instantiate(animationEvents[i]).gameObject;
                //effect.transform.SetParent(this.transform.parent);
                effect.transform.localScale = Vector3.one;
                effect.transform.localPosition = new Vector3(0, 1.5f, 0);
                effect.transform.SetSiblingIndex(1000);
                return animationEvents[i].Key;
            }
        }

        return Common.RULItem.ID.NONE;
    }

    public Action addResultAnimationCallback;
    void SetResultAnimationCallback(Common.RULItem.ID id, AnimationEvent @event)
    {
        switch (id)
        {
            case Common.RULItem.ID.Attack:
                @event.SetCallBack(Callback_Attack);
                break;
            case Common.RULItem.ID.Shield:
                @event.SetCallBack(Callback_FindPicture); // FIX : 21.02.22
                break;
            case Common.RULItem.ID.Spins:
                @event.SetCallBack(OnFinish_SpineItemReward);
                break;
            case Common.RULItem.ID.Steal:
                @event.SetCallBack(Callback_Quiz);
                break;
            default:
                @event.SetCallBack(null);
                break;
        }

        if(addResultAnimationCallback != null)
        {
            @event.SetCallBack(addResultAnimationCallback);
            addResultAnimationCallback = null;
        }
    }

    void OnFinish_SpineItemReward()
    {
        // TODO : 네트워크 매니저 수정해서 매개변수 들어가는것 추가하기 ( use Count 관련 함수 여러개 두지않게. ) 
        NetworkManager.instance.UpdateRouletteData(UserManager.instance.GoldOnEvent, useCount + 5, resultParam, magnification, Callback_SpineItemReward);
    }


    void Callback_SpineItemReward()
    {
        SetUseCount(useCount + 5 * magnification);
    }
    void Callback_Quiz()
    {
        GameObject steal = Instantiate(quizReward);
        steal.transform.SetParent(this.transform.parent);
        steal.transform.localScale = Vector3.one;
        steal.transform.localPosition = Vector3.zero;
        steal.transform.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
    }
    void Callback_Attack()
    {
        //attackReward
        GameObject attack = Instantiate(attackReward);
        attack.transform.SetParent(this.transform.parent);
        attack.transform.localScale = Vector3.one;
        attack.transform.localPosition = Vector3.zero;
        (attack.transform as RectTransform).sizeDelta = Vector2.zero;

    }
    // FIX : 21.02.22
    void Callback_FindPicture()
    {
        GameObject attack = Instantiate(findPicture);
        attack.transform.SetParent(this.transform.parent);
        attack.transform.localScale = Vector3.one;
        attack.transform.localPosition = Vector3.zero;
        (attack.transform as RectTransform).sizeDelta = Vector2.zero;
    }
    #endregion

    private void Update()
    {
        if (useCount < Common.RULUseCountMax)
        {
            stackTime += Time.deltaTime;

            if (1.0f <= stackTime)
            {
                stackTime -= 1.0f;
                lastTime += 1;
            }
            if (Common.RULRecoveryDelay <= lastTime)
            {
                //= 2020 01 19 룰랫 카운트 증가 방식 변경
                int count = (int)(lastTime / Common.RULRecoveryDelay);
                lastTime -= (Common.RULRecoveryDelay * count);

                recoveryTime = DateTime.UtcNow;
                PlayerPrefs.SetString(Common.RULRecoveryTimeFormat, recoveryTime.Ticks.ToString());

                NetworkManager.instance.UpdateRouletteData(UserManager.instance.GoldOnEvent, useCount, resultParam, magnification, delegate
                {
                    Callback_AddUseCount(count);
                });
            }
        }
        else
        {
            lastTime = 0;
            stackTime = 0;
        }

        if (BuildingManager.instance != null && BuildingManager.instance.UiSet.buildings != null)
        {
            for (int i = 0; i < BuildingManager.instance.UiSet.buildings.Count; i++)
            {
                BuildingManager.instance.UiSet.buildings[i].ConstructionGraphicSet(false);
            }
        }


        //[SerializeField] bool isPress;
        //[SerializeField] bool isPressLock;
        //[SerializeField, ReadOnly] float pressOnDelay = 1.2f;
        //[SerializeField, ReadOnly] float pressOnCurrent;

        if (isPress)
        {
            pressOnCurrent += Time.deltaTime;
            if (pressOnDelay < pressOnCurrent)
            {
                pressOnCurrent = 0;
                isPress = false;
                isPressLock = true;
            }
        }

    }
    void Callback_AddUseCount(int add)
    {
        // 기존부터 최대치를 넘어있는 상태
        if (Common.RULUseCountMax < useCount + add)
        {
            SetUseCount(useCount);
        }
        else
        {
            int count = (Common.RULUseCountMax < useCount + add) ? Common.RULUseCountMax : useCount + add;
            SetUseCount(useCount + add);
        }
    }

}