﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;
using HoneyPlug.Audio;

public class BabsaeQuiz : MonoBehaviour
{
    [SerializeField]
    Image[] pictures; 
    [SerializeField]
    Text title;
    [SerializeField]
    GameObject[] result;
    [SerializeField]
    GameObject gameWindow;
    [SerializeField]
    GameObject adPopup;

    bool startGame;
    bool isSuccess;

    int answerNum;
    bool initQuiz;
    bool isTouchLock;

    Action endCallback;

    [Header("UI")]
    [SerializeField] Slider slider;
    [SerializeField] Text timer;

    void Start() 
    {
        InitSet();    
    }

    public void InitSet()
    {
        //this.endCallback = endCallback;

        initQuiz = false; isTouchLock = false;
        answerNum = UnityEngine.Random.Range(0, 9);

        int randQuizNum = UnityEngine.Random.Range(0, 5);
        Sprite normalSpr = Resources.Load<Sprite>("Characters/Quiz/q" + randQuizNum + "_0");
        Sprite answerSpr = Resources.Load<Sprite>("Characters/Quiz/q" + randQuizNum + "_1");

        for(int i = 0; i < pictures.Length; i++)
        {
            if(i != answerNum)
                pictures[i].sprite = normalSpr;
            else
                pictures[i].sprite = answerSpr;
        }

        Init();

        startGame = true;
        initQuiz  = true;
    }

    public void ClickObj(int index)
    {
        if(initQuiz == false)   return;
        if(isTouchLock == true) return;

        isTouchLock = true; startGame = false;
        if(answerNum ==  index)
        {
            SoundManager.instance.PlayOneSound(SoundManager.instance.rewardSfx);
            isSuccess = true;
            result[0].SetActive(true); // 성공
        }
        else
        {
            isSuccess = false;
            result[1].SetActive(true); // 실패
        }

        StartCoroutine(CheckResult());
    }

    IEnumerator CheckResult()
    {
        yield return new WaitForSeconds(1.5f);
        //endCallback?.Invoke();

        if(isSuccess)
        {
            gameWindow.SetActive(false); adPopup.SetActive(true);
        }
        else
            Destroy(this.gameObject);
    }

    public void ClickADs()
    {
        ADMobManager.instance.callbackCloseEarendAD += delegate 
        {
            NetworkManager.instance.RewardSpinCount(ADClose, true);
        }; 

        ADMobManager.instance.callbackADClose += delegate 
        { 
            NetworkManager.instance.RewardSpinCount(ADClose, false);
        }; 

        ADMobManager.instance.OpenRewardAD();
    }

    public void ClickClose()
    {
        NetworkManager.instance.RewardSpinCount(ADClose, false);
    }

    void ADClose()
    {
        Destroy(this.gameObject);
    }

    [Header("Play Time")]
    [SerializeField] float limitTime;
    [SerializeField] float currentTime;
    [SerializeField] bool timerFlag;
    DG.Tweening.Core.TweenerCore<float, float, DG.Tweening.Plugins.Options.FloatOptions> sliderTween;
    DG.Tweening.Core.TweenerCore<string, string, DG.Tweening.Plugins.Options.StringOptions> timeTween;

    void Init()
    {
        currentTime = limitTime;
        slider.value = 0.0f;
        if (sliderTween != null)
        {
            sliderTween.Kill(false);
            sliderTween = null;
        }
        if (timeTween != null)
        {
            timeTween.Kill(false);
            timeTween = null;
        }

        sliderTween = null;
        timeTween = null;
        timerFlag = false;
    }

    #region UI reference
    void UpdateTimeUI(Slider slider, Text timer, float max, float current)
    {
        if (sliderTween == null)
        {
            float value = current / max;
            float difference = Mathf.Abs(slider.value - value) * 2;
            sliderTween = slider.DOValue(value, difference);
            sliderTween.SetEase(Ease.Linear);
            sliderTween.OnComplete(
            () =>
            {
                sliderTween = null;
                timerFlag = true;
            });
        }
        if (timeTween == null && timerFlag)
        {
            timeTween = timer.DOText(((int)current).ToString(), 0.05f).
            OnComplete(() =>
            {
                timeTween = null;
            });
        }
        else if (timerFlag == false)
        {
            timer.text = "Ready!";
        }
    }
    #endregion

    void LateUpdate()
    {
        if (startGame)
        {
            #region Check Time
            currentTime -= Time.deltaTime;
            if (timerFlag == false) { currentTime = limitTime; }
            UpdateTimeUI(slider, timer, limitTime, currentTime);

            if (currentTime <= 0)
            {
                startGame = false;
                result[1].SetActive(true); // 실패
                StartCoroutine(CheckResult());
            }
            #endregion
        }
    }
}
