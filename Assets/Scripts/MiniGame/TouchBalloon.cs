﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using HoneyPlug.Audio;
using UnityEngine.EventSystems;

public class TouchBalloon : MiniGame
{
    [Header("Utility")]
    [SerializeField] bool isPlaying;
    [SerializeField] Canvas canvas;
    [SerializeField] RectTransform chaser;

    [Header("Info")]
    [SerializeField,ReadOnly] int score;
    [SerializeField] int combo;
    [SerializeField, ReadOnly] const int MaxCombo = 25;
    [SerializeField] int baseScore;
    [SerializeField] GraphicRaycaster raycaster;

    [Header("UI")]
    [SerializeField] Slider slider;
    [SerializeField] Text scoreText;
    [SerializeField] Text timer;

    [Header("Play Time")]
    [SerializeField] float limitTime;
    [SerializeField] float currentTime;
    [SerializeField] bool timerFlag;
    DG.Tweening.Core.TweenerCore<float, float, DG.Tweening.Plugins.Options.FloatOptions> sliderTween;
    DG.Tweening.Core.TweenerCore<string, string, DG.Tweening.Plugins.Options.StringOptions> timeTween;

    [Header("Create Reference")]
    [SerializeField] RectTransform createZone;
    [SerializeField] RectTransform finishLine;
    [SerializeField] float baseCreateDelay;
    [SerializeField] float createAcceleration;
    [SerializeField,ReadOnly] float createTime;
    [SerializeField,ReadOnly] float createDelay;
    
    [SerializeField] Balloon prefab;
    [SerializeField] RectTransform field;
    [SerializeField] List<Sprite> balloonResource;
    [SerializeField] List<Color> balloonColors;
    [SerializeField] List<Balloon> fieldObjects;
    [SerializeField] SpriteAnimation touchEffect;

    [SerializeField] float speed;
    [SerializeField] float baseSpeed = 50.0f;
    [SerializeField] float acceleration = 0.2f;

    public void Init(Canvas canvas)
    {
        this.canvas = canvas;
        SetCanvas();
        if (fieldObjects != null)
        {
            for(int i = 0; i < fieldObjects.Count; i++)
            {
                fieldObjects[i].UnActive();
            }
        }
        else
        {
            fieldObjects = new List<Balloon>();
        }
        currentTime = limitTime;
        slider.value = 0.0f;
        isPlaying = false;
        score = 0;
        speed = baseSpeed;
        createTime = 0;
        createDelay = baseCreateDelay;

        if(sliderTween != null)
        {
            sliderTween.Kill(false);
            sliderTween = null;
        }
        if(timeTween != null)
        {
            timeTween.Kill(false);
            timeTween = null;
        }

        sliderTween = null;
        timeTween = null;
        timerFlag = false;
    }

    void SetCanvas()
    {
        raycaster = this.canvas.GetComponent<GraphicRaycaster>();
    }

    #region UI reference
    void UpdateTimeUI(Slider slider,Text timer,float max, float current)
    {
        if(sliderTween == null)
        {
            float value = current / max;
            float difference = Mathf.Abs(slider.value - value) * 2;
            sliderTween = slider.DOValue(value, difference);
            sliderTween.SetEase(Ease.Linear);
            sliderTween.OnComplete(
            ()=>
            {
                sliderTween = null;
                timerFlag = true;
            });
        }
        if(timeTween == null && timerFlag)
        {
            timeTween = timer.DOText(((int)current).ToString(), 0.05f).
            OnComplete(() =>
            {
                timeTween = null;
            });
        }
        else if(timerFlag == false)
        {
            timer.text = LanguageManager.instance.GetString("TCode_N7");
        }
    }
    #endregion
    #region Play reference
    void IncreaseTime(int time)
    {
        currentTime += time;
        UpdateTimeUI(slider,timer,limitTime,currentTime);
    }
    void IncreaseScore(int combo)
    {
        if (MaxCombo < combo) combo = MaxCombo;
        score = score + baseScore * ((combo / 5) + 1);
        scoreText.text = score.ToString();
    }
    void IncreaseCombo(ref int combo)
    {
        combo++;
    }
    void DecreaseScore(int _score)
    {
        score = score - _score;
        scoreText.text = score.ToString();
    }
    void ClearCombo(ref int combo)
    {
        combo = 0;
    }
    void CreateBalloon()
    {
        #region Set balloon obeject : Monobehaviour
        Balloon balloon = fieldObjects.Find(delegate (Balloon b)
        {
            return b.IsPlaying == Balloon.State.Disable;
        });
        if(balloon == null)
        {
            balloon = Instantiate(prefab);
            balloon.transform.SetParent(field);
            balloon.transform.localScale = Vector3.one;
            fieldObjects.Add(balloon);
            balloon.Active();
        }
        else
        {
            balloon.Active();
        }
        #endregion
        SetBalloon(balloon);
    }
    void SetBalloon(Balloon balloon)
    {
        int rand = UnityEngine.Random.Range(0, 1000);
        Vector3 startPos = new Vector3(
            Random.Range(createZone.localPosition.x - createZone.rect.width * .5f, createZone.localPosition.x + createZone.rect.width * .5f),
            Random.Range(createZone.localPosition.y - createZone.rect.height * .5f, createZone.localPosition.y + createZone.rect.height * .5f),
            0
            );
        float finishLine = this.finishLine.localPosition.y;
        int spriteIndex = Random.Range(0, balloonResource.Count);

        // Normal
        if (0 <= rand && rand <= 850)
        {
            balloon.Set(balloonResource[spriteIndex], balloonColors[spriteIndex], Balloon.eType.Normal, startPos, finishLine, speed, OnClickBalloon, OnFinishBalloon);
        }
        // IncreaseTime
        else if (851 <= rand && rand <= 900)
        {
            balloon.Set(balloonResource[spriteIndex], balloonColors[spriteIndex], Balloon.eType.IncreaseTime, startPos, finishLine, speed, OnClickBalloon, OnFinishBalloon);
        }
        // Trap
        else
        {
            balloon.Set(balloonResource[spriteIndex], balloonColors[spriteIndex], Balloon.eType.Trap, startPos, finishLine, speed, OnClickBalloon, OnFinishBalloon);
        }

        // ballon size set
        float randSize = Random.Range(0.4f, 1.1f);
        balloon.transform.localScale = Vector3.one * randSize;

    }
    void OnFinishBalloon(Balloon balloon)
    {
        balloon.UnActive();
        if(balloon.Type != Balloon.eType.Trap)
        {
            IncreaseTime(-1);
            ClearCombo(ref combo);
        }

        SetBalloon(balloon);
        balloon.Active();
    }
    void OnClickBalloon(Balloon balloon)
    {
        switch(balloon.Type)
        {
            case Balloon.eType.IncreaseTime:
                IncreaseTime(5);
                IncreaseScore(combo);
                IncreaseCombo(ref combo);
                SoundManager.instance.PlayOneSound(SoundManager.instance.increaseTime);
                break;
            case Balloon.eType.Normal:
                IncreaseScore(combo);
                IncreaseCombo(ref combo);
                SoundManager.instance.PlayOneSound(SoundManager.instance.touchBalloon);
                break;
            case Balloon.eType.Trap:
                IncreaseTime(-5);
                DecreaseScore(baseScore * 5);
                ClearCombo(ref combo);
                SoundManager.instance.PlayOneSound(SoundManager.instance.trap);
                break;
        }
        balloon.UnActive();

        // Create Touch Effect
        SpriteAnimation animation = Instantiate<SpriteAnimation>(touchEffect);
        animation.SetEventAnimation(balloon.color, EffectDestory);
        animation.transform.SetParent(field);
        animation.transform.localPosition = balloon.transform.localPosition;
        animation.transform.localScale = balloon.transform.localScale * 2f;
    }
    void EffectDestory(GameObject game)
    {
        Destroy(game);
    }
    #endregion
    #region External function
    public void Refresh()
    {
        currentTime = 0;
        score = 0;
        UpdateTimeUI(slider, timer,limitTime, currentTime);
    }
    public void GameStart()
    {
        isPlaying = true;
    }
    public void GameEnd()
    {
        isPlaying = false;
        for(int i = 0; i < fieldObjects.Count; i++)
        {
            fieldObjects[i].UnActive();
        }

        // 리워드 결과 셋팅
        GetComponent<DestroyEvent>().SetRewardLevel(GetScoreLevel());

        Destroy(this.gameObject);
    }
    #endregion
    int GetScoreLevel()
    {
        if (300 <= score && score < 600) return 1;
        else if (600 <= score && score < 1100) return 2;
        else if (1100 <= score) return 3;
        else return 0;
    }
    Balloon CheckTouch()
    {
        PointerEventData ped = new PointerEventData(null);
        ped.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        raycaster.Raycast(ped, results);

        if (results.Count <= 0) return null;
        else
        {
            for(int i = 0; i < results.Count; i++)
            {
                Balloon balloon = results[i].gameObject.GetComponent<Balloon>();
                if (balloon != null)
                {
                    if(balloon.IsPlaying == Balloon.State.Playing) return balloon;
                }
            }

            return null;
        }
    }

    public override void Pause()
    {
        base.Pause();

        if(fieldObjects != null)
        {
            for(int i = 0; i < fieldObjects.Count; i++)
            {
                fieldObjects[i].Pause();
            }
        }
    }

    public override void Continue()
    {
        base.Continue();

        if(fieldObjects != null)
        {
            for(int i = 0; i < fieldObjects.Count; i++)
            {
                fieldObjects[i].Active();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (IsPause) return;

        #region Check Touch
        if (isPlaying && Input.GetMouseButtonDown(0))
        {
            Balloon balloon = CheckTouch();
            if (balloon != null) balloon.OnClickEvent();
        }
        #endregion

        #region Check Time
        if (isPlaying)
        {
            currentTime -= Time.deltaTime;
            if (timerFlag == false) { currentTime = limitTime; }
            UpdateTimeUI(slider, timer,limitTime, currentTime);

            if(currentTime <= 0)
            {
                GameEnd();
            }
            else
            {
                createTime += Time.deltaTime;
                if (createDelay <= createTime)
                {
                    createTime -= createDelay;
                    createDelay -= createAcceleration;
                    if (createDelay < baseCreateDelay * .25f)
                    {
                        createDelay = baseCreateDelay * .25f;
                    }
                    CreateBalloon();
                }

                speed = speed + (acceleration * Time.deltaTime);
                if(baseSpeed * 3 < speed)
                {
                    speed = baseSpeed * 5;
                }
            }
        }
        #endregion
    }
}
