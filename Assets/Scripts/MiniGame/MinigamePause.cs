﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinigamePause : MonoBehaviour
{
    [Header("Languge Pack")]
    [SerializeField] Text @continue;
    [SerializeField] Text exit;

    [Header("Caching")]
    [SerializeField,ReadOnly] MiniGame minigame;
    [SerializeField, ReadOnly] StartGame startGame;

    public void Set(MiniGame minigame,StartGame startGame)
    {
        this.minigame = minigame;
        this.startGame = startGame;
        minigame.Pause();

        //= 2020.02.03 언어 대응 해야함.
        @continue.text = "Continue";
        exit.text = "Exit";
    }

    public void Continue()
    {
        minigame.Continue();
        Destroy(this.gameObject);
    }

    public void Exit()
    {
        //= Popup 추가
        PopupManager.instance.Open(Common.PopupCode.MinigameExit,
            "게임을 종료하면 보상을 받을 수 없습니다.\n정말로 종료하시겠습니까?",
            PopupManager.ButtonStyle.YesNo,
            PopupManager.MoveType.CenterPumping,
            delegate
            {
                DestroyEvent destroyEvent = minigame.GetComponent<DestroyEvent>();
                if (destroyEvent != null)
                {
                    destroyEvent.SetRewardLevel(-1);
                    startGame.GameEnd(-1);
                    BuildingManager.instance.UiSet.NpcManager.Enable();
                    Destroy(minigame.gameObject);
                    Destroy(this.gameObject);
                }
                else
                {
#if USE_DEBUG
                    Debug.Log("Not find Destory Event component");
#endif
                }
            },
            delegate
            {
                //= 팝업만 닫고 아무것도 하지 않음
            }, true);
    }
}
