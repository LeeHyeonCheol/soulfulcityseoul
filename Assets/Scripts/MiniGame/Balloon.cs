﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Balloon : MonoBehaviour
{
    public enum State
    {
        Playing,
        Pause,
        Disable
    }
    public enum eType
    {
        Normal,
        IncreaseTime,
        Trap
    }

    [Header("Info")]
    [SerializeField] Image image;
    [SerializeField] float speed;
    [SerializeField, ReadOnly] float finishLine;
    [SerializeField, ReadOnly] State isPlaying;

    [Header("other graphic")]
    [SerializeField] Image increase;
    [SerializeField] Image trap;

    [Header("Caching")]
    [SerializeField, ReadOnly] new Transform transform;
    [SerializeField, ReadOnly] Vector3 localPosition;

    UnityAction<Balloon> onFinishAction;
    UnityAction<Balloon> onClickEvent;

    #region Property List
    public State IsPlaying { get => isPlaying; }
    public eType Type { get; set; }
    public Image Image { get => image; }
    public Color color { get; set; }
    #endregion

    private void Start()
    {

    }
    void SetGraphic(eType type)
    {
        switch(type)
        {
            case eType.IncreaseTime:
                increase.gameObject.SetActive(true);
                trap.gameObject.SetActive(false);
                break;
            case eType.Trap:
                increase.gameObject.SetActive(false);
                trap.gameObject.SetActive(true);
                break;
            case eType.Normal:
                increase.gameObject.SetActive(false);
                trap.gameObject.SetActive(false);
                break;
        }
    }

    #region Extral function
    public void Set(Sprite sprite,Color color,eType type,Vector2 position,float finishLine,float speed,UnityAction<Balloon> onClickEvent,UnityAction<Balloon> onFinishLine)
    { 
        image.sprite = sprite;
        //image.SetNativeSize();

        this.Type = type;
        this.finishLine = finishLine;
        this.speed = speed;
        this.color = color;

        if (this.transform == null) transform = GetComponent<Transform>();
        this.transform.localPosition = position;
        localPosition = position;

        this.onClickEvent = onClickEvent;
        onFinishAction = onFinishLine;

        SetGraphic(type);
    }

    public void UnActive()
    {
        isPlaying = State.Disable;

        image.color = Common.ColorAlpha;
        increase.gameObject.SetActive(false);
        trap.gameObject.SetActive(false);
    }
    public void Pause()
    {
        isPlaying = State.Pause;
    }
    public void Active()
    {
        isPlaying = State.Playing;

        image.color = Color.white;
    }
    public void OnClickEvent()
    {
        onClickEvent?.Invoke(this);
    }
    #endregion


    private void Update()
    {
        if(isPlaying == State.Playing)
        {
            localPosition.y += Time.deltaTime * speed;
            if (finishLine <= localPosition.y)
            {
                onFinishAction?.Invoke(this);
            }
            transform.localPosition = localPosition;
        }
    }
}
