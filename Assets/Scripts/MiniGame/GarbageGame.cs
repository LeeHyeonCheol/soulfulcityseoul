﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GarbageGame : MiniGame
{
    [Header("UI Data")]
    [SerializeField] Text noticeText;
    [SerializeField] Text scoreText;
    [SerializeField] Text timer;
    [SerializeField] Slider slider;
    [SerializeField,ReadOnly] bool timerFlag;
    DG.Tweening.Core.TweenerCore<float, float, DG.Tweening.Plugins.Options.FloatOptions> sliderTween;
    DG.Tweening.Core.TweenerCore<string, string, DG.Tweening.Plugins.Options.StringOptions> timeTween;

    [Header("PlayTime")]
    [SerializeField] float maxTime;
    [SerializeField,ReadOnly] float currentTime;

    [Header("Animation Event")]
    [SerializeField] AnimationEvent successs;   // 성공
    [SerializeField] AnimationEvent fail;       // 실패

    [Header("Transform info")]
    [SerializeField] RectTransform range;
    [SerializeField] Transform parent;
    [SerializeField] List<Transform> humans;
    [SerializeField] Transform buildingPosition;

    [Header("Resource Data")]
    [SerializeField] List<Sprite> garbageResource;

    [Header("In Game Data")]
    [SerializeField,ReadOnly] List<Image> garbages;
    [SerializeField, ReadOnly] float score;
    [SerializeField, ReadOnly] bool isPlay;

    //= Tutorial
    Action tutorialCallback;

    private void Start()
    {
        Init();
    }

    #region External Function
    public void Init()
    {
        //= play update
        isPlay = false;

        //= Score update
        score = 0;
        UpdateScoreText();

        //= notice update
        TextSetting(noticeText, "Gabage1");

        //= garbages update
        garbages = new List<Image>();

        //= slider update
        slider.value = 0.0f;
        currentTime = maxTime;
        UpdateTimeUI(slider, timer, maxTime, currentTime, MoveHumans);

        //= building set
        SetRandBuild(buildingPosition);

        //= ui set
        UIManager.instance.CloseUI_Upper();

    }
    
    void TextSetting(Text text, string textCode)
    {
        ArabicText arabicText = text.GetComponent<ArabicText>();
        if (LanguageManager.instance.IsArabic() == false)
        {
            arabicText.enabled = false;
            text.alignment = TextAnchor.UpperLeft;
        }
        else
        {
            arabicText.enabled = true;
            text.alignment = TextAnchor.UpperRight;
        }

        if (LanguageManager.instance.IsArabic() == false)
            text.text = LanguageManager.instance.GetString(textCode);
        else
        {
            arabicText.Text = LanguageManager.instance.GetString(textCode);
        }
    }
    
    public void GameClear()
    {
        isPlay = false;

        //= 리워드 처리 ( 클리어 보상 )
        AnimationEvent @event = Instantiate(successs);
        @event.transform.SetParent(this.transform.parent);
        @event.transform.localPosition = Vector3.zero;
        @event.transform.localScale = Vector3.one;
        (@event.transform as RectTransform).sizeDelta = Vector2.zero;

        @event.SetCallBack(OnAD /* 승리 보상 */);

        void OnAD()
        {
            PopupManager.instance.Open(Common.PopupCode.CleaningRewardAD,
                "",
                PopupManager.ButtonStyle.AD,
                PopupManager.MoveType.CenterPumping,
                delegate
                {
                    //= 광고 시청
                    NetworkManager.instance.GetRewardCleaning(OnReward, BuildingManager.instance.CalculationAllLevels(true), true);
                },
                delegate
                {
                    //= 광고 안시청
                    NetworkManager.instance.GetRewardCleaning(OnReward, BuildingManager.instance.CalculationAllLevels(true), false);
                },
                true,
                -80.0f);
        }

        void OnReward(string nowGold,string addGold)
        {
            PopupManager.instance.Open(Common.PopupCode.CleaningRewardGold,
                "+ " + ulong.Parse(addGold).ToString("#,##0"),
                PopupManager.ButtonStyle.Ok,
                PopupManager.MoveType.CenterPumping,
                delegate
                {
                    //= 골드 삽입
                    UserManager.instance.GoldOnEvent += ulong.Parse(addGold);
                    

                    //= 2020 01 21 튜토리얼 콜백 추가
                    if(BuildingManager.instance.attackTutorial_finish == null)
                        Destroy(this.gameObject);
                    else
                    {
                        BuildingManager.instance.attackTutorial_finish?.Invoke();
                        BuildingManager.instance.attackTutorial_finish = null;
                        BuildingManager.instance.PrevScene();
                        Destroy(this.gameObject);
                    }
                    UIManager.instance.OpenUI_Upper();
                },
                delegate
                {
                    //= 골드 삽입
                    UserManager.instance.GoldOnEvent += ulong.Parse(addGold);
                    
                    //= 2020 01 21 튜토리얼 콜백 추가
                    if(BuildingManager.instance.attackTutorial_finish == null)
                        Destroy(this.gameObject);
                    else
                    {
                        BuildingManager.instance.attackTutorial_finish?.Invoke();
                        BuildingManager.instance.attackTutorial_finish = null;
                        BuildingManager.instance.PrevScene();
                        Destroy(this.gameObject);
                    }
                    UIManager.instance.OpenUI_Upper();
                },
                true,
                -80.0f);
        }
    }
    
    public void GameFail()
    {
        isPlay = false;

        UIManager.instance.OpenUI_Upper();

        //= 리워드 처리 ( 실패 보상 )
        AnimationEvent @event = Instantiate(fail);
        @event.transform.SetParent(this.transform.parent);
        @event.transform.localPosition = Vector3.zero;
        @event.transform.localScale = Vector3.one;
        (@event.transform as RectTransform).sizeDelta = Vector2.zero;

        @event.SetCallBack(OnReward /* 실패 보상 */);

        void OnReward()
        {
            Destroy(this.gameObject);
            UIManager.instance.OpenUI_Upper();
        }
    }
    #endregion
    void SetRandBuild(Transform parent)
    {
        Building building = BuildingManager.instance.CreateBuilding(UnityEngine.Random.Range(1, Common.BUDMaxCount));

        //= Create building levels
        int[] levels = new int[(int)Building.Kind.MaxSize];
        for (int i = 0; i < levels.Length; i++)
        {
            levels[i] = UnityEngine.Random.Range(1, Common.BUDMaxLevel);
        }

        //= Building info set
        building.OnlyLevelSet(levels);
        building.ConstructionGraphicSet(false);

        //= Building position set
        building.transform.SetParent(parent);
        building.transform.localPosition = Vector3.zero;
        building.transform.localScale = Vector3.one;
    }

    void MoveHumans()
    {
        Sequence animation = DOTween.Sequence();
        //= 짝수 : 왼쪽 / 홀수 : 오른쪽
        for(int i = 0; i < humans.Count; i++)
        {
            if(i % 2 == 0)
            {
                animation.Join(humans[i].DOLocalMoveX(range.rect.xMin - 400, 4));
            }
            else
            {
                animation.Join(humans[i].DOLocalMoveX(range.rect.xMax + 400, 4));
            }
        }
       
        animation.OnComplete(() => { isPlay = true; });
        closeButton.image.DOColor(Color.white, 1f).OnComplete(() =>
        {
            SetGarbage();
        });
        animation.Play();

        
    }
    void UpdateTimeUI(Slider slider, Text timer, float max, float current,Action sliderCallback = null)
    {
        if (sliderTween == null)
        {
            float value = current / max;
            float difference = Mathf.Abs(slider.value - value) * 2;
            sliderTween = slider.DOValue(value, difference);
            sliderTween.SetEase(Ease.Linear);
            sliderTween.OnComplete(
            () =>
            {
                sliderTween = null;
                timerFlag = true;

                sliderCallback?.Invoke();
            });
        }
        if (timeTween == null && timerFlag)
        {
            timeTween = timer.DOText(((int)current).ToString(), 0.05f).
            OnComplete(() =>
            {
                timeTween = null;
            });
        }
        else if (timerFlag == false)
        {
            timer.text = LanguageManager.instance.GetString("TCode_N7");
        }
    }
    void OnClickFieldGarbage(Image image)
    {
        float alpha = image.color.a;

        if (alpha <= 0.7)
        {
            //= 가비지 삭제 스코어 상승
            if (garbages != null)
            {
                garbages.Remove(image);
                Destroy(image.gameObject);

                score += 100;
                UpdateScoreText();

                //= 쓰레기 모두 치우면 종료
                if (garbages.Count == 0) GameClear();
            }
        }
        else
        {
            //= 알파값 감소
            Color color = image.color;
            color.a -= 0.25f;

            image.DOColor(color, 0.02f);
        }
    }
    void UpdateScoreText()
    {
        scoreText.DOText(score.ToString(), .5f);
    }

    //= 이제 사용하지 않음 [ 2020.02.16 ]
    int GetScoreLevel()
    {
        if (garbages.Count == 0) return 3;
        else if (garbages.Count < 3) return 2;
        else return 1;
    }

    void SetGarbage()
    {
        for(int i = 0; i < UnityEngine.Random.Range(12, 17); i++)
        {
            Vector2 pos = new Vector2(
                UnityEngine.Random.Range(range.rect.xMin, range.rect.xMax),
                UnityEngine.Random.Range(range.rect.yMin, range.rect.yMax));
            CreateGarbage(pos);
        }
    }

    void CreateGarbage(Vector2 pos)
    {
        int spriteRand = UnityEngine.Random.Range(0, garbageResource.Count);
        Sprite sprite = garbageResource[spriteRand];

        //= 오브젝트 생성
        GameObject garbage = new GameObject("Garbage");

        //= 이미지 셋팅
        Image image = garbage.AddComponent<Image>();
        image.sprite = sprite;
        image.SetNativeSize();

        //= 버튼 삽입
        Button button = garbage.AddComponent<Button>();
        button.targetGraphic = image;
        button.onClick.AddListener(()=> { OnClickFieldGarbage(image); });

        //= 좌표 삽입
        garbage.transform.SetParent(this.parent);
        garbage.transform.localScale = Vector3.one;
        garbage.transform.localPosition = pos;

        //= 리스트 삽입
        garbages.Add(image);
    }

    private void Update()
    {
        if (isPlay == false) return;
        if (isPause) return;

        currentTime -= Time.deltaTime;
        if(currentTime <= 0)
        {
            UpdateTimeUI(slider, timer, maxTime, 0);
            GameFail();
        }
        else
        {
            UpdateTimeUI(slider, timer, maxTime, currentTime);
        }
    }
}
