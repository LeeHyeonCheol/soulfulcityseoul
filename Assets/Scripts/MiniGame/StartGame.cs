﻿using System.Collections;
using System.Collections.Generic;
using System;

using UnityEngine;
using HoneyPlug.Audio;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{
    [SerializeField] Canvas parent;
    [SerializeField] List<GameObject> otherUiGroup;
   
    [SerializeField] GameObject minigame_1;
    [SerializeField] GarbageGame minigame_2;
    [SerializeField] GameObject minigame_3;
    [SerializeField] TouchBalloon balloon;

    [SerializeField] MinigamePause pause;
    [SerializeField] bool isPlaying = false;

    #region Property List
    public bool IsPlaying { get => isPlaying; }
    #endregion

    #region Button event function
    public DestroyEvent GameStart_Ballon()
    {
        GameStart();
        TouchBalloon game = Instantiate(balloon);
        game.transform.SetParent(parent.transform);
        game.transform.localPosition = Vector3.zero;
        game.transform.localScale = Vector3.one;

        MinigamePause _pause = null;
        game.CloseButton.onClick.AddListener(() => { _pause = CreatePause(game); });


        DestroyEvent gameEndEvent = game.gameObject.AddComponent<DestroyEvent>();
        gameEndEvent.AddEvent(GameEnd);
        gameEndEvent.AddEvent(delegate
        {
            if(_pause != null)
            {
                Destroy(_pause.gameObject);
                _pause = null;
            }
        });

        game.Init(parent);
        game.GameStart();
        return gameEndEvent;
    }
    public DestroyEvent GameStart_Minigame_1()
    {
        GameStart();
        GameObject game = Instantiate(minigame_1);
        game.transform.SetParent(parent.transform);
        game.transform.localPosition = Vector3.zero;
        game.transform.localScale = Vector3.one;

        MiniGame minigame = game.GetComponent<MiniGame>();
        MinigamePause _pause = null;
        minigame.CloseButton.onClick.AddListener(() => { _pause = CreatePause(minigame); });
     
        DestroyEvent gameEndEvent = game.AddComponent<DestroyEvent>();
        gameEndEvent.AddEvent(GameEnd);
        gameEndEvent.AddEvent(delegate
        {
            if (_pause != null)
            {
                Destroy(_pause.gameObject);
                _pause = null;
            }
        });

        return gameEndEvent;
    }
    public DestroyEvent GameStart_Minigame_2()
    {
        GameStart();
        GarbageGame game = Instantiate(minigame_2);
        game.transform.SetParent(parent.transform);
        game.transform.localPosition = Vector3.zero;
        game.transform.localScale = Vector3.one;
        (game.transform as RectTransform).sizeDelta = (parent.transform as RectTransform).sizeDelta;

        MiniGame minigame = game.GetComponent<MiniGame>();
        MinigamePause _pause = null;
        minigame.CloseButton.onClick.AddListener(() => { _pause = CreatePause(minigame); });

        DestroyEvent gameEndEvent = game.gameObject.AddComponent<DestroyEvent>();
        gameEndEvent.AddEvent(GameEnd);
        gameEndEvent.AddEvent(delegate
        {
            if (_pause != null)
            {
                Destroy(_pause.gameObject);
                _pause = null;
            }
        });

        return gameEndEvent;
    }
    public DestroyEvent GameStart_Minigame_3()
    {
        GameStart();
        GameObject game = Instantiate(minigame_3);
        game.transform.SetParent(parent.transform);
        game.transform.localPosition = Vector3.zero;
        game.transform.localScale = Vector3.one;

        MiniGame minigame = game.GetComponent<MiniGame>();
        MinigamePause _pause = null;
        minigame.CloseButton.onClick.AddListener(() => { _pause = CreatePause(minigame); });

        DestroyEvent gameEndEvent = game.AddComponent<DestroyEvent>();
        gameEndEvent.AddEvent(GameEnd);
        gameEndEvent.AddEvent(delegate
        {
            if (_pause != null)
            {
                Destroy(_pause.gameObject);
                _pause = null;
            }
        });

        return gameEndEvent;
    }
    public void OtherUIGroupSet(bool isActive)
    {
        for (int i = 0; i < otherUiGroup.Count; i++)
        {
            otherUiGroup[i].SetActive(isActive);
        }
    }
   

    void GameStart()
    {
        OtherUIGroupSet(false);

        SoundManager.instance.PlayMusic(SoundManager.instance.minigame,.5f);
        isPlaying = true;
    }
    public void GameEnd(int rewardLevel)
    {
        OtherUIGroupSet(true);
        isPlaying = false;

        if (rewardLevel == -1)
        {
            BuildingManager.instance.UiSet.NpcManager.Enable();
            SoundManager.instance.PlayOneSound(SoundManager.instance.miniRewardSfx);
            SoundManager.instance.PlayMusic(SoundManager.instance.background_home, .5f);
            return;
        }

        PopupManager.instance.Open(998726, "", PopupManager.ButtonStyle.AD, PopupManager.MoveType.CenterPumping,
               delegate
               {
                   Debug.Log("View end ad");

                   NetworkManager.instance.GetRewardMiniGames(SuccessGetReward, BuildingManager.instance.CalculationAllLevels(true), rewardLevel,true);
               },
               delegate
               {
                   Debug.Log("View skip ad");

                   NetworkManager.instance.GetRewardMiniGames(SuccessGetReward, BuildingManager.instance.CalculationAllLevels(true), rewardLevel, false);
               }, true);
    }
    #endregion

    void SuccessGetReward(string currGold)
    {
        GameObject obj = Instantiate(Resources.Load<GameObject>("Prefabs/MinigameRewards"));
        obj.transform.SetParent(parent.transform);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localScale    = Vector3.one;

        ulong earnGold = Convert.ToUInt64(currGold) - UserManager.instance.GoldOffEvent;
        UserManager.instance.GoldOnEvent = Convert.ToUInt64(currGold);
        obj.GetComponent<UIMiniGameReward>().SetData(earnGold, BuildingManager.instance.UiSet.NpcManager.Enable);

        SoundManager.instance.PlayOneSound(SoundManager.instance.miniRewardSfx);
        SoundManager.instance.PlayMusic(SoundManager.instance.background_home, .5f);

        //= 삽
    }

    MinigamePause CreatePause(MiniGame minigame)
    {
        MinigamePause pause = Instantiate(this.pause);
        pause.Set(minigame,this);
        pause.transform.SetParent(parent.transform);
        pause.transform.localPosition = Vector3.zero;
        pause.transform.localScale = Vector3.one;

        return pause;
    }
}
