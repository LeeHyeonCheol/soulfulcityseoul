﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGame : MonoBehaviour
{
    [Header("Minigame reference")]
    [SerializeField] protected Button closeButton;
    [SerializeField] protected bool isPause;

    #region Property List
    public bool IsPause { get => isPause; set => isPause = value; }
    public Button CloseButton { get => closeButton; }
    #endregion

    public virtual void Pause()
    {
        IsPause = true;
    }

    public virtual void Continue()
    {
        IsPause = false;
    }

    public void SetCloseButtton(Action action)
    {
        if(closeButton != null)
        {
            closeButton.onClick.AddListener(()=> { action?.Invoke(); });
        }
        else
        {
#if USE_DEBUG
            Debug.Log("Null reference Close button");
#endif
        }
    }
}
