﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILanguage : MonoBehaviour
{
    [SerializeField]
    string textCode;
    Text content;

    void Start()
    {
        content = GetComponent<Text>();
        ArabicText arabicText = transform.GetComponent<ArabicText>();
        if(LanguageManager.instance.IsArabic() == false)
        {
            arabicText.enabled = false;
            content.alignment = TextAnchor.UpperCenter;
        }
        else
        {
            arabicText.enabled = true;
            content.alignment = TextAnchor.UpperRight;
        }
        
        string txt = LanguageManager.instance.GetString(textCode);
        if(LanguageManager.instance.IsArabic() == false)
            content.text = txt;
        else
            arabicText.Text = txt;
    }
}
