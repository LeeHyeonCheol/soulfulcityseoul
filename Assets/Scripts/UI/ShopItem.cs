﻿[System.Serializable]
public class ShopItem
{
    public string id;
    public string name;
    public int price;
    public string resName;
}
