﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldUI : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] Text textUI = null;
    [SerializeField] FadeText fadeText = null;

    [Header("Param")]
    [SerializeField] float fadeTextOffsetY = 30f;
    [SerializeField, ReadOnly] ulong goldBase = 0;


    public ulong GoldBase { get => goldBase; }

    private void Start()
    {
        UserManager.instance.GoldChange += AddGoldAnimation;
        goldBase = UserManager.instance.GoldOffEvent;
        TextSetting(goldBase);
    }

    private void OnDestroy()
    {

    }

    private void OnEnable()
    {
        TextSetting(UserManager.instance.GoldOffEvent);
    }

    void TextSetting(ulong gold)
    {
        string text = GoldConversion.GoldToString(gold);
        textUI.text = text;
    }

    void AddGoldAnimation(ulong gold)
    {
        Animation(
        (goldBase < gold) ? (gold - goldBase) : (goldBase - gold),  // gold
        (goldBase < gold) ? false : true);                        // negative
    }

    void Animation(ulong difference,bool isNegative)
    {
        goldBase = UserManager.instance.GoldOnEvent;
        CreateFadeText(difference, isNegative);
        TextSetting(goldBase);
    }

    void CreateFadeText(ulong gold,bool isNegative)
    {
        FadeText obj = Instantiate(fadeText);
        obj.transform.SetParent(transform);
        
        obj.transform.localScale = Vector3.one;

        // 살짝 상단에서 생성할 수 있도록 할것.
        obj.transform.localPosition = textUI.transform.localPosition + new Vector3(0, fadeTextOffsetY, 0);


        if (isNegative)
        {
            obj.Run("-" + GoldConversion.GoldToString(gold));
        }
        else
        {
            obj.Run(GoldConversion.GoldToString(gold));
        }
    }

}
