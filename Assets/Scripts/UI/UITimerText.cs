﻿using System.Collections;
using System.Collections.Generic;
using System;

using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Text))]
public class UITimerText : MonoBehaviour
{
    Text timer;
    bool started;
    float swFrame;
    int remainTotalSec;

    void Start()
    {
        timer = GetComponent<Text>(); started = false;
    }

    int index;
    Action<int> endCallback;
    public void StartTimer(long endTime, int index = 0, Action<int> endCallback = null)
    {
        this.index = index;

        DateTime endT  = HoneyPlug.Utils.Time.GetUniversalTimeByLongType(endTime);
        TimeSpan t     = endT - DateTime.UtcNow;
        remainTotalSec = (int)t.TotalSeconds; 

        if(timer == null) timer = GetComponent<Text>();
        timer.text = HoneyPlug.Utils.Time.FormatTimeHourMinSec(remainTotalSec);

        swFrame = 0;
        started = true;
    }

    public void StopTimer()
    {
        started = false;
    }

    void LateUpdate()
    {
        if(started)
        {
            swFrame += Time.deltaTime;
            if(swFrame >= 1f)
            {
                remainTotalSec--;
                if(remainTotalSec < 0)
                {
                    started = false; endCallback?.Invoke(index);
                }
                else
                {
                    timer.text = HoneyPlug.Utils.Time.FormatTimeHourMinSec(remainTotalSec);
                    swFrame = 0f;
                }
            }
        }   
    }
}
