﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMapViewBuildingItem : MonoBehaviour
{
    [SerializeField]
    GameObject[] icon;
    int id;
    int[] level;

    public void SetItemData(int id, int[] lv)
    {
        this.id = id; level = lv;

        //UpdateWorkerIcons();

        GameObject obj = Instantiate(Resources.Load<GameObject>("Prefabs/UIBuildItem/building " + ((id < 10) ? "0"+id.ToString() : id.ToString())));    
        obj.transform.SetParent(transform);
        obj.transform.localScale = new Vector3(0.8f, 0.8f, 1);
        obj.transform.position   = Vector3.zero;

        //= 그래픽 셋팅 수정
        BuildingOnlyShow onlyShow = obj.GetComponent<BuildingOnlyShow>();
        if(onlyShow != null)
        {
            onlyShow.OnColorChange = false;
            onlyShow.Set(Building.Kind.Nothing,
                level[(int)Building.Kind.MainBuilding],
                level[(int)Building.Kind.MainSculpture],
                level[(int)Building.Kind.SubSculpture],
                0 /* add order */);
        }
    }

    // public void UpdateWorkerIcons()
    // {
    //     string[] szWorkerId = NetworkManager.instance.GetWorkerFromBuildingId(id).Split(Common.splitWord);
    //     string[] szEndTime  = NetworkManager.instance.GetWorkerEndStrTimesByBUID(id); 
    //     for(int i = 0; i < 3; i++)
    //     {
    //         if(szWorkerId[i] == "0" || szEndTime[i] == "0") icon[i].SetActive(false);
    //         else 
    //         {
    //             icon[i].SetActive(true);
    //         }
    //     }
    // }

    // public void ClickSetWorker()
    // {
    //     int totalLevel = 0;
    //     for(int index = 0; index < (int)Building.Kind.MaxSize; index++)
    //     {
    //         totalLevel += level[index];
    //     }
    //     totalLevel /= (int)Building.Kind.MaxSize;
    //     if (totalLevel < 1) return;
        
    //     UIManager.instance.OpenUI_WorkerPopup(id, totalLevel);
    // }
}
