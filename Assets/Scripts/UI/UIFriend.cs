﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

using PlayFab.ClientModels;

public class UIFriend : UIPanelShow
{
    public override void Open(Action callBack, float dealy = 0f)
    {
        base.Open(callBack, dealy); selectIndex = 0;
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close(Action callBack, float dealy = 0f)
    {
        base.Close(callBack, dealy);
    }

    public void ClickClose()
    {   
        Close(null);
    }

    [SerializeField]
    Transform scrollCotent;
    [SerializeField]
    GameObject listItemObj;

    List<PlayerLeaderboardEntry> friends;
    List<PlayerLeaderboardEntry> recom_friends;
    int selectIndex;

    public void SetDataFriends(List<PlayerLeaderboardEntry> friends)
    {
        int i;

        this.friends = friends;
        for(i = 0; i < friends.Count; i++)
        {
            if(NetworkManager.instance.EqualMyId(friends[i].PlayFabId)) continue;

            GameObject obj = Instantiate(listItemObj);
            obj.transform.SetParent(scrollCotent);
            obj.transform.localScale = Vector3.one;

            obj.GetComponent<FriendListItem>().SetItemData(friends[i], 0);
        }
    }

    public void SetDataRecomFriends(List<PlayerLeaderboardEntry> recom_friends)
    {
        int i;

        this.recom_friends = recom_friends;
        for(i = 0; i < recom_friends.Count; i++)
        {
            if(NetworkManager.instance.EqualMyId(recom_friends[i].PlayFabId)) continue;
            if(friends.Find(item => item.PlayFabId == recom_friends[i].PlayFabId) != null) continue;

            GameObject obj = Instantiate(listItemObj);
            obj.transform.SetParent(scrollCotent);
            obj.transform.localScale = Vector3.one;

            obj.GetComponent<FriendListItem>().SetItemData(recom_friends[i], 1);
        }
    }

    public void ClickMyFriend()
    {
        if(selectIndex == 0) return;

        ClearList(0);
        NetworkManager.instance.GetMyFriendList(SetDataFriends);
    }

    public void ClickFriendRecommend()
    {
        if(selectIndex == 1) return;

        ClearList(1);
        NetworkManager.instance.GetRecommendFriend(SetDataRecomFriends);
    }

    void ClearList(int idx)
    {
        int i;
        selectIndex = idx;

        for(i = 0; i < scrollCotent.childCount; i++)
        {
            Destroy(scrollCotent.GetChild(i).gameObject);
        }
        //friends = null; recom_friends = null;
    }
}
