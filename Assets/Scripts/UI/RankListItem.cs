﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using PlayFab.ClientModels;

public class RankListItem : MonoBehaviour
{
    [SerializeField]
    Sprite[] rankBg;
    [SerializeField]
    Sprite[] rankPos;
    [SerializeField]
    Image rankIcon;
    [SerializeField]
    Text rankPosNum;
    [SerializeField]
    Image faceIcon;
    [SerializeField]
    Text nickName;
    [SerializeField]
    Text score;

    PlayerLeaderboardEntry info;

    public void SetItemData(PlayerLeaderboardEntry info)
    {
        this.info     = info;
        nickName.text = info.PlayFabId;
        score.text    = info.StatValue.ToString();

        Image bg = this.GetComponent<Image>();
        // Rank 1,2,3
        if(info.Position < 3)
        {
            bg.sprite = rankBg[1];
            rankIcon.gameObject.SetActive(true);
            rankIcon.sprite = rankPos[info.Position];
            rankPosNum.gameObject.SetActive(false);
        }
        else
        {
            bg.sprite = rankBg[0];
            rankIcon.gameObject.SetActive(false);
            rankPosNum.gameObject.SetActive(true);
            rankPosNum.text = (info.Position + 1).ToString();
        }
    }
}
