﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 빌딩들 레이케스트 켜져있는지 확인하기
/// </summary>

public class StealReward : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] RectTransform[] positions;
    [SerializeField] Text[] nicknames;
    [SerializeField] Text notice;
    [SerializeField] GameObject parant;

    [Header("Reward")]
    [SerializeField] List<KeyValuePair<Building, ulong>> buildingNGold;

    [Header("DB")]
    [SerializeField,ReadOnly] List<Building> buildingPrefabs;

    private void Start()
    {
        NetworkManager.instance.StealGoldFromOtherPlayer(Set);
        parant.gameObject.SetActive(false);
    }

    private void OnValidate()
    {
        if(positions.Length != Common.StealSelectedMaxLength)
        {
            System.Array.Resize(ref positions, Common.StealSelectedMaxLength);
#if USE_DEBUG
            Debug.LogError("Steal Reward의 positions size는 Common.StealSelectedMaxLength(" + Common.StealSelectedMaxLength + ")외의 값을 가질 수 없습니다.");
#endif
        }
        if(nicknames.Length != Common.StealSelectedMaxLength)
        {
            System.Array.Resize(ref nicknames, Common.StealSelectedMaxLength);
#if USE_DEBUG
            Debug.LogError("Steal Reward의 nicknames size는 Common.StealSelectedMaxLength(" + Common.StealSelectedMaxLength + ")외의 값을 가질 수 없습니다.");
#endif
        }

        //= DB(prefabs) Loading
        if (buildingPrefabs == null || buildingPrefabs.Count != Common.BUDMaxCount)
        {
            PrefabsLoading();
        }
    }
    private void PrefabsLoading()
    {
        buildingPrefabs = new List<Building>();

        var load = Resources.LoadAll<Building>(Common.BUDPrefabPath);
        buildingPrefabs.AddRange(load);
    }

    private void Close()
    {
        //= 상단 ui 활성화
        UIManager.instance.OpenUI_Upper();

        Destroy(this.gameObject);
    }

    public void Set(string pack)
    {
        //= 초기화 직전까지 비활성화.
        parant.gameObject.SetActive(true);

        //= 상단 ui 비활성화
        UIManager.instance.CloseUI_Upper();

        //= Set Notice
        SetNotice("STEAL_REWARD_NOTICE");

        //= UnPacking
        ulong[] haveGold = new ulong[Common.StealSelectedMaxLength];
        string[] nicks = new string[Common.StealSelectedMaxLength];

        Debug.Log(pack);

        string[] split = pack.Split(Common.splitWord);

#if USE_DEBUG
        if (split.Length != Common.StealSelectedMaxLength) Debug.LogError("Error packet : " + pack);
#endif

        //= nickname,money|nickname,money|nickname,money
        for(int i = 0; i < split.Length; i++)
        {
            string[] nickNGold = split[i].Split(',');
#if USE_DEBUG
            if (nickNGold == null || nickNGold.Length != 2) Debug.LogError("Error pack : " + pack);
#endif
            nicks[i] = nickNGold[0];
            haveGold[i] = ulong.Parse(nickNGold[1]);
        }

        //= Create & Set
        SetUserNickname(nicks);
        SetBuildingNGold(ref buildingNGold, haveGold);
    }

    //= 안내문 텍스트 셋팅
    void SetNotice(string textCode)
    {
        if(LanguageManager.instance != null)
        {
            notice.text = LanguageManager.instance.GetString(textCode);
        }
    }

    //= 유저 닉네임 셋팅
    void SetUserNickname(string[] nicknames)
    {
        for(int i = 0; i < this.nicknames.Length; i++)
        {
            this.nicknames[i].text = nicknames[i];
        }
    }

    //= 빌딩 랜덤 생성
    void SetBuildingNGold(ref List<KeyValuePair<Building,ulong>> BNG,ulong[] haveGold)
    {
        Clear(ref BNG);
        BNG = new List<KeyValuePair<Building, ulong>>();

        //= TODO : 진행중인 위치
        int randID = Random.Range(0, Common.BUDMaxCount) + 1;
        
        for(int i = 0; i < Common.StealSelectedMaxLength; i++)
        {
            int id = Random.Range(randID - 1, randID + 1);
            if (id == 0) id = 1;
            else if (Common.BUDMaxCount < id) id = Common.BUDMaxCount;

            Building building = CreateBuilding(id);
            if(building != null)
            {
                #region Create Random Building
                int[] levels = new int[(int)Building.Kind.MaxSize];
                for(int index = 0; index < (int)Building.Kind.MaxSize; index++)
                {
                    levels[index] = Random.Range(0, Common.BUDMaxLevel) + 1;
                }

                #endregion
                SetBuilding(building, levels, haveGold[i],positions[i]);
                InsertClickEvent(building);
                BNG.Add(new KeyValuePair<Building, ulong>(building, haveGold[i]));
                building.gameObject.SetActive(true);
            }
        }
    }

    void Clear(ref List<KeyValuePair<Building, ulong>> BNG)
    {
        if (BNG != null && BNG.Count != 0)
        {
            for(int i = 0; i <BNG.Count; i++)
            {
                Destroy(BNG[i].Key.gameObject);
            }
        }
    }

    //= 빌딩 오브젝트 생성 및 반환
    Building CreateBuilding(int id)
    {
        if(buildingPrefabs == null && buildingPrefabs.Count != Common.BUDMaxCount)
        {
            PrefabsLoading();
        }

        Building building = buildingPrefabs.Find(delegate (Building v)
        {
            return v.ID == id;
        });

#if USE_DEBUG
        if (building == null) Debug.LogError("Not Find building data");
#endif

        if (building != null)
        {
            return Instantiate(building);
        }
        else return null;
    }

    //= 빌딩 정보 셋팅
    void SetBuilding(Building building,int[] level,ulong haveGold,RectTransform parent)
    {
        building.transform.SetParent(parent);
        building.transform.localScale = Vector3.one;
        building.transform.localPosition = Vector3.zero;
        building.gameObject.name = haveGold.ToString();
        building.OnlyLevelSet(level);
    }

    //= 클릭 이벤트 삽입
    void InsertClickEvent(Building building)
    {
        //= 이미지 삽입
        GameObject trigger = new GameObject("Trigger");
        trigger.transform.SetParent(building.transform);
        trigger.transform.localPosition = Vector3.zero;
        trigger.transform.localScale = Vector3.one * 10;

        Image image = trigger.gameObject.GetComponent<Image>();
        if(image == null)
        {
            image = trigger.gameObject.AddComponent<Image>();
        }
        image.color = new Color(0, 0, 0, 1f / 255f);

        //= 버튼 삽입
        Button button = trigger.gameObject.GetComponent<Button>();
        if(button == null)
        {
            button = trigger.gameObject.AddComponent<Button>();
        }

        button.targetGraphic = image;
        button.interactable = true;
        Navigation navigation = new Navigation();
        navigation.mode = Navigation.Mode.None;
        button.navigation = navigation;
        button.transition = Selectable.Transition.None;

        //= 이벤트 삽입
        button.onClick.AddListener(delegate { OnClickEvent(building); });

        
    }

    //= 클릭 리워드 반환
    void OnClickEvent(Building building)
    {
        //= 등수 비교
        ulong maxGold = 0;
        for(int i = 0; i < buildingNGold.Count; i++)
        {
            ulong gold = buildingNGold[i].Value;
            if(maxGold < gold)
            {
                maxGold = gold;
            }
        }
        ulong haveGold = buildingNGold.Find(delegate (KeyValuePair<Building,ulong> pair)
        {
            return pair.Key == building;
        }).Value;

        //= 성공
        if(maxGold <= haveGold)
        {
            PopupManager.instance.Open(Common.PopupCode.StealRewardAD, "",
                PopupManager.ButtonStyle.AD,
                PopupManager.MoveType.CenterPumping,
                delegate
                {
                    //= 서버로 통신 보상x2
                    NetworkManager.instance.RewardStealGold(maxGold.ToString(), CallbackServer,true);
                    Debug.Log("True Shot!");
                },
                delegate
                {
                    //= 서버로 통신 보상[normal]
                    NetworkManager.instance.RewardStealGold(maxGold.ToString(), CallbackServer, false);
                }, true);
           
        }
        //= 실패
        else
        {
            FailSelected();
        }
    }

    void CallbackServer(ulong reward)
    {
        //= 팝업 생성
        string format = LanguageManager.instance.GetString("STEAL_SUCCESS_FORMAT");
        string notice = string.Format(format, reward.ToString());

        PopupManager.instance.Open(Common.PopupCode.StealReward,
            notice,
            PopupManager.ButtonStyle.Ok,
            PopupManager.MoveType.CenterPumping,
            GetReward,
            null,
            true);

        void GetReward()
        {
            //= 상단 ui 활성화
            UIManager.instance.OpenUI_Upper();
            //= 해당 수치만큼 골드 획득
            UserManager.instance.GoldOnEvent += reward;

            Close();
        }
    }

    void FailSelected()
    {
        //= 팝업 생성
        PopupManager.instance.Open(Common.PopupCode.StealReward,
            "STEAL_FAIL",
            PopupManager.ButtonStyle.Ok,
            PopupManager.MoveType.CenterPumping,
            Close);
    }

}
