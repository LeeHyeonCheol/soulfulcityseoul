﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Kind = Building.Kind;

public class BuildingFromMapView : MonoBehaviour
{
    [Header("Prefab Data")]
    [SerializeField] Building.ObjectData objects;
    [SerializeField] protected GameObject lockImage = null;
    [SerializeField] protected Animation ani = null;
    [Header("Server Data")]
    [SerializeField, ReadOnly] protected int[] level = new int[(int)Kind.MaxSize];

    bool IsLevelZero
    {
        get { return ((level[(int)Kind.MainBuilding] + level[(int)Kind.MainSculpture] + level[(int)Kind.SubSculpture]) == 0); }
    }

    public void GraphicSet(int[] lv)
    {
        level = lv;

        //= 오브젝트 비활성화 작업
        for (int i = 0; i < (int)Kind.MaxSize; i++)
        {
            for (int j = 0; j < Common.BUDMaxLevel; i++)
            {
                objects[i][j].SetActive(false);
            }
        }
        if (IsLevelZero)
        {
            lockImage.gameObject.SetActive(true);

            ani?.Stop();
        }
        else
        {
            lockImage.gameObject.SetActive(false);

            for (int i = 0; i < (int)Kind.MaxSize; i++)
            {
                objects[i/* 종류 */][level[i] /* 레벨 */].SetActive(true);
            }

            ani?.Play();
        }
    }
}
