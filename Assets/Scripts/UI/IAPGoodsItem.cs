﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPGoodsItem : MonoBehaviour
{
    public void ClickPurchase(UnityEngine.Purchasing.Product e)
    {
        StoreIAP.instance.ProcessPurchase(e, SuccessEndPurchase);
    }

    void SuccessEndPurchase(string productId)
    {
        NetworkManager.instance.ProcPurchaseItems(productId, SuccessEndPurchase);
    }

    void SuccessEndPurchase(ulong gold, int spin)
    {
        UserManager.instance.GoldOnEvent = gold;
        GameDataManager.instance.Roulette.SetUseCount(spin);

        PopupManager.instance.Open(Common.PopupCode.SuccessPurchase, "BUY_OK", PopupManager.ButtonStyle.Ok, 
                PopupManager.MoveType.CenterPumping, null, null);
    }
}
