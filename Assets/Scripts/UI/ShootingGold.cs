﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShootingGold : MonoBehaviour
{
    [Header("Object Info")]
    [SerializeField] Sprite gold;
    [SerializeField] ulong rewardGold;
    [SerializeField] Transform target;

    [Header("Animation Info")]
    [SerializeField] ulong count;
    [SerializeField] float duration;
    [SerializeField] float delay;
    [SerializeField] Vector2 range;

    Coroutine goldRoutine;
    List<DG.Tweening.Core.TweenerCore<Vector3, Vector3, DG.Tweening.Plugins.Options.VectorOptions>> dotweens;
    

    /// <summary>
    /// 초기화 함수
    /// </summary>
    /// <param name="rewardGold"> 보상 금액 ( 추가되는 골드 )</param>
    /// <param name="target"> 골드가 도착할 좌표 </param>
    /// <param name="count"> 분할 횟수 </param>
    /// <param name="duration"> 날아가는데 걸리는 시간 </param>
    /// <param name="delay"> 각각 코인 생성간의 딜레이 </param>
    /// <param name="randRange"> 코인이 생성되는 범위 ( 랜덤 값 -x ~ x , -y ~ y )</param>
    public void Init(ulong rewardGold,Transform target, ulong count,float duration,float delay,Vector2 randRange)
    {
        this.rewardGold = rewardGold;
        this.target = target;
        this.count = count;
        this.duration = duration;
        this.delay = delay;
        this.range = randRange;
    }

    private void OnDisable()
    {
        if(goldRoutine != null)
        {
            StopCoroutine(goldRoutine);
        }

        if(dotweens != null)
        {
            for(int i = 0; i < dotweens.Count; i++)
            {
                if(dotweens[i].IsComplete() == false)
                {
                    dotweens[i].Kill(true);
                }
            }
            dotweens = null;
        }
    }
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            if (goldRoutine != null)
            {
                StopCoroutine(goldRoutine);
            }

            if (dotweens != null)
            {
                for (int i = 0; i < dotweens.Count; i++)
                {
                    if (dotweens[i].IsComplete() == false)
                    {
                        dotweens[i].Kill(true);
                    }
                }
                dotweens = null;
            }
        }
    }

    public void OnClickTrigger(Vector3 position)
    {
        if(goldRoutine == null) goldRoutine = StartCoroutine(Shooting(position));
    }

    IEnumerator Shooting(Vector3 position)
    {
        WaitForSeconds wait = new WaitForSeconds(delay);
        dotweens = new List<DG.Tweening.Core.TweenerCore<Vector3, Vector3, DG.Tweening.Plugins.Options.VectorOptions>>();

        for (ulong i = 0; i < count; i++)
        {
            GameObject goldObj = new GameObject("Gold : " + i);
            goldObj.transform.SetParent(this.transform);
            goldObj.transform.localScale = Vector3.one;
            goldObj.transform.position = position + new Vector3(Random.Range(-range.x, range.x), Random.Range(-range.y, range.y));
            Image image = goldObj.AddComponent<Image>();
            image.sprite = gold;
            image.SetNativeSize();
            dotweens.Add(image.transform.DOMove(target.position, duration)
                .OnComplete(()=>
                {
                    UserManager.instance.GoldOnEvent += (rewardGold)/ count;
                    Destroy(image.gameObject);
                }));
            yield return wait;
        }

        goldRoutine = null;
    }
}
