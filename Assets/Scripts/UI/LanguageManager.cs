﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.U2D;

using PlayFab.Internal;

[Serializable]
public class LanguageData
{
    public string code;
    public string kr;
    public string en;
    public string chn_z;
    public string chn_b;
    public string japanese;
    public string vietnamese;
    public string Portuguese;
    public string Spanish;
    public string French;
    public string German;
    public string Russain;
    public string Indonesian;
    public string Thai;
    public string Arabic;
}

public class LanguageManager : SingletonMonoBehaviour<LanguageManager>
{
    [SerializeField]
    TextAsset languageDat;

    LanguageData[] datas;

    public int langCode;

    private void Start()
    {
        InitData();
    }

    public void InitData()
    {
        datas = JsonHelper.FromJson<LanguageData>(languageDat.ToString());

        langCode = PlayerPrefs.GetInt("LangCode", -1);
        if(langCode == -1)
        {
            langCode = (int)Application.systemLanguage;
        }
    }

    public string GetString(string code)
    {
        int i;

        for(i = 0; i < datas.Length; i++)
        {
            if(datas[i].code == code)
            {
                switch(langCode)
                {
                    case (int)SystemLanguage.Korean: return datas[i].kr;
                    case (int)SystemLanguage.English: return datas[i].en;
                    case (int)SystemLanguage.ChineseSimplified: return datas[i].chn_z;
                    case (int)SystemLanguage.ChineseTraditional: return datas[i].chn_b;
                    case (int)SystemLanguage.Japanese: return datas[i].japanese;
                    case (int)SystemLanguage.Vietnamese: return datas[i].vietnamese;
                    case (int)SystemLanguage.Portuguese: return datas[i].Portuguese;
                    case (int)SystemLanguage.Spanish: return datas[i].Spanish;
                    case (int)SystemLanguage.French: return datas[i].French;
                    case (int)SystemLanguage.German: return datas[i].German;
                    case (int)SystemLanguage.Russian: return datas[i].Russain;
                    case (int)SystemLanguage.Indonesian: return datas[i].Indonesian;
                    case (int)SystemLanguage.Thai: return datas[i].Thai;
                    case (int)SystemLanguage.Arabic: return datas[i].Arabic;

                    default: return datas[i].en;
                }                
            }
        }

        return "None Text";
    }

    public void SetLanguageCode(int langNum)
    {
        langCode = langNum;
        PlayerPrefs.SetInt("LangCode", langNum);
    }

    public string CurrLanguageTitle()
    {
        switch(langCode)
        {
            case (int)SystemLanguage.Korean: return "한국어";
            case (int)SystemLanguage.English: return "English";
            case (int)SystemLanguage.ChineseSimplified: return "簡體字";
            case (int)SystemLanguage.ChineseTraditional: return "繁体字";
            case (int)SystemLanguage.Japanese: return "日本語";
            case (int)SystemLanguage.Vietnamese: return "Tiếng Việt";
            case (int)SystemLanguage.Portuguese: return "Português";
            case (int)SystemLanguage.Spanish: return "Español";
            case (int)SystemLanguage.French: return "français";
            case (int)SystemLanguage.German: return "Deutsche";
            case (int)SystemLanguage.Russian: return "ру́сский";
            case (int)SystemLanguage.Indonesian: return "bahasa Indonesia";
            case (int)SystemLanguage.Thai: return "ไทย";
            case (int)SystemLanguage.Arabic: return "عربى";
        }

        return "English";
    }

    public bool IsArabic()
    {
        return langCode == (int)SystemLanguage.Arabic;
    }
}
