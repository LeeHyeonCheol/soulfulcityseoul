﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PopupUI : MonoBehaviour
{
    [HideInInspector] public new RectTransform transform = null;

    [Header("UI")]
    [SerializeField] Text notice = null;
    [SerializeField] Button mainButton = null; 
    [SerializeField] Button subButton = null;
    [SerializeField] Text mainButtonText = null;
    [SerializeField] Text subButtonText = null;
    [SerializeField] GameObject lockClick = null;

    [Header("Button Position")]
    [SerializeField] Transform leftPos = null;
    [SerializeField] Transform centerPos = null;
    [SerializeField] Transform rightPos = null;

    [Header("Flag")]
    [SerializeField, ReadOnly] public bool isAD;
    [SerializeField, ReadOnly] bool runFlag;
    [SerializeField, ReadOnly] uint kindCode;

    #region Routine
    Sequence openSeq;
    Sequence closeSeq;
    #endregion
    #region Property
    public Button MainButton { get => mainButton; set => mainButton = value; }
    public Button SubButton { get => subButton; set => subButton = value; }
    public Text MainButtonText { get => mainButtonText; set => mainButtonText = value; }
    public Text SubButtonText { get => subButtonText; set => subButtonText = value; }
    public Text Notice { get => notice; set => notice = value; }
    public bool RunFlag { get => runFlag; set => runFlag = value; }
    public uint KindCode { get => kindCode; set => kindCode = value; }

    public Transform LeftPos { get => leftPos; }
    public Transform CenterPos { get => centerPos; }
    public Transform RightPos { get => rightPos; }
    public GameObject LockClick { get => lockClick; }    
    #endregion
        
    public void Start()
    {
        if(transform == null)
        {
            transform = GetComponent<RectTransform>();
        }
        runFlag = true;
    }
    public void Close()
    {
        runFlag = false;
        closeSeq.Play();
    }
    public void Run(Sequence open, Sequence close)
    {
        #region init
        openSeq = open;
        closeSeq = close;

        openSeq.Pause();
        closeSeq.Pause();
        #endregion

        open.Play();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && runFlag)
        {
            Close();
        }
    }
}
