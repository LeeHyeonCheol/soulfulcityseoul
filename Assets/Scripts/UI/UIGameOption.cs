﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;

using HoneyPlug.Audio;

public class UIGameOption : UIPanelShow
{
    public override void Open(Action callBack, float dealy = 0f)
    {
        base.Open(callBack, dealy);

        InitOption(); InitLanguage();
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close(Action callBack, float dealy = 0f)
    {
        base.Close(callBack, dealy);
    }

    public void CloseButton()
    {
        base.Close();
    }

    [SerializeField]
    GameObject[] popupWindow;
    [SerializeField]
    Text currLang;

    public void ClickLoginSocial()
    {
        //FBController.instance.LogOnFacebook();
    }

    public void ClickLanguage()
    {
        popupWindow[0].SetActive(false); popupWindow[1].SetActive(true);
    }

    public void ClickSelectLang(int langNum)
    {
        LanguageManager.instance.SetLanguageCode(langNum);
        CloseSelectLang();
    }

    public void CloseSelectLang()
    {
        popupWindow[1].SetActive(false); popupWindow[0].SetActive(true);
        InitLanguage();
    }

    public void ClickHelp()
    {
        UIManager.instance.OpenUI_TutorialBook();
    }

    public void ClickCredit()
    {

    }
    // 개인 정보 동의 안내
    public void ClickPrivacy()
    {
        Application.OpenURL("https://honeyplug.cafe24.com/");
    }

    [SerializeField]
    Slider soundSlider;
    [SerializeField]
    Slider musicSlider;

    void InitOption()
    {
        musicSlider.value = PlayerPrefs.GetFloat("MusicVol", 1);
        soundSlider.value = PlayerPrefs.GetFloat("SndVol", 1);
    }

    void InitLanguage()
    {
        currLang.text = LanguageManager.instance.CurrLanguageTitle();
    }

    public void ValueChangeSound()
    {
        print("ValueChangeSound " + soundSlider.value);

        SoundManager.instance.PlayOneSound(SoundManager.instance.rewardSfx);
        SoundManager.instance.SetSFXVolume(soundSlider.value);
    }

    public void ValueChangeMusic()
    {
        print("ValueChangeMusic " + musicSlider.value);

        //SoundManager.instance.PlayMusic(SoundManager.instance.background_home, 0.5f);
        SoundManager.instance.SetMusicVolume(musicSlider.value);
    }
}
