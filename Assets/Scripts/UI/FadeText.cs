﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeText : MonoBehaviour
{
    [SerializeField] Text textUI;

    [Header("param")]
    [SerializeField] float duration;
    [SerializeField] float addY;

    public void Run(string text)
    {
        textUI.text = text;

        Color alpha = textUI.color;
        alpha.a = 0;

        Sequence seq = DOTween.Sequence();
        seq.Append(textUI.DOColor(alpha, duration));
        seq.Join(textUI.transform.DOLocalMoveY(
            textUI.transform.localPosition.y + addY, duration)
            .SetEase(Ease.Flash));
        seq.OnComplete(
            () =>
            {
                Destroy(this.gameObject);
            });
        seq.Play();
    }
}
