﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public class ShopListItem : MonoBehaviour
{
    [SerializeField]
    Image goods;
    [SerializeField]
    Text goodsName;
    [SerializeField]
    Text price;

    ShopItem data;

    ulong nPrice;
    int dataId;
    int shopType;

    public void SetItem(ShopItem data, int index)
    {
        this.data      = data;
        nPrice         = (ulong)data.price;
        dataId         = int.Parse(data.id);

        goodsName.text = data.name;
        price.text     = string.Format("{0:n0}", data.price);

        shopType = index;
        if(shopType == 0)
        {
            goods.sprite = Resources.Load<Sprite>("UI_Sprite/UpgradeBuild/upgrade_deck_01");
            goods.SetNativeSize();
        }
        else
        {
            goods.sprite = Resources.Load<Sprite>("Images/Sculpture/" + data.resName);
            goods.SetNativeSize();
            if(goods.rectTransform.sizeDelta.y < 600)
                goods.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
            else
                goods.transform.localScale = new Vector3(0.2f, 0.2f, 1f);
        }
    }

    public void ClickBuyGoods()
    {
        if(shopType == 0) return;


        #region buy item
        if(nPrice <= UserManager.instance.GoldOffEvent)
        {
            string szBuy = data.price + LanguageManager.instance.GetString("BUYITEM_GOLD");
            PopupManager.instance.Open(1, szBuy, PopupManager.ButtonStyle.YesNo, PopupManager.MoveType.HorizontalUp, BuySculpture, 
                null, true);   
        }
        else
        {
            // 골드 부족
            PopupManager.instance.Open(
                Common.PopupCode.BUDUpgradeFail,
                "TCode_N1",
                PopupManager.ButtonStyle.Ok,
                PopupManager.MoveType.CenterPumping,
                null,
                null);
        }
        #endregion
    }

    void BuySculpture()
    {
        NetworkManager.instance.UpdateSculptureInven(nPrice, dataId, SuccessBuyGoods); 
    }

    void SuccessBuyGoods()
    {
        SculptureManger.Instance.InsertSculpture(dataId);

        PopupManager.instance.Open(
            Common.PopupCode.BUDUpgradeFail,
            "BUY_OK",
            PopupManager.ButtonStyle.Ok,
            PopupManager.MoveType.CenterPumping,
            null,
            null);
    }
}
