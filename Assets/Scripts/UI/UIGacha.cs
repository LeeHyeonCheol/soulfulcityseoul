﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class UIGacha : UIPanelShow
{
    public override void Open(Action callBack, float dealy = 0f)
    {
        base.Open(callBack, dealy);
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close(Action callBack, float dealy = 0f)
    {
        base.Close(callBack, dealy);
    }

    [SerializeField]
    TextAsset characDatas;
    CharacterDatas[] jsonDatas;

    [SerializeField]
    Image character;
    [SerializeField]
    Text chName;
    [SerializeField]
    GameObject[] effects;

    string characId;
    string characName;

    public void SetData(string characId)
    {
        this.characId = characId;

        character.sprite = Resources.Load<Sprite>("UI_Sprite/Gacha/" + characId);
        character.SetNativeSize();

        chName.text = "????";
        jsonDatas = JsonHelper.FromJson<CharacterDatas>(characDatas.ToString());
        for(int i = 0; i < jsonDatas.Length; i++)
        {
            if(jsonDatas[i].id == characId)
            {
                characName = jsonDatas[i].name;
                break;
            }
        }
    }

    public void ClickClose()
    {   
        Close(null);
    }

    [SerializeField]
    DOTweenAnimation characAnim;
    public void CompleteRotate()
    {
        //effects[0].SetActive(true); effects[1].SetActive(true);
        characAnim.DOPlay(); chName.text = characName;
    }
}
