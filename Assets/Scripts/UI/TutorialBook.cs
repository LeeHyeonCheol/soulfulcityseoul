﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TutorialBook : MonoBehaviour
{
    [SerializeField] Button closeButton;
    [SerializeField] Button prev;
    [SerializeField] Button next;
    [SerializeField] Text notice;
    [SerializeField] List<string> db;

    [SerializeField, ReadOnly] int noticeIndex;

    private void Start()
    {
        Init();
    }

    void Init()
    {
        db = new List<string>();
        
        db.Add(LanguageManager.instance.GetString("Tutuorial_9"));
        db.Add(LanguageManager.instance.GetString("Book_1"));
        db.Add(LanguageManager.instance.GetString("Book_2"));
        db.Add(LanguageManager.instance.GetString("Book_3"));
        db.Add(LanguageManager.instance.GetString("Book_4"));
        db.Add(LanguageManager.instance.GetString("Book_5"));

        SetNotice(0);
        prev.gameObject.SetActive(false);
    }

    #region External function
    public void SetCloseButtonCallback(UnityAction callback)
    {
        closeButton.onClick.AddListener(delegate
        {
            callback?.Invoke();
            Close();
        });
    }
    public void Close()
    {
        Destroy(this.gameObject);
    }
    #endregion

    void SetNotice(int index)
    {
        //= 추후 언어대응으로 변경
        notice.text = db[index];
    }

    public void NextNotice()
    {
        noticeIndex++;
        if(noticeIndex == db.Count - 1)
        {
            next.gameObject.SetActive(false);
        }
        prev.gameObject.SetActive(true);

        SetNotice(noticeIndex);
    }
    public void PrevNotice()
    {
        noticeIndex--;
        if(noticeIndex == 0)
        {
            prev.gameObject.SetActive(false);
        }
        next.gameObject.SetActive(true);

        SetNotice(noticeIndex);
    }
}
