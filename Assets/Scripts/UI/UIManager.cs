﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using DG.Tweening;

using UnityEngine;

using PlayFab.ClientModels;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [SerializeField] private List<GameObject> uiButtons;
    [SerializeField] private List<GameObject> upperUI;
    List<bool> buttonsActive;

    [SerializeField]
	private GameObject upgradeBuild;
    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private GameObject mapView;
    [SerializeField]
    private GameObject friend;
    [SerializeField]
    private GameObject shop;
    [SerializeField]
    private GameObject leaderBoard;
    [SerializeField]
    private GameObject option;
    [SerializeField]
    private GameObject gacha;
    [SerializeField]
    private GameObject inventory;
    private bool isInventoryOpen;
    public bool IsInventoryOpen { get => isInventoryOpen; }

    [SerializeField] TutorialBook book;

    [SerializeField] GoldUI goldUI;
    //[SerializeField] ShieldUI shieldUI;
    [SerializeField] GameObject mainMenuButton;

    [SerializeField]
    private Transform managerCanvas;

    UIMapView mapViewPanel;
    UIFriend friendPanel;
    UICharactersBook bookPanel;
    UIRanking rankingPanel;
    UIGameOption optionPanel;

    UIMainMenu mainMenuPanel;

    void Start()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void ActiveMainUI(bool active)
    {
        goldUI.gameObject.SetActive(active);
        //shieldUI.gameObject.SetActive(active);
        mainMenuButton.gameObject.SetActive(active);
    }
    public void ReflushShieldUI()
    {
        //shieldUI.Refrush();
    }   

    UIUpgradeBuild upgradeBuildPanel = null;
    public void OpenUI_UpgradeBuild(List<Building> buildings,int id = -1)
    {
        GameObject obj = Instantiate(upgradeBuild);
        obj.transform.SetParent(this.managerCanvas);
        obj.transform.localScale = Vector3.one;

        upgradeBuildPanel = obj.GetComponent<UIUpgradeBuild>();
        upgradeBuildPanel.Init();
        if(id != -1) { UIUpgradeBuild.LastBuildingIndex = id - 1; }
        upgradeBuildPanel.Open(null, 0.1f);
        upgradeBuildPanel.SetListItems(buildings);
        EscapeManager.instance.SetObjectDestoryAuto(obj);
    }

    public void CloseUI_UpgradeBuild()
    {
        if(upgradeBuildPanel != null) upgradeBuildPanel.Close(null);
    }

    public void OpenUI_MainMenu()
    {
        GameObject obj = Instantiate(mainMenu);
        obj.transform.SetParent(this.managerCanvas);
        obj.transform.localScale = Vector3.one;

        if(mainMenuPanel == null)
            mainMenuPanel = obj.GetComponent<UIMainMenu>();
        mainMenuPanel.Init();
        mainMenuPanel.Open(null, 0.1f);
        EscapeManager.instance.SetObjectDestoryAuto(obj);
    }

    public void CloseUI_MainMenu()
    {
        if(mainMenuPanel != null)
            mainMenuPanel.Close();
    }

    public void OpenUI_MapView()
    {
        NetworkManager.instance.GetUserBuildingData(PlayFabController.instance.myPlayFabID, SuccessGetBuildingAllData);
    }

    void SuccessGetBuildingAllData(GetUserDataResult result)
    {
        CloseUI_MainMenu();

        GameObject obj = Instantiate(mapView);
        obj.transform.SetParent(this.managerCanvas);
        obj.transform.localScale = Vector3.one;

        if(mapViewPanel == null)
            mapViewPanel = obj.GetComponent<UIMapView>();
        mapViewPanel.Init();
        mapViewPanel.Open(null, 0.1f);

        mapViewPanel.SetList(NetworkManager.instance.CheckArrangeBuildingId(result), NetworkManager.instance.CheckArrangeBuildingLevel(result));
        EscapeManager.instance.SetObjectDestoryAuto(obj);
    }

    // public void OpenUI_WorkerPopup(int buildingId, int buildingLv)
    // {
    //     mapViewPanel.OpenWorkerPopup(buildingId, buildingLv);
    // }

    public void OpenUI_Friend()
    {
        NetworkManager.instance.GetMyFriendList(SuccessMyFriend);
    }

    void SuccessMyFriend(List<PlayerLeaderboardEntry> friends)
    {
        CloseUI_MainMenu();
        
        GameObject obj = Instantiate(friend);
        obj.transform.SetParent(this.managerCanvas);
        obj.transform.localScale = Vector3.one;

        if(friendPanel == null)
            friendPanel = obj.GetComponent<UIFriend>();
        friendPanel.Init();
        friendPanel.Open(null, 0.1f);
        friendPanel.SetDataFriends(friends);
        EscapeManager.instance.SetObjectDestoryAuto(obj);
    }

    public void CloseUI_Friend()
    {
        friendPanel.Close(null);
    }

    public void OpenUI_CharactersBook()
    {
        //==PlayFabController.instance.GetCharactersBook(SuccessGetBookDatas);

        // CloseUI_MainMenu();

        // GameObject obj = Instantiate(charactersBook);
        // obj.transform.SetParent(this.managerCanvas);
        // obj.transform.localScale = Vector3.one;

        // if(bookPanel == null)
        //     bookPanel = obj.GetComponent<UICharactersBook>();
        // bookPanel.Init();
        // bookPanel.Open(null, 0.1f); 
        // bookPanel.SetData(NetworkManager.instance.BookDatas);
        // EscapeManager.instance.SetObjectDestoryAuto(obj);
    }

    // void SuccessGetBookDatas(string datas)
    // {

    // }

    // public void SelectCharactersBook(int faceId, bool isLock)
    // {
    //     bookPanel.SelectCharacterInfo(faceId, isLock);
    // }

    UIShop shopPanel;
    public void OpenUI_Shop()
    {
        CloseUI_MainMenu();

        GameObject obj = Instantiate(shop);
        obj.transform.SetParent(this.managerCanvas);
        obj.transform.localScale = Vector3.one;

        shopPanel = obj.GetComponent<UIShop>();
        shopPanel.Init();
        shopPanel.Open(null, 0.1f);
        EscapeManager.instance.SetObjectDestoryAuto(obj);
    }

    void GetCatalogItems(List<CatalogItem> items)
    {

    }

    public void OpenUI_LeaderBoard()
    {
        NetworkManager.instance.GetLeaderBoard(SuccessLeaderBoard);
    }

    void SuccessLeaderBoard(List<PlayerLeaderboardEntry> ranking)
    {
        CloseUI_MainMenu();

        GameObject obj = Instantiate(leaderBoard);
        obj.transform.SetParent(this.managerCanvas);
        obj.transform.localScale = Vector3.one;

        if(rankingPanel == null)
            rankingPanel = obj.GetComponent<UIRanking>();
        rankingPanel.Init();
        rankingPanel.Open(null, 0.1f);
        rankingPanel.SetDataLeaderBoard(ranking);
        EscapeManager.instance.SetObjectDestoryAuto(obj);
    }

    public void OpenUI_Option()
    {
        CloseUI_MainMenu();

        GameObject obj = Instantiate(option);
        obj.transform.SetParent(this.managerCanvas);
        obj.transform.localScale = Vector3.one;

        UIGameOption optionPanel = obj.GetComponent<UIGameOption>();
        optionPanel.Init();
        optionPanel.Open(null, 0.1f);
        EscapeManager.instance.SetObjectDestoryAuto(obj);
    }

    public void OpenUI_Gacha()
    {
        //CloseUI_MainMenu();
        NetworkManager.instance.ExeGacha(SuccessGacha);
    }

    void SuccessGacha(string chId, bool isNew)
    {
        GameObject obj = Instantiate(gacha);
        obj.transform.SetParent(this.managerCanvas);
        obj.transform.localScale = Vector3.one;

        UIGacha gachaPanel = obj.GetComponent<UIGacha>();
        gachaPanel.Init();
        gachaPanel.Open(null, 0.1f);
        gachaPanel.SetData(chId);
        EscapeManager.instance.SetObjectDestoryAuto(obj);
    }

    public void OpenUI_Inventory()
    {
        inventory.SetActive(true);
        CloseUI_Button();

        if (BuildingManager.instance != null)
        {
            BuildingManager.instance.UiSet.UnActiveNPC();
        }

        //= 2020.02.05 인벤토리 상태 추가
        isInventoryOpen = true;
    }

    public void CloseUI_Inventory()
    {
        inventory.SetActive(false);
        OpenUI_Button();

        if (BuildingManager.instance != null)
        {
            BuildingManager.instance.UiSet.ReActiveNPC();
        }

        //= 2020.02.05 인벤토리 상태 추가
        isInventoryOpen = false;
    }

    public void CloseUI_Button()
    {
        buttonsActive = new List<bool>();
        for (int i = 0; i < uiButtons.Count; i++)
        {
            buttonsActive.Add(uiButtons[i].activeSelf);
            uiButtons[i].gameObject.SetActive(false);
        }
    }
    public void OpenUI_Button(bool active = false)
    {
        if (buttonsActive == null || buttonsActive.Count == 0) return;

        for (int i = 0; i < uiButtons.Count; i++)
        {
            if(active)
            {
                uiButtons[i].SetActive(true);
            }
            else
            {
                uiButtons[i].SetActive(buttonsActive[i]);
            }
        }
    }

    public void CloseUI_Upper()
    {
        for(int i = 0; i < upperUI.Count; i++)
        {
            upperUI[i].SetActive(false);
        }
    }
    public void OpenUI_Upper()
    {
        for(int i = 0; i < upperUI.Count; i++)
        {
            upperUI[i].SetActive(true);
        }
    }

    [SerializeField]
    GameObject[] adRewardPopup;

    public void OpenADRewardPortion()
    {
        adRewardPopup[0].SetActive(true);
    }

    public void OpenADRewardGold()
    {
        adRewardPopup[1].SetActive(true);
    }

    bool OpenedADReward()
    {
        if(adRewardPopup[0].activeSelf || adRewardPopup[1].activeSelf) return true;

        return false;
    }

    public void ClickADReward(int type)
    {
        ADMobManager.instance.callbackCloseEarendAD += delegate 
        {
            if(type == 0)
                GameDataManager.instance.Roulette.AddUseCount(5);
            else
                UserManager.instance.GoldOnEvent += 10000;

            NetworkManager.instance.GetRewardFromADs(null, type);

            CloseADReward(type);
        }; 

        ADMobManager.instance.callbackADClose += delegate { CloseADReward(type); }; 

        ADMobManager.instance.OpenRewardAD();
    }

    public void CloseADReward(int type)
    {
        adRewardPopup[type].SetActive(false);
    }

    public void OpenUI_TutorialBook()
    {
        TutorialBook book = Instantiate(this.book);
        book.transform.SetParent(managerCanvas);
        book.transform.localPosition = Vector3.zero;
        book.transform.localScale = Vector3.one;

    }

    // ##### 21.02.05 Patch 1.0.1
    public bool OpenedUI_BuildingMap()
    {
        if(mainMenuPanel != null && mainMenuPanel.UIState)
        {
            return true;
        }
        else if(shopPanel != null && shopPanel.UIState)
        {
            return true;
        }
        else if(upgradeBuildPanel != null && upgradeBuildPanel.UIState)
        {
            return true;
        }
        else if(mapViewPanel != null && mapViewPanel.UIState)
        {
            return true;
        }
        else if(bookPanel != null && bookPanel.UIState)
        {
            return true;
        }
        else if(friendPanel != null && friendPanel.UIState)
        {
            return true;
        }
        else if(rankingPanel != null && rankingPanel.UIState)
        {
            return true;
        }
        else if(optionPanel != null && optionPanel.UIState)
        {
            return true;
        }
        else if(isInventoryOpen)
        {
            return true;
        }
        else if(OpenedADReward())
        {
            return true;
        }

        return false;
    }
}
