﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

using PlayFab.ClientModels;

public class UIRanking : UIPanelShow
{
    public override void Open(Action callBack, float dealy = 0f)
    {
        base.Open(callBack, dealy);
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close(Action callBack, float dealy = 0f)
    {
        base.Close(callBack, dealy);
    }

    public void CloseButton()
    {
        base.Close();
    }

    [SerializeField]
    Transform scrollCotent;
    [SerializeField]
    GameObject rankItemObj;

    List<PlayerLeaderboardEntry> rank;

    public void SetDataLeaderBoard(List<PlayerLeaderboardEntry> rank)
    {
        int i;

        this.rank = rank;
        for(i = 0; i < rank.Count; i++)
        {
            // if(NetworkManager.instance.EqualMyId(rank[i].PlayFabId)) continue;

            GameObject obj = Instantiate(rankItemObj);
            obj.transform.SetParent(scrollCotent);
            obj.transform.localScale = Vector3.one;

            obj.GetComponent<RankListItem>().SetItemData(rank[i]);
        }
    }
}
