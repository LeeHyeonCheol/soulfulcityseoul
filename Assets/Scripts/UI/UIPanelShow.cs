using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

// UI Panel 관리
public class UIPanelShow : MonoBehaviour
{
	[Serializable]
	public class ShowMoveObject
	{
		public GameObject obj;
		public Vector3 startValue = Vector3.zero;
		public Vector3 endValue   = Vector3.zero;
		public float animTime;
		public float delay;
	}

	[Serializable]
	public class ShowAphlaObject
	{
		public GameObject obj;
		public Color startColor = Color.white;
		public Color endColor   = Color.white;
		public float animTime;
		public float delay;
	}

	[Serializable]
	public class ShowScaleObject
	{
		public GameObject obj;
		public Vector3 startScale = Vector3.zero;
		public Vector3 endScale   = Vector3.zero;
		public float animTime;
		public float delay;
	}

	protected float delay;

	private Vector3 position = Vector3.zero;

	[SerializeField]
	protected List<UIPanelShow.ShowMoveObject> showList = new List<UIPanelShow.ShowMoveObject>();
	[SerializeField]
	private List<UIPanelShow.ShowAphlaObject> showAphlaList = new List<UIPanelShow.ShowAphlaObject>();
	[SerializeField]
	private List<UIPanelShow.ShowScaleObject> showScaleList = new List<UIPanelShow.ShowScaleObject>();

	private Action fadeInCallBack;
	private Action fadeOutCallBack;

	private bool _Tweener_k__BackingField;
	private bool _UIState_k__BackingField;

	protected Ease inEase = Ease.OutQuad;
	protected Ease outEase = Ease.InQuad;

	public bool Tweener
	{
		get;
		private set;
	}

	public bool UIState
	{
		get;
		private set;
	}

	private void Awake()
	{
		this.position = base.GetComponent<RectTransform>().anchoredPosition3D;
	}

	public virtual void Init()
	{
		this.In();
		//this.Hide();
	}

	protected virtual void In()
	{
		foreach (UIPanelShow.ShowMoveObject current in this.showList)
		{
			if (current.obj != null)
			{
				current.obj.GetComponent<RectTransform>().anchoredPosition3D = current.startValue;
			}
		}
		foreach (UIPanelShow.ShowAphlaObject current2 in this.showAphlaList)
		{
			if (current2.obj != null)
			{
				Image component = current2.obj.GetComponent<Image>();
				if (component != null)
				{
					component.color = current2.startColor;
				}
				Text component2 = current2.obj.GetComponent<Text>();
				if (component2 != null)
				{
					Color color = component2.color;
					color.a = current2.startColor.a;
					component2.color = color;
				}
			}
		}
		foreach (UIPanelShow.ShowScaleObject current3 in this.showScaleList)
		{
			if (current3.obj != null)
			{
				current3.obj.GetComponent<RectTransform>().localScale = current3.startScale;
			}
		}
	}

	protected virtual void Out()
	{
		foreach (UIPanelShow.ShowMoveObject current in this.showList)
		{
			if (current.obj != null)
			{
				current.obj.GetComponent<RectTransform>().anchoredPosition3D = current.endValue;
			}
		}
		foreach (UIPanelShow.ShowAphlaObject current2 in this.showAphlaList)
		{
			if (current2.obj != null)
			{
				Image component = current2.obj.GetComponent<Image>();
				if (component != null)
				{
					component.color = current2.endColor;
				}
				Text component2 = current2.obj.GetComponent<Text>();
				if (component2 != null)
				{
					Color color = component2.color;
					color.a = current2.endColor.a;
					component2.color = color;
				}
			}
		}
		foreach (UIPanelShow.ShowScaleObject current3 in this.showScaleList)
		{
			if (current3.obj != null)
			{
				current3.obj.GetComponent<RectTransform>().localScale = current3.endScale;
			}
		}
	}

	protected virtual void FadeIn(Action callBack = null)
	{
		if (this.Tweener && this.UIState)
		{
			return;
		}
		this.Tweener = true;
		this.UIState = true;
		this.Out();
		this.Show();
		this.fadeInCallBack = callBack;
		this.FadeInAction();
	}

	private void FadeInAction()
	{
		float num = 0f;
		foreach (UIPanelShow.ShowMoveObject current in this.showList)
		{
			if (current.obj != null)
			{
				num = Mathf.Max(num, current.animTime + current.delay + this.delay);
				current.obj.GetComponent<RectTransform>().DOAnchorPos3D(current.startValue, current.animTime, false).SetEase(this.inEase).SetDelay(current.delay + this.delay);
			}
		}
		foreach (UIPanelShow.ShowAphlaObject current2 in this.showAphlaList)
		{
			if (current2.obj != null)
			{
				num = Mathf.Max(num, current2.animTime + current2.delay + this.delay);
				Image component = current2.obj.GetComponent<Image>();
				if (component != null)
				{
					component.DOColor(current2.startColor, current2.animTime).SetEase(this.inEase).SetDelay(current2.delay + this.delay);
				}
				Text component2 = current2.obj.GetComponent<Text>();
				if (component2 != null)
				{
					component2.DOFade(current2.startColor.a, current2.animTime).SetEase(this.inEase).SetDelay(current2.delay + this.delay);
				}
			}
		}
		foreach (UIPanelShow.ShowScaleObject current3 in this.showScaleList)
		{
			if (current3.obj != null)
			{
				num = Mathf.Max(num, current3.animTime + current3.delay);
				current3.obj.GetComponent<RectTransform>().DOScale(current3.startScale, current3.animTime).SetEase(Ease.InOutBack).SetDelay(current3.delay + this.delay);
			}
		}
		base.GetComponent<RectTransform>().DOAnchorPos3D(Vector3.zero, num, false).OnComplete(delegate
		{
			if (this.fadeInCallBack != null)
			{
				this.fadeInCallBack();
				this.fadeInCallBack = null;
			}
			this.Tweener = false;
		});
	}

	protected virtual void FadeOut(Action callBack = null)
	{		
		if (this.Tweener && !this.UIState)
		{
			return;
		}
		this.Tweener = true;
		this.fadeOutCallBack = callBack;
		this.FadeOutAction();
		this.UIState = false;
	}

	private void FadeOutAction()
	{
		float num = 0f;
		foreach (UIPanelShow.ShowMoveObject current in this.showList)
		{
			if (current.obj != null)
			{
				num = Mathf.Max(num, current.animTime + current.delay + this.delay);
				current.obj.GetComponent<RectTransform>().DOAnchorPos3D(current.endValue, current.animTime, false).SetEase(this.outEase).SetDelay(current.delay + this.delay);
			}
		}
		foreach (UIPanelShow.ShowAphlaObject current2 in this.showAphlaList)
		{
			if (current2.obj != null)
			{
				num = Mathf.Max(num, current2.animTime + current2.delay + 0.2f + this.delay);
				Image component = current2.obj.GetComponent<Image>();
				if (component != null)
				{
					component.DOColor(current2.endColor, current2.animTime).SetEase(this.outEase).SetDelay(current2.delay + 0.2f + this.delay);
				}
				Text component2 = current2.obj.GetComponent<Text>();
				if (component2 != null)
				{
					component2.DOFade(current2.endColor.a, current2.animTime).SetEase(this.outEase).SetDelay(current2.delay + 0.2f + this.delay);
				}
			}
		}
		foreach (UIPanelShow.ShowScaleObject current3 in this.showScaleList)
		{
			if (current3.obj != null)
			{
				num = Mathf.Max(num, current3.animTime + current3.delay + this.delay);
				current3.obj.GetComponent<RectTransform>().DOScale(current3.endScale, current3.animTime).SetEase(Ease.InBack).SetDelay(current3.delay + this.delay);
			}
		}
		base.transform.DOLocalMove(this.position, num, false).OnComplete(delegate
		{
			if (this.fadeOutCallBack != null)
			{
				this.fadeOutCallBack();
				this.fadeOutCallBack = null;
			}
			this.Tweener = false;
			this.Hide();
		});
	}

	// private void Load()
	// {
	// 	foreach (UIPanelShow.ShowMoveObject current in this.showList)
	// 	{
	// 		if (current.obj != null)
	// 		{
	// 			current.endValue = current.obj.transform.localPosition;
	// 			current.startValue = current.obj.transform.localPosition;
	// 		}
	// 	}
	// }

	protected virtual void Show()
	{
		base.gameObject.SetActive(true);
	}

	protected virtual void Hide()
	{
		//base.gameObject.SetActive(false);
		Destroy(base.gameObject);
	}

	public virtual void Open(Action callBack = null, float dealy = 0f)
	{
		this.Refresh();
		this.delay = dealy;
		this.FadeIn(callBack);
	}

	public virtual void Open(string text, Action callBack = null, float dealy = 0f)
	{
		this.Refresh();
		this.delay = dealy;
		this.FadeIn(callBack);
	}

	public virtual void Open(string text, Action confirmCallback, Action callBack = null, float dealy = 0f)
	{
		this.Refresh();
		this.delay = dealy;
		this.FadeIn(callBack);
	}

	public virtual void Close(Action callBack = null, float dealy = 0f)
	{
		this.delay = dealy;
		this.FadeOut(callBack);
	}

	public virtual void Refresh()
	{
	}

	[ContextMenu("ClearConfig")]
	private void ClearConfig()
	{
		for (int i = this.showList.Count - 1; i >= 0; i--)
		{
			if (this.showList[i].obj == null)
			{
				this.showList.RemoveAt(i);
			}
		}
		for (int j = this.showAphlaList.Count - 1; j >= 0; j--)
		{
			if (this.showAphlaList[j].obj == null)
			{
				this.showAphlaList.RemoveAt(j);
			}
		}
		for (int k = this.showScaleList.Count - 1; k >= 0; k--)
		{
			if (this.showScaleList[k].obj == null)
			{
				this.showScaleList.RemoveAt(k);
			}
		}
	}
}
