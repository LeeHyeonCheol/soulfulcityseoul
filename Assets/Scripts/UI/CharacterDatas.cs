﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterDatas
{
    public string id;
    public string name;
    public string backgr;       // 배경
    public string personality;  // 성격
    public string like;         // 좋아하는 것
    public string pick;         // 놀이기구PICK
}
