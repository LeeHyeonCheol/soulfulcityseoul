﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class UIMainMenu : UIPanelShow
{
    [SerializeField] GameObject clickBlock;

    public void Close()
    {
        Close(CallBackClose, 0);
    }
    public override void Close(Action callBack = null, float dealy = 0)
    {
        base.Close(callBack, dealy);
    }

    public override void Init()
    {
        base.Init();
    }
        
    public override void Open(Action callBack = null, float dealy = 0)
    {
        base.Open(CallBackOpen, dealy);
    }

    #region CallBack function
    void CallBackOpen()
    {
        clickBlock.SetActive(true); SetInfo();
    }
    void CallBackClose()
    {
        clickBlock.SetActive(false);
    }
    #endregion

    #region 메뉴 버튼 

    public void ClickMapButton()
    {
        UIManager.instance.OpenUI_MapView();
    }

    public void ClickCharactersButton()
    {
        UIManager.instance.OpenUI_CharactersBook();
    }

    public void ClickFriendButton()
    {
        UIManager.instance.OpenUI_Friend();
    }

    public void ClickStoreButton()
    {
        UIManager.instance.OpenUI_Shop();
    }

    public void ClickLeaderBoardButton()
    {
        UIManager.instance.OpenUI_LeaderBoard();
    }

    public void ClickOptionButton()
    {
        UIManager.instance.OpenUI_Option();
    }

    #endregion

    [SerializeField]
    Image myPicture;
    [SerializeField]
    Text nickName;

    void SetInfo()
    {
        nickName.text = PlayFabController.instance.nickName;
    }
}
