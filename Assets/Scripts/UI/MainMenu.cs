﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class MainMenu : MonoBehaviour
{
    [SerializeField] Button openButton;
    [SerializeField] Button clickBlock;
    [SerializeField] GameObject menu;
    
    [Header("Param")]
    [SerializeField] float duration;
    [SerializeField] AnimationCurve openCurve;
    [SerializeField] AnimationCurve closeCurve;
    [SerializeField] RectTransform windowTrans;
    [SerializeField,ReadOnly] float windowWidth;
    [SerializeField,ReadOnly] Vector2 startPos;
    [SerializeField,ReadOnly] Vector2 endPos;


    private void OnValidate()
    {
        if(windowTrans != null)
        {
            windowWidth = windowTrans.rect.width;
        }
    }


    private void Start()
    {
        menu.SetActive(false);
        openButton.onClick.AddListener(OpenAnimation);
        clickBlock.onClick.AddListener(CloseAnimation);
        startPos = new Vector2(-windowWidth / 2,menu.transform.localPosition.y);
        endPos = new Vector2(windowWidth / 2, menu.transform.localPosition.y);

    }

    void OpenAnimation()
    {
        menu.SetActive(true);

        menu.transform.DOLocalMoveX(endPos.x, duration)
            .OnStart(() =>
            {
                clickBlock.gameObject.SetActive(true);
            })
            .SetEase(openCurve);
    }
    void CloseAnimation()
    {
        menu.transform.DOLocalMoveX(startPos.x, duration)
            .OnComplete(() =>
            {
                clickBlock.gameObject.SetActive(false);
            })
            .SetEase(closeCurve);
    }
}
