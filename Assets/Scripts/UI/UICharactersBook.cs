﻿using System.Collections;
using System.Collections.Generic;
using System;

using UnityEngine;
using UnityEngine.UI;

public class UICharactersBook : UIPanelShow
{
    public override void Open(Action callBack, float dealy = 0f)
    {
        base.Open(callBack, dealy);
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close(Action callBack, float dealy = 0f)
    {
        base.Close(callBack, dealy);
    }

    public void CloseButton()
    {
        base.Close();
    }
}
