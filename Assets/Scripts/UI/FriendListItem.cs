﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using PlayFab.ClientModels;

public class FriendListItem : MonoBehaviour
{
    [SerializeField]
    Image faceIcon;
    [SerializeField]
    Text nickName;
    [SerializeField]
    Text score;
    [SerializeField]
    GameObject visitButton;
    [SerializeField]
    GameObject addButton;
    [SerializeField]
    Material grayMat;

    PlayerLeaderboardEntry info;

    bool attackEnable;

    void Start()
    {
        
    }

    public void SetItemData(PlayerLeaderboardEntry info, int idx)
    {
        this.info     = info;
        nickName.text = info.PlayFabId;
        score.text    = info.StatValue.ToString();
        if(idx == 0) // 친구 방문
        {
            addButton.SetActive(false); visitButton.SetActive(true);
            attackEnable = NetworkManager.instance.CheckFriendAttacked(info.PlayFabId);
            if(attackEnable == false)
                visitButton.GetComponent<Image>().material = grayMat;
            else
                visitButton.GetComponent<Image>().material = null;
        }
        else         // 친구 추가
        {
            addButton.SetActive(true); visitButton.SetActive(false);
        }
    }
    
    // 친구 방문 하기
    public void ClickVisit()
    {
        if(attackEnable == false) return;
        // info > 친구 정보
        NetworkManager.instance.GetUserBuildingData(info.PlayFabId, SuccessUserBuildingData);
    }

    public void ClickAdd()
    {
        NetworkManager.instance.AddFriend(info.PlayFabId, SuccessAddFriend);
    }

    GetUserDataResult dataRes;
    void SuccessUserBuildingData(GetUserDataResult res)
    {
        dataRes = res;
        // 지어진 건물이 없음. > 방문 제한
        if(res.Data.Count == 0 || NetworkManager.instance.IsEnableAttackBuilding(res) == false)
        {
            PopupManager.instance.Open(0, "NOEXIST_BUILD", PopupManager.ButtonStyle.Ok, 
                PopupManager.MoveType.CenterPumping, null, null);
            return;
        }

        NetworkManager.instance.UpdateFriendAttackedList(info.PlayFabId, SuccessUpdateFriendAttacked);
    }

    void SuccessUpdateFriendAttacked()
    {
        BuildingManager.instance.SetFriendData(NetworkManager.instance.CheckArrangeBuildingId(dataRes), NetworkManager.instance.CheckArrangeBuildingLevel(dataRes), info.PlayFabId, 
            NetworkManager.instance.CheckArrangeShieldCount(dataRes), info.PlayFabId);
        
        UIManager.instance.CloseUI_Friend();
    }

    void SuccessAddFriend()
    {
        Destroy(this.gameObject);
    }
}
