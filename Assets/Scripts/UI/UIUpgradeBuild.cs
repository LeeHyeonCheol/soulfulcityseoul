﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

using Kind = Building.Kind;

public class UIUpgradeBuild : UIPanelShow
{
    public override void Open(Action callBack, float dealy = 0f)
    {
        base.Open(callBack, dealy);
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close(Action callBack, float dealy = 0f)
    {
        base.Close(callBack, dealy);
    }

    public void CloseButton()
    {
        base.Close();

        //= 위치 연산
        //int magnification = BuildingManager.instance.OpenCount - currentTapID;

        //
        //float spacing = (magnification * BuildingManager.instance.UiSet.Spacting);
        //float offset = BuildingManager.instance.UiSet.EndOffsetX;

        //Debug.Log(-BuildingManager.instance.UiSet.ContentW + w + spacing - offset);

        ////= 위치 이동
        //BuildingManager.instance.UiSet.ContentTrans.localPosition =
        //    new Vector3(-BuildingManager.instance.UiSet.ContentW + w + spacing - offset,
        //                BuildingManager.instance.UiSet.ContentTrans.localPosition.y,
        //                BuildingManager.instance.UiSet.ContentTrans.localPosition.z);

        BuildingManager.instance.UiSet.MoveContentForBuilding(currentTapID);
    }

    public override void Init()
    {
        base.Init();
    }

    [Header("Data")]
    [SerializeField] List<BuildingOnlyShow> prefabs;

    [Header("UI")]
    [SerializeField] RectTransform[] positions = new RectTransform[(int)Kind.MaxSize];
    [SerializeField] List<Button> taps = new List<Button>();
    [SerializeField] List<GameObject> locks = new List<GameObject>();

    [Header("other data")]
    [SerializeField, ReadOnly] List<Building> buildings;
    [SerializeField, ReadOnly] int currentTapID;
    [SerializeField, ReadOnly] List<GameObject> currentActive;
    [SerializeField] Text[] info = new Text[(int)Kind.MaxSize];
    [SerializeField] Text[] price = new Text[(int)Kind.MaxSize];

    public static int LastBuildingIndex = 0;

    public void SetListItems(List<Building> buildings)
    {
        this.buildings = buildings;

        SetTap(buildings[LastBuildingIndex]);

        //= 탭 잠금 기능 추가
        for(int i = 0; i < buildings.Count; i++)
        {
            if(buildings[i].TotalLevel() == 0)
            {
                locks[i].gameObject.SetActive(true);
                taps[i].interactable = false;
            }
            else
            {
                locks[i].gameObject.SetActive(false);
                taps[i].interactable = true;
            }
        }
    }

    public void SetTap(int id)
    {
        SetTap(buildings[id-1]);
    }
    public void Construction(int kind)
    {
        //= 빌딩 찾기 해야함.
        Building find = buildings.Find((v) => v.ID == currentTapID);

        if (Common.BUDMaxLevel <= find.GetLevel(kind /* index */)) return;

        if(find != null)
        {
            find.SetOneSuccessConstructionCallback(CloseButton);
            find.OnConstruction((Kind)kind, find.RequireGold((Kind)kind));
        }
        else
        {
#if USE_DEBUG
            Debug.Log("Not find building : Upgrade");
#endif
        }
    }

    void SetTap(Building building)
    {
        if (building.TotalLevel() == 0) Close(null);

        BuildingOnlyShow prefab = prefabs.Find((BuildingOnlyShow @object) => @object.ID == building.ID);

        if (prefab != null)
        {
            currentTapID = building.ID;
            LastBuildingIndex = currentTapID - 1;


            if (currentActive != null && currentActive.Count != 0)
            {
                for(int i = 0; i < currentActive.Count; i++)
                {
                    Destroy(currentActive[i]);
                }
            }
            currentActive = new List<GameObject>();


            #region Main Building Upgrade
            BuildingOnlyShow mainBuilding = Instantiate(prefab);
            mainBuilding.name = "Main Building Upgrade";
            mainBuilding.transform.SetParent(positions[(int)Kind.MainBuilding]);
            mainBuilding.transform.localScale = Vector3.one;
            mainBuilding.transform.localPosition = Vector3.zero;
            mainBuilding.Set(
                Kind.MainBuilding,
                building.GetLevel(Kind.MainBuilding) + 1,
                building.GetLevel(Kind.MainSculpture),
                building.GetLevel(Kind.SubSculpture));
            #endregion

            #region Main Sculpture Upgrade
            BuildingOnlyShow mainSculpture = Instantiate(prefab);
            mainSculpture.name = "Main Sculpture Upgrade";
            mainSculpture.transform.SetParent(positions[(int)Kind.MainSculpture]);
            mainSculpture.transform.localScale = Vector3.one;
            mainSculpture.transform.localPosition = Vector3.zero;
            mainSculpture.Set(
                Kind.MainSculpture,
                building.GetLevel(Kind.MainBuilding),
                building.GetLevel(Kind.MainSculpture) + 1,
                building.GetLevel(Kind.SubSculpture));
            #endregion

            #region Sub Sculpture Upgrade
            BuildingOnlyShow subSculpture = Instantiate(prefab);
            subSculpture.name = "Sub Sculpture Upgrade";
            subSculpture.transform.SetParent(positions[(int)Kind.SubSculpture]);
            subSculpture.transform.localScale = Vector3.one;
            subSculpture.transform.localPosition = Vector3.zero;
            subSculpture.Set(
                Kind.SubSculpture,
                building.GetLevel(Kind.MainBuilding),
                building.GetLevel(Kind.MainSculpture),
                building.GetLevel(Kind.SubSculpture) + 1);
            #endregion

            currentActive.Add(mainBuilding.gameObject);
            currentActive.Add(mainSculpture.gameObject);
            currentActive.Add(subSculpture.gameObject);

            #region Set Price and info
            //= 2020 01 18 표기방식 변경
            //= [ Building level + 1 / MaxLevel ] => [ Building level / MaxLevel ]
            for(int index = 0; index < (int)Kind.MaxSize; index++)
            {
                ulong priceValue = building.RequireGold((Kind)index);
                price[index].text = (priceValue == 0) ? "MAX" : priceValue.ToString();
                info[index].text = (Common.BUDMaxLevel <= building.GetLevel((Kind)index)) ?
                    "Max Level" : (building.GetLevel((Kind)index)).ToString() + " / " + Common.BUDMaxLevel.ToString();
            }
            #endregion
        }
    }
}
