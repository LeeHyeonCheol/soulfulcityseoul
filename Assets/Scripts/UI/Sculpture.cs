﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 인벤토리에 배경 들어가니 상태에 따라서 비활성화 활성화 하는 코드 추가하기
/// 이름 기능 추가되었으니 인벤토리 내부에있을때만 활성화하는 기능 추가하기
/// 각 인벤토리 칸이 동일한 사이즈로 변경되었으니, 배치하는 연산식 수정하기
/// 카운트 위치 고정
/// 배치물 기본좌표 0,0 에서 각기 수정되었으니 연산식 수정할 필요가 있음.
/// </summary>
public class Sculpture : MonoBehaviour
{
    public enum State
    {
        None,
        Inventory,
        Arrangment
    }

    [System.Serializable]
    public struct Info
    {
        public int id;
        public string name;
        public ulong price;
    }
    [SerializeField] Info info;

    [Header("Prefab data")]
    [SerializeField] Image image;       // prefab 고정
    [SerializeField,ReadOnly] State state = State.None;
    [SerializeField] Button button;

    [Header("External data")]
    [SerializeField] Vector3 pos;

    [Header("Inventory data")]
    [SerializeField] Image background;
    [SerializeField] GameObject countParent;
    [SerializeField] Text countText;
    [SerializeField] Text nameText;

#if USE_DEBUG
    [Header("Use Debug")]
    [SerializeField] Sprite setSprite;
#endif

#if USE_DEBUG
    private void OnValidate()
    {
        if(setSprite != null) image.sprite = setSprite;
        image.SetNativeSize();

        try
        {
            string[] numbers = this.name.Split(' ');
            if (numbers.Length == 2)
            {
                info.id = int.Parse(numbers[1]);
            }
        }
        catch(System.FormatException)
        {

        }

        if (DataManager.instance != null)
        {
            ShopItem item = DataManager.instance.GetSculptureData(info.id);
            if (item != null)
            {
                info.name = item.name;
            }
        }
    }
#endif

    public delegate void OnClickEvent(Sculpture sculpture);
    public OnClickEvent onClickEvent;

    #region Property List
    public int ID { get => info.id; }
    public string Name { get => info.name; }
    public Vector3 Position { get => pos; }
    public Image Image { get => image; }

    public State IsState { get => state; }
    #endregion

    private void Start()
    {
        button.onClick.AddListener(OnClick);
    }

    #region External function
    public void Init(Vector3 pos)
    {
        // local
        if (pos.z == 0)
        {
            transform.localPosition = pos;
        }
        else
        {
            transform.position = pos;
            // NOTE : UI화 진행하면서 z축 삭제.
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
        }

        this.pos = transform.localPosition;
    }
    public static Vector3 MakeVectorFormSculputre(Sculpture sculpture, Vector3 pos)
    {
        // Save position
        Vector3 localPosition = sculpture.transform.localPosition;

        // Make position
        sculpture.Init(pos);
        Vector3 revalue = sculpture.transform.localPosition;
        sculpture.Init(localPosition);

        return revalue;
    }

    public string GetInfo()
    {
        return  string.Format("{0,1:D3}", info.id) +Common.splitWord + pos.x + "," + pos.y;
    }

    public void OnClick()
    {
        onClickEvent?.Invoke(this);
    }

    public void SetState(State state, int count = 1)
    {
        switch(state)
        {
            case State.Arrangment:
                StateArrangment();
                break;
            case State.Inventory:
                StateInventory();
                break;
        }
        
        void StateInventory()
        {
            background.color = Color.white;
            countParent.SetActive(true);
            countText.text = count.ToString();
            nameText.gameObject.SetActive(true);
            nameText.text = info.name;

            image.rectTransform.pivot = new Vector2(0.5f, 0.5f);
            image.transform.localScale = Vector3.one * .6f;
            if((this.transform as RectTransform).pivot.y != 1)
            {
                image.rectTransform.localPosition = Vector2.zero;
            }
            else
            {
                image.rectTransform.localPosition = new Vector2(0, -(this.transform as RectTransform).sizeDelta.y * .5f);
            }
        }

        void StateArrangment()
        {
            nameText.gameObject.SetActive(false);
            countParent.SetActive(false);
            background.color = Common.ColorAlpha;

            image.rectTransform.pivot = new Vector2(0.5f, 0.0f);
            image.transform.localScale = Vector3.one;
            image.rectTransform.localPosition = Vector3.zero;
        }
    }
    #endregion

    private void Update()
    {

    }
}
