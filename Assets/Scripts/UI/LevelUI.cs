﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelUI : MonoBehaviour
{
    [SerializeField] Text level;

    private void Start()
    {
        Init();
    }

    void Init()
    {
        if(Refresh() == false)
        {
            Invoke("Init", Time.deltaTime);
        }
    }

    public bool Refresh()
    {
        if (BuildingManager.instance != null && BuildingManager.instance.UiSet.buildings != null)
        {
            int total = BuildingManager.instance.CalculationAllLevels(true);
            level.DOText(total.ToString(), .5f);
            return true;
        }
        else
        {
            return false;
        }
    }

}
