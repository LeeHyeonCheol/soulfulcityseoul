﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

using QuizTable = DataManager.QuizTable;

public class QuizReward : MonoBehaviour
{
    [Header("Show UI")]
    [SerializeField] Text notice;
    [SerializeField] Slider timer;

    [Header("Play data")]
    [SerializeField] bool isPlay;

    [Header("Info")]
    [SerializeField] float limitTime;
    [SerializeField, ReadOnly] float currentTime;
    [SerializeField, ReadOnly] QuizTable quizTable;
    [SerializeField] GameObject[] result;

    private void Start()
    {
        Init();
    }


    void Init()
    {
        quizTable = DataManager.instance.GetRandomQuizTable();

        //= Text set
        SetText(notice, quizTable.textCode);

        //= Slider(Timer) set
        currentTime = limitTime;
        SliderUpdate(timer, limitTime, currentTime, false);

        isPlay = true;
    }

    void SetText(Text text,string textCode)
    {
        ArabicText arabicText = text.GetComponent<ArabicText>();
        if (LanguageManager.instance.IsArabic() == false)
        {
            arabicText.enabled = false;
            text.alignment = TextAnchor.UpperLeft;
        }
        else
        {
            arabicText.enabled = true;
            text.alignment = TextAnchor.UpperRight;
        }

        if (LanguageManager.instance.IsArabic() == false)
            text.text = LanguageManager.instance.GetString(textCode);
        else
        {
            arabicText.Text = LanguageManager.instance.GetString(textCode);
        }
    }
    void SliderUpdate(Slider slider,float limit,float current,bool animation)
    {
        if(animation)
        {
            slider.DOValue(current / limit /* value */, current / limit/* duration */);
        }
        else
        {
            slider.value = current / limit;
        }
    }

    #region External function
    public void OnClickYes()
    {
        GetReward(QuizTable.Answer.O);
    }
    public void OnClickNo()
    {
        GetReward(QuizTable.Answer.X);
    }
    #endregion

    bool CheckAnswer(QuizTable table,QuizTable.Answer answer)
    {
        return table.answer == answer.ToString();
    }
    void GetReward(QuizTable.Answer answer)
    {
        isPlay = false;
        if(CheckAnswer(quizTable, answer))
        {
            //= 정답 보상
            Debug.Log("Answer");
            result[0].SetActive(true);
        }
        else
        {
            //= 실패 보상
            Debug.Log("No Answer");
            result[1].SetActive(true);
        }
    }

    public void EndResult()
    {
        PopupManager.instance.Open(Common.PopupCode.QuizRewardAD,
        "",
        PopupManager.ButtonStyle.AD,
        PopupManager.MoveType.CenterPumping,
        delegate
        {
            //= 광고 시청
            NetworkManager.instance.RewardBonus_Quiz(RewardBonus_Quiz, "Y", true);
        },
        delegate
        {
            //= 광고 안시청
            NetworkManager.instance.RewardBonus_Quiz(RewardBonus_Quiz, "N", false);
        },
        true);
    }

    void RewardBonus_Quiz(string currGold, string addGold)
    {
        PopupManager.instance.Open(Common.PopupCode.QuizRewardAD,
            "+ " + ulong.Parse(addGold).ToString("#,##0"),
            PopupManager.ButtonStyle.Ok,
            PopupManager.MoveType.CenterPumping,
            delegate
            {
                //= 골드 삽입
                UserManager.instance.GoldOnEvent += ulong.Parse(addGold);
                Destroy(this.gameObject);
            },
            delegate
            {
                //= 골드 삽입
                UserManager.instance.GoldOnEvent += ulong.Parse(addGold);
                Destroy(this.gameObject);
            },
            true);
    }

    void TimeOver()
    {
        //= 시간초과
        PopupManager.instance.Open(Common.PopupCode.QuizTimeOver,
            "시간 초과",
            PopupManager.ButtonStyle.Ok,
            PopupManager.MoveType.CenterPumping,
            delegate
            {
                //= 오브젝트 파괴, 보상 없음
                Destroy(this.gameObject);
            },
            null,
            true);
    }

    private void Update()
    {
        //= 타이머
        if(isPlay)
        {
            currentTime -= Time.deltaTime;

            if (currentTime <= 0)
            {
                currentTime = 0;
                SliderUpdate(timer, limitTime, 0, false);
                isPlay = false;

                //= 실패 리워드
                TimeOver();
            }
            else
            {
                SliderUpdate(timer, limitTime, currentTime, true);
            }
        }
        
    }
}
