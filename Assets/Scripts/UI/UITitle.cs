﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using PlayFab.ClientModels;

public class UITitle : MonoBehaviour
{
    [SerializeField]
    GameObject companyLogo;
    [SerializeField]
    GameObject gameTitle;
    [SerializeField]
    Image loadingGauge;
    [SerializeField]
    Text loadingInf;

    void Start()
    {
        StartCoroutine(CheckLoadingDatas());
    }

    IEnumerator CheckLoadingDatas()
    {
        yield return new WaitForSeconds(3f);
        
        companyLogo.SetActive(false);
        gameTitle.SetActive(true);

        NetworkManager.instance.ConnectUser(SuccessLogin);
    }

    void SuccessLogin()
    {
        loadingGauge.fillAmount = 0.2f;
        NetworkManager.instance.GetWorkerDataFromUserData(SuccessGetDatas);
    }

    void SuccessGetDatas()
    {
        loadingGauge.fillAmount = 0.35f;
        NetworkManager.instance.GetLeaderBoard(SuccessLeaderBoard, 1);
    }

    void SuccessLeaderBoard(List<PlayerLeaderboardEntry> ranking)
    {
        if(ranking.Count > 0)
        {
            NetworkManager.instance.rank1PlayerInfo = ranking[0].PlayFabId + "\n" +
                ranking[0].StatValue + " Point"; 
        }
        loadingGauge.fillAmount = 0.4f;
        NetworkManager.instance.UpdateCheckDatas(SuccessCheckDatas);
    }

    void SuccessCheckDatas()
    {
        loadingGauge.fillAmount = 0.5f;
        StartCoroutine(Load("Main_Scene"));
    } 

    IEnumerator Load(string sceneName)
    {
        AsyncOperation op = SceneManager.LoadSceneAsync(sceneName);
        op.allowSceneActivation = false;

        float timer = 0.0f;
        while (!op.isDone)
        {
            yield return null;

            timer += Time.unscaledDeltaTime;
            if (op.progress < 0.5f)
            {
                loadingGauge.fillAmount = 0.5f;
            }
            else if (op.progress < 0.9f)
            {
                loadingGauge.fillAmount = Mathf.Lerp(loadingGauge.fillAmount, op.progress, timer);
                if (loadingGauge.fillAmount >= op.progress)
                {
                    timer = 0f;
                }
            }
            else
            {
                loadingGauge.fillAmount = Mathf.Lerp(loadingGauge.fillAmount, 1f, timer);
                if (loadingGauge.fillAmount == 1.0f)
                {
                    op.allowSceneActivation = true;
                    yield break;
                }
            }
        }
    }
}
