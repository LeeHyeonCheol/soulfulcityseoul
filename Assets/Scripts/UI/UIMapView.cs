﻿using System.Collections;
using System.Collections.Generic;
using System;

using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class UIMapView : UIPanelShow
{
    public override void Open(Action callBack, float dealy = 0f)
    {
        base.Open(callBack, dealy);
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close(Action callBack, float dealy = 0f)
    {
        base.Close(callBack, dealy);
    }

    public void CloseButton()
    {
        base.Close();
    }

    [SerializeField]
    Transform scrollCotent;
    [SerializeField]
    GameObject listItemObj;
    [SerializeField] Image bab;
    [SerializeField] Transform leftStart;
    [SerializeField] Transform rightEnd;
 
    // [SerializeField]
    // GameObject rewardGold;
    // [SerializeField]
    // RectTransform claimBtnPos;

    int currPageBuildingId;
    int currListPage;
#region    맵 리스트 부분

    List<int> buildingIds;

    bool isTouchLock;

    private void Start() 
    {
        GetComponent<RectTransform>().offsetMin = Vector2.zero;
        GetComponent<RectTransform>().offsetMax = Vector2.zero;
        //workState = new WorkState[3];

        currListPage = 0; isTouchLock = false;
        buildingIds = DataManager.instance.GetAllBuildingID();

        currPageBuildingId = buildingIds[currListPage];
        //UpdateRewardGoldEff(currListPage);

        //= 2020.02.10 뱁새 애니메이션 추가
        Sequence babSeq = DOTween.Sequence();
        babSeq.Append(bab.transform.DOLocalMoveX(leftStart.localPosition.x, 0));
        babSeq.Append(bab.transform.DOLocalMoveX(rightEnd.localPosition.x, 3.5f).SetEase(Ease.Linear));
        babSeq.SetLoops(-1);
        babSeq.Play();
    }

    public void SetList(string ids, string level)
    {
        int i;
        string[] szId = ids.Split('|');
        string[] szLv = level.Split('|');

        for(i = 0; i < szId.Length; i++)
        {
            GameObject obj = Instantiate(listItemObj);
            obj.transform.SetParent(scrollCotent);
            obj.transform.localScale = Vector3.one;

            int id = Convert.ToInt32(szId[i]);
            //= Create detail level
            string[] detailsLevels = szLv[i].Split(Common.subSplitWord);
            int[] intLevels = new int[(int)Building.Kind.MaxSize];
            for (int j = 0; j < (int)Building.Kind.MaxSize; j++)
            {
                intLevels[j] = int.Parse(detailsLevels[j]);
            }
            obj.GetComponent<UIMapViewBuildingItem>().SetItemData(id, intLevels);
        }

        SetInfoData(0);
    }

    [SerializeField]
    Text infoContent;
    [SerializeField]
    ArabicText infoArabic;

    void SetInfoData(int index)
    {
        string[,] datas = 
        {
            {"Quiz_25", "Quiz_26", "Quiz_27", "Quiz_28"}, // 서울역
            {"Quiz_29", "Quiz_30", "Quiz_31", "Quiz_32"},
            {"Quiz_17", "Quiz_18", "Quiz_19", "Quiz_20"},
            {"Quiz_09", "Quiz_10", "Quiz_11", "Quiz_12"},
            {"Quiz_37", "Quiz_38", "Quiz_39", "Quiz_40"},
            {"Quiz_33", "Quiz_34", "Quiz_35", "Quiz_36"},
            {"Quiz_13", "Quiz_14", "Quiz_15", "Quiz_16"},
            {"Quiz_41", "Quiz_42", "Quiz_43", "Quiz_44"}, 
            {"Quiz_21", "Quiz_22", "Quiz_23", "Quiz_24"},  
            {"Quiz_01", "Quiz_02", "Quiz_03", "Quiz_04"},
            {"Quiz_05", "Quiz_06", "Quiz_07", "Quiz_08"}  
        };

        if(LanguageManager.instance.IsArabic() == false)
        {
            infoArabic.enabled = false;
            infoContent.alignment = TextAnchor.MiddleLeft;
        }
        else
        {
            infoArabic.enabled = true;
            infoContent.alignment = TextAnchor.MiddleRight;
        }

        string txt = null;
        for(int i = 1; i <= 4; i++)
        {
            txt += i + ". " + LanguageManager.instance.GetString(datas[index, i - 1]);
            if(i < 4) txt += "\n";
        }
        
        if(LanguageManager.instance.IsArabic() == false)
            infoContent.text = txt;
        else
            infoArabic.Text = txt;
    }

    // public void ClickClaimGold()
    // {
    //     bool findState = false;
    //     // FIX ME : 2020.12.18
    //     int  expectedGold = 0; // 알바로 얻을 수 있는 예상 수익
    //     for(int i = 0; i < workState.Length; i++)
    //     {
    //         if(workState[i] == WorkState.Off)
    //         {
    //             if(i == 0)
    //             {
    //                 expectedGold += currPageBuildingId * 2500;
    //             }
    //             else if(i == 1)
    //             {
    //                 expectedGold += currPageBuildingId * 5000;
    //             }
    //             else
    //             {
    //                 expectedGold += currPageBuildingId * 12500;
    //             }

    //             //SetListState(i, WorkState.Ready);
    //             findState = true;
    //         }
    //     }
    //     if(findState == false || isTouchLock) 
    //     {
    //         PopupManager.instance.Open(Common.PopupCode.WorkmanWarning,
    //             "NO_GETGOLD", PopupManager.ButtonStyle.Ok, PopupManager.MoveType.CenterPumping,
    //             null);
    //         return;
    //     }
    //     // FIX ME : 2020.12.18
    //     // 알바로 얻을 수 있는 예상 수익
    //     PopupManager.instance.Open(Common.PopupCode.WorkmanGetGold,
    //         expectedGold + LanguageManager.instance.GetString("GETGOLD_QA"), 
    //         PopupManager.ButtonStyle.YesNo, PopupManager.MoveType.CenterPumping,
    //         delegate
    //         {
    //             PopupManager.instance.Open(Common.PopupCode.WokrmanRewardAD,
    //                 "",
    //                 PopupManager.ButtonStyle.AD, PopupManager.MoveType.CenterPumping,
    //                 delegate
    //                 {
    //                     StartRewardToWorker(true);
    //                 },
    //                 delegate
    //                 {
    //                     StartRewardToWorker(false);

    //                 }, true);
	//         }
    //         , null, true);
    // }

    // void StartRewardToWorker(bool adreward)
    // {
    //     NetworkManager.instance.RewardToWorker(currPageBuildingId, currPageWorkEndT, SuccessRewardToWorker, adreward);
    //     isTouchLock = true;
    // }

    // void SuccessRewardToWorker(string gold, string getGold)
    // {
    //     for(int i = 0; i < workState.Length; i++)
    //     {
    //         if(workState[i] == WorkState.Off)
    //             SetListState(i, WorkState.Ready);
    //     }

    //     //= UserManager.instance.GoldOffEvent = Convert.ToUInt64(gold);

    //     // 골드 애니 끄기
       
    //     // 얼굴 아이콘
    //     scrollCotent.GetChild(currListPage).GetComponent<UIMapViewBuildingItem>().UpdateWorkerIcons();

    //     GameObject obj = Instantiate(rewardGold);
    //     obj.transform.SetParent(transform);
    //     obj.transform.localPosition = Vector3.zero;
    //     obj.transform.localScale    = Vector3.one;

    //     ulong lastGold = Convert.ToUInt64(getGold);

    //     print("=========== lastGold : " + lastGold);
    //     print("=========== getGold : " + getGold);

    //     ShootingGold shooting = obj.GetComponent<ShootingGold>();
    //     shooting.Init(lastGold, 
    //         GameObject.Find("coin icon").transform, 20, 1.5f, 0.02f, new Vector2(50, 50));
    //     shooting.OnClickTrigger(claimBtnPos.position);
    //     isTouchLock = false;
    // }

    public void ListSelectionEnd(int index)
    {
        currPageBuildingId = buildingIds[index];
        currListPage       = index;
        SetInfoData(index);
        //UpdateRewardGoldEff(index);
    }

    // int enableRewardCount;
    // long[] currPageWorkEndT;
    // // UI 에 골드 쌓아놓은 연출
    // void UpdateRewardGoldEff(int index)
    // {
    //     currPageWorkEndT  = NetworkManager.instance.GetWorkerEndLongTimeByBUID(buildingIds[index]);

    //     enableRewardCount = 0;
    //     for(int i = 0; i < 3; i++)
    //     {
    //         if(currPageWorkEndT[i] == 0) // Ready
    //         {
    //             SetListState(i, WorkState.Ready);
    //             continue;
    //         }
    //         else if(currPageWorkEndT[i] > HoneyPlug.Utils.Time.GetMillisecondsByDateTime(DateTime.UtcNow))
    //         {
    //             SetListState(i, WorkState.On);
    //         }
    //         else
    //         {
    //             SetListState(i, WorkState.Off);
    //         }

    //         enableRewardCount++;
    //     }
    //     if(enableRewardCount < 1)
    //     {
            
    //     }
    //     else if(enableRewardCount == 1)
    //     {
           
    //     }
    //     else // 많이 쌓임.
    //     {
           
    //     }
    // }

#endregion 맵 리스트 부분
    // enum WorkState
    // {
    //     Ready,
    //     On,
    //     Off
    // }

    // ADD 2020.11.23
    // 알바생 고용
    // [SerializeField]
    // GameObject setWorkerPopup;
    // [SerializeField]
    // GameObject[] workerObj;
    // [SerializeField]
    // Image[] workerIcon;
    // [SerializeField]
    // Text[] workerTime;

    // [SerializeField]
    // Image[] listBack;
    // [SerializeField]
    // Image[] buttonImg;

    // [SerializeField]
    // Sprite[] stateBack;
    // [SerializeField]
    // Sprite[] stateIcon;

    // int currBuildingId;
    // int currBuildingLv;

    // int selecIndex;

    // long[] endWorkTimes;
    // WorkState[] workState;

    // public void OpenWorkerPopup(int buildingId, int buildingLv)
    // {
    //     currBuildingId = buildingId; currBuildingLv = buildingLv;

    //     setWorkerPopup.SetActive(true);
    //     if(buildingLv == 1)
    //     {
    //         workerObj[0].SetActive(true); workerObj[1].SetActive(false); workerObj[2].SetActive(false); 
    //     }
    //     else if(buildingLv == 2)
    //     {
    //         workerObj[0].SetActive(true); workerObj[1].SetActive(true); workerObj[2].SetActive(false); 
    //     }
    //     else if(buildingLv == 4)
    //     {
    //         workerObj[0].SetActive(true); workerObj[1].SetActive(true); workerObj[2].SetActive(true); 
    //     }

    //     endWorkTimes = NetworkManager.instance.GetWorkerEndLongTimeByBUID(currBuildingId);
    //     StartCoroutine(StartWorkTimer());
    // }

    // IEnumerator StartWorkTimer()
    // {
    //     yield return new WaitForEndOfFrame();

    //     for(int i = 0; i < 3; i++)
    //     {
    //         if(endWorkTimes[i] == 0)
    //         {
    //             SetListState(i, WorkState.Ready);
    //             workerTime[i].text = string.Empty;
    //             continue;
    //         }
    //         if(endWorkTimes[i] > HoneyPlug.Utils.Time.GetMillisecondsByDateTime(DateTime.UtcNow))
    //         {
    //             SetListState(i, WorkState.On);
    //             workerTime[i].GetComponent<UITimerText>().StartTimer(endWorkTimes[i], i, EndTimerWorker);
    //         }
    //         else
    //         {
    //             SetListState(i, WorkState.Off);
    //             workerTime[i].text = string.Empty;
    //         }
    //     }
    // }

    // void SetListState(int index, WorkState state)
    // {
    //     workState[index] = state;
    //     int arrIndex = 0;
    //     switch(state)
    //     {
    //         case WorkState.Ready:
    //             arrIndex = 0;
    //             break;
    //         case WorkState.On:
    //             arrIndex = 1;
    //             break;
    //         case WorkState.Off:
    //             arrIndex = 2;
    //             break;
    //     }
    //     listBack[index].sprite  = stateBack[arrIndex]; 
    //     buttonImg[index].sprite = stateIcon[arrIndex];
    // }

    // 알바 시키기 팝업
    // public void CloseWorkerPopup()
    // {
    //     // TODO : 알바생 관련 코드 처리 위치
    //     BuildingManager.instance.ProcSetWorkman(DataManager.instance.GetAllBuildingID().ToArray());

    //     for(int i = 0; i < 3; i++)
    //         workerTime[i].GetComponent<UITimerText>().StopTimer();
    //     setWorkerPopup.SetActive(false);

    //     scrollCotent.GetChild(currListPage).GetComponent<UIMapViewBuildingItem>().UpdateWorkerIcons();
    // }

    // public void ClickStartWork(int index)
    // {
    //     if(workState[index] != WorkState.Ready) return;

    //     selecIndex = index;
    //     NetworkManager.instance.UpdateSetWorker(currBuildingId, index, index + 1, SuccessSetting);
    // }

    // void SuccessSetting(long endWorkerT)
    // {
    //     SetListState(selecIndex, WorkState.On);

    //     workerTime[selecIndex].GetComponent<UITimerText>().StartTimer(endWorkerT, selecIndex, EndTimerWorker);
    
    //     PopupManager.instance.Open(Common.PopupCode.WorkmanStart,
    //         "START_WORK", PopupManager.ButtonStyle.Ok, PopupManager.MoveType.CenterPumping,
    //         null);
    // }

    // void EndTimerWorker(int index)
    // {
    //     SetListState(index, WorkState.Off);
    //     workerTime[index].text = string.Empty;
    // }
}
