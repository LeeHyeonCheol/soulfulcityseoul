﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMiniGameReward : MonoBehaviour
{
    [SerializeField]
    GameObject[] effects;
    [SerializeField]
    Text rewardGold;

    Action endCallBack;

    public void SetData(ulong gold,Action callback)
    {
        rewardGold.text = string.Format("{0:#,0}", gold);

        endCallBack = callback;

        StartCoroutine(StartEffect());
    }

    IEnumerator StartEffect()
    {
        for(int i = 0; i < effects.Length; i++) effects[i].SetActive(true);
        yield return new WaitForSeconds(3f);

        endCallBack?.Invoke();

        Destroy(this.gameObject);
    }
}
