﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldUI : MonoBehaviour
{
    [SerializeField] List<Image> shields;

    private void Start()
    {
        Init();
    }

    void Init()
    {
        Refrush();
    }
    
    public void Refrush()
    {
        for(int i = 0; i < shields.Count && i < UserManager.instance.ShieldCount; i++)
        {
            shields[i].color = Color.white;
        }

        for(int i = UserManager.instance.ShieldCount; i < shields.Count; i++)
        {
            shields[i].color = Common.ColorHalfAlpha;
        }
    }
}
