﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public class UIShop : UIPanelShow
{
    public override void Open(Action callBack, float dealy = 0f)
    {
        base.Open(callBack, dealy);

        InitList();
        SetListItems(0);
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close(Action callBack, float dealy = 0f)
    {
        base.Close(callBack, dealy);
    }

    public void CloseButton()
    {
        base.Close();
    }

    [SerializeField]
    GameObject[] iapGoods;
    // [SerializeField]
    // TextAsset shopAsset;
    // ShopItem[] shopData;
    ShopItem[] sculptureData;
    [SerializeField]
    Transform scrollCotent;
    [SerializeField]
    GameObject goodsObj;

    [SerializeField]
    ToggleGroup toggleGrp;
    int currTabNum;

    void InitList()
    {
        sculptureData = DataManager.instance.sculptureData;
        currTabNum = -1;
    }

    void SetListItems(int tabNum)
    {
        int i;
        int itemCount = 0;

        if(tabNum == currTabNum) return;

        if(scrollCotent.childCount > 0)
        {
            for(i = 0; i < scrollCotent.childCount; i++)
                Destroy(scrollCotent.GetChild(i).gameObject);
        }

        if(tabNum == 0)
        {
            itemCount = iapGoods.Length;
            for(i = 0; i < itemCount; i++)
            {
                GameObject obj = Instantiate(iapGoods[i]);
                obj.transform.SetParent(scrollCotent);
                obj.transform.localScale = Vector3.one;
            }
        }
        else
        {
            itemCount = sculptureData.Length;
            for(i = 0; i < itemCount; i++)
            {
                GameObject obj = Instantiate(goodsObj);
                obj.transform.SetParent(scrollCotent);
                obj.transform.localScale = Vector3.one;

                ShopItem itemData = null;
                itemData = sculptureData[i];
                obj.transform.GetComponent<ShopListItem>().SetItem(itemData, tabNum);
            }
        }
        
        currTabNum = tabNum;
    }

    public void CheckToggleOn ()
    {
        foreach (Toggle t in toggleGrp.ActiveToggles()) 
        {
            if (t.isOn == true) 
            {
                if(t.name.Equals("Toggle1"))
                    SetListItems(0);
                else
                    SetListItems(1);
                break;
            }
        }
    } 
}
