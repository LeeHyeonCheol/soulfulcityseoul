﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class MapPosition : MonoBehaviour
{
    [SerializeField]
    Sprite redDot;
    [SerializeField]
    Image[] posDot;
    [SerializeField]
    GameObject[] naviDots;

    void Start()
    {
        
    }

    public void SetMapPosition(int posNum)
    {
        int i;
        int checkNum = posNum % 2;

        for(i = 0; i < posDot.Length; i++)
        {
            posDot[i].GetComponent<DOTweenAnimation>().DOKill();
            if(i == checkNum)
            {
                posDot[i].sprite = redDot;
                posDot[i].raycastTarget = false;
                naviDots[i].SetActive(true);
            }
            else
            {
                posDot[i].enabled = false;
                naviDots[i].SetActive(false);
            }
        }
        
    }
}
