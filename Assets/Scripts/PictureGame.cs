﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class PictureGame : MiniGame
{
    [SerializeField]
    ScrollRect backScroll;
    [SerializeField]
    RectTransform backGroundTrans;

    [SerializeField]
    DOTweenAnimation womanAnim;
    [SerializeField]
    GameObject bearObj;
    [SerializeField]
    GameObject blurObj;
    [SerializeField]
    GameObject[] clearResult;
    [Header("UI")]
    [SerializeField] Slider slider;
    [SerializeField] Text timer;

    [SerializeField]
    GameObject failEff;
    bool startGame;
    bool endGame;
    bool gameOver;

    [SerializeField]
    ScrollRect scroll;

    void Start()
    {
        gameOver = false;
        Init();
        startGame = true;
        endGame = false;
    }

    void LateUpdate()
    {
        if (endGame) return;
        if (isPause) return;


        if (startGame)
        {
            #region Check Time
            currentTime -= Time.deltaTime;
            if (timerFlag == false) { currentTime = limitTime; }
            UpdateTimeUI(slider, timer, limitTime, currentTime);

            if (currentTime <= 0)
            {
                startGame = false;
                gameOver = true;
            }
            else
            {
                if (backGroundTrans.localPosition.x >= -623f && backGroundTrans.localPosition.x <= -619f)
                {
                    if (backGroundTrans.localPosition.y >= 29f && backGroundTrans.localPosition.y <= 34f)
                    {
                        startGame = false;
                        GameClear();
                    }
                }
            }
            #endregion
        }
        else if (gameOver)
        {
            GameOver();
        }
    }

    void GameClear()
    {
        backScroll.enabled = false;
        blurObj.SetActive(false);
        clearResult[0].SetActive(true);

        StartCoroutine(RewardResult()); endGame = true;
    }

    public void ClearResult()
    {
        clearResult[1].SetActive(true);
    }

    void GameOver()
    {
        backScroll.enabled = false;
        bearObj.SetActive(true); womanAnim.DOPlay();

        GameObject obj = Instantiate(failEff);
        obj.transform.SetParent(this.transform);
        obj.transform.localScale = Vector3.one;
        obj.transform.localPosition = Vector3.zero;

        StartCoroutine(RewardResult()); endGame = true;
    }

    IEnumerator RewardResult()
    {
        yield return new WaitForSeconds(3f);

        OnClose();
    }

    int GetScoreLevel()
    {
        if (slider.value >= 0.6f) return 3;
        else if (slider.value >= 0.4f) return 2;
        else if (slider.value >= 0.1f) return 1;

        return 0;
    }

    void OnClose()
    {
        int scoreLevel = -1;
        // 리워드 결과 셋팅
        if (gameOver == false) scoreLevel = GetScoreLevel();
        GetComponent<DestroyEvent>().SetRewardLevel(scoreLevel);

        Destroy(this.gameObject);
    }

    [Header("Play Time")]
    [SerializeField] float limitTime;
    [SerializeField] float currentTime;
    [SerializeField] bool timerFlag;
    DG.Tweening.Core.TweenerCore<float, float, DG.Tweening.Plugins.Options.FloatOptions> sliderTween;
    DG.Tweening.Core.TweenerCore<string, string, DG.Tweening.Plugins.Options.StringOptions> timeTween;

    void Init()
    {
        currentTime = limitTime;
        slider.value = 0.0f;
        if (sliderTween != null)
        {
            sliderTween.Kill(false);
            sliderTween = null;
        }
        if (timeTween != null)
        {
            timeTween.Kill(false);
            timeTween = null;
        }

        sliderTween = null;
        timeTween = null;
        timerFlag = false;
    }

    #region UI reference
    void UpdateTimeUI(Slider slider, Text timer, float max, float current)
    {
        if (sliderTween == null)
        {
            float value = current / max;
            float difference = Mathf.Abs(slider.value - value) * 2;
            sliderTween = slider.DOValue(value, difference);
            sliderTween.SetEase(Ease.Linear);
            sliderTween.OnComplete(
            () =>
            {
                sliderTween = null;
                timerFlag = true;
            });
        }
        if (timeTween == null && timerFlag)
        {
            timeTween = timer.DOText(((int)current).ToString(), 0.05f).
            OnComplete(() =>
            {
                timeTween = null;
            });
        }
        else if (timerFlag == false)
        {
            timer.text = "Ready!";
        }
    }
    #endregion

    public override void Pause()
    {
        base.Pause();

        scroll.enabled = false;        
    }

    public override void Continue()
    {
        base.Continue();

        scroll.enabled = true;
    }
}
