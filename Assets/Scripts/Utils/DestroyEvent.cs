﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEvent : MonoBehaviour
{
    public delegate void OnEndEvent(int param);
    OnEndEvent endEvent;

    int rewardLevel = 0;

    #region External function
    public void AddEvent(OnEndEvent @event)
    {
        endEvent += @event;
    }
    public void ClearEvent()
    {
        endEvent = null;
    }
    public void SetRewardLevel(int level)
    {
        rewardLevel = level;
    }
    #endregion

    private void OnDestroy()
    {
        endEvent?.Invoke(rewardLevel);
    }
}
