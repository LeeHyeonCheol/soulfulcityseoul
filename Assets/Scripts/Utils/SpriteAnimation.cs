﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteAnimation : MonoBehaviour
{
    [SerializeField] Image target;
    [SerializeField] List<Sprite> sprites;

    [SerializeField] float duration;
    [SerializeField, ReadOnly] float current = 0.0f;
    [SerializeField, ReadOnly] int index = 0;

    [SerializeField] bool eventFlag = false;
    Action<GameObject> action;

    private void OnEnable()
    {
        current = 0.0f;
        index = 0;
    }

    public void SetEventAnimation(Color color,Action<GameObject> action)
    {
        eventFlag = true;
        target.color = color;
        this.action = action;
    }


    // Update is called once per frame
    void Update()
    {
        current += Time.deltaTime;

        if(duration <= current)
        {
            current -= duration;
            index++;
            if(sprites.Count <= index && eventFlag)
            {
                action?.Invoke(this.gameObject);
            }
            else
            {
                index = (index) % sprites.Count;
                target.sprite = sprites[index];
            }
            
           
        }
    }
}
