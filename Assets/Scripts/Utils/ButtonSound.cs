﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HoneyPlug.Audio
{
    [RequireComponent(typeof(Button))]
    public class ButtonSound : MonoBehaviour
    {
        void Start()
        {
            Button b = GetComponent<Button>();
            AudioSource audio = GetComponent<AudioSource>();
            b.onClick.AddListener(delegate() 
            { 
                SoundManager.instance.PlayOneSound(SoundManager.instance.button);
            });
        }
    }
}
