﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GoldConversion
{
    static char[] keys ={
        ' ','K','M','G','T','P','E'
    };

    public static string GoldToString(ulong gold)
    {
        ulong unit = Common.unitE;
        for(int i = keys.Length-1; 0 <= i; i--)
        {
            if (unit < 1000) return gold.ToString();
            if ((gold / unit) != 0)
            {
                // 소수점 제작!!!!
                ulong dec = gold % unit / (unit / 10);
                return (gold / unit) + "." + dec + keys[i].ToString();
            }

            unit /= 1000;
        }

        return Common.NA;
    }


    public static ulong StringToGold(string gold)
    {
        // 123.4M = string
        for(int i = 0; i < keys.Length; i++)
        {
            if(gold[gold.Length-1] == keys[i])
            {
                ulong unit = MyMath.pow(Common.unitK, i);
                string removeUnit = gold.Remove(gold.Length - 1);

                // 소수점 탐색
                string[] data = removeUnit.Split('.');
                // 단위 분할
                if(data.Length == 2)
                {
                    return ulong.Parse(data[0]) * unit + (ulong.Parse(data[1]) * unit / 10);
                }
                else
                {
                    return ulong.Parse(data[0]) * unit;
                }
            }
        }
#if USE_DEBUG
        Debug.LogWarning("Not Find unit..");
#endif
        return 0;
    }

    public static char GetUnit(ulong gold)
    {
        return GetUnit(GoldToString(gold));
    }
    public static char GetUnit(string gold)
    {
        // 123.4M = string
        for (int i = 0; i < keys.Length; i++)
        {
            if (gold[gold.Length - 1] == keys[i])
            {
                return keys[i];
            }
        }
        return ' ';
    }
    public static ulong UnitToULong(char key)
    {
        for (int i = 0; i < keys.Length; i++)
        {
            if (key == keys[i])
            {
                ulong unit = MyMath.pow(Common.unitK, i);

                if (unit == 0) return 1;
                else return unit;
            }
        }
#if USE_DEBUG
        Debug.LogWarning("Not Find unit..");
#endif
        return 0;
    }
    public static bool IsGold(string gold)
    {
        // Find unit
        for (int i = 0; i < keys.Length; i++)
        {
            if (gold[gold.Length - 1] == keys[i])
            {
                return true;
            }
        }
        // is gold?
        try
        {
            ulong parse = ulong.Parse(gold);
            if (GoldToString(parse) == Common.NA) return false;
            else return true;
        }
        catch(Exception)
        {
            return false;
        }
    }
}
    