﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HoneyPlug.Utils
{
    public class Time
    {    
        public static DateTime GetUniversalTimeByLongType(long unixDate) 
        {
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(unixDate).ToUniversalTime();

            return date;
        }

        public static long GetUniversalTimeByLongType_AddMin(long unixDate, int min) 
        {
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(unixDate).ToUniversalTime();

            return GetMillisecondsByDateTime(date.AddMinutes(min));
        }  

        public static DateTime GetUTCNow(string value) 
        {
            long unixDate = long.Parse(value);
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(unixDate).ToUniversalTime();

            return date;
        } 

        public static long GetMillisecondsByDateTime(DateTime date)
        {
            long resultTime = (long)date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            return resultTime;
        }

        public static long GetSecondsByDateTime(DateTime date)
        {
            long resultTime = (long)date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            return resultTime;
        }
        
        public static string FormatTimeHourMinSec(int totalSec)
        {
            int hour = (totalSec / 3600);
            int min  = (totalSec % 3600 / 60);
            int sec  = (totalSec % 3600 % 60);
            string remainT = string.Format("{0:D2}h{1:D2}m{2:D2}s", hour, min, sec);

            return remainT;
        }

        public static string FormatTimeHourMin(int totalSec)
        {
            int hour = (totalSec / 3600);
            int min  = (totalSec % 3600 / 60);
            string remainT = string.Format("{0:D2}h{1:D2}m", hour, min);

            return remainT;
        }

        public static string FormatTimeMinSec(int totalSec)
        {
            int min = (totalSec % 3600 / 60);
            int sec = (totalSec % 3600 % 60);
            string remainT = string.Format("{0:D2}:{1:D2}", min, sec);

            return remainT;
        }
    }
}
