﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent : MonoBehaviour
{
    [SerializeField] Common.RULItem.ID key;
    [SerializeField] Animation ani;
    [SerializeField] System.Action finishCallback;


    public Common.RULItem.ID Key { get => key; }
    
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        ani.Play();
    }

    public void SetCallBack(System.Action action)
    {
        finishCallback += action;
    }

    public void Finish()
    {
        finishCallback?.Invoke();
        finishCallback = null;
        Destroy(this.gameObject);
        
    }
}
