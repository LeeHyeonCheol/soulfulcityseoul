﻿using System;
public class MyMath
{
    public static ulong pow(ulong lvalue,int rvalue)
    {
        ulong revalue = lvalue;

        if (rvalue == 0) return 0;
        if (rvalue == 1) return lvalue;

        for (int i = 1; i < rvalue; i++)
        {
            revalue *= lvalue;
        }

        return revalue;
    }
}
