﻿using UnityEngine;
using UnityEngine.UI;

namespace HoneyPlug.Utils
{
    public class SafeArea : MonoBehaviour
    {
        RectTransform Panel;
        Rect LastSafeArea = new Rect(0, 0, 0, 0);

        public Text message;

        void Awake()
        {
            Panel = GetComponent<RectTransform>();
            Refresh();
        }

        void Update()
        {
            Refresh();
        }

        void Refresh()
        {
            Rect safeArea = GetSafeArea();

#if UNITY_EDITOR
            if (Screen.width == 1125 && Screen.height == 2436)
            {
                safeArea.y = 102;
                safeArea.height = 2202;
            }
            if (Screen.width == 2436 && Screen.height == 1125)
            {
                safeArea.x = 132;
                safeArea.y = 63;
                safeArea.height = 1062;
                safeArea.width = 2172;
            }
#endif

            if (safeArea != LastSafeArea)
                ApplySafeArea(safeArea);
        }

        public void ForceRefresh()
        {
            LastSafeArea = new Rect(0, 0, 0, 0);
            Refresh();
        }

        Rect GetSafeArea()
        {
            return Screen.safeArea;
        }

        void ApplySafeArea(Rect r)
        {
            // Panel.anchoredPosition = Vector2.zero;
            // Panel.sizeDelta = Vector2.zero;
            // var anchorMin = r.position;
            // var anchorMax = r.position + r.size;
            // anchorMin.x /= Screen.width;
            // anchorMin.y /= Screen.height;
            // anchorMax.x /= Screen.width;
            // anchorMax.y /= Screen.height;
            // Panel.anchorMin = anchorMin;
            // Panel.anchorMax = anchorMax;
            // LastSafeArea = r;

            LastSafeArea = r;

            Vector2 anchorMin = r.position;
            Vector2 anchorMax = r.position + r.size;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;
            Panel.anchorMin = anchorMin;
            Panel.anchorMax = anchorMax;
        }
    }
}
