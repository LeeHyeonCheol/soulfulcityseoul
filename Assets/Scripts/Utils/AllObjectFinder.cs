﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllObjectFinder : MonoBehaviour
{
    [SerializeField]
    List<Text> gameObjects;

    private void OnValidate()
    {
        gameObjects = new List<Text>();

        var games = GameObject.FindObjectsOfType<GameObject>();

        for (int i = 0; i < games.Length; i++)
        {
            if(games[i].GetComponent<Text>() != null)
            {
                gameObjects.Add(games[i].GetComponent<Text>());
            }
        }
    }

}
