﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventSystemAndroid : MonoBehaviour
{
    const float inchToCm = 2.54f;

    [SerializeField] EventSystem system;
    [SerializeField] float dragThresholdCM = .5f;

    void SetDragThreshold()
    {
        if(system != null)
        {
            system.pixelDragThreshold = (int)(dragThresholdCM * Screen.dpi / inchToCm);
        }
    }

    void Awake()
    {
        SetDragThreshold();
    }
}
