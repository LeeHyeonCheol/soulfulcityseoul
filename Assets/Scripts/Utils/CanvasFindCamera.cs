﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFindCamera : MonoBehaviour
{
    [SerializeField] Canvas canvas;
    [SerializeField] string layerName;

    // Start is called before the first frame update
    void Start()
    {
        canvas.worldCamera = Camera.main;
        canvas.sortingOrder = 1;
        canvas.sortingLayerName = layerName;
        canvas.planeDistance = 100;
    }
}
