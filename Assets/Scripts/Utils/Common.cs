﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Common
{
    const string testVersion = "[Test ver 0.0022] ";

    #region Building : BUD
    public const int BUDMaxLevel = 4;
    public const int BUDMaxCount = 11;
    public const long BUDConstructionTime = 3;
    public const string BUDPrefabPath = "Prefabs/Building";
    public const string BUDSceneName = "BuildingTest";
    public const string BUDUpgradeFormat        = testVersion + " Building Construction : {0} ID";
    public const string BUDUpgradeTimeFormat    = testVersion + " Building Upgrade Time : {0} ID";
    public const string BUDUpgradeADFormat      = testVersion + " Building Construction AD Check : {0} ID {1} Level";
    public const string BUDLevel                = testVersion + " Building level : {0} ID";
    public const string BUDOpenCount            = testVersion + " Building open count";
    public const string BUDSculpturePath = "Image/Building/Sculpture/";

    #endregion
    #region Popup Code
    public class PopupCode
    {
        public const uint BUDUpgrade = 1;
        public const uint BUDUpgradeFail = 2;
        public const uint WorkmanArrangementFail = 3;

        public const uint WorkmanWarning = 101;
        public const uint WorkmanGetGold = 102;
        public const uint WorkmanStart   = 103;
        public const uint WokrmanRewardAD = 104;

        public const uint StealReward = 201;
        public const uint StealRewardAD = 202;

        public const uint PlayfabError = 406;

        public const uint ExitPopup = 501;
        public const uint MinigameExit = 510;

        public const uint QuizTimeOver = 601;

        public const uint CleaningRewardAD = 701;
        public const uint CleaningRewardGold = 702;

        public const uint QuizRewardAD = 801;

        public const uint BapbirdGame = 850;
        public const uint BapbirdGameAD = 851;
        public const uint BapbirdGameReward = 852;

        public const uint LessPortion = 990;
        public const uint LessGold    = 991;

        public const uint SuccessPurchase = 999;
    }
    #endregion
    #region Gold Conversion : unit
    // Note : 단위 한계 18446744073709551615;
    //                18E 446P 744T 073G 709M 551K 615
    public const ulong unitK = 1000;
    public const ulong unitM = 1000000;
    public const ulong unitG = 1000000000;
    public const ulong unitT = 1000000000000;
    public const ulong unitP = 1000000000000000;
    public const ulong unitE = 1000000000000000000;
    #endregion
    #region Roulette : RUL
    public const int RULUseCountMax = 50;
    public const int RULRecoveryDelay = 300;
    public const string RULUseCountFormat = testVersion + " Roulette use count : ";
    public const string RULRecoveryTimeFormat = testVersion + " Roulette recovery time start : ";
    public const string RULSceneName = "Main_Scene";
    public static int[] RULMGValue = { 1, 2, 5, 10, 20 };
    #region Roulette Item
    public class RULItem
    {
        public enum ID
        {
            Attack,
            Shield,
            Steal,
            Spins,
            Gold,
            NONE
        }

        public const string ShildFormat = testVersion + " Roulette shild count : ";
    }
    public const int StealSelectedMaxLength = 3;
    #endregion
    #endregion
    #region Workman
    public const string WorkmanPrefabPath = "Prefabs/Workman/";
    public const int WorkmanArrangmentMaxCount = 3;
    #endregion
    #region Sculpture
    public const string SculpturePrefabPath = "Prefabs/Sculpture/";
    #endregion

    static Color colorAlpha = new Color(1, 1, 1, 0);
    static Color colorHalfAlpha = new Color(1, 1, 1, .5f);
    public static Color ColorAlpha { get => colorAlpha; }
    public static Color ColorHalfAlpha { get => colorHalfAlpha; }
    public const char splitWord = '|';
    public const char subSplitWord = ',';
    public const char emptyHyphen = '-';

    //= 인스펙터에서 사용하기 위하여 적용됨.
    [System.Serializable]
    public class IList
    {
        [SerializeField] public List<Image> list;

        public IList()
        {
            list = new List<Image>();
        }
        public Image this[int index]
        {
            get=> list[index];
            set => list[index] = value;
        }
        public int Count
        {
            get => list.Count;
        }
    }
    [System.Serializable]
    public class GList
    {
        [SerializeField] public List<GameObject> list;

        public GList()
        {
            list = new List<GameObject>();
        }
        public GameObject this[int index]
        {
            get => list[index];
            set => list[index] = value;
        }
        public int Count
        {
            get => list.Count;
        }
    }
    
    public const string LeaderBoardName = "Best Score";
    public const string InitSetBuildingId      = "1|2|3|4|5|6|7|8|9|10|11";
    public const string InitSetBuildingLevel   = "1,1,1|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0";
    public const string InitSetBuildingBombing = "0|0|0|0|0|0|0|0|0|0|0";
    
    public const string WorkerBasicKey  = "worker_";
    public const string WorkEndBasicKey = "setWTime_"; 

    // 알바생 일하는 시간(분)
    public const int WorkLevel1_MinTime = 120;
    public const int WorkLevel2_MinTime = 240;
    public const int WorkLevel4_MinTime = 480;

    public const string NA = "N/A";

}
