﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class FindMapGame : MonoBehaviour
{
    [SerializeField]
    Image[] navigatePos;
    [SerializeField]
    GameObject[] navigateDot;
    [SerializeField]
    Sprite redDot;
    [SerializeField]
    Image goal;

    int[,] map1Datas = {
        {2, 3},
        {6, 7},
        {3, 5},
        {4, -1},
        {-1, 5},
        {-1, 100},
        {7, 9},
        {6, 8},
        {7, 10},
        {5, -1},
        {-1, 100}
    };

    int prePosNum;

    [Header("UI")]
    [SerializeField] Slider slider;
    [SerializeField] Text timer;

    bool startGame;
    bool endGame;
    bool gameOver;

    [SerializeField]
    GameObject failEff;

    void Start()
    {
        prePosNum = -1;
        gameOver = false; 
        Init();
        startGame = true;
        endGame   = false;
    }

    void LateUpdate()
    { 
        if(endGame) return;

        if(startGame)
        {
            #region Check Time
            currentTime -= Time.deltaTime;
            if (timerFlag == false) { currentTime = limitTime; }
            UpdateTimeUI(slider, timer,limitTime, currentTime);

            if(currentTime <= 0)
            {
                startGame = false;
                gameOver = true;
            }
            #endregion
        }
        else if(gameOver)
        {
            GameOver();
        }
    }

    public void OnClickStreetPos(int posNum)
    {
        int i;
    
        // Goal
        if(posNum == 100)
        {
            goal.raycastTarget = false;
            goal.transform.DOScale(new Vector3(4, 4, 4), 0.5f).SetEase(Ease.InBounce);

            GameClear();
            return;
        }

        for(i = 0; i < navigatePos.Length; i++)
        {
            navigatePos[i].GetComponent<DOTweenAnimation>().DOKill();
            if(i == map1Datas[posNum, 0] || i == map1Datas[posNum, 1])
            {
                navigatePos[i].gameObject.SetActive(true);
            }
            else
            {
                if(i == posNum)
                {
                    navigatePos[i].gameObject.SetActive(true);
                    navigatePos[i].sprite        = redDot;
                    navigatePos[i].raycastTarget = false;
                }
                else
                {
                    if(navigatePos[i].sprite != redDot)
                        navigatePos[i].gameObject.SetActive(false);
                }
            }
        }

        switch(posNum)
        {
            case 0: case 1: case 2: case 4:
                navigateDot[posNum].SetActive(true);
                break;
            case 3:
                if(prePosNum == 0) navigateDot[3].SetActive(true);
                if(prePosNum == 2) navigateDot[13].SetActive(true);
                break;
            case 5:
                if(prePosNum == 2) navigateDot[5].SetActive(true);
                if(prePosNum == 4) navigateDot[12].SetActive(true);
                if(prePosNum == 9) navigateDot[6].SetActive(true);
                break;
            case 7:
                if(prePosNum == 1) navigateDot[9].SetActive(true);
                break;
            case 6: case 10:
                navigateDot[posNum + 1].SetActive(true);
                break;
            case 8:
                navigateDot[posNum + 2].SetActive(true);
                break;
            case 9:
                navigateDot[posNum - 1].SetActive(true);
                break;
        }
        if(posNum == 5 || posNum == 9 || posNum == 10) goal.raycastTarget = true;

        prePosNum = posNum;
    }

    void GameClear()
    {
        startGame = false; endGame = true;
        StartCoroutine(RewardResult());
    }

    void GameOver()
    {
        GameObject obj = Instantiate(failEff);
        obj.transform.SetParent(this.transform);
        obj.transform.localScale    = Vector3.one;
        obj.transform.localPosition = Vector3.zero;

        StartCoroutine(RewardResult()); endGame = true;
    }

    IEnumerator RewardResult()
    {
        yield return new WaitForSeconds(3f);

        OnClose();
    }

    int GetScoreLevel()
    {
        if(slider.value >= 0.6f) return 3;
        else if(slider.value >= 0.4f) return 2;
        else if(slider.value >= 0.1f) return 1;

        return 0;
    }

    void OnClose()
    {
        int scoreLevel = -1;
        // 리워드 결과 셋팅
        if(gameOver == false) scoreLevel = GetScoreLevel();
        GetComponent<DestroyEvent>().SetRewardLevel(scoreLevel);
        
        Destroy(this.gameObject);
    }

    [Header("Play Time")]
    [SerializeField] float limitTime;
    [SerializeField] float currentTime;
    [SerializeField] bool timerFlag;
    DG.Tweening.Core.TweenerCore<float, float, DG.Tweening.Plugins.Options.FloatOptions> sliderTween;
    DG.Tweening.Core.TweenerCore<string, string, DG.Tweening.Plugins.Options.StringOptions> timeTween;
    
    void Init()
    {
        currentTime = limitTime;
        slider.value = 0.0f;
        if(sliderTween != null)
        {
            sliderTween.Kill(false);
            sliderTween = null;
        }
        if(timeTween != null)
        {
            timeTween.Kill(false);
            timeTween = null;
        }

        sliderTween = null;
        timeTween = null;
        timerFlag = false;
    }

    #region UI reference
    void UpdateTimeUI(Slider slider,Text timer,float max, float current)
    {
        if(sliderTween == null)
        {
            float value = current / max;
            float difference = Mathf.Abs(slider.value - value) * 2;
            sliderTween = slider.DOValue(value, difference);
            sliderTween.SetEase(Ease.Linear);
            sliderTween.OnComplete(
            ()=>
            {
                sliderTween = null;
                timerFlag = true;
            });
        }
        if(timeTween == null && timerFlag)
        {
            timeTween = timer.DOText(((int)current).ToString(), 0.05f).
            OnComplete(() =>
            {
                timeTween = null;
            });
        }
        else if(timerFlag == false)
        {
            timer.text = "Ready!";
        }
    }
    #endregion
}
