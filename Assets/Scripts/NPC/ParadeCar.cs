﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParadeCar : NPC
{
    [SerializeField] Image character;
    
    #region Override function
    protected override void Start()
    {
        base.Start();
        this.gameObject.tag = "ParadeCar";
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }

    protected override void OnApplicationPause(bool pause)
    {
        base.OnApplicationPause(pause);
    }

    protected override void OnApplicationFocus(bool focus)
    {
        base.OnApplicationFocus(focus);
    }

    protected override void Update()
    {
        base.Update();
    }
    #endregion
}