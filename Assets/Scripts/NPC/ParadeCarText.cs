﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParadeCarText : MonoBehaviour
{
    [SerializeField] Transform[] imgs;
    void LateUpdate()
    {
        if(this.transform.localScale != imgs[0].localScale)
        {
            for(int i = 0; i < imgs.Length; i++) imgs[0].localScale = this.transform.localScale;
        }
    }
}
