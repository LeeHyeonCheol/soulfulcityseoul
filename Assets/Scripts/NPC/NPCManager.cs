﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCManager : MonoBehaviour
{
    [Header("NPC")]
    [SerializeField] List<NPC> people;
    [SerializeField] List<ParadeCar> paradeCars;
    [SerializeField, ReadOnly] int activeCount;
    [SerializeField, ReadOnly] List<NPC> total;

    [Header("Rottie")]
    //= 골드 박스 => 광고로 직행
    [SerializeField] NPC rottie;
    [SerializeField] Sprite rottieSpeechImage;
    [SerializeField] bool isDisableUpdateNPC;
    //= 좌표 건물 우측 혹은 좌측에서 등장


    [Header("NPC Minigame")]
    [SerializeField] StartGame startGame;
    [SerializeField] List<UnityEngine.Events.UnityAction> gameFlag;
    [SerializeField] List<Sprite> buttonImages;
    
    [Header("UI")]
    [SerializeField] RectTransform parent;
    [SerializeField, ReadOnly] bool isActive;

    [Header("Create")]
    [SerializeField] float miniDelay;
    [SerializeField] float rottieDelay;
    [SerializeField] float rottieDuration;
    [SerializeField] bool isRottieRunning;
    [SerializeField, ReadOnly] float rottieCurrent;
    [SerializeField, ReadOnly] float rottieLast;
    [SerializeField, ReadOnly] float miniCurrent;

    #region Property List
    public bool IsDisableUpdateNPC { get => isDisableUpdateNPC; set => isDisableUpdateNPC = value; }
    public StartGame StartGame { get => startGame; }
    #endregion

    private void Start()
    {
        Init();
    }

    void Init()
    {
        gameFlag = new List<UnityEngine.Events.UnityAction>();
        gameFlag.Add(delegate
        {
            startGame.GameStart_Minigame_1();
            Disable();
        });
        gameFlag.Add(delegate
        {
            startGame.GameStart_Minigame_2();
            Disable();
        });
        gameFlag.Add(delegate
        {
            startGame.GameStart_Minigame_3();
            Disable();
        });
        gameFlag.Add(delegate
        {
            startGame.GameStart_Ballon();
            Disable();
        });

        //isRottieRunning = false;
    }

    #region External function
    public void Set(int level)
    {
        activeCount = (people.Count + paradeCars.Count < level) ? people.Count + paradeCars.Count : level;

        total = new List<NPC>();
        total.AddRange(people.ToArray());
        total.AddRange(paradeCars.ToArray());

        for (int i = 0; i < activeCount; i++)
        {
            total[i].gameObject.SetActive(true);
        }
    }
    public void Disable()
    {
        parent.gameObject.SetActive(false);
    }
    public void Enable()
    {
        parent.gameObject.SetActive(true);
    }
    public void Enable(int dummy)
    {
        Enable();
    }
    #endregion

    void AssignmentMinigame()
    {
        //= Rand : Minigame index
        int rand = Random.Range(0, gameFlag.Count);
        int index = Random.Range(0, activeCount);
        float duration = Random.Range(10.0f, 25.0f);

        //= 이미 미니게임 버튼이 활성화 되어있는 경우 스킵.
        if (total[index].AssignmentSpeechBubble == false)
        {
            if(total[index].tag == "NPC")
            {
                total[index].SetSpeechBubble(buttonImages[rand], duration, gameFlag[rand]);
            }
        }
    }

    private void Update()
    {
        if (isDisableUpdateNPC) return;

        //= 미니게임 오브젝트 생성 및 파괴
        miniCurrent += Time.deltaTime;

        if(miniDelay < miniCurrent)
        {
            miniCurrent = 0;
            AssignmentMinigame();
        }

        //= 로티 생성 및 파괴
        if (isRottieRunning)
        {
            rottieLast += Time.deltaTime;

            if (rottieDuration < rottieLast)
            {
                rottieLast = 0;

                //= 로티 제거
                rottie.gameObject.SetActive(false);
                isRottieRunning = false;
            }
        }
        else
        {
            rottieCurrent += Time.deltaTime;
            if (rottieDelay < rottieCurrent)
            {
                rottieCurrent = 0;

                rottie.gameObject.SetActive(true);
                //= 로티 위치 배치
                int openCount = BuildingManager.instance.OpenCount;
                int rand = Random.Range(0, openCount);
                Building target = BuildingManager.instance.UiSet.buildings[rand];

                float saveLottieY = rottie.transform.localPosition.y;

                rottie.transform.position = target.transform.position;
                rottie.transform.localPosition = new Vector3(
                    rottie.transform.localPosition.x + 300,
                    saveLottieY,
                    0 /* 2D UI zero z set*/
                    );

                rottie.SetSpeechBubble(rottieSpeechImage, rottieDuration,
                delegate
                {
                    int randomReward = Random.Range(0, 2); //= 0~1
                    if(randomReward == 0) UIManager.instance.OpenADRewardGold();
                    else UIManager.instance.OpenADRewardPortion();
                    rottie.gameObject.SetActive(false);
                    isRottieRunning = false;
                    rottieLast = 0;
                });

                isRottieRunning = true;
            }
        }
    }


    // ##### 21.02.05 Patch 1.0.1
    public bool ActiveRottie()
    {
        return rottie.gameObject.activeSelf;
    }
}
