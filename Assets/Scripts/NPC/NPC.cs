﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class NPC : MonoBehaviour
{
    [System.Serializable]
    public enum State : short
    {
        NonAnimation = 1 << 0,
        Animation    = 1 << 1,
        OffEffect   = 1 << 2,
    }

    
    [SerializeField] State state = State.Animation;
    [SerializeField] bool normalNPCSet = true;
    [SerializeField] Image image;
    [SerializeField] List<Sprite> images;


    [Header("Minigame reference")]
    [SerializeField] RectTransform parent;
    [SerializeField] Button minigameTrigger;
    [SerializeField] float minigameDuration;

    [Header("effect")]
    [SerializeField] List<GameObject> effects;
    [SerializeField] float effectDuration;

    [SerializeField] float changeDelay;
    [SerializeField] float speed;
    [SerializeField] Vector2 moveLockAt = new Vector2(1,1);
    [SerializeField, ReadOnly] float mapStartPos;
    [SerializeField, ReadOnly] float mapEndPos;
    [SerializeField, ReadOnly] Vector2 goalPos = Vector2.zero;
    [SerializeField, ReadOnly] Vector2 direction = Vector2.left;

    [Header("Debug")]
    [SerializeField, ReadOnly] float current;
    [SerializeField, ReadOnly] int index;
    [SerializeField, ReadOnly] bool initFlag;
    
    [SerializeField] bool enableNoFlip; // ADD : 21.02.23

    Coroutine routine;

    #region Property
    public bool AssignmentSpeechBubble { get; set; }
    #endregion

    protected virtual void Start()
    {
        if(routine == null)
        {
            routine = StartCoroutine(Init());
        }
    }
    #region 비활성화 관련 안전 대책
    protected virtual void OnDisable()
    {
        if(initFlag == false && routine != null)
        {
            routine = null;
        }
    }
    protected virtual void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            if (routine != null)
            {
                StopCoroutine(routine);
                routine = null;
            }
        }
        else
        {
            if (routine == null && initFlag == false)
            {
                routine = StartCoroutine(Init());
            }
        }
    }
    protected virtual void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            if (routine == null)
            {
                routine = StartCoroutine(Init());
            }
        }
        else
        {
            if (routine != null && initFlag == false)
            {
                StopCoroutine(routine);
                routine = null;
            }
        }
    }
    #endregion

    #region External function
    //speech bubble
    public void SetSpeechBubble(Sprite buttonImage,float duration,UnityEngine.Events.UnityAction onClickCallback)
    {
        //= 미니게임 버튼에 들어가는 이미지를 받아옴
        //= 미니게임을 실행하기 위한 이벤트 함수를 받아옴.
        parent.gameObject.SetActive(true);
        minigameTrigger.onClick.RemoveAllListeners();
        minigameTrigger.onClick.AddListener(onClickCallback);
        minigameTrigger.image.sprite = buttonImage;

        minigameDuration = duration;

        AssignmentSpeechBubble = true;
    }
    #endregion
    void DisableSpeechBubble()
    {
        parent.gameObject.SetActive(false);
        AssignmentSpeechBubble = false;
    }

    IEnumerator Init()
    {
        yield return new WaitUntil(() => BuildingManager.instance != null);

        if(normalNPCSet && parent != null) { parent.gameObject.SetActive(false); }
       
        mapStartPos = 0;
        mapEndPos = BuildingManager.instance.UiSet.ContentW;

        if(normalNPCSet) transform.localPosition = new Vector2(Random.Range(mapStartPos, mapEndPos), transform.localPosition.y);

        if(state == State.OffEffect && effects != null)
        {
            for(int i = 0; i < effects.Count; i++)
            {
                effects[i].SetActive(true);
            }
        }

        MoveSet();

        initFlag = true;
    }

    void MoveSet()
    {
        while(true)
        {
            goalPos = new Vector2(Random.Range(0, mapEndPos), transform.localPosition.y);

            if (10.0f <= Vector2.Distance(goalPos,(Vector2)this.transform.localPosition))
            {
                break;
            }
        }

        if(this.transform.localPosition.x < goalPos.x)
        {
            direction = Vector2.right;
            image.transform.localScale = moveLockAt;
        }
        else
        {
            direction = Vector2.left;
            Vector2 lockAt = moveLockAt;
            if(enableNoFlip == false) lockAt.x *= -1; // flip
            image.transform.localScale = lockAt;
        }
    }

    protected virtual void Update()
    {
        if(initFlag)
        {
            #region Animation and move
            current += Time.deltaTime;
            if (changeDelay <= current)
            {
                current %= changeDelay;
                if (state == State.Animation)
                {
                    index = (index + 1) % images.Count;
                    image.sprite = images[index];
                }
                else if (state == State.OffEffect)
                {
                    index = (index + 1) % effects.Count;

                    effects[index].SetActive(false);
                    for(int i = 0; i < effects.Count; i++)
                    {
                        if(i != index) effects[i].SetActive(true);
                    }
                }
            }

            this.transform.localPosition += (Vector3)(direction * Time.deltaTime * speed);

            if(Vector2.Distance(goalPos, (Vector2)this.transform.localPosition) <= speed * Time.deltaTime * 2)
            {
                MoveSet();
            }
            #endregion
            #region minigame
            if(AssignmentSpeechBubble)
            {
                minigameDuration -= Time.deltaTime;

                if(minigameDuration <= 0)
                {
                    DisableSpeechBubble();
                }
            }
            #endregion
        }


    }

}
