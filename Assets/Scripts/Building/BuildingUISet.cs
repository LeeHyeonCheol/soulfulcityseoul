﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class BuildingUISet : MonoBehaviour
{
    [Header("Building")]
    [SerializeField] RectTransform buildingParent;
    [SerializeField] float startOffsetX = 0.0f;
    [SerializeField] float endOffsetX = 0.0f;
    [SerializeField] float spacing = 0.0f;
    [SerializeField] float yPivot = 0.0f;
    [SerializeField] Transform onlyOneBuildingPosition;

    [Header("NPC")]
    [SerializeField] NPCManager npcManager;

    [Header("Background")]
    [SerializeField] List<Sprite> backgroundImage = null;
    [SerializeField] List<Image> backgrounds = null;
    [SerializeField] List<Image> frontgrounds = null;

    [Header("Title")]
    [SerializeField] List<Sprite> tileImage = null;
    [SerializeField] List<Image> backTile = null;
    [SerializeField] List<Image> frontTile = null;


    [SerializeField] float turnDelay;   // 배경 전환 시간
    [SerializeField] float duration;    // 각 배경 유지시간
    [SerializeField, ReadOnly] float currentTime = 0.0f;
    [SerializeField, ReadOnly] int animationIndex = 0;

    [Header("ScrollView")]
    [SerializeField] RectTransform content = null;
    [SerializeField] ScrollRect scrollRect = null;

   

    [SerializeField, ReadOnly] public List<Building> buildings;
    [SerializeField, ReadOnly] float contentMaxSize = 0.0f;
    [SerializeField, ReadOnly] float contentCurrentSize = 0.0f;


    #region Property List
    public float ContentW { get => content.rect.width; }
    public RectTransform ContentTrans { get => content; }
    public float Spacting { get => spacing; }
    public float EndOffsetX { get => endOffsetX; }
    public NPCManager NpcManager { get => npcManager; }
    #endregion


    private void Update()
    {
        #region Background Change
        currentTime += Time.deltaTime;
        if (duration <= currentTime)
        {
            // 남은시간 정리
            currentTime -= duration;

            animationIndex++;
            animationIndex %= backgroundImage.Count;

            //= background
            for (int i = 0; i < backgrounds.Count; i++)
            {
                frontgrounds[i].sprite = backgrounds[i].sprite;
                backgrounds[i].sprite = backgroundImage[animationIndex];

                frontgrounds[i].color = Color.white;
                //backgrounds[i].color = Common.ColorAlpha /* alpha */;
                //backgrounds[i].DOColor(Color.white,turnDelay);
                frontgrounds[i].DOColor(Common.ColorAlpha, turnDelay);
            }

            for (int i = 0; i < backTile.Count; i++)
            {
                frontTile[i].sprite = backTile[i].sprite;
                backTile[i].sprite = tileImage[animationIndex];

                frontTile[i].color = Color.white;
                //backTile[i].color = Common.ColorAlpha /* alpha */;
                //backTile[i].DOColor(Color.white, turnDelay);
                frontTile[i].DOColor(Common.ColorAlpha, turnDelay);
            }
        }
        #endregion
    }

    public void Init()
    {
        for (int i = 0; i < backgrounds.Count; i++)
        {
            contentMaxSize += backgrounds[i].rectTransform.rect.width;
        }

        // Content position base set
        content.localPosition = new Vector3(0.0f, content.localPosition.y, content.localPosition.z);

        Refresh();
    }
    void Set(int lastIndex,bool isLastBuilding /* 친구 */)
    {
        // Unaffected by "is Last Building" flag
        npcManager.Set(lastIndex);

        // Show only one building
        if (isLastBuilding) InitContentSize(1);
        else                InitContentSize(lastIndex);
        if (isLastBuilding) SetOnlyOneBuildingPoistion(lastIndex);
        else                SetBuildingPosition(lastIndex);
    }
    void InitContentSize(int lastIndex)
    {
        float size = endOffsetX + lastIndex * spacing;

        contentCurrentSize = (size < Screen.width) ? Screen.width : size;
        contentCurrentSize = (contentMaxSize < contentCurrentSize) ? contentMaxSize : contentCurrentSize;

        content.sizeDelta = new Vector2(contentCurrentSize, content.sizeDelta.y);
        buildingParent.sizeDelta = content.sizeDelta;

        Debug.Log("<color=cyan>" + size + " " + Screen.width + " " + contentMaxSize + " " + contentCurrentSize + "</color>");
        Debug.Log("<color=cyan>" + content.sizeDelta + "</color>");
    }
    void SetBuildingPosition(int lastIndex)
    {
        for (int i = 0; i < lastIndex; i++)
        {
            buildings[i].gameObject.SetActive(true);
            buildings[i].transform.localPosition = new Vector3(startOffsetX + (i * spacing), yPivot, 0);
            buildings[i].Refresh();
        }

        for(int i = lastIndex; i < buildings.Count; i++)
        {
            buildings[i].gameObject.SetActive(false);
            buildings[i].transform.localPosition = new Vector3(startOffsetX + (i * spacing), yPivot, 0);
        }
    }
    void SetOnlyOneBuildingPoistion(int lastIndex)
    {
        for (int i = 0; i < buildings.Count; i++)
        {
            buildings[i].gameObject.SetActive(false);
        }

        // last index is "last building index + 1"
        buildings[lastIndex - 1].gameObject.SetActive(true);
        //buildings[lastIndex - 1].transform.localPosition = new Vector3(startOffsetX, yPivot, 0);

        buildings[lastIndex - 1].transform.position = new Vector3(onlyOneBuildingPosition.position.x, 0, 0);
        buildings[lastIndex - 1].transform.localPosition =
            new Vector3(buildings[lastIndex - 1].transform.localPosition.x,
            yPivot,
            0 /* ui z vector is zero*/);

        buildings[lastIndex - 1].FriendGraphicSet();
    }

    
    void ParsingClientData(Action callBack)
    {
        string id = "";
        string level = "";

        for(int i = 0; i < buildings.Count; i++)
        {
            //= id : 1|2|3|4|5
            id += buildings[i].ID;

            //= level : 1,1,1|1,2,4|4,1,3.....|4,1,1
            for(int kind = 0; kind < (int)Building.Kind.MaxSize; kind++)
            {
                level += buildings[i].GetLevel(kind).ToString();
                
                if(kind != (int)Building.Kind.MaxSize - 1)
                {
                    level += Common.subSplitWord;
                }
            }

            if(i != buildings.Count-1)
            {
                id += "|";
                level +=  "|";
            }
        }
        NetworkManager.instance.UpdateBuildingData(id, level, UserManager.instance.GoldOffEvent, null, callBack);
    }


    #region External function
    public void Refresh(bool isLastBuilding = false)
    {
        Clear();
        Set(BuildingManager.instance.OpenCount, isLastBuilding);
    }
    public void Clear()
    {
        if (buildings == null) return;

        for (int i = 0; i < buildings.Count; i++)
        {
            buildings[i]?.gameObject.SetActive(false);
        }
      
    }
    public void AppendBuilding(Building building)
    {
        if (buildings == null) buildings = new List<Building>();
        buildings.Add(building);
        building.transform.SetParent(buildingParent);
        building.transform.localScale = Vector3.one;
        building.clientPush = ParsingClientData;
    }
    public void UnActiveNPC()
    {
        npcManager.Disable();
    }
    public void ReActiveNPC()
    {
        npcManager.Enable();
    }
    public void LockScrollRect()
    {
        scrollRect.horizontal = false;
    }
    public void UnLockScrollRect()
    {
        scrollRect.horizontal = true;
    }
    public Transform GetBuildingTrans(int id)
    {
        Building find = buildings.Find((v) => v.ID == id);

        if (find != null)
        {
            return find.transform;
        }
        else
        {
#if USE_DEBUG
            Debug.Log("Not find building");
#endif
            return null;
        }
    }
    public Transform GetBuildingParent()
    {
        return buildingParent;
    }


    public void MoveContentForBuilding(int id)
    {
        Building find = buildings.Find((v) => v.ID == id);

        if (find != null)
        {
            RectTransform parent = (find.transform.parent.transform as RectTransform);

            float pax = parent.localPosition.x;
            float bux = find.transform.localPosition.x;

            float pos = pax + bux;
            float halfW = 360.0f;

            content.localPosition = new Vector3(-pos + halfW,content.localPosition.y, content.localPosition.z);
        }
    }
    public void LockContent()
    {
        scrollRect.horizontal = false;

#if USE_DEBUG
        Debug.Log("스크롤 잠김");
#endif
    }
    public void UnLockContent()
    {
        scrollRect.horizontal = true;

#if USE_DEBUG
        Debug.Log("스크롤 잠금 해제");
#endif
    }
    #endregion

}
