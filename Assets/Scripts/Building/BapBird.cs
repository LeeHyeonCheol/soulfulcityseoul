﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BapBird : MonoBehaviour
{

    [Header("Animation data")]
    [SerializeField] Image image;
    [SerializeField] List<Sprite> sprites;
    [SerializeField] float animationDelay;
    [SerializeField, ReadOnly] float animationTime;
    [SerializeField] int animationIndex;

    [Header("InGame data")]
    [SerializeField] float speed;
    [SerializeField] float respawnDelay;
    [SerializeField, ReadOnly] float respawnTime;

    [Header("Event action")]
    [SerializeField] Button button;

    #region Position Data
    [Header("Move Data")]
    [SerializeField, ReadOnly] Vector2 minPos;
    [SerializeField, ReadOnly] Vector2 maxPos;
    [SerializeField, ReadOnly] Transform parent;
    [SerializeField, ReadOnly] Vector2 turnPos;
    [SerializeField,ReadOnly] Vector2 direction;
    #endregion

    [SerializeField, ReadOnly] bool isRunning;

    #region External function
    public void Init(Vector2 minPos, Vector2 maxPos,Transform parent)
    {
        #region Set param to member
        this.minPos = minPos;
        this.maxPos = maxPos;
        this.parent = parent;
        isRunning = true;
        #endregion

        Vector2 startPos = SetMoveData();
        SetTransformData(startPos);

        //= speed rend set
        speed = GetRandSpeed();

        //= Animation Set
        animationIndex = 0;
        image.sprite = sprites[animationIndex];
    }
    public void OnClickMouseDown()
    {
        Die();

        BuildingManager.instance.FriendGameUpCatchesCount();
    }
    #endregion

    #region Move and transform set
    Vector2 SetMoveData()
    {
        Vector2 startPos = CreatePositionData();
        turnPos = CreateTurnPos(startPos.y);
        direction = OperationDirection(startPos, turnPos);
        
        return startPos;
    }
    void SetTransformData(Vector2 startPos)
    {
        transform.SetParent(this.parent);
        transform.localScale = Vector3.one;
        transform.localPosition = startPos;
        SetImageDirection(direction);
    }
    void SetImageDirection(Vector2 direction)
    {
        //= 기존 이미지가 정 반대방향을 가지고있기 때문에 direction과 반대되는 방향으로 이미지 변경
        image.transform.localScale = new Vector3(-direction.x, 1, 1);
    }
    #endregion
    #region Respawn and die
    void Die()
    {
        RespawnDie(false);
    }
    void Respawn()
    {
        RespawnDie(true);

        Vector2 startPos = SetMoveData();
        SetTransformData(startPos);
    }
    void RespawnDie(bool running)
    {
        isRunning = running;

        //= 버튼 클릭 설정
        button.interactable = running;

        //= 이미지 설정
        image.DOColor((running) ? Color.white : Common.ColorAlpha, .25f);

        //= 애니메이션 설정
        animationIndex = 0;
        image.sprite = sprites[animationIndex];
    }
    #endregion
    #region Create and operation
    Vector2 CreatePositionData()
    {
        float xPos = Random.Range(minPos.x, maxPos.x);
        float yPos = Random.Range(minPos.y, maxPos.y);

        return new Vector2(xPos, yPos);
    }
    Vector2 CreateTurnPos(float yPos)
    {
        float xPos = Random.Range(minPos.x, maxPos.x);
        return new Vector2(xPos,yPos);
    }
    Vector2 OperationDirection(Vector2 start,Vector2 turn)
    {
        Vector2 directionCheck = (turn - start).normalized;

        return (directionCheck.x < 0) ? Vector2.left : Vector2.right;
    }
    bool CheckTurnPosOver(Vector2 pos,Vector2 turnPos,Vector2 direction)
    {
        if(direction.x <= Vector2.left.x)
        {
            //= Move direction is left [ <<<<< ]
            //===[ pos.x ]====[ turnPos.x ]=====
            if (pos.x <= turnPos.x) return true;
            else return false;
        }
        else if(direction.x >= Vector2.right.x)
        {
            //= Move direction is right [ >>>>> ]
            //===[ turnPos.x ]====[ pos.x ]=====
            if (turnPos.x <= pos.x) return true;
            else return false;
        }
        else
        {
            return true;
        }
    }
    float GetRandSpeed()
    {
        return Random.Range(150.0f, 600.0f);
    }
    #endregion

    private void FixedUpdate()
    {
        if(isRunning)
        {
            //= 인게임
            transform.localPosition += (Vector3)direction * Time.deltaTime * speed;

            if(CheckTurnPosOver(transform.localPosition, turnPos,direction))
            {
                turnPos = CreateTurnPos(transform.localPosition.y);
                direction = OperationDirection(transform.localPosition, turnPos);
                SetImageDirection(direction);
                speed = GetRandSpeed();
            }
            respawnTime = respawnDelay;

            //= 애니메이션
            animationTime += Time.deltaTime;
            if(animationDelay <= animationTime)
            {
                animationTime = 0;
                animationIndex = (animationIndex + 1) % sprites.Count;
            }
        }
        else
        {
            respawnTime -= Time.deltaTime;
            if(respawnTime <= 0)
            {
                isRunning = true;
                Respawn();
            }
        }
    }

}
