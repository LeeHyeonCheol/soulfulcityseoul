﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kind = Building.Kind;

public class BuildingOnlyShow : MonoBehaviour
{
    #region Inner class
    [System.Serializable]
    public class ImagesData
    {
        public List<Image> mainBuilding;
        public List<Image> mainSculpture;
        public List<Image> subSculpture;
       

        public ImagesData()
        {
            mainBuilding = new List<Image>();
            mainSculpture = new List<Image>();
            subSculpture = new List<Image>();
        }

        public List<Image> this[int index]
        {
            get
            {
                if (index == 0) return mainBuilding;
                else if (index == 1) return mainSculpture;
                else if (index == 2) return subSculpture;
                else
                {
#if USE_DEBUG
                    Debug.Log("Error Index : " + index + " in Object Data");
#endif
                    return null;
                }

            }
        }
        public bool IsEmpty
        {
            get
            {
                if (mainBuilding.Count == 0 && mainSculpture.Count == 0 && subSculpture.Count == 0) return true;
                else return false;
            }
        }
    }
    [System.Serializable]
    public class ExceptionImage
    {
        public Image image;
        public Color color;
        public Kind kind;
    }
    #endregion

    [Header("UI")]
    [SerializeField] List<Canvas> canvas;

    [Header("Info")]
    [SerializeField] int id;
    [SerializeField] Building.ObjectData objects;
    [SerializeField] List<ImagesData> imagesData;
    [SerializeField] List<Image> alwaysActive;
    [SerializeField] Kind alwaysKind;
    [SerializeField] List<ExceptionImage> exceptionImages;
    [SerializeField] Color changeColor = new Color(0, 0, 0, .26f);
    [SerializeField] float viewScale;
    [Header("Flag")]
    [SerializeField] bool onColorChange = true;

    [SerializeField] GameObject lockImage;


    #region Property function
    public int ID { get => id; set => id = value; }
    public bool OnColorChange { get => onColorChange; set => onColorChange = value; }
    #endregion

    /// <summary>
    /// Set building data 
    /// </summary>
    /// <param name="MBLevel"> Main building level </param>
    /// <param name="MSLevel"> Main Sculpture level </param>
    /// <param name="SSLevel"> Sub Sculpture level </param>
    public void Set(Kind kind,int MBLevel,int MSLevel,int SSLevel,int order = 1000)
    {
        int sumLevel = MBLevel + MSLevel + SSLevel;

        if (Common.BUDMaxLevel < MBLevel) MBLevel = Common.BUDMaxLevel;
        if (Common.BUDMaxLevel < MSLevel) MSLevel = Common.BUDMaxLevel;
        if (Common.BUDMaxLevel < SSLevel) SSLevel = Common.BUDMaxLevel;

        //= 오더순서 변
        for (int i = 0; i < canvas.Count; i++)
        {
            canvas[i].sortingOrder += order;
        }

        //= 기존 오브젝트 비 활성화 작업
        for (int i = 0; i < (int)Kind.MaxSize; i++)
        {
            for (int j = 0; j < Common.BUDMaxLevel; j++)
            {
                objects[i][j].SetActive(false);
            }
        }

        //= 애니메이션 사용안됨.
        if (sumLevel == 0)
        {
            lockImage.gameObject.SetActive(true);
            if (objects.alwaysActive != null)
            {
                objects.alwaysActive.SetActive(false);
            }
        }
        else
        {
            lockImage.gameObject.SetActive(false);

            objects[(int)Kind.MainBuilding ][MBLevel - 1].SetActive(true);
            objects[(int)Kind.MainSculpture][MSLevel - 1].SetActive(true);
            objects[(int)Kind.SubSculpture ][SSLevel - 1].SetActive(true);

            //= 색상 변경 [ 현재 활성화 중인 이미지만 : 최적화를 위하여 ]
            if(onColorChange)
            {
                int[] indexArray = new int[(int)Kind.MaxSize];
                indexArray[(int)Kind.MainBuilding] = MBLevel - 1;
                indexArray[(int)Kind.MainSculpture] = MSLevel - 1;
                indexArray[(int)Kind.SubSculpture] = SSLevel - 1;

                //= 기본 이미지 색상 변경
                for (int kindIndex = 0; kindIndex < (int)Kind.MaxSize; kindIndex++)
                {
                    if (kind != (Kind)kindIndex)
                    {
                        for (int i = 0; i < imagesData[indexArray[kindIndex]][kindIndex].Count; i++)
                        {
                            imagesData[indexArray[kindIndex]][kindIndex][i].color = changeColor;
                        }
                    }
                }

                //= 상시 활성화 이미지 색상 변경
                if (alwaysKind != kind && alwaysActive != null && alwaysActive.Count != 0)
                {
                    for (int i = 0; i < alwaysActive.Count; i++)
                    {
                        alwaysActive[i].color = changeColor;
                    }
                }

                //= 예외 색상 변경
                if (exceptionImages != null && exceptionImages.Count != 0)
                {
                    for (int i = 0; i < exceptionImages.Count; i++)
                    {
                        if (exceptionImages[i].kind != kind)
                        {
                            exceptionImages[i].image.color = exceptionImages[i].color;
                        }
                    }
                }

            }
            if (objects.alwaysActive != null)
            {
                objects.alwaysActive.SetActive(true);
            }
        }
    }
}
