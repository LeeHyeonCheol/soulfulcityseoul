﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using HoneyPlug.Audio;

public class Cannon : MonoBehaviour
{
    [SerializeField] Transform startPos;
    [SerializeField] float speed = 2.5f;

    [Header("Bullet")]
    [SerializeField] Image bullet;
    [SerializeField, ReadOnly] Transform bulletTrans;
    [SerializeField] Sprite[] bulletSps;

    [Header("Finish effect")]
    [SerializeField,ReadOnly] Image effect;
    [SerializeField] Sprite[] effects;
    [SerializeField] float effectDuration = 0.8f;

    [Header("Umbrella")]
    [SerializeField] Image umbrella;

    [Header("Shoot animation")]
    [SerializeField] Image cannon;
    [SerializeField] float duration;

    [Header("Target Info")]
    [SerializeField, ReadOnly] string playerName;
    [SerializeField, ReadOnly] Building target;
    [SerializeField, ReadOnly] int buildingID;

    [Header("Event")]
    [SerializeField] RectTransform eventParent;
    [SerializeField] AnimationEvent hit;
    [SerializeField] AnimationEvent hitGold;
    [SerializeField] AnimationEvent shield;

    Coroutine coroutine = null;

    //= Tutorial
    Action tutorialCallback;

    private void Start()
    {
        Init();
    }

    private void OnEnable()
    {
        if(effect != null)
        {
            effect.gameObject.SetActive(false);
        }
    }

    private void Init()
    {
        if (effect == null)
        {
            effect = Instantiate(EffectManager.instance.GetEffect("Paint").effect).GetComponent<Image>();
            effect.transform.SetParent(this.transform);
        }
        effect.gameObject.SetActive(false);
        umbrella.gameObject.SetActive(false);
    }

    void GraphicSet()
    {
        int index = UnityEngine.Random.Range(0, bulletSps.Length);

        bullet.gameObject.SetActive(true);
        bullet.sprite = bulletSps[index];
        bulletTrans = bullet.transform;
        bulletTrans.position = startPos.position;
        bulletTrans.localScale = Vector3.one;

        effect.sprite = effects[index];
        effect.gameObject.SetActive(false);
        umbrella.gameObject.SetActive(false);
    }

    #region External function

    bool enableShield;
    Vector2 targetOffset;
    public void Shooting(string playerName,int shieldCount ,Building target, Vector2 offset, string anotherPlayFabId,Action callback)
    {
        this.playerName = playerName;
        this.target     = target;
        buildingID      = this.target.ID;
        targetOffset    = offset;

        if(shieldCount > 0) enableShield = true;
        else enableShield = false;

        tutorialCallback = callback;

        NetworkManager.instance.GetRewardAtkOtherPlayer(SuccessEndAttack, anotherPlayFabId, buildingID, target.TotalLevel(), enableShield);
    }

    ulong getGold;
    void SuccessEndAttack(string currGold, string plusGold)
    {
        getGold = Convert.ToUInt64(plusGold);

        GraphicSet();

        if(enableShield) Shooting(targetOffset, Shield);
        else Shooting(targetOffset, Hit);
    }
    #endregion

    void Shooting(Vector2 offset,Action onComplete)
    {
        if (coroutine == null)
        {
            coroutine = StartCoroutine(Parabola(offset, onComplete));
            SoundManager.instance.PlayOneSound(SoundManager.instance.cannonShot);
        }
    }

    IEnumerator Parabola(Vector2 targetOffset,Action onComplete)
    {
        #region 발사 연출
        float cannonPosY = cannon.transform.localPosition.y;
        cannon.transform.DOLocalMoveY(cannonPosY - 40, duration * 0.5f).
            OnComplete(delegate { cannon.transform.DOLocalMoveY(cannonPosY, duration * 0.5f); });
        #endregion

        Vector2 pos = startPos.position;
        Vector2 finish = (Vector2)target.transform.position + targetOffset;

        float distance = Vector2.Distance(pos, finish);
        float gravite = 9.8f;
        float maxHeight = finish.y + targetOffset.y + distance / 5.0f;
        float distanceHeight = finish.y - pos.y;
        float height = maxHeight - pos.y;
        float ty = Mathf.Sqrt(2 * gravite * height);
        float zy = -2 * ty;
        float dat = (-zy + Mathf.Sqrt(zy * zy - 4 * gravite * 2 * distanceHeight)) / (2 * gravite);
        float tx = -(pos.x - finish.x) / dat;
        float elapsed = 0.0f;

        int direction = (UnityEngine.Random.Range(0, 2) == 0 ? 1 : -1);

        while (elapsed < dat)
        {
            elapsed += Time.deltaTime * speed;

            float percent = elapsed / dat;
            float y = pos.y + ty * elapsed - 0.5f * gravite * elapsed * elapsed;
            float x = pos.x + tx * elapsed + (SlerpY(180,200, percent) * direction);

            // 좌표 설정
            Vector2 basePos = new Vector2(x, y);
            bulletTrans.position = basePos;

            // z position 삭제
            Vector3 local = bulletTrans.localPosition;
            local.z = 0.0f;
            bulletTrans.localPosition = local;

            // 스케일 조정
            Vector3 scale = Vector3.one - (0.5f /* finish scale */ * Vector3.one * percent);
            bulletTrans.localScale = scale;

            yield return null;
        }

        coroutine = null;
        onComplete.Invoke();
        
    }

    float SlerpY(float w,float h, float percent /* 0 ~ 1 */)
    {
        Vector3 center = new Vector3(w * 0.5f, h, 0);
        Vector3 riseRel = Vector3.zero -center;
        Vector3 setRel = new Vector3(w, 0, 0) - center;

        Vector3 slerp = Vector3.Slerp(riseRel, setRel, percent);
        slerp += center;
        return slerp.y;
    }


    void Hit()
    {
        #region 활성화 비활성화 관련
        bullet.gameObject.SetActive(false);
        effect.gameObject.SetActive(true);
        #endregion

        #region 연출 관련
        target.transform.DOShakePosition(1, 10);
        SoundManager.instance.PlayOneSound(SoundManager.instance.bulletHit);
        effect.transform.position = bullet.transform.position;
        effect.transform.localScale = Vector3.one * .5f;
        effect.transform.DOScale(Vector3.one * 2, effectDuration);
        #endregion

        //= popup으로 띄울 골드 : getGold
        AnimationEvent _hit = Instantiate(hit);
        _hit.transform.SetParent(eventParent);
        _hit.transform.localScale = Vector3.one;
        _hit.transform.localPosition = Vector3.zero;
        _hit.SetCallBack(SuccessFinish);

        //= TODO : 연출 수정필요
        void SuccessFinish()
        {
            AnimationEvent _hitgold = Instantiate(hitGold);
            _hitgold.transform.localScale = Vector3.one;
            _hitgold.transform.localPosition = new Vector3(0, 1.5f, 0);

            _hitgold.SetCallBack(HitGoldFinish);
        }

        void HitGoldFinish()
        {
            string pack = LanguageManager.instance.GetString("Cannon_Attak_Hit");
            pack = string.Format(pack, getGold);

            PopupManager.instance.Open(1209878, pack, PopupManager.ButtonStyle.Ok, PopupManager.MoveType.CenterPumping,
            delegate
            {
                PopupManager.instance.Open(998726, "", PopupManager.ButtonStyle.AD, PopupManager.MoveType.CenterPumping,
                //= 광고 시청 완료
                delegate
                {
                    //= 2020 01 19 어택시 골드 획득연출 및 실시간 반영 추가. ( 광고 2배 )
                    UserManager.instance.GoldOnEvent += (getGold * 2);

                    //= 2020 01 21 튜토리얼 콜백 추가
                    tutorialCallback?.Invoke();
                    tutorialCallback = null;

                    //= TODO : 서버에 광고 봤다고 알려주기.
                    BuildingManager.instance.PrevScene();
                },
                //= 광고 실행 후 취소
                delegate
                {
                    //= 2020 01 19 어택시 골드 획득연출 및 실시간 반영 추가. ( 기본값 )
                    UserManager.instance.GoldOnEvent += getGold;

                    //= 2020 01 21 튜토리얼 콜백 추가
                    tutorialCallback?.Invoke();
                    tutorialCallback = null;

                    BuildingManager.instance.PrevScene();
                }, true);
            },
            //= 광고 자체를 보지않음.
            delegate
            {
                //= 2020 01 19 어택시 골드 획득연출 및 실시간 반영 추가. ( 기본값 )
                UserManager.instance.GoldOnEvent += getGold;

                //= 2020 01 21 튜토리얼 콜백 추가
                tutorialCallback?.Invoke();
                tutorialCallback = null;

                BuildingManager.instance.PrevScene();
            }, true);

           
        }
    }

    void Shield()
    {
        #region 활성화 비활성화 관련
        bullet.gameObject.SetActive(false);
        umbrella.gameObject.SetActive(true);
        #endregion

        #region 연출 관련
        // NOTE : 애니메이션?
        umbrella.transform.position = target.transform.position;
        umbrella.transform.localScale = Vector3.one;
        Vector3 umbrellaPos = umbrella.transform.localPosition;
        umbrella.color = Common.ColorHalfAlpha;
        umbrella.DOColor(Color.white, effectDuration).
        OnStart(
        () =>
        {
            umbrella.transform.DOScale(Vector3.one * .8f, effectDuration / 2)
            .SetEase(Ease.OutCubic)
            .OnComplete(
            () =>
            {
                umbrella.transform.DOScale(Vector3.one, effectDuration / 2)
                .SetEase(Ease.InCubic)
                .OnComplete(
                () =>
                {
                    umbrella.DOColor(Common.ColorAlpha, effectDuration).
                    OnComplete(
                    () =>
                    {
                        AnimationEvent fail = Instantiate(shield);
                        fail.transform.SetParent(eventParent);
                        fail.transform.localPosition = Vector3.zero;
                        fail.transform.localScale = Vector3.one;
                        fail.SetCallBack(FailFinish);
                    });
                });
            });
        });

        #endregion
        void FailFinish()
        {
            // => 작업 내용 작성 : 노트 확인
            BuildingManager.instance.PrevScene();
        }
    }

}
