﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuildingData
{
    public int id;
    public int level;           // level : 0 은 잠금 / 1 ~ 4까지 기본 / 5 ~ 12 까지 배치물
    public string name;
    public int requireGold;
    public string iconName;
    public string kind;

    [System.Serializable]
    public class Friend
    {
        public int id;
        public int[] level;

        public Friend(int id = 1,int[] level = null)
        {
            this.id = id;

            if(level == null)
            {
                this.level = new int[(int)Building.Kind.MaxSize];
                for(int i = 0; i < (int)Building.Kind.MaxSize; i++)
                {
                    //= 기본값 삽입
                    this.level[i] = 1;
                }
            }
            else
            {
                this.level = level;
            }
            
        }
    }
}

