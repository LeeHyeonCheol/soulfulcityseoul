﻿using System;
using System.Collections;
using System.Collections.Generic;
using HoneyPlug.Audio;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

using Kind = Building.Kind;
using Random = UnityEngine.Random;

using DG.Tweening;

public class BuildingManager : MonoBehaviour
{
    public static BuildingManager instance;

    // 필요한 정보들을 삽입할 위치
    [SerializeField, ReadOnly] List<int> idDB;
    [SerializeField] GameObject buildingParent;

    [Header("Scene Loader")]
    [SerializeField] Button nextButton = null;
    [SerializeField] Button prevButton = null;

    [Header("Sound Reference")]
    [SerializeField] SoundManager soundManager;

    [Header("Workman")]
    [SerializeField] WorkmanManager workmanManager;

    [Header("Friend relevance")]
    [SerializeField] GameObject friendUIGroup;
    [SerializeField] GameObject userUIGroup;
    [SerializeField] string nickname;
    [SerializeField] Text nickText;

    //= 친구방문 게임 관련
    [SerializeField] BapBird bapBird;
    [SerializeField] Transform catchesParent;
    [SerializeField] Text catchesText;
    [SerializeField] int catchesCount;

    //= 친구방문 제한 시간 관련
    [SerializeField] Text friendTimerText;
    [SerializeField] Slider friendTimerSlider;
    [SerializeField] float friendLimitTime;
    [SerializeField, ReadOnly] float friendCurrentTime;

    //= 매니저 내부 플러그 관련
    [SerializeField, ReadOnly] bool isFriendGamePlay;


    [Header("Scripts")]
    [SerializeField] BuildingUISet uiSet;
    [SerializeField] LevelUI levelUI;

    Dictionary<int/* id */, Building/* prefabs */> prefabs = null;
    Dictionary<int/* id */, int[]> levelDB;
    Dictionary<int/* id */, bool    /* is Neutralization*/> neutralizationDB = null;
    Dictionary<int/* id */, long    /* start time*/> constructionDB = null;
    Dictionary<int/* id */, BuildingData.Friend> friendData = null;

    [Header("Battle")]
    [SerializeField, ReadOnly] bool isUser = true;

#if USE_DEBUG
    [Header("Only Debug use / show editor")]
    [SerializeField] List<Building> onlyShowBuildingPrefabs;
#endif
    #region Property List
    public int OpenCount
    {
        get
        {
            if (isUser)
            {
                if (levelDB != null)
                {
                    int count = 0;
                    foreach (int id in levelDB.Keys)
                    {
                        bool completion = true;
                        for(int index = 0; index < levelDB[id].Length;index++)
                        {
                            if(levelDB[id][index] != Common.BUDMaxLevel)
                            {
                                completion = false;
                                break;
                            }
                        }

                        if(completion){ count++; }
                    }
                    if (count == 0) return 1;   /* level zero => basic build open */
                    if (levelDB.Count <= count) count = levelDB.Count /* level max */;
                    else count++; /* next level open */

                    return count;
                }
                return 0;
            }
            else
            {
                if (friendData != null)
                {
                    int count = 0;
                    foreach (int key in friendData.Keys)
                    {
                        bool completion = true;
                        for (int index = 0; index < friendData[key].level.Length; index++)
                        {
                            if (friendData[key].level[index] != Common.BUDMaxLevel)
                            {
                                completion = false;
                                break;
                            }
                        }

                        if (completion) { count++; }
                    }
                    if (count == 0) return 1;   /* level zero => basic build open */
                    if (levelDB.Count < count) count = levelDB.Count /* level max */;

                    return count;
                }
                else
                {
                    return 0;
                }
            }
        }
        set
        {
            if (isUser)
            {
                uiSet.Refresh();
            }
            else
            {
#if USE_DEBUG
                Debug.Log("Friend Data는 수정할 수 없습니다.");
#endif
            }
        }
    }
    public BuildingUISet UiSet { get => uiSet; }
    public bool IsUser { get => isUser; }
    public bool ParentActiveSelf { get => buildingParent.activeSelf; }
    public SoundManager SoundManager { get => soundManager; set => soundManager = value; }
    #endregion

    void Start()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

        #region Set Event
        prevButton.onClick.AddListener(PrevScene);
        nextButton.onClick.AddListener(NextScene);
        #endregion
    }
    public void Init(string id, string level, string neutralization,string construction)
    {
        Clear();
        LoadingPrefabs();
        #region Set id
        Dictionary<int, Building>.KeyCollection ids = prefabs.Keys;
        idDB = new List<int>();
        foreach (int idvalue in ids) { idDB.Add(idvalue); }
        #endregion
        #region Set Data
        if (CheckServerPacket(id, InitialFirstData_ID()) == false) id = InitialFirstData_ID();
        if (CheckServerPacket(level, InitialFirstData_Level()) == false) level = InitialFirstData_Level();
        if (CheckServerPacket(neutralization, InitialFirstData_Neutralization()) == false) neutralization = InitialFirstData_Neutralization();
        SetData(id, level, neutralization, construction);
        friendData = new Dictionary<int, BuildingData.Friend>();
        Set();
        #endregion
        #region Set UI
        friendUIGroup.SetActive(false);
        uiSet.Refresh(); // clear init
        uiSet.Init();
        #endregion

        #region Inner Function
        bool CheckServerPacket(string packet,string compare)
        {
            if (packet == null || packet == string.Empty) return false;
            return (packet.Length == compare.Length);
        }

        string InitialFirstData_ID()
        {
            return Common.InitSetBuildingId;
        }
        string InitialFirstData_Level()
        {
            return Common.InitSetBuildingLevel;
        }
        string InitialFirstData_Neutralization()
        {
            return Common.InitSetBuildingBombing;
        }
        #endregion
    }
    void Set()
    {
        Clear();
        for (int i = 0; i < idDB.Count; i++)
        {
            uiSet.AppendBuilding(CreateBuilding(idDB[i]));
        }
    }
    void SetData(string id, string level, string neutralization,string construction)
    {
        levelDB = new Dictionary<int, int[]>();
        neutralizationDB = new Dictionary<int, bool>();
        constructionDB = new Dictionary<int, long>();

        string[] ids = id.Split(Common.splitWord);
        string[] levels = level.Split(Common.splitWord);
        string[] neutralizations = neutralization.Split(Common.splitWord);
        
        //= level db 초기화.
        for (int i = 0; i < ids.Length && i < levels.Length; i++)
        {
            //= level packet => 1,2,3|3,2,1|4,1,1|0,0,0|........|0,0,0
            string[] detailsLevels = levels[i].Split(Common.subSplitWord);
            int[] intLevels = new int[(int)Kind.MaxSize];
            for(int j = 0; j < (int)Kind.MaxSize; j++)
            {
                intLevels[j] = int.Parse(detailsLevels[j]);
            }
            if (levelDB.ContainsKey(int.Parse(ids[i])) == false) levelDB.Add(int.Parse(ids[i]), intLevels);
            else
            {
#if USE_DEBUG
                Debug.LogError("Error : Contains key id [ " + int.Parse(ids[i]) + "]");
#endif
            }
        }
        for (int i = 0; i < ids.Length && i < neutralizations.Length; i++)
        {
            neutralizationDB.Add(int.Parse(ids[i]), (int.Parse(neutralizations[i]) == 0) ? false : true);
        }

        #region Default set id
        for (int i = 1; i <= Common.BUDMaxCount; i++)
        {
            int[] intLevels = new int[(int)Kind.MaxSize];
            if (levelDB.ContainsKey(i) == false) levelDB.Add(i, intLevels/* base level : All zero*/);
            if (neutralizationDB.ContainsKey(i) == false) neutralizationDB.Add(i, false);
        }
        #endregion

        //= Set Construction
        if(construction != "")
        {
            string[] constructions = construction.Split(Common.splitWord);

            for(int i = 0; i < constructions.Length; i++)
            {
                //= part [ 0 = id ][ 1 = start time ]
                string[] part = constructions[i].Split(Common.subSplitWord);
                if(constructionDB.ContainsKey(int.Parse(part[0])) == false)
                {
                    constructionDB.Add(int.Parse(part[0]), long.Parse(part[1]));
                }
            }
        }
       
    }
    void Clear()
    {
        if(uiSet.buildings != null && uiSet.buildings.Count != 0)
        {
            for(int i = 0; i < uiSet.buildings.Count; i++)
            {
                Destroy(uiSet.buildings[i].gameObject);
            }
            uiSet.buildings = null;
        }
    }
    void LoadingPrefabs()
    {
        prefabs = new Dictionary<int, Building>();
#if USE_DEBUG
        onlyShowBuildingPrefabs = new List<Building>();
#endif

        Building[] buildings = Resources.LoadAll<Building>(Common.BUDPrefabPath);

        for (int i = 0; i < buildings.Length; i++)
        {
            Building building = buildings[i];

            if (prefabs.ContainsKey(building.ID) == false)
            {
                prefabs.Add(building.ID, building);
#if USE_DEBUG
                onlyShowBuildingPrefabs.Add(building);
#endif
            }
#if USE_DEBUG
            else
            {
                Debug.LogError("Building DB에 동일한 키값을 가지고있는 객체가있습니다." + building.ID);
            }
#endif
        }

    }

    #region Scene Function
    public void PrevScene()
    {
        if (SceneLoader.instance != null)
        {
            SceneLoader.instance.LoadScene(Common.RULSceneName, ChangePlayerTure);
            soundManager.PlayMusic(soundManager.background_home, .5f);
            uiSet.UnLockScrollRect();

            Invoke("ChangePlayerTure", .6f);
            
            isBuildingMap = false;
        }
    }
    public void NextScene()
    {
        nextButton.interactable = false;
        NetworkManager.instance.GetUserBuildingData(PlayFabController.instance.myPlayFabID, SuccessGetBuildingAllData);
        UIManager.instance.OpenUI_Button(true);
        uiSet.UnLockScrollRect();
        prevButton.gameObject.SetActive(true);

        SculptureManger.Instance.OnAllArrangement();
        uiSet.ReActiveNPC();
        FriendUISet(false);
        
        isBuildingMap = true;
        InitEvent();
    }
    void OnFriendSet()
    {
        if (SceneLoader.instance != null)
        {
            SceneLoader.instance.LoadScene(Common.BUDSceneName);
            soundManager.PlayMusic(soundManager.background_town, .5f);
            UIManager.instance.CloseUI_Button();
            //= 2021.02.18 : 이제 더이상 스크린을 잠구지 않음.
            //uiSet.LockScrollRect();
            prevButton.gameObject.SetActive(false);

            SculptureManger.Instance.OffAllArrangement();
            uiSet.UnActiveNPC();
        }
    }
    #endregion

    void SuccessGetBuildingAllData(PlayFab.ClientModels.GetUserDataResult result)
    {
        UserManager.instance.ShieldCount = NetworkManager.instance.CheckArrangeShieldCount(result);

        // string ids           = NetworkManager.instance.CheckArrangeBuildingId(result);
        // string[] buildingIds = ids.Split('|');
        // int[] nIds           = new int[buildingIds.Length];
        // for(int i = 0; i < buildingIds.Length; i++)
        // {
        //     nIds[i] = System.Convert.ToInt32(buildingIds[i]);
        // } 

        NetworkManager.instance.UpdateMyBuildingData(result);
        ProcSetWorkman(DataManager.instance.GetAllBuildingID().ToArray());

        SceneLoader.instance.LoadScene(Common.BUDSceneName);
        nextButton.interactable = true;
        soundManager.PlayMusic(soundManager.background_town, .5f);

        //= 2020 01 18 추가
        //= 기존 유저 정보로 그래픽 셋팅하는 코드를 추가.
        ChangePlayer(true);
    }
    void ChangePlayerTure()
    {
        ChangePlayer(true);
    }

    #region External function
    public Building CreateBuilding(int id)
    {
        if (prefabs == null) LoadingPrefabs();

        if (prefabs.ContainsKey(id))
        {
            Building building = Instantiate(prefabs[id]);
#if USE_DEBUG
            #region Server Data Init error
            if (levelDB == null) Debug.LogError("Level not find. No have a database");
            if (levelDB.ContainsKey(id) == false) Debug.LogError("Level not find. No have a key in database [ ID(Key) : " + id + " ]");
            #endregion
#endif
            //= Construction set
            bool construction = constructionDB.ContainsKey(id);

            #region Building DataBase Set
            List<BuildingData> mainBuilding = DataManager.instance.GetBuildingData(Kind.MainBuilding,id);
            List<BuildingData> mainSculpture = DataManager.instance.GetBuildingData(Kind.MainSculpture, id);
            List<BuildingData> subSculpture = DataManager.instance.GetBuildingData(Kind.SubSculpture, id);
            Building.BuildingDB buildingDB = new Building.BuildingDB(mainBuilding, mainSculpture, subSculpture);
            #endregion

            building.DataSet(levelDB[id], buildingDB, neutralizationDB[id]);
            building.gameObject.name = "building " + ((id < 10) ? "0" + id : id.ToString());
            return building;
        }
        else
        {
#if USE_DEBUG
            Debug.LogError("Building not find. No have a key in database [ ID(Key) : " + id + " ]");
#endif
            return null;
        }
    }

    // public void SetFriendData(BuildingData.Friend[] data,string nickname)
    // {
    //     friendData = new Dictionary<int, BuildingData.Friend>();
    //     for (int i = 0; i < data.Length; i++)
    //     {
    //         if(friendData.ContainsKey(data[i].id) == false)
    //         {
    //             friendData.Add(data[i].id, data[i]);
    //         }
    //     }
    //     this.nickText.text = nickname;
    //     this.nickname = nickname;

    //     ChangePlayer(false);
    // }

    public string PackNeutralizationDB(int unNeturalizationID, bool value)
    {
        if (neutralizationDB == null) return "";

        //= Packet : 1|0|......1

        string pack = string.Empty;
        int packvalue = (value) ? 1 : 0;
        foreach (int key in neutralizationDB.Keys)
        {
            if (key != unNeturalizationID)
            {
                pack += ((neutralizationDB[key]) ? 1 : 0).ToString();
            }
            else
            {
                pack += packvalue.ToString();
            }
            pack += Common.splitWord;
        }

        if (pack != string.Empty && pack.LastIndexOf(Common.splitWord) == pack.Length - 1)
        {
            pack = pack.Remove(pack.LastIndexOf(Common.splitWord));
        }

        Debug.Log("<color=cyan> Pack : </color>" + pack);

        return pack;
    }
    public string PackNeutralizationDB()
    {
        if (neutralizationDB == null) return "";

        string pack = "";
        foreach (int key in neutralizationDB.Keys)
        {
            pack += ((neutralizationDB[key]) ? 1 : 0).ToString();
            pack += Common.splitWord;
        }

        if (pack != "" && pack.LastIndexOf(Common.splitWord) == pack.Length - 1)
        {
            pack.Remove(pack.LastIndexOf(Common.splitWord));
        }

        return pack;
    }
    public void UpdateNeutralizationDB(int unNeturalizationID, bool value)
    {
        if (neutralizationDB != null)
        {
            if (neutralizationDB.ContainsKey(unNeturalizationID))
            {
                neutralizationDB[unNeturalizationID] = value;
            }
        }
    }

    public void OpenUI_UpgradeBuild()
    {
        UIManager.instance?.OpenUI_UpgradeBuild(uiSet.buildings);

#if USE_DEBUG
        Debug.Log(UIManager.instance);
#endif
    }
    public void ChangePlayer(bool isUser)
    {
        if (this.isUser == isUser) return;
        this.isUser = isUser;

        List<Building> buildings = uiSet.buildings;
        if (isUser)
        {
            foreach (int key in levelDB.Keys)
            {
                Building build = buildings.Find((v) => v.ID == key);

                if (build != null)
                {
                    build.DataSet(levelDB[key]);
                }
            }
            IsUserTrue();
        }
        else
        {
            foreach (int key in friendData.Keys)
            {
                Building build = buildings.Find((v) => v.ID == friendData[key].id);

                if (build != null)
                {
                    build.DataSet(friendData[key].level);
                }
            }
            IsUserFalse();
        }

        void IsUserTrue()
        {
            UIManager.instance.ActiveMainUI(true);
            friendUIGroup.SetActive(false);
            userUIGroup.SetActive(true);
            uiSet.Refresh(false); // active all building;
        }

        void IsUserFalse()
        {
            friendUIGroup.SetActive(true);
            userUIGroup.SetActive(false);
            UIManager.instance.ActiveMainUI(false);
            OnFriendSet();

            //= 2021.02.18 모든 건물을 출력하도록 변경됨.
            //uiSet.Refresh(true); // active last index building;
            uiSet.Refresh(false); // active all building;

            FriendUISet(true);
            FriendGameSet();
        }
    }
    public delegate void UpdateLevelEvent(int level);
    public UpdateLevelEvent updateLevelEvent;
    public void UpdateLevel(int id,Building.Kind kind,int[] level)
    {
        if (levelDB.ContainsKey(id))
        {
            levelDB[id] = level;

            uiSet.Refresh();
            updateLevelEvent?.Invoke(OpenCount);
        }

        NetworkManager.instance.UpdateLeaderBoard(CalculationAllLevels(true), SuccessUpdateScore);
        levelUI.Refresh();
    }

    void SuccessUpdateScore()
    {
        SoundManager.instance.PlayOneSound(SoundManager.instance.buildEnd);
    }

    public int GetLevel(int id,Kind kind)
    {
        if (levelDB == null)
        {
#if USE_DEBUG
            Debug.Log("Not init levelDB");
#endif

            return -1;
        }

        return levelDB[id][(int)kind];
    }
    public int CalculationAllLevels(bool isPlayer)
    {
        int revalue = 0;
        if (isPlayer)
        {
            if (levelDB == null)
            {
#if USE_DEBUG
                Debug.Log("<color=red> Please level db initialize</color>");
#endif
                return 0;
            }
            foreach (var id in levelDB.Keys)
            {
                for(int i = 0;  i < (int)Kind.MaxSize; i++)
                {
                    if (0 < levelDB[id][i])
                    {
                        revalue += levelDB[id][i];
                    }
                }
            }
            return revalue;
        }
        else
        {
            if (friendData == null)
            {
#if USE_DEBUG
                Debug.Log("<color=red> Please friend data initialize</color>");
#endif
                return 0;
            }
            foreach (var id in friendData.Keys)
            {
                for (int i = 0; i < (int)Kind.MaxSize; i++)
                {
                    if (0 < friendData[id].level[i])
                    {
                        revalue += friendData[id].level[i];
                    }
                }
            }
            return revalue;
        }
    }
    public void ProcSetWorkman(int[] ids)
    {
        workmanManager.ArrangementWorkman(ids,
            NetworkManager.instance.GetValuesSetWorkerIds(),
            NetworkManager.instance.GetValuesSetWorkerTimes());
    }

    #endregion

    #region Friend Data reference
    string anotherPlayFabId;
    public void SetFriendData(string id, string level, string nickname, int friendShieldCount, string playFabId)
    {
        friendData = new Dictionary<int, BuildingData.Friend>();

        string[] ids = id.Split(Common.splitWord);
        string[] levels = level.Split(Common.splitWord);

        for (int i = 0; i < ids.Length && i < levels.Length; i++)
        {
            string[] detailsLevels = levels[i].Split(Common.subSplitWord);
            int[] intLevels = new int[(int)Kind.MaxSize];
            for (int j = 0; j < (int)Kind.MaxSize; j++)
            {
                intLevels[j] = int.Parse(detailsLevels[j]);
            }

            if (friendData.ContainsKey(int.Parse(ids[i])) == false)
            {
                friendData.Add(int.Parse(ids[i]), new BuildingData.Friend(int.Parse(ids[i]), intLevels));
            }
        }
        this.nickText.text = nickname;
        this.nickname = nickname;
        anotherPlayFabId = playFabId;
        ChangePlayer(false);
    }

    public Action attackTutorial_start;
    public Action attackTutorial_finish;
    public void OnClickTargetPoint(int index)
    {
        //= TODO : 뱁새 잡기 튜토리얼
    }

    void FriendUISet(bool active)
    {
        //[SerializeField] GameObject friendUIGroup;
        //[SerializeField] GameObject userUIGroup;
        //[SerializeField] string nickname;
        //[SerializeField] Text nickText;

        ////= 친구방문 게임 관련
        //[SerializeField] BapBird bapBird;
        //[SerializeField] Transform catchesParent
        //[SerializeField] Text catchesText;
        //[SerializeField] int catchesCount;

        ////= 친구방문 제한 시간 관련
        //[SerializeField] Text friendTimerText;
        //[SerializeField] Slider friendTimerSlider;
        //[SerializeField] float friendLimitTime;
        //[SerializeField, ReadOnly] float friendCurrentTime;

        //= 활성화 관련
        friendUIGroup.SetActive(active);
        userUIGroup.SetActive(!active);
        bapBird.gameObject.SetActive(active);
        friendTimerText.gameObject.SetActive(active);
        friendTimerSlider.gameObject.SetActive(active);
        catchesText.gameObject.SetActive(active);

        nickText.text = nickname;
        catchesCount = 0;
        catchesText.text = catchesCount.ToString();

        for (int i = 0; i < uiSet.buildings.Count; i++)
        {
            if (!active) uiSet.buildings[i].Refresh();
            else uiSet.buildings[i].FriendGraphicSet();
        }
    }

    void FriendGameSet()
    {
        friendCurrentTime = friendLimitTime;
        friendTimerSlider.value = 1;
        friendTimerText.text = friendCurrentTime.ToString();

        //= bapBird 활성화
        bapBird.Init(
            new Vector2(UiSet.ContentTrans.rect.min.x,- 845),
            new Vector2(uiSet.ContentTrans.rect.max.x,- 945),
            UiSet.ContentTrans);

        Invoke("DelayPopup", 1.2f);
    }
    void DelayPopup()
    {
        PopupManager.instance.Open(
        Common.PopupCode.BapbirdGame,
        "BapbirdGame",
        PopupManager.ButtonStyle.Yes,
        PopupManager.MoveType.CenterPumping,
        delegate
        {
            isFriendGamePlay = true;
        },
        delegate
        {
            isFriendGamePlay = true;
        },
        false);
    }
    void FriendGameUpdate()
    {
        if(0 < friendCurrentTime)
        {
            friendCurrentTime -= Time.deltaTime;
        }
        else
        {
            friendCurrentTime = 0;
            FriendGameEnd();
        }
        friendTimerSlider.value = friendCurrentTime / friendLimitTime;
        friendTimerText.text = friendCurrentTime.ToString();
    }
    public void FriendGameUpCatchesCount()
    {
        catchesCount++;
        catchesText.text = catchesCount.ToString();
    }
    void FriendGameEnd()
    {
        isFriendGamePlay = false;
        // TODO : 서버 붙여야함.
        PopupManager.instance.Open(Common.PopupCode.BapbirdGameAD,
            "",
            PopupManager.ButtonStyle.AD,
            PopupManager.MoveType.CenterPumping,
            ()=> NetworkManager.instance.RewardBonus_VisitFriend(ServerCallback, OpenCount, catchesCount, true),
            () => NetworkManager.instance.RewardBonus_VisitFriend(ServerCallback, OpenCount, catchesCount, false),
            true);

        //
        //= 레벨이랑 카운트 서버로 보낼것.

        void ServerCallback(string s,string addGold)
        {
            PopupManager.instance.Open(Common.PopupCode.BapbirdGameReward,
                "+ " + ulong.Parse(addGold).ToString("#,##0"),
                PopupManager.ButtonStyle.Yes,
                PopupManager.MoveType.CenterPumping,
                delegate
                {
                    UserManager.instance.GoldOnEvent += ulong.Parse(addGold);
                    PrevScene();
                },
                delegate
                {
                    UserManager.instance.GoldOnEvent += ulong.Parse(addGold);
                    PrevScene();
                }, true);
        }
    }
    #endregion

    #region 21.02.05 Patch 1.0.1

    int eventWeight;
    void InitEvent()
    {
        eventChkTime = 0;
        eventWeight = 0;
        if (TutorialManager.isTutorial) startEvent = true;
        else startEvent = false;
    }

    bool isBuildingMap;
    float eventChkDelay = 2f;
    float eventChkTime;
    bool startEvent;

    int eventNumber;

    private void LateUpdate()
    {
        if(IsUser == false && isFriendGamePlay)
        {
            FriendGameUpdate();
        }

        if(isBuildingMap == false) return;

        if(startEvent)
        {
            return;
        }

        eventChkTime += Time.deltaTime;
        if(eventChkTime >= eventChkDelay)
        {
            eventChkTime = 0;
            // 아르바이트생 있는 건물 존재 + 로티 없을때 + 메뉴 다 닫혀있을때
            if(uiSet.NpcManager.ActiveRottie() == false &&
                UIManager.instance.OpenedUI_BuildingMap() == false)
            {
                if(UiSet.NpcManager.StartGame.IsPlaying == false)
                {
                    if(UnityEngine.Random.Range(0, 1000) < 100 + eventWeight)
                    {
                        if(Random.Range(0, 1000) < 500) // 물병 주기
                        {
                            eventNumber = 0;
                        }
                        else // 선물 보따리
                        {
                            eventNumber = 1;
                        }
                        startEvent = true;
                        ReadyEvent(0);
                    }
                    else eventWeight += 20;
                }
            }
        }
    }

    [SerializeField]
    Image eventFade;
    [SerializeField]
    GameObject[] eventObjs;
    [SerializeField]
    DOTweenAnimation eventTween;
    [SerializeField]
    GameObject eventPopup;
    [SerializeField]
    Text eventContent;
    [SerializeField]
    ArabicText arabicText;

    void ReadyEvent(int eventBuildingId)
    {
        //UiSet.MoveContentForBuilding(eventBuildingId);
        uiSet.LockContent();
        StartCoroutine(EventFade());
    }

    IEnumerator EventFade()
    {
        float setAlpha = 0f;

        eventFade.gameObject.SetActive(true);
        while(true)
        {
            eventFade.color = new Color(1f, 1f, 1f, setAlpha);
            yield return new WaitForEndOfFrame();

            setAlpha += 0.2f;
            if(eventFade.color.a == 1) break;
        }

        yield return new WaitForSeconds(0.1f);
        setAlpha = 1f;
        while(true)
        {
            eventFade.color = new Color(1f, 1f, 1f, setAlpha);
            yield return new WaitForSeconds(0.15f);

            setAlpha -= 0.1f;
            if(eventFade.color.a <= 0) break;
        }

        eventObjs[0].SetActive(true);
        eventObjs[0].transform.localPosition = new Vector3(-1000f, 350, 0);
        eventObjs[0].transform.DOLocalMoveX(0f, 0.2f)
            .OnComplete(() =>
            {
                eventTween.DOPlay();
            })
            .SetEase(Ease.OutBounce);
        yield return new WaitForSeconds(0.3f);
        eventObjs[1].SetActive(true); // 레이저
        
        string textCode = null;
        if(eventNumber == 0)
        {
            eventObjs[3].SetActive(true); // 물약
            yield return new WaitForSeconds(2f);
            eventObjs[1].SetActive(false); // 레이저
            eventPopup.SetActive(true);
            textCode = "Event2";
        }
        else
        {
            eventObjs[2].SetActive(true); // 돈박스
            yield return new WaitForSeconds(2f);
            eventObjs[1].SetActive(false); // 레이저
            eventPopup.SetActive(true);
            textCode = "Event2";
        }

        if(LanguageManager.instance.IsArabic() == false)
        {
            arabicText.enabled = false;
            eventContent.alignment = TextAnchor.UpperCenter;
        }
        else
        {
            arabicText.enabled = true;
            eventContent.alignment = TextAnchor.UpperRight;
        }
        
        string txt = LanguageManager.instance.GetString(textCode);
        if(LanguageManager.instance.IsArabic() == false)
            eventContent.text = txt;
        else
            arabicText.Text = txt;
    }

    public void ClickEventAD()
    {
        ADMobManager.instance.callbackCloseEarendAD += delegate 
        {
            eventPopup.SetActive(false);
            eventObjs[1].SetActive(true); // 레이저
            if(eventNumber == 0)
            {
                eventObjs[3].SetActive(false);
                SuccessOffWork(0);
                GameDataManager.instance.Roulette.AddUseCount(5);
                NetworkManager.instance.GetRewardFromADs(null, 0);
            }
            else
            {
                eventObjs[2].SetActive(false);
                UserManager.instance.GoldOnEvent += 10000;
                NetworkManager.instance.GetRewardFromADs(null, 1);
                SuccessOffWork(1);
            }
        }; 

        ADMobManager.instance.callbackADClose += delegate { EndEventAD(); }; 

        ADMobManager.instance.OpenRewardAD();
// #if UNITY_IPHONE
//         Firebase.Analytics.FirebaseAnalytics.LogEvent("Reward_UFO");
// #endif
    }

    public void EndEventAD()
    {
        eventPopup.SetActive(false);
        eventObjs[1].SetActive(true); // 레이저
        if(eventNumber == 0)
        {    
            eventObjs[3].transform.DOLocalMoveY(-152, 2f);
        }
        else
        {
            eventObjs[2].transform.DOLocalMoveY(-152, 2f);
        }
        StartCoroutine(GetGift());
    }

    IEnumerator GetGift()
    {
        yield return new WaitForSeconds(2f);
        eventObjs[0].transform.DOLocalMoveX(1000f, 0.2f)
            .OnComplete(() =>
            {
                eventObjs[2].SetActive(false); eventObjs[3].SetActive(false);
                EndEventAnim();
                InitEvent();
            })
            .SetEase(Ease.InBounce);
    }

    void SuccessOffWork(int type)
    {
        ProcSetWorkman(DataManager.instance.GetAllBuildingID().ToArray());
        eventObjs[0].transform.DOLocalMoveX(1000f, 0.2f)
            .OnComplete(() =>
            {
                EndEventAnim();
                if(type == 0)
                    PopupManager.instance.Open(Common.PopupCode.SuccessPurchase, "+5 SpinCount", PopupManager.ButtonStyle.Ok, 
                        PopupManager.MoveType.CenterPumping, null, null, true);
                else
                    PopupManager.instance.Open(Common.PopupCode.SuccessPurchase, "Gold +10K", PopupManager.ButtonStyle.Ok, 
                        PopupManager.MoveType.CenterPumping, null, null, true);
            })
            .SetEase(Ease.InBounce);
    }

    void EndEventAnim()
    {
        eventWeight = 0;
        eventFade.gameObject.SetActive(false);
        uiSet.UnLockContent();
        eventObjs[0].SetActive(false); eventObjs[1].SetActive(false);
        eventTween.DORewind();
    }
#endregion
}
