﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class BuildingSculpture
{
    //id level   sculpture_1st sculpture_2rd
    public int id;
    public int level;
    public string sculpture_1st;
    public string sculpture_2rd;
}
