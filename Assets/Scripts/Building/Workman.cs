﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

using ComID = Common.RULItem.ID;

public class Workman : MonoBehaviour
{
    [System.Serializable]
    public struct Infomation
    {
        public int ID;
        public ComID compensationID;
        public ulong reward;
        public int percentage;
        public float compensationsTime;

        public Infomation(int id,ComID compensationID,ulong reward,int percentage,float compensationsTime)
        {
            this.ID = id;
            this.compensationID = compensationID;
            this.reward = reward;
            this.percentage = percentage;
            this.compensationsTime = compensationsTime;
        }
    }

    [Header("Info")]
    [SerializeField] int id;
    [SerializeField] List<Infomation> compensations;
    [SerializeField] long startTime;

    [Header("Animation")]
    [SerializeField] Animation ani;
    [SerializeField] List<Image> images;
    [SerializeField,ReadOnly] float duration;
    [SerializeField, ReadOnly] Vector2 basePos;

    [Header("Compensation")]
    [SerializeField, ReadOnly] float lastTime;

    #region Poroperty
    public List<Infomation> Compensation { get => compensations; }
    public int ID { get => id; }
    #endregion

    public void Init(Infomation[] _compensations,float duration,long startTime)
    {
        compensations = new List<Infomation>();
        for(int i = 0; i < _compensations.Length; i++)
        {
            compensations.Add(_compensations[i]);
        }
        this.duration = duration;
        basePos = transform.localPosition;
        this.startTime = startTime;
        Refresh();
    }

    public void Refresh()
    {
        if(ani != null)
        {
            ani.Stop();
            ani.Play();
        }

        for(int i = 0; i < images.Count; i++)
        {
            images[i].color = Color.white;
        }
        this.transform.localScale = Vector3.one;
        this.transform.localPosition = basePos;
    }

    Infomation GetRandomReward()
    {
        int totalPercentage = 0;
        for(int i = 0; i < compensations.Count; i++)
        {
            totalPercentage += compensations[i].percentage;
        }
        int value = Random.Range(0, totalPercentage);

        int accumulate = 0;
        for (int i = 0; i < compensations.Count; i++)
        {
            if (accumulate <= value && value < accumulate + compensations[i].percentage)
            {
                return compensations[i];
            }
            // 연산범위 추가 연사
            accumulate += compensations[i].percentage;
        }

        return new Infomation(-1,ComID.NONE, 0, 0,0);
    }
    public bool GetReward()
    {
        // 시간 연산 방식 변경

        if (CheckGetReward() == false) return false;

        Infomation info = GetRandomReward();

        // 결과 삽입
        switch(info.compensationID)
        {
            case ComID.Attack:

                break;
            case ComID.Shield:

                break;
            case ComID.Spins:

                break;
            case ComID.Steal:

                break;
            case ComID.Gold:
                
                break;
            case ComID.NONE:

                break;
        }

        #region 알바생 퇴근
        for(int i = 0; i < images.Count; i++)
        {
            images[i].DOColor(Common.ColorAlpha, duration);
        }
        transform.DOLocalMoveY(basePos.y + 20, duration);
        transform.DOScale(Vector3.one * 0.7f, duration).
            OnComplete(() => Destroy(gameObject));
        #endregion

        return true;
    }

    public bool CheckGetReward()
    {
        if (lastTime < compensations[0].compensationsTime) return false;
        else return true;
    }
    private void Update()
    {
        lastTime += Time.deltaTime;
    }

}
