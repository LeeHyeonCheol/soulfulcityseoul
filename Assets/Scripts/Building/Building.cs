﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using HoneyPlug.Audio;

public class Building : MonoBehaviour
{
    public enum Kind : int
    {
        Lock            = -1,
        Nothing         = -2,

        MainBuilding    = 0,
        MainSculpture   = 1,
        SubSculpture    = 2,
        MaxSize         = 3
    }

    #region Inner class
    [System.Serializable]
    public class ObjectData
    {
        public List<GameObject> mainBuilding;
        public List<GameObject> mainSculpture;
        public List<GameObject> subSculpture;
        public GameObject alwaysActive;

        public ObjectData()
        {
            mainBuilding = new List<GameObject>();
            mainSculpture = new List<GameObject>();
            subSculpture = new List<GameObject>();
        }

        public List<GameObject> this[int index]
        {
            get
            {
                if (index == 0) return mainBuilding;
                else if (index == 1) return mainSculpture;
                else if (index == 2) return subSculpture;
                else
                {
#if USE_DEBUG
                    Debug.Log("Error Index : " + index + " in Object Data");
#endif
                    return null;
                }

            }
        }
        public bool IsEmpty
        {
            get
            {
                if (mainBuilding.Count == 0 && mainSculpture.Count == 0 && subSculpture.Count == 0) return true;
                else return false;
            }
        }
    }

    [System.Serializable]
    public class BuildingDB
    {
        public List<BuildingData> mainBuilding;
        public List<BuildingData> mainSculpture;
        public List<BuildingData> subSculpture;

        public BuildingDB()
        {
            mainBuilding = new List<BuildingData>();
            mainSculpture = new List<BuildingData>();
            subSculpture = new List<BuildingData>();
        }

        public BuildingDB(List<BuildingData> _mainBuilding,
            List<BuildingData> _mainSculpture,
            List<BuildingData> _subSculpture)
        {
            this.mainBuilding = _mainBuilding;
            this.mainSculpture = _mainSculpture;
            this.subSculpture = _subSculpture;
        }

        public List<BuildingData> this[int index]
        {
            get
            {
                if (index == 0) return mainBuilding;
                else if (index == 1) return mainSculpture;
                else if (index == 2) return subSculpture;
                else
                {
#if USE_DEBUG
                    Debug.Log("Error Index : Building DB");
#endif
                    return null;
                }

            }
        }
        public bool IsEmpty
        {
            get
            {
                if (mainBuilding.Count == 0 && mainSculpture.Count == 0 && subSculpture.Count == 0) return true;
                else return false;
            }
        }
    }

    [System.Serializable]
    public class NeturalEffect
    {
        public List<Common.IList> mainBuilding;
        public List<Common.IList> mainSculpture;
        public List<Common.IList> subSculpture;

        public NeturalEffect()
        {
            mainBuilding = new List<Common.IList>();
            mainSculpture = new List<Common.IList>();
            subSculpture = new List<Common.IList>();
        }

        public List<Common.IList> this[int index]
        {
            get
            {
                if (index == 0) return mainBuilding;
                else if (index == 1) return mainSculpture;
                else if (index == 2) return subSculpture;
                else
                {
#if USE_DEBUG
                    Debug.Log("Error Index : Netral effect");
#endif
                    return null;
                }

            }
        }
        public bool IsEmpty
        {
            get
            {
                if (mainBuilding.Count == 0 && mainSculpture.Count == 0 && subSculpture.Count == 0) return true;
                else return false;
            }
        }
    }
    #endregion

    [Header("Client Data")]
    [SerializeField] int id = 0;


    [Header("Prefab Data")]
    [SerializeField] Image image;
    [SerializeField] Button trigger;
    [SerializeField] ObjectData objects;
    [SerializeField] protected GameObject lockImage = null;
    [SerializeField, ReadOnly] BuildingDB db;

    [Header("Update Data")]
    [SerializeField] Text timer;                            // 건축 남은 시간 출력용 UI
    [SerializeField] GameObject timerParent;                // 활성화를 위한 오브젝트 timerParent
    [SerializeField, ReadOnly] bool construction = false;   // 건축진행중 여부
    [SerializeField] Button unlockButton = null;
    [SerializeField] protected Animation ani = null;
    [SerializeField] GameObject createAnimation = null;
    protected Coroutine constructionCheck = null;

    [Header("Neturalization")]
    [SerializeField] protected bool isNeutralization;
    [SerializeField] bool isFriendShow = false;
   
    [SerializeField] NeturalEffect neturalEffect;

    [Header("Server Data")]
    [SerializeField, ReadOnly] protected int[] level = new int[(int)Kind.MaxSize];

    public delegate void ClientPushServer(Action callback);
    public ClientPushServer clientPush;

    #region Property
    public int ID { get => id; }
    public bool Construction { get => construction; }
    #endregion
    #region Data function
    public ulong RequireGold(Kind kind)
    {
        return RequireGold(kind, level[(int)kind]);
    }
    public ulong RequireGold(Kind kind,int level)
    {
        if (level == Common.BUDMaxLevel) return 0;

        for(int i = 0; i < db[(int)kind].Count; i++)
        {
            if(db[(int)kind][i].level == level + 1)
            {
                return (ulong)db[(int)kind][i].requireGold;
            }
        }
#if USE_DEBUG
        Debug.LogError("Not Find level >> Require gold level : " + (level + 1).ToString());
#endif
        return 0;
    }
    public ulong TotalRequireGold(int level)
    {
        ulong requireGold = 0;
        for(int i = 0; i < (int)Kind.MaxSize; i++)
        {
            Debug.Log(i + " " + db[i].Count);
            for (int index = 0; index < db[i].Count; index++)
            {
                if (db[i][index].level == level + 1)
                {
                    requireGold += (ulong)db[i][index].requireGold;
                }
            }
        }

        return requireGold;
    }
    public int TotalLevel()
    {
        int revalue = 0;
        for (int i = 0; i < level.Length; i++)
        {
            revalue += level[i];
        }
        return revalue;
    }
    public int GetLevel(Kind kind)
    {
        return level[(int)kind];
    }
    public int GetLevel(int index)
    {
        return level[index];
    }
    public bool CheckLevelMax()
    {
        for(int i = 0; i < level.Length; i++)
        {
            if(level[i] != Common.BUDMaxLevel)
            {
                return false;
            }
        }

        return true;
    }
    public string CurrentIconName(Kind kind)
    {
        for(int index = 0; index < (int)Kind.MaxSize; index++)
        {
            for(int lv = 0; lv < Common.BUDMaxLevel; lv++)
            {
                if (db[index][lv].kind == kind.ToString() &&
                    db[index][lv].level == level[(int)kind])
                {
                    return db[index][lv].iconName;
                }
            }
        }

        return string.Empty;
    }
    public string NextIconName(Kind kind)
    {
        for (int index = 0; index < (int)Kind.MaxSize; index++)
        {
            for (int lv = 0; lv < Common.BUDMaxLevel; lv++)
            {
                if (db[index][lv].kind == kind.ToString() &&
                    db[index][lv].level == level[(int)kind] + 1)
                {
                    return db[index][lv].iconName;
                }
            }
        }

        return string.Empty;
    }


    void LevelUp(Kind kind)
    {
        level[(int)kind]++;

        if(Common.BUDMaxLevel < level[(int)kind])
        {
#if USE_DEBUG
            Debug.LogError("Level overflow : " + id + " " + kind.ToString());
#endif
            level[(int)kind] = Common.BUDMaxLevel;
        }
    }
    #endregion
#if USE_DEBUG
    [Header("Debug Test Mode")]
    [SerializeField] bool isTestMode;
    [SerializeField] int totalLevel;
#endif
    private void OnValidate()
    {
        if(GetComponent<Image>() == null)
        {
            image = this.gameObject.AddComponent<Image>();
            image.color = Common.ColorAlpha;
        }

        if(GetComponent<Button>() == null)
        {
            trigger = this.gameObject.AddComponent<Button>();
            trigger.targetGraphic = image;
        }
#if USE_DEBUG
        //isTestMode = false;

        if (isTestMode)
        {
            level = new int[(int)Kind.MaxSize];
            for (int i = 0; i < level.Length; i++)
            {
                level[i] = totalLevel;
            }
            GraphicSet();
        }
#endif
    }


    protected virtual void Awake()
    {
        //= 2020.12.22 network 연동과 함께 비활성화된 코드
        //
        //  construction = PlayerPrefs.GetInt(string.Format(Common.BUDUpgradeFormat, id), 0) == 0 ? false : true;
        //  string time = PlayerPrefs.GetString(string.Format(Common.BUDUpgradeTimeFormat, id),"0");
        //  if(time != "0" && construction)
        //  {
        //      startTime = new DateTime(long.Parse(time));
        //  }
        //

        unlockButton?.onClick.AddListener(UnLock);
        if(createAnimation != null) createAnimation.SetActive(false);
    }
    #region Setting 관련
    public void DataSet(int[] _level)
    {
        // level set
        UnpackLevel(ref level, _level);
        GraphicSet();
    }
    // ID로 구분하여 생성
    public void DataSet(int[] _level, BuildingDB _datas,bool _neutralization)
    {
        // level set
        UnpackLevel(ref level, _level);
        db = _datas;
        isNeutralization = _neutralization;

        //= 2020.01.07  건물 설치 시간 3초로 변경되어 기본값을 false로 지정
        //=             서버의 영향을 받지 않도록 수정
        construction = false;
        GraphicSet();
    }
    public void OnlyLevelSet(int[] _level)
    {
        UnpackLevel(ref level, _level);
        isNeutralization = false;
        construction = false;
        GraphicSet();
    }
    void UnpackLevel(ref int[] level,int[] _data)
    {
        level[(int)Kind.MainBuilding] = _data[(int)Kind.MainBuilding];
        level[(int)Kind.MainSculpture] =_data[(int)Kind.MainSculpture];
        level[(int)Kind.SubSculpture] = _data[(int)Kind.SubSculpture];
    }
    bool IsLevelZero
    {
        get { return ((level[(int)Kind.MainBuilding] + level[(int)Kind.MainSculpture] + level[(int)Kind.SubSculpture]) == 0); }
    }

    // [ 클라이언트에 있는 데이터와 서버로 부터 받은 데이터를 기반으로 설정.
    protected virtual void GraphicSet()
    {
        //= 오브젝트 비활성화 작업
        for(int i = 0; i < (int)Kind.MaxSize; i++)
        {
            for(int j = 0; j < Common.BUDMaxLevel; j++)
            {
                objects[i][j].SetActive(false);
            }
        }
        if (IsLevelZero)
        {
            lockImage.gameObject.SetActive(true);
            if(objects.alwaysActive != null)
            {
                objects.alwaysActive.SetActive(false);
            }
            ani?.Stop();
        }
        else
        {
            lockImage.gameObject.SetActive(false);

            for(int i = 0; i < (int)Kind.MaxSize; i++)
            {
                objects[i/* 종류 */][level[i] /* 레벨 */ - 1].SetActive(true);
            }
            if (objects.alwaysActive != null)
            {
                objects.alwaysActive.SetActive(true);
            }

            if (isNeutralization == false) ani?.Play();
            else ani?.Stop();
        }

        // Start of set for delay initialize 
        Invoke("SetButtonEvent", Time.deltaTime);
        ConstructionGraphicSet(false);
    }

    #endregion
    #region event
    public void Refresh()
    {
        isFriendShow = false;
        GraphicSet();
    }
    public void FriendGraphicSet()
    {
        isFriendShow = true;
        GraphicSet();
    }
   
    void UnLock()
    {
        //= 레벨1 비용 전부 계산해서 수거하고 모든 레벨을 1로 해금하는 코드 추가
        unlockButton.interactable = false;
        OnConstruction(Kind.Lock,TotalRequireGold(0));
    }
    public void OnConstruction(Kind kind,ulong requireGold)
    {
        if (UserManager.instance.GoldOnEvent < (ulong)requireGold)
        {
            PopupManager.instance.Open(
                Common.PopupCode.LessPortion,
                "Under_Gold",
                PopupManager.ButtonStyle.YesNo,
                PopupManager.MoveType.CenterPumping,
                delegate
                {
                    UIManager.instance.OpenUI_Shop();
                    CancelConstruction();
                },
                delegate 
                {
                    UIManager.instance.CloseUI_UpgradeBuild(); 
                    UIManager.instance.OpenADRewardGold();
                    CancelConstruction();
                },
                false);

            // PopupManager.instance.Open(
            //     Common.PopupCode.BUDUpgradeFail,
            //     "TCode_N1",
            //     PopupManager.ButtonStyle.Ok,
            //     PopupManager.MoveType.CenterPumping,
            //     CancelConstruction,
            //     null);
        }
        else if (construction || ( kind != Kind.Lock && level[(int)kind] == Common.BUDMaxLevel))
        {
            PopupManager.instance.Open(
                Common.PopupCode.BUDUpgradeFail,
                CreateNotice(kind, requireGold),
                PopupManager.ButtonStyle.Ok,
                PopupManager.MoveType.CenterPumping,
                CancelConstruction,
                null);
        }
        //= 건설
        else
        {
            PopupManager.instance.Open(
                Common.PopupCode.BUDUpgrade,
                CreateNotice(kind, requireGold),
                PopupManager.ButtonStyle.OkCancel,
                PopupManager.MoveType.CenterPumping,
                delegate { OKConstruction(kind,requireGold); },
                CancelConstruction,
                true);
        }
    }

    string CreateNotice(Kind kind,ulong requireGold)
    {
        if (kind != Kind.Lock && level[(int)kind] == Common.BUDMaxLevel) return "TCode_N2";
        
        if (construction)
        {
            if (kind == Kind.Lock) return "TCode_N3";
            else return "TCode_N4";
        }
        else
        {
            Debug.Log(requireGold + " " + GoldConversion.GoldToString(requireGold) + " " + LanguageManager.instance.GetString("TCode_N6"));

            if (kind == Kind.Lock) return string.Format(LanguageManager.instance.GetString("TCode_N5"), GoldConversion.GoldToString(requireGold),"3s");
            else return string.Format(LanguageManager.instance.GetString("TCode_N6"), GoldConversion.GoldToString(requireGold),"3s");
        }
    }
    void OKConstruction(Kind kind,ulong requireGold)
    {
        if (requireGold <= UserManager.instance.GoldOnEvent)
        {
            clientPush.Invoke(delegate { SuccessOKConstruction(kind,requireGold); });
        }
    }

    public delegate void SuccessConstructionCallback();
    SuccessConstructionCallback scCallback;
    public void SetOneSuccessConstructionCallback(SuccessConstructionCallback callback)
    {
        scCallback = null;
        scCallback += callback;
    }
    public void SuccessOKConstruction(Kind kind,ulong requireGold)
    {
        UserManager.instance.GoldOnEvent -= requireGold;
        construction = true;
        if (constructionCheck == null)
        {
            constructionCheck = StartCoroutine(ConstructionCheck(kind));
        }
        SoundManager.instance.PlayOneSound(SoundManager.instance.buildStart);
        scCallback?.Invoke();
    }

    void CancelConstruction()
    {
        if (unlockButton.interactable == false) unlockButton.interactable = true;
    }
    protected IEnumerator ConstructionCheck(Kind kind)
    {
        if (construction == false || CheckLevelMax())
        {
            ConstructionGraphicSet(Common.BUDConstructionTime);
            constructionCheck = null;
            ConstructionGraphicSet(false);
            yield break;
        }

        yield return new WaitUntil(() => db != null);

        UIManager.instance.CloseUI_Button();

        ConstructionGraphicSet(true);
        if (kind == Kind.Lock)
        {
            LevelUp(Kind.MainBuilding);
            LevelUp(Kind.MainSculpture);
            LevelUp(Kind.SubSculpture);
        }
        else
        {
            LevelUp(kind);
        }
        WaitForSeconds waitDelay = new WaitForSeconds(1);
        for(int i = 0; i < Common.BUDConstructionTime; i++)
        {
            ConstructionGraphicSet(i);
            yield return waitDelay;
        }
       
        #region 플레그 비활성화 및 저장
        construction = false;
        #endregion
        #region 건설 완료
        ConstructionGraphicSet(false);
        UIManager.instance.OpenUI_Button();
        BuildingManager.instance.UpdateLevel(id,kind,level);
        if (CheckLevelMax()) BuildingManager.instance.OpenCount++;
        clientPush.Invoke(null);
        #endregion
        Refresh();
        constructionCheck = null;
    }
    void ConstructionGraphicSet(long lastTime)
    {
        if(timerParent == null) return;
        long remainingTime = Common.BUDConstructionTime - lastTime;
        if (construction && 0.2f < remainingTime )
        {
            timerParent.gameObject.SetActive(true);
           
            string showString = String.Format("{0} : {1} : {2}", remainingTime / 3600       /* H */,
                                                                 remainingTime % 3600 / 60  /* M */,
                                                                 remainingTime % 360 % 60   /* S*/);

            timer.text = showString;
        }
        else
        {
            ConstructionGraphicSet(false);
        }
    }

    public void ConstructionGraphicSet(bool active)
    {
        if(timerParent != null) timerParent.gameObject.SetActive(active);
        if(createAnimation != null) createAnimation.SetActive(active);
    }

    protected void SetButtonEvent()
    {
        if(IsLevelZero)
        {
            trigger.enabled = false;
        }
        else if(isNeutralization && isFriendShow == false)
        {
            SetNeutralization();
        }
        else if(isNeutralization == false && isFriendShow == false)
        {
            SetUpgradeBuildUI();
        }
        else
        {
            ClearButtonEvent();
            SetNeutralization();
        }
    }

    void SetNeutralization()
    {
        if (neturalEffect.IsEmpty) return;

        //= 버튼은 본 건물에 통합
        if (isNeutralization && isFriendShow == false)
        {
            #region 버튼 활성화 설정
            trigger.enabled = true;
            trigger.onClick.RemoveAllListeners();
            trigger.onClick.AddListener(RemoveNeturalizationPaint);
            #endregion

            //= 색상 변경 ( 활성화 )
            for (int kind = 0; kind < (int)Kind.MaxSize; kind++)
            {
                for (int level = 0; level < Common.BUDMaxLevel; level++)
                {
                    if (neturalEffect[kind][level] != null)
                    {
                        for (int i = 0; i < neturalEffect[kind][level].Count; i++)
                        {
                            neturalEffect[kind][level][i].gameObject.SetActive(true);
                            neturalEffect[kind][level][i].color = Color.white;
                        }
                    }
                }
            }
        }
        else
        {
            #region 버튼 비활성화 설정
            trigger.enabled = false;
            trigger.onClick.RemoveAllListeners();
            #endregion

            //= 색상 변경 ( 비활성화 )
            for (int kind = 0; kind < (int)Kind.MaxSize; kind++)
            {
                for (int level = 0; level < Common.BUDMaxLevel; level++)
                {
                    for (int i = 0; i < neturalEffect[kind][level].Count; i++)
                    {
                        if(neturalEffect[kind][level][i] == null) continue;

                        neturalEffect[kind][level][i].gameObject.SetActive(false);
                        neturalEffect[kind][level][i].color = Common.ColorAlpha;
                    }
                }
            }
        }
    }
    void SetUpgradeBuildUI()
    {
        trigger.enabled = true;
        trigger.onClick.RemoveAllListeners();
        trigger.onClick.AddListener(delegate
        {
            UIManager.instance.OpenUI_UpgradeBuild(BuildingManager.instance.UiSet.buildings,ID);
        });
    }

    void ClearButtonEvent()
    {
        trigger.enabled = false;
        trigger.onClick.RemoveAllListeners();
    }

    void RemoveNeturalizationPaint()
    {
        if (neturalEffect.IsEmpty) return;

        // TODO : Server callback 삽입 위치

        // 패킹 함수 : PackNeutralizationDB (param, param)
        string pack = BuildingManager.instance.PackNeutralizationDB(id, false);
        // callback 함수 Callback_RemoveNetualizationPaint

        NetworkManager.instance.CleaningBuilding(pack, Callback_RemoveNetualizationPaint);
    }
    
    void Callback_RemoveNetualizationPaint()
    {
        if(BuildingManager.instance != null)
        {
            isNeutralization = false;
            BuildingManager.instance.UpdateNeutralizationDB(id, isNeutralization);
            SetButtonEvent();
        }

        #region Set paint Animation
        for(int kind = 0; kind < (int)Kind.MaxSize; kind++)
        {
            for(int level = 0; level < Common.BUDMaxLevel; level++)
            {
                if(level == this.level[kind]-1)
                {
                    //= 애니메이션 진행 부분
                    for(int i = 0; i < neturalEffect[kind][level].Count; i++)
                    {
                        neturalEffect[kind][level][i].DOColor(Common.ColorAlpha, 3.5f);
                    }
                }
                else
                {
                    //= 애니메이션 진행 없이 제거되는 부분
                    for (int i = 0; i < neturalEffect[kind][level].Count; i++)
                    {
                        neturalEffect[kind][level][i].color = Common.ColorAlpha;
                    }
                }
            }
        }
        #endregion

        #region Towel Animation
        GameObject towel = Instantiate(EffectManager.instance.GetEffect("Towel").effect);
        towel.transform.SetParent(this.transform);
        towel.transform.localPosition = Vector3.zero;
        towel.transform.localScale = Vector3.one;
        ani?.Play();
        #endregion
    }
    #endregion
}
