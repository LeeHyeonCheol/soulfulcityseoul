﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkmanManager : MonoBehaviour
{
    [SerializeField] List<Workman> prefabs;
    [SerializeField] Dictionary<Building,List<Workman>> arrangement;
    [SerializeField] float distance;

    [Header("Workman Animation")]
    [SerializeField] float duration;

#if USE_DEBUG
    [SerializeField] int inputBildingID;
#endif

    private void Start()
    {
        Init();
    }

    void Init()
    {
        arrangement = new Dictionary<Building, List<Workman>>();
        PrefabsLoading();
    }

    void PrefabsLoading()
    {
        prefabs = new List<Workman>();

        Workman[] workmens = Resources.LoadAll<Workman>(Common.WorkmanPrefabPath);
        Debug.Log(workmens.ToString());

        for(int i = 0; i < workmens.Length; i++)
        {
            prefabs.Add(workmens[i]);
        }
    }
    void SetWorkmanTransform(Transform parent,Transform workman,int position)
    {
        workman.SetParent(parent);
        workman.localScale = Vector3.one;

        switch(position)
        {
            case 0 /* left   */: workman.localPosition = new Vector2(-.95f,-1.05f)* distance; break;
            case 1 /* right  */: workman.localPosition = new Vector2(.95f, -1.05f) * distance; break;
            case 2 /* center */: workman.localPosition = new Vector2(-0.25f, -1.20f) * distance; break;
       }
    }

    int GetWorkmanArrangmentMaxCount(Building building)
    {
        switch(building.TotalLevel() / (int)Building.Kind.MaxSize)
        {
            case 0: return 0;
            case 1: return 1;
            case 2: return 2;
            case 3: return 2;
            case 4: return 3;
            default: return 0;
        }
    }
#region External function
    public bool ArrangementWorkman(int[] buildingIDS, string[] workmanIDS,string[] times)
    {
        for(int i = 0; i <buildingIDS.Length && i< workmanIDS.Length;i++)
        {
            Building building = BuildingManager.instance.UiSet.buildings.Find(
            delegate (Building build)
            {
                return build.ID == buildingIDS[i];
            });

            if (building == null) return false;
            else
            {
                if (SetFunction(building, workmanIDS[i], times[i]) == false)
                {
                    return false;
                }
            }
        }
        return true;

        bool SetFunction(Building building, string workmanID, string time)
        {
            if (arrangement.ContainsKey(building) == false)
            {
                arrangement.Add(building, new List<Workman>());
            }
            else
            {
                //= 2020.02.05 추가 패치내역
                for(int i = 0; i < arrangement[building].Count; i++)
                {
                    Destroy(arrangement[building][i]);
                    arrangement[building] = new List<Workman>();
                }
               
            }
            //= level 0 건물 패스
            if (GetWorkmanArrangmentMaxCount(building) == 0)
            {
                return true;
            }

            int arrangementCount = 0;
            for (int i = 0; i < arrangement[building].Count; i++)
            {
                if (arrangement[building][i] != null) arrangementCount++;
            }

            if (arrangementCount < GetWorkmanArrangmentMaxCount(building))
            {
                string[] ids = workmanID.Split(Common.splitWord);
                string[] startTimes = time.Split(Common.splitWord);

                for (int i = 0; i < ids.Length && i < Common.WorkmanArrangmentMaxCount; i++)
                {
                    int id = int.Parse(ids[i]);
                    long startTime = long.Parse(startTimes[i]);
                    //= id is 0 => null workman
                    if (id == 0) continue;
                    //= 이미 동일개체 배치중일땐 Continue
                    if (ContainsWorkman(building, id)) continue;

                    Workman prefab = prefabs.Find(delegate (Workman w)
                    {
                        return w.ID == id;
                    });

                    if (prefab == null)
                    {
#if USE_DEBUG
                        Debug.Log("ID : " + id + " prefabs not find [ workman ]");
#endif
                        return false;
                    }

                    Workman workman = Instantiate(prefab);
                    List<Workman.Infomation> info = DataManager.instance.GetWorkmanInformation(id);
                    SetWorkmanTransform(building.transform, workman.transform, i);
                    workman.Init(info.ToArray(), duration, startTime);
                    arrangement[building].Add(workman);
                }
            }
            return true;
        }
    }

   /// <summary>
   /// 알바생 퇴근 가능여부 확인
   /// </summary>
   /// <param name="buildingID"> 배치된 빌딩 아이디 </param>
   /// <param name="workmanID"> 배치된 알바생 아이디 </param>
   /// <returns></returns>
    public bool CheckRewardWorkman(int buildingID, int workmanID)
    {
        Building building = BuildingManager.instance.UiSet.buildings.Find(
          delegate (Building build)
          {
              return build.ID == buildingID;
          });
        if (building == null) return false;
        else
        {
            if (arrangement.ContainsKey(building) == false) return false;

            Workman workman = arrangement[building].Find(delegate (Workman w)
            {
                return w.ID == workmanID;
            });

            if (workman == null) return false;
            return workman.CheckGetReward();
        }
    }


    public bool RewardWorkman(int buildingID,int workmanID)
    {
        Building building = BuildingManager.instance.UiSet.buildings.Find(
           delegate (Building build)
           {
               return build.ID == buildingID;
           });

        if (building == null) return false;
        else return RewardWorkman(building, workmanID);
    }
    public bool RewardWorkman(Building building, int workmanID)
    {
        if (arrangement.ContainsKey(building) == false) return false;

        Workman workman = arrangement[building].Find(delegate (Workman w)
        {
            return w.ID == workmanID;
        });

        if (workman == null) return false;

        bool reward = workman.GetReward();
        if(reward)
        {
            arrangement[building].Remove(workman);
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion
    bool ContainsWorkman(Building building,int workmanID)
    {
        if (arrangement[building].Find(delegate(Workman w)
        {
            return w.ID == workmanID;

        }) != null) return true;
        else return false;
    }
#if USE_DEBUG
    private void Update()
    {

    }
#endif
}
