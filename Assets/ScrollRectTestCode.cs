﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollRectTestCode : MonoBehaviour
{

    [SerializeField] GameObject prefab;
    [SerializeField] RectTransform content;


    [SerializeField] float delay;
    [SerializeField,ReadOnly] float current;


    // Update is called once per frame
    void Update()
    {
        current += Time.deltaTime;
        if(delay <= current)
        {
            current -= delay;

            GameObject obj = Instantiate(prefab);
            obj.transform.SetParent(content);
        }
    }
}
